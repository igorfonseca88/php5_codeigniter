<!DOCTYPE html>
<html lang="en">
<?php 
$arq = $_GET['arq'];

?>
<head>
    <title>Go Talent</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="css/style.css">

    <style>
    audio {
        vertical-align: bottom;
        width: 10em;
    }
    video {
        vertical-align: top;
        max-width: 100%;
    }
    input {
        border: 1px solid #d9d9d9;
        border-radius: 1px;
        font-size: 2em;
        margin: .2em;
        width: 30%;
    }
    p,
    .inner {
        padding: 1em;
    }
    li {
        border-bottom: 1px solid rgb(189, 189, 189);
        border-left: 1px solid rgb(189, 189, 189);
        padding: .5em;
    }
    label {
        display: inline-block;
        width: 8em;
    }
    </style>

    <!-- script used for audio/video/gif recording -->
    <script src="js/RecordRTC.js">
    </script>
</head>

<body>
                <div align="center" >
                <img src='img/topo.png'/>
                <br /><br />
                </div>
				<div align="center" id="container" >
				</div>
				<div align="center" >
                <br /><br />
				<button id="replay">Replay</button>
                <br /><br />
                <img src='img/rodape.png'/>
				</div>

        <script>
			var mediaElementV;
			var mediaElementA;
			function carregaVideo(){
                mediaElementV = document.createElement('video');
                var source = document.createElement('source');
                var href = location.href.substr(0, location.href.lastIndexOf('/') + 1);
                source.src = href + 'uploads/'+<?php echo $arq ?>+'.webm';
                source.type = 'video/webm; codecs="vp8, vorbis"';
                mediaElementV.appendChild(source);
				mediaElementV.controls = false;
                mediaElementV.poster = 'images/loader.gif';
				container.appendChild(mediaElementV);
			}
			function carregaAudio(){
			    mediaElementA = document.createElement('audio');
                var source = document.createElement('source');
                var href = location.href.substr(0, location.href.lastIndexOf('/') + 1);
                source.src = href + 'uploads/'+<?php echo $arq ?>+'.wav';
                source.type = !!navigator.mozGetUserMedia ? 'audio/ogg' : 'audio/wav';
                mediaElementA.appendChild(source);
				mediaElementA.controls = false;
				container.appendChild(mediaElementA);
			}
			carregaVideo();
			carregaAudio();

			setTimeout(sincronizar, 3000);
			function sincronizar(){
                mediaElementV.poster = '';
				var realTime;
				var audioTime = mediaElementA.duration;
				var videoTime = mediaElementV.duration;
				if(audioTime > videoTime){
					realTime = audioTime-videoTime;
					mediaElementA.currentTime = realTime;
				}else{
					if(videoTime > audioTime){
						realTime = videoTime-audioTime;
						mediaElementA.currentTime = realTime/2;
					}
				}		
				mediaElementV.play();
                mediaElementA.play();
			}
			
			replay.onclick = function() {
				container.innerHTML = "";
                carregaVideo();
                carregaAudio();
				setTimeout(sincronizar, 3000);
			};
        </script>

    <!-- commits.js is useless for you! -->
    <script src="js/commits.js" async>
    </script>
</body>

</html>