function statusChangeCallback(response) {
    if (response.status === 'connected') {
        testAPI();
    } else if (response.status === 'not_authorized') {
        console.log('Please log ' + 'into this app.');
    } else {
        console.log('Please log ' + 'into Facebook.');
    }
}

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '1506491609655047',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.5' // use graph api version 2.5
    });
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI() {
    FB.api('/me?fields=name,first_name,last_name,birthday,website,location,email,gender,work, link,picture.width(200).height(200), education', function (response) {
        //Tranforma JSON em String
        var json = JSON.stringify(response);
        //Criacao de um formulario
        var f = document.createElement("form");
        f.setAttribute('method', "POST");
        f.setAttribute('action', "http://www.gotalent.com.br/facebookController/entrarFacebook");
        f.setAttribute("id", "form");
        f.setAttribute("style", "display:none;");
        //Criacao de um input
        var input = document.createElement("input");
        input.type = "text";
        input.setAttribute('name', 'data');
        input.setAttribute('value', json);
        input.setAttribute("style", "display:none;");
        //Adicionando input ao formulario
        f.appendChild(input);
        //Adicionando formulario ao home.
        var body = document.getElementById("home");
        body.appendChild(f);
        //Enviando formulario
        f.submit();
    });
}