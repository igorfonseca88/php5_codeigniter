var caminho = "http://www.gotalent.com.br/";

$(window).load(function () {

    //Mascaras
    $("#phone").mask("(00)0000-00000");
    $("#telefoneEmpresa").mask("(00)0000-00000");
    $("#telefone").mask("(00)0000-00000");
    
    $("#cnpjEmpresa").mask("00.000.000/0000-00");

    //Fancybox
    $('.fancybox').fancybox();

    //Menu
    $(window).scroll(function () {
        if ($(this).scrollTop() > 95) {
            $("#header").addClass("roll");
        } else {
            $("#header").removeClass("roll");
        }
    });

    //Sliders
    $(".vagas").flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 310,
        itemMargin: 20,
        controlNav: false
    });
    $(".home").flexslider({
        animation: "fade",
        controlNav: true,
        slideshowSpeed: 4000,
        directionNav: false,
        start: function (slider) {
            slider.removeClass('loading');
        }
    });
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 150,
        asNavFor: '#principal'
    });
    $('#principal').flexslider({
        animation: "slide",
        controlNav: false,
        directionNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
    $(".lista_parceiros").flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 150,
        itemMargin: 10,
        controlNav: true
    });


    //Abas
    $(".abas a").click(function () {
        $(".abas a").removeClass("select");
        $(this).addClass("select");
        $(".aba").removeClass("select");
        $("#aba-" + $(this).attr("id")).addClass("select");
    });
    var hash = window.location.hash;
    if (hash == "#profissional") {
        $(".abas a").removeClass("select");
        $("#lnkProfissional").addClass("select");
        $(".aba").removeClass("select");
        $("#aba-lnkProfissional").addClass("select");
    } else if (hash == "#empresa") {
        $(".abas a").removeClass("select");
        $("#lnkEmpresa").addClass("select");
        $(".aba").removeClass("select");
        $("#aba-lnkEmpresa").addClass("select");
    }

    $("#page_navigation").click(function () {
        $('html,body').animate({scrollTop: $('#listaVagas').offset().top}, 500);
    });

});

function validaFormProfissional() {

    var msg = "";

    var name = document.getElementById('nome').value;
    if (name.trim() == "") {
        msg = "Preencha o campo Nome Completo.  </br>";
    }
    var email = document.getElementById('emailLogin').value;
    if (email.trim() == "") {
        msg += "Preencha o campo E-mail. </br>";
    }
    /*var phone = document.getElementById('phone').value;
    if (phone.trim() == "") {
        msg += "Preencha o campo Telefone. </br>";
    }*/

    var senha = document.getElementById('senha').value;
    if (senha.trim() == "") {
        msg += "Preencha o campo Senha. </br>";
    }
    /*var idArea = document.getElementById('idArea').value;
    if (idArea.trim() == "") {
        msg += "Preencha o campo Área. </br>";
    }*/

    if (msg != "") {
        document.getElementById("erro").innerHTML = msg;

    }
    else if (document.getElementById('emailLogin').value != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(document.getElementById("emailLogin").value)) {
            msg += 'Por favor, digite o email corretamente';
            document.getElementById("erro").innerHTML = msg;

        }
        else {
            document.getElementById("formCadProfissional").submit();
        }
    }
}

function validaFormProfissionalFacebook() {

    var msg = "";

    var name = document.getElementById('nome').value;
    if (name.trim() == "") {
        msg = "Preencha o campo Nome Completo.  </br>";
    }
    var email = document.getElementById('emailLogin').value;
    if (email.trim() == "") {
        msg += "Preencha o campo E-mail. </br>";
    }
    /*var phone = document.getElementById('phone').value;
     if (phone.trim() == "") {
     msg += "Preencha o campo Telefone. </br>";
     }*/

    var senha = document.getElementById('senha_').value;
    if (senha.trim() == "") {
        msg += "Preencha o campo Senha. </br>";
    }
    /*var idArea = document.getElementById('idArea').value;
     if (idArea.trim() == "") {
     msg += "Preencha o campo Área. </br>";
     }*/

    if (msg != "") {
        document.getElementById("erro").innerHTML = msg;

    }
    else if (document.getElementById('emailLogin').value != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(document.getElementById("emailLogin").value)) {
            msg += 'Por favor, digite o email corretamente';
            document.getElementById("erro").innerHTML = msg;

        }
        else {
            document.getElementById("formCadProfissional").submit();
        }
    }
}

function validaFormEmpresa() {
    var msg = "";

    var cnpjEmpresa = document.getElementById('cnpjEmpresa').value;
    if (cnpjEmpresa.trim() == "") {
        msg = "Preencha o campo CNPJ.  </br>";
    }
    var nomeEmpresa = document.getElementById('nomeEmpresa').value;
    if (nomeEmpresa.trim() == "") {
        msg = "Preencha o campo Empresa.  </br>";
    }
    var emailEmpresa = document.getElementById('emailEmpresa').value;
    if (emailEmpresa.trim() == "") {
        msg += "Preencha o campo E-mail. </br>";
    }
    var contatoEmpresa = document.getElementById('nomeContato').value;
    if (contatoEmpresa.trim() == "") {
        msg += "Preencha o campo Nome do contato. </br>";
    }
    var telefoneEmpresa = document.getElementById('telefoneEmpresa').value;
    if (telefoneEmpresa.trim() == "") {
        msg += "Preencha o campo Telefone. </br>";
    }
    if (document.getElementById('senhaEmpresa').value == "") {
        msg += "Preencha o campo Senha. </br>";
    }

    if (msg != "") {
        document.getElementById("erro").innerHTML = msg;

    }
    else if (document.getElementById('emailEmpresa').value != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(document.getElementById("emailEmpresa").value)) {
            msg += 'Por favor, digite o email corretamente';
            document.getElementById("erro").innerHTML = msg;

        }
        else {
            document.getElementById("formCadEmpresa").submit();
        }
    }
}

function validaFormPlanoEmpresa() {
    var msg = "";

    var plano = "";
    $('input:radio[name=plano]').each(function () {
        if ($(this).is(':checked')) {
            plano = $(this).val();
        }
    });

    if (plano == "") {
        $("#erro").html("Escolha um plano.");
    } else {
        document.getElementById("formPlanoEmpresa").submit();
    }
}

function validaForm() {
    var msg = "";


    var nome = document.getElementById('nome').value;
    if (nome.trim() == "") {
        msg = "Preencha o campo Nome.  </br>";
    }
    var email = document.getElementById('email').value;
    if (email.trim() == "") {
        msg += "Preencha o campo E-mail. </br>";
    }

    var telefone = document.getElementById('telefone').value;
    if (telefone.trim() == "") {
        msg += "Preencha o campo Telefone. </br>";
    }

    var mensagem = document.getElementById('mensagem').value;
    if (mensagem.trim() == "") {
        msg += "Preencha o campo Mensagem. </br>";
    }


    if (msg != "") {
        document.getElementById("erro").innerHTML = msg;

    }
    else if (document.getElementById('email').value != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(document.getElementById("email").value)) {
            msg += 'Por favor, digite o email corretamente';
            document.getElementById("erro").innerHTML = msg;

        }
        else {
            document.getElementById("formContato").submit();
        }
    }
}


$(function () {

    if (!isMobile()) {
        function onScrollInit(items, trigger) {
            items.each(function () {
                var osElement = $(this),
                        osAnimationClass = osElement.attr('data-os-animation'),
                        osAnimationDelay = osElement.attr('data-os-animation-delay');

                osElement.css({
                    '-webkit-animation-delay': osAnimationDelay,
                    '-moz-animation-delay': osAnimationDelay,
                    'animation-delay': osAnimationDelay
                });

                var osTrigger = (trigger) ? trigger : osElement;

                osTrigger.waypoint(function () {
                    osElement.addClass('animated').addClass(osAnimationClass);
                }, {
                    triggerOnce: true,
                    offset: '90%'
                });
            });
        }

        onScrollInit($('.os-animation'));
        onScrollInit($('.staggered-animation'), $('.staggered-animation-container'));
    }
});

function isMobile() {
    var userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1)
        return true;
}


$(document).ready(function () {

//how much items per page to show
    var show_per_page = 15;
    //getting the amount of elements inside content div
    var number_of_items = $('#content').children().size()-1;
    
    //calculate the number of pages we are going to have
    var number_of_pages = Math.ceil(number_of_items / show_per_page);
    
    //set the value of our hidden input fields
    $('#current_page').val(0);
    $('#show_per_page').val(show_per_page);
    //now when we got all we need for the navigation let's make it '

    /* 
     what are we going to have in the navigation?
     - link to previous page
     - links to specific pages
     - link to next page
     */
    var navigation_html = '<a class="previous_link" href="javascript:previous();">Anterior</a>';
    var current_link = 0;
    while (number_of_pages > current_link) {
        navigation_html += '<a class="page_link" href="javascript:go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
        current_link++;
    }
    navigation_html += '<a class="next_link" href="javascript:next();">Próximo</a>';
    $('#page_navigation').html(navigation_html);
    //add active_page class to the first page link
    $('#page_navigation .page_link:first').addClass('active_page');
    //hide all the elements inside content div
    $('#content').children().css('display', 'none');
    //and show the first n (show_per_page) elements
    $('#content').children().slice(0, show_per_page).css('display', 'block');
});
function previous() {

    new_page = parseInt($('#current_page').val()) - 1;
    //if there is an item before the current active link run the function
    if ($('.active_page').prev('.page_link').length == true) {
        go_to_page(new_page);
    }

}

function next() {
    new_page = parseInt($('#current_page').val()) + 1;
    //if there is an item after the current active link run the function
    if ($('.active_page').next('.page_link').length == true) {
        go_to_page(new_page);
    }
}

function go_to_page(page_num) {
    //get the number of items shown per page
    var show_per_page = parseInt($('#show_per_page').val());
    //get the element number where to start the slice from
    start_from = page_num * show_per_page;
    //get the element number where to end the slice
    end_on = start_from + show_per_page;
    //hide all children elements of content div, get specific items and show them
    $('#content').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');
    /*get the page link that has longdesc attribute of the current page and add active_page class to it
     and remove that class from previously active page link*/
    $('.page_link[longdesc=' + page_num + ']').addClass('active_page').siblings('.active_page').removeClass('active_page');
    //update the current page input field
    $('#current_page').val(page_num);
}

function cadProfissionalParceiro() {
    var nome = $("#nome").val();
    var phone = $("#phone").val();
    var emailLogin = $("#emailLogin").val();
    var password = $("#senha").val();

    var msg = "";

    if (nome.trim() == "") {
        msg = "Preencha o campo Nome Completo.  </br>";
    }

    if (emailLogin.trim() == "") {
        msg += "Preencha o campo E-mail. </br>";
    }

    if (phone.trim() == "") {
        msg += "Preencha o campo Telefone. </br>";
    }
    if (password == "") {
        msg += "Preencha o campo Senha. </br>";
    }

    if (msg != "") {
        document.getElementById("erro").innerHTML = msg;

    }
    else if (emailLogin != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(emailLogin)) {
            msg += 'Por favor, digite o email corretamente';
            document.getElementById("erro").innerHTML = msg;

        }

    }


    if (msg == "") {
        $(document).ready(function () {
            $.post("http://www.gotalent.com.br/cempresa/cadastroProfissionalParceiro", {nome: nome, phone: phone, emailLogin: emailLogin, password: password},
            function (result) {

                if (result == "sucesso") {
                    $("#sucesso").html("Cadastro efetuado com sucesso! Confirme o seu cadastro através do e-mail que enviamos para " + emailLogin + ".");
                    $('#divCadastro').css('display', 'none');
                    ga('send', 'pageview', '/cadastrousuarioparceiro');
                }
                else if (result == "erro") {
                    $("#erro").html("Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.");
                } else if (result == "erro-email-cadastrado") {
                    $("#erro").html("O e-mail informado já está cadastrado em nossa base, faça o login e candidate-se à vaga.");
                }

            });
        });
    }
}

function cadNews() {
    var email = $("#emailNews").val();
    var empresa = $("#hemp").val();


    var msg = "";

    if (email.trim() == "") {
        msg += "<span style='color:red'>Preencha o campo com o seu E-mail. </span>";
    }


    if (msg != "") {
        document.getElementById("respNews").innerHTML = msg;

    }
    else if (email != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(email)) {
            msg = "<span style='color:red'>Por favor, digite o email corretamente.</span>";
            document.getElementById("respNews").innerHTML = msg;

        }

    }


    if (msg == "") {
        $(document).ready(function () {
            $.post("http://www.gotalent.com.br/cempresa/cadNews", {email: email, empresa: empresa},
            function (result) {

                if (result == "sucesso") {
                    $("#respNews").html("<span style='color:green'>Cadastro efetuado com sucesso!</span>");
                    $("#emailNews").val("");
                    

                }
                else if (result == "erro") {
                    $("#respNews").html("<span style='color:red'>Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente.</span>");
                }

            });
        });
    }
}


function setLinhas(){
    $(document).ready(function () {
        $("div.vagas").removeClass("colunas");
        $("div.vagas").addClass("linhas");
    });   
}

function setColunas(){
    $(document).ready(function () {
        $("div.vagas").removeClass("linhas");
        $("div.vagas").addClass("colunas");
    });   
}


$(window).load(function(){
	//Menu
	$("#menuDropDown").click(function() {
		if ($("#header .menu").hasClass("ativo")) {
			$("#header .menu").removeClass("ativo");
			$("#menuDropDown").removeClass("fechar");
		} else {
			$("#header .menu").addClass("ativo");
			$("#menuDropDown").addClass("fechar");
		}
	});
	
	//Abas
	$("ul.abas li").click(function() {
		$("ul.abas li").removeClass("select");
		$(this).addClass("select");
		$("div.abas .aba").removeClass("select");
		$("div.abas .aba#" + $(this).attr("name")).addClass("select");
	});
	
	//Sliders
	$('#slider').flexslider({
		animation: "slide",
		directionNav: false,
		controlNav: true,
		slideshow: true,
		start: function(slider) {
			$("#slider").removeClass('loading');
		}
	});
	$('#sliderEmpresas').flexslider({
		animation: "slide",
		directionNav: true,
		controlNav: false,
		slideshow: true,
		itemWidth: 119,
		itemMargin: 0,
		start: function(slider) {
			$("#sliderEmpresas").removeClass('loading');
		}
	});
	$('#sliderVagas').flexslider({
		animation: "slide",
		directionNav: true,
		controlNav: false,
		slideshow: true,
		itemWidth: 59,
		itemMargin: 0,
		start: function(slider) {
			$("#sliderVagas").removeClass('loading');
		}
	});
	
	//Galeria
	$('.fancybox').fancybox();
	
	//placeholder 
	$(".placeholder").focus(function() {
		if ($(this).attr("placeholder") == $(this).val()) {
			$(this).val("");
		}
	}).blur(function() {
		if ($(this).val() == "") {
			$(this).val($(this).attr("placeholder"));
		}
	});
		
    //Exibe Modal leading
    /*if (Cookies.get('areaInteresse') == null) {
        
        $("#maskAreaInteresse").delay(1000).fadeIn();
        $("#modalAreaInteresse").delay(1000).fadeIn();
    }*/        
    
    
                
});

/*function setArea(){
    $(document).ready(function () {
        
        var area = $("#idAreaInteresse").val();
        var date = new Date(); date.setTime(date.getTime() + (129600 * 60 * 1000)); 
        Cookies.set('areaInteresse', area, {expires:date});
       
        $("#maskAreaInteresse").fadeOut();
        $("#modalAreaInteresse").fadeOut();
        
    });
}*/

function faleComConsultor() {
    var email = $("#emailFormConsultor").val();
    var nome = $("#nomeFormConsultor").val();
    var telefone = $("#telefoneFormConsultor").val();


    var msg = "";

    if (email.trim() == "") {
        msg += "<span style='color:red'>Preencha o E-mail. </span>";
    }


    if (msg != "") {
        document.getElementById("respFormConsultor").innerHTML = msg;

    }
    else if (email != "") {
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (!filter.test(email)) {
            msg = "<span style='color:red'>Digite o email corretamente.</span>";
            document.getElementById("respFormConsultor").innerHTML = msg;

        }

    }


    if (msg == "") {
        $(document).ready(function () {
            $.post("http://www.gotalent.com.br/fale-com-consultor", {email: email, nome: nome, telefone: telefone},
            function (result) {

                if (result == "sucesso") {
                    $("#respFormConsultor").html("<span style='color:green'>Cadastro efetuado com sucesso!</span>");
                    $("#emailFormConsultor").val("");
                    $("#nomeFormConsultor").val("");
                    $("#telefoneFormConsultor").val("");
                    

                }
                else if (result == "erro") {
                    $("#respFormConsultor").html("<span style='color:red'>Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente.</span>");
                }

            });
        });
    }
}