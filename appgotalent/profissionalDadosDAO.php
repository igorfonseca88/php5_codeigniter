<?php
include_once "conexao.php";

class profissionalDadosDAO
{
    function getDadosProfissional($id){
        $conexao = new Conexao;
        $con = $conexao->conectarMysql();
        $query = mysql_query("SELECT IF(prof.id IS NULL, '', prof.id) AS id,
       IF(prof.nome IS NULL,'', prof.nome) AS nome,
       IF(prof.email IS NULL,'', prof.email) AS email,
       IF(prof.telefone IS NULL,'', prof.telefone) AS telefone,
       IF(prof.nascimento IS NULL,'', prof.nascimento) AS nascimento,
       IF(prof.sexo IS NULL,'', prof.sexo) AS sexo,
       IF(prof.pretensao IS NULL, '0.0' , prof.pretensao) AS pretensao,
       IF(c.nome IS NULL,'', c.nome) AS cidade,
       IF(e.nome IS NULL,'', e.nome) AS estado,
       IF(prof.facebook IS NULL,'', prof.facebook) AS facebook,
	   IF(prof.linkedin IS NULL,'', prof.linkedin) AS linkedin,
	   IF(prof.repositorio IS NULL, '', prof.repositorio) AS repositorio,
	   IF(prof.resumo IS NULL, '', prof.resumo) AS resumo,
     IF(prof.foto IS NULL, '', prof.foto) AS foto
       FROM tb_profissional prof  LEFT JOIN cidade c ON prof.cidade = c.id LEFT JOIN estado e ON prof.estado = e.id WHERE prof.id =".$id.";");
        mysql_close($con);
        return $query;
    }
}