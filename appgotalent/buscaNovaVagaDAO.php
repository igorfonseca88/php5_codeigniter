<?php
include_once "conexao.php";

class buscaNovaVagaDAO
{
    public function getVaga($idV, $idP)
    {
        $conexao = new Conexao;
        $con = $conexao->conectarMysql();
        $query = mysql_query("SELECT IF(vg.id IS NULL, '', vg.id) AS ID,
IF (vg.vaga IS NULL,'',vg.vaga)AS VAGA,
IF(vg.descricao IS NULL, '' ,vg.descricao) AS DESCRICAO,
IF(vg.cidade IS NULL,'' ,vg.cidade) AS CIDADE,
IF(vg.estado IS NULL, '' ,vg.estado) AS ESTADO,
IF(vg.tipo IS NULL, '', vg.tipo) AS TIPO,
IF(vg.tipoProfissional IS NULL, '', vg.tipoProfissional) AS TIPO_PROFISSIONAL,
IF(vg.salario IS NULL, '0.0' , vg.salario) AS SALARIO,
IF(emp.razaosocial IS NULL, '',emp.razaosocial) AS RAZAO_SOCIAL_EMP,
IF(vg.quantidade IS NULL, '', vg.quantidade) QUANTIDADE_VAGAS,
IF(vg.informacoesAdicionais IS NULL, '', vg.informacoesAdicionais) AS INFORMACOES_ADICIONAIS,
IF(vg.requisitos IS NULL, '',vg.requisitos) AS REQUISITOS,
IF(vg.diferenciais IS NULL, '',vg.diferenciais) AS DIFERENCIAIS,
IF(vg.dataPublicacao IS NULL, '',vg.dataPublicacao) AS DATA_PUBLICACAO,
IF(vg.situacao IS NULL, '',vg.situacao) AS SITUACAO_VAGA,
(select count(id) from tb_profissional_vaga pv where pv.idVaga = vg.id and idProfissional = ".$idP.") as CANDIDATO
FROM tb_vaga vg JOIN tb_empresa emp ON vg.idEmpresa =  emp.id  WHERE vg.situacao = 'ativo' AND vg.id = ".$idV.";");
        mysql_close($con);
        return $query;
    }
}