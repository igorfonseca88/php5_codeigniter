<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Mapa de Vagas - Go Talent</title>
	
	<?php 
		$lat = $_GET["lat"];
		$lng = $_GET["lng"];
	?>
	
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	  <link rel="stylesheet" href="css/mapa.css">
	
    <script type="text/javascript">
    //<![CDATA[

    var customIcons = {
      restaurant: {
        icon: 'images/icone.png',
        shadow: 'images/icone.png'
      },
      bar: {
        icon: 'images/icone.png',
        shadow: 'images/icone.png'
      }
    };

    function load() {
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(<?=$lat?>, <?=$lng?>),
        zoom: 13,
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("phpsqlajax_genxml.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var cidade = markers[i].getAttribute("cidade");
          var empresa = markers[i].getAttribute("empresa");
          var id = markers[i].getAttribute("id");
          var numero = markers[i].getAttribute("numero");
          var link = markers[i].getAttribute("link");
          var type = markers[i].getAttribute("type");
          var salario = markers[i].getAttribute("salario");

          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          var html = "<p class='link_vaga'>" + name + "</p> <br/><br/> <b>Empresa: " + empresa + "</b><br/>Salário: "+ salario +"<br/><br/>" + address + " - " + numero + " - " + cidade + "<br/><br/><a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+id+"-"+link+"' >Mais Infos</a>";
          var icon = customIcons[type] || {};
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: icon.icon,
            shadow: icon.shadow,
            draggable:true,
            animation: google.maps.Animation.DROP
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
      });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}

    //]]>
  </script>
  </head>

  <body onload="load()">
    <div id="map" style="width: 100%; height: 100%"></div>
  </body>
</html>
