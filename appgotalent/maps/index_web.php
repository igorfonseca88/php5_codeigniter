<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Mapa de Vagas - Go Talent</title>

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/data.json"></script>
    <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
    <link rel="stylesheet" href="css/mapa.css">

    <script type="text/javascript">
    //<![CDATA[

  function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }
  function showPosition(position) {
    load(position.coords.latitude, position.coords.longitude);
  }

    function load(lat, lng) {
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(lat, lng),
        zoom: 13,
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("phpsqlajax_genxmlweb.php", function(data) {
        var xml = data.responseXML;
        var html = "";

        //var mks = [];
        
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var cidade = markers[i].getAttribute("cidade");
          var empresa = markers[i].getAttribute("empresa");
          var id = markers[i].getAttribute("id");
          var numero = markers[i].getAttribute("numero");
          var link = markers[i].getAttribute("link");
          var type = markers[i].getAttribute("type");
          var salario = markers[i].getAttribute("salario");
          var id_empresa = markers[i].getAttribute("id_empresa");
          var qtd_vagas = markers[i].getAttribute("qtd_vagas");

          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));

          html = "";
          for(var j = 0; j < markers.length; j++){
            if(id_empresa == markers[j].getAttribute("id_empresa")){
              html += "<h3>" + markers[j].getAttribute("name") + "</h3><br/> <b>Empresa: " + markers[j].getAttribute("empresa") + "</b><br/>Salário: "+ markers[j].getAttribute("salario") +"<br/>" + markers[j].getAttribute("address") + " - " + markers[j].getAttribute("numero") + " - " + markers[j].getAttribute("cidade") + "<br/><br/> <a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+ markers[j].getAttribute("id") +"-"+ markers[j].getAttribute("link") +"' >Mais Infos</a><br /> <hr size='2' width='90%'' align='center' noshade>";
            }
          }

          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: 'images/icone_num'+qtd_vagas+'.png',
            shadow: 'images/icone_num'+qtd_vagas+'.png',
            //draggable:true,
            animation: google.maps.Animation.DROP
          });
          bindInfoWindow(marker, map, infoWindow, html);
          //mks.push(marker);
        }
        //var markerCluster = new MarkerClusterer(map, mks);
      });
      //google.maps.event.addDomListener(window, 'load', initialize);
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}

    //]]>
  </script>
  <script>
          /*  (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53411814-1', 'auto');
            ga('send', 'pageview');
*/
        </script>
  </head>

  <body onload="getLocation()">
    <div id="map" style="width: 100%; height: 100%"></div>
  </body>
</html>
