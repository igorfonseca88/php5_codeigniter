<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Mapa de Vagas - Go Talent</title>
	
	<?php 
		$lat = $_GET["lat"];
		$lng = $_GET["lng"];
    $idProfissional = $_GET["idProfissional"];

    include_once "profissionalGetVagasCandidaturaDAO.php";
    $p = new profissionalGetVagasCandidaturaDAO;
    $c = $p->consultar($idProfissional);
    while($row = mysql_fetch_array($c))
    {
      $array[] = array($row[0]);
    }
	?>

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <link rel="stylesheet" href="css/mapa.css">
	
    <script type="text/javascript">
    //<![CDATA[

    function load() {
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(<?=$lat?>, <?=$lng?>),
        zoom: 13,
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("phpsqlajax_genxml.php?idProfissional="+<?=$idProfissional?>, function(data) {
        var xml = data.responseXML;
        var html = "";
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var cidade = markers[i].getAttribute("cidade");
          var empresa = markers[i].getAttribute("empresa");
          var id = markers[i].getAttribute("id");
          var numero = markers[i].getAttribute("numero");
          var link = markers[i].getAttribute("link");
          var type = markers[i].getAttribute("type");
          var salario = markers[i].getAttribute("salario");
          var id_empresa = markers[i].getAttribute("id_empresa");
          var qtd_vagas = markers[i].getAttribute("qtd_vagas");
          var dias_publicacao = markers[i].getAttribute("dias_publicacao");
          var porcentagem = markers[i].getAttribute("porcentagem");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));

          html = "";
          var cand = <?php echo json_encode($array); ?>;
          
          for(var j = 0; j < markers.length; j++){
            if(id_empresa == markers[j].getAttribute("id_empresa")){
              if(cand != null){
                    var percent = markers[j].getAttribute("porcentagem").toString().split(".");
                    var listCand = cand.toString().split(",");
                    if(listCand.indexOf(markers[j].getAttribute("id")) >= 0) {
                      html += "<h3>" + markers[j].getAttribute("name") + "</h3><br/> <b>Empresa: " + markers[j].getAttribute("empresa") + "</b><br/>Salário: "+ markers[j].getAttribute("salario") +"<br/>" + markers[j].getAttribute("address") + " - " + markers[j].getAttribute("numero") + " - " + markers[j].getAttribute("cidade") + "<br/><br/><b>Compatibilidade: " + percent[0] + "%</b><br/><br/> <a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+ markers[j].getAttribute("id") +"-"+ markers[j].getAttribute("link") +"' >Mais Infos</a> <a class='link_vaga' href='#' >Você já é candidato!</a> <br /> <hr size='2' width='90%'' align='center' noshade>";
                    }else{
                      if(dias_publicacao >= 2){
                        html += "<h3>" + markers[j].getAttribute("name") + "</h3><br/> <b>Empresa: " + markers[j].getAttribute("empresa") + "</b><br/>Salário: "+ markers[j].getAttribute("salario") +"<br/>" + markers[j].getAttribute("address") + " - " + markers[j].getAttribute("numero") + " - " + markers[j].getAttribute("cidade") + "<br/><br/><b>Compatibilidade: " + percent[0] + "%</b><br/><br/> <a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+ markers[j].getAttribute("id") +"-"+ markers[j].getAttribute("link") +"' >Mais Infos</a> <a class='link_ver' href='http://www.gotalent.com.br/appgotalent/profissionalVagasCandidaturaMapa.php?lat=<?=$lat?>&lng=<?=$lng?>&idProfissional=<?=$idProfissional?>&idVaga="+ markers[j].getAttribute("id") +"' >Candidatar-me</a> <br /> <hr size='2' width='90%'' align='center' noshade>";
                      }else{
                        html += "<h3>" + markers[j].getAttribute("name") + "</h3><br/> <b>Empresa: " + markers[j].getAttribute("empresa") + "</b><br/>Salário: "+ markers[j].getAttribute("salario") +"<br/>" + markers[j].getAttribute("address") + " - " + markers[j].getAttribute("numero") + " - " + markers[j].getAttribute("cidade") + "<br/><br/><b>Compatibilidade: " + percent[0] + "%</b><br/><br/> <a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+ markers[j].getAttribute("id") +"-"+ markers[j].getAttribute("link") +"' >Mais Infos</a> <a class='link_vaga' target='_blank' href='http://www.gotalent.com.br/principal/plano' >Exclusivo para Premium!</a> <br /> <hr size='2' width='90%'' align='center' noshade>";
                      }
                    }
                
              }else{
                  if(dias_publicacao >= 2){
                    html += "<h3>" + markers[j].getAttribute("name") + "</h3><br/> <b>Empresa: " + markers[j].getAttribute("empresa") + "</b><br/>Salário: "+ markers[j].getAttribute("salario") +"<br/>" + markers[j].getAttribute("address") + " - " + markers[j].getAttribute("numero") + " - " + markers[j].getAttribute("cidade") + "<br/><br/> <a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+ markers[j].getAttribute("id") +"-"+ markers[j].getAttribute("link") +"' >Mais Infos</a> <a class='link_ver' href='http://www.gotalent.com.br/appgotalent/profissionalVagasCandidaturaMapa.php?lat=<?=$lat?>&lng=<?=$lng?>&idProfissional=<?=$idProfissional?>&idVaga="+ markers[j].getAttribute("id") +"' >Candidatar-me</a> <br /> <hr size='2' width='90%'' align='center' noshade>";
                  }else{
                    html += "<h3>" + markers[j].getAttribute("name") + "</h3><br/> <b>Empresa: " + markers[j].getAttribute("empresa") + "</b><br/>Salário: "+ markers[j].getAttribute("salario") +"<br/>" + markers[j].getAttribute("address") + " - " + markers[j].getAttribute("numero") + " - " + markers[j].getAttribute("cidade") + "<br/><br/> <a class='link_mapa' target='_blank' href='http://www.gotalent.com.br/vaga/"+ markers[j].getAttribute("id") +"-"+ markers[j].getAttribute("link") +"' >Mais Infos</a> <a class='link_vaga' target='_blank' href='http://www.gotalent.com.br/principal/plano' >Exclusivo para Premium!</a> <br /> <hr size='2' width='90%'' align='center' noshade>";
                  }
              }
            }
          }

          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: 'images/icone_num'+qtd_vagas+'.png',
            shadow: 'images/icone_num'+qtd_vagas+'.png',
            //draggable:true,
            animation: google.maps.Animation.DROP
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
      });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}

    //]]>
  </script>
  <script>
          /*  (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53411814-1', 'auto');
            ga('send', 'pageview');
*/
        </script>
  </head>

  <body onload="load()">
    <div id="map" style="width: 100%; height: 100%"></div>
  </body>
</html>
