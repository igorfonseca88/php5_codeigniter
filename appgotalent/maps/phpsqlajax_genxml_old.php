<?php
require("phpsqlajax_dbinfo.php");

$idProfissional = $_GET["idProfissional"];

function parseToXML($htmlStr) 
{ 
$xmlStr=str_replace('<','&lt;',$htmlStr); 
$xmlStr=str_replace('>','&gt;',$xmlStr); 
$xmlStr=str_replace('"','&quot;',$xmlStr); 
$xmlStr=str_replace("'",'&#39;',$xmlStr); 
$xmlStr=str_replace("&",'&amp;',$xmlStr); 
return $xmlStr; 
} 

// Opens a connection to a MySQL server
$connection=mysql_connect ($server, $username, $password);
mysql_set_charset('UTF8', $connection);
if (!$connection) {
  die('Not connected : ' . mysql_error());
}

// Set the active MySQL database
$db_selected = mysql_select_db($database, $connection);
if (!$db_selected) {
  die ('Can\'t use db : ' . mysql_error());
}

// Select all the rows in the markers table
$query = "SELECT v.id, v.vaga, e.razaosocial, e.endereco, c.nome cidade, e.numero, 'bar' type, v.salario, e.id id_empresa, (select count(va.id) from tb_vaga va where va.idempresa = e.id and va.situacao = 'Ativo') qtd_vagas, DATEDIFF(NOW(), v.datapublicacao) dias_publicacao,(((select count(idSkill) from tb_profissional_skills where idSkill in ( select idSkill from tb_vaga_skills  where idVaga = v.id) and idProfissional = ".$idProfissional.") *100)/ (select count(idSkill) from tb_vaga_skills where idVaga = v.id)) as porcentagem FROM tb_vaga v join tb_empresa e on v.idempresa = e.id join cidade c on c.id = e.cidade WHERE v.situacao = 'Ativo' and e.endereco != '' order by e.id";
$result = mysql_query($query);
if (!$result) {
  die('Invalid query: ' . mysql_error());
}

header('Content-Type: text/xml; charset=UTF-8');

// Start XML file, echo parent node
echo '<?xml version="1.0" encoding="UTF-8" ?>';
echo '<markers>';

// Iterate through the rows, printing XML nodes for each
while ($row = @mysql_fetch_assoc($result)){
  // ADD TO XML DOCUMENT NODE
  echo '<marker ';
  echo 'name="' . parseToXML($row['vaga']) . '" ';
  echo 'address="' . parseToXML($row['endereco']) . '" ';
  echo 'cidade="' . parseToXML($row['cidade']) . '" ';
  echo 'empresa="' . parseToXML($row['razaosocial']) . '" ';
  echo 'id="' . parseToXML($row['id']) . '" ';
  echo 'numero="' . parseToXML($row['numero']) . '" ';
  echo 'salario="' . parseToXML($row['salario']) . '" ';
  echo 'id_empresa="' . parseToXML($row['id_empresa']) . '" ';
  echo 'qtd_vagas="' . parseToXML($row['qtd_vagas']) . '" ';
  echo 'dias_publicacao="' . parseToXML($row['dias_publicacao']) . '" ';
  echo 'porcentagem="' . parseToXML($row['porcentagem']) . '" ';

  $address = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', str_replace(" ","-",$row['endereco']) ) ); 
  $cidade = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', str_replace(" ","-",$row['cidade']) ) ); 
  $numero = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', str_replace(" ","-",$row['numero']) ) );
  $link = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', str_replace(" ","-",$row['vaga']) ) ); 

  echo 'link="' .$link. '" ';

  $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'-'.$numero.'-'.$cidade.'&sensor=false');
  $output= json_decode($geocode);
  $lat = $output->results[0]->geometry->location->lat;
  $long = $output->results[0]->geometry->location->lng;

  echo 'lat="'.$lat.'" ';
  echo 'lng="'.$long.'" ';
  echo 'type="' . $row['type'] . '" ';
  echo '/>';
}

// End XML file
echo '</markers>';
?>
