<?php
include_once "profissionalEditarFotoDAO.php";
$idProfissional = $_POST['idProfissional'];
$foto = $_POST['foto'];

$foto = str_replace('data:image/jpg;base64,', '', $foto);
$foto = str_replace(' ', '+', $foto);
$data = base64_decode($foto);
$numeroRand = rand(00, 9999);
$file = '../upload/profissional/Foto' . $idProfissional . '_'. $numeroRand . '.jpg';
$nomeFoto = 'Foto'.$idProfissional.'_'.$numeroRand.'.jpg';
$success = file_put_contents($file, $data);

$editarFoto = new profissionalEditarFotoDAO();
$c= $editarFoto->setEditarFotoProfissional($idProfissional, $nomeFoto);

$array[] = array('alterou' => $c);
$json["editarFotoProfissional"] = $array;
print(json_encode($json));
?>