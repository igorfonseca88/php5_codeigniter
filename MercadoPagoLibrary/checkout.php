<?php
require_once "lib/mercadopago.php";

$mp = new MP("2372843291048079", "bD85KKctxK3qmu22TGOcviXPrOLw2Kk3");

$preference_data = array(
    "items" => array(
        array(
            "id" => "",
            "title" => "Title of what you are paying for",
            "description" => "",
            "currency_id" => "BRL",
            "quantity" => 1,
            "unit_price" => 10.2
        )
    ),
    "payer" => array(
        array(
            "name" => "",
            "email" => ""
        )
    )
);

$preference = $mp->create_preference($preference_data);
?>

<!doctype html>
<html>
    <head>
        <title>MercadoPago SDK - Create Preference and Show Checkout Example</title>
    </head>
    <body>
       	<a href="<?php echo $preference["response"]["init_point"]; ?>" name="MP-Checkout" class="orange-ar-m-sq-arall">Pay</a>
        <script type="text/javascript" src="http://mp-tools.mlstatic.com/buttons/render.js"></script>
    </body>
</html>
