$(document).ready(function () {
    /*Máscaras*/

    $(".cpf").mask("999.999.999-99");
    $(".cep").mask("99999-999");
    $(".data").mask("99/99/9999");
    $(".hora").mask("99:99");
    $(".cnpj").mask("99.999.999/9999-99");
    $('.telefone').focusout(function () {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');


    $(".numeros").keyup(function () {
        var valor = $(this).val().replace(/[^0-9\,\.]+/g, '');
        $(this).val(valor);
    });


    $(".money").maskMoney({showSymbol: true, symbol: "R$", decimal: ",", thousands: "."});

    $('#dataTables-example').dataTable({
        responsive: true
    });
    $('#dataTables-skill').dataTable({
        responsive: true,
        paging: false,
        stateSave: true,
        scrollY: "300px",
        scrollCollapse: true
    });

    $("#idTiposPerguntas").change(function () {
        var idTiposPerguntas = $("#idTiposPerguntas").val();
        $.ajax({
            url: 'http://www.gotalent.com.br/empresaController/buscarPerguntas/' + idTiposPerguntas,
            success: function (data) {
                $("#idPerguntas").html(data);
            },
            beforeSend: function () {
                $(".loader").css({display: "block"});
            },
            complete: function () {
                $(".loader").css({display: "none"});
            }
        });
    });




    $("#estado").change(function () {
        var idestado = $("#estado").val();
        $.ajax({
            url: 'http://www.gotalent.com.br/estadoCidadeController/preencheCidades/' + idestado,
            success: function (data) {
                $("#cidade").html(data);
            },
            beforeSend: function () {
                $(".loader").css({display: "block"});
            },
            complete: function () {
                $(".loader").css({display: "none"});
            }
        });
    });

    $("#bc_cargo").autocomplete('http://www.gotalent.com.br/bcController/autocompletarPorCargo', {
        matchContains: true,
        minChars: 2,
        selectFirst: true
    });

    $("#bc_skill").autocomplete('http://www.gotalent.com.br/bcController/autocompletarPorSkill', {
        matchContains: true,
        minChars: 2,
        selectFirst: true
    });

    $(".profissional_cargo").autocomplete('http://www.gotalent.com.br/profissionalController/autocompletarPorCargo', {
        matchContains: true,
        minChars: 2,
        selectFirst: true
    });

    $(".profissional_empresa").autocomplete('http://www.gotalent.com.br/profissionalController/autocompletarPorEmpresa', {
        matchContains: true,
        minChars: 2,
        selectFirst: true
    });

    $(".profissional_instituicao").autocomplete('http://www.gotalent.com.br/profissionalController/autocompletarPorInstituicao', {
        matchContains: true,
        minChars: 2,
        selectFirst: true
    });

    $(".profissional_curso").autocomplete('http://www.gotalent.com.br/profissionalController/autocompletarPorCurso', {
        matchContains: true,
        minChars: 2,
        selectFirst: true
    });

   

});

function carregarSliderCursos(){
    $(document).ready(function () {
     //Sliders
    $('#sliderAfiliados').flexslider({
        animation: "slide",
        directionNav: true,
        controlNav: false,
        slideshow: true,
        itemWidth: 119,
        itemMargin: 0,
        start: function(slider) {
            $("#sliderAfiliados").removeClass('loading');
        }
    });
    });
    
}


/*Carrega as subareas de uma vaga */
function carregaSubArea() {
    $(document).ready(function () {

        var comboArea = $("#comboArea").val();
        if (comboArea != "") {
            $.ajax({
                url: 'http://www.gotalent.com.br/empresaController/buscarSubAreas/' + comboArea,
                success: function (data) {
                    if (data !== "") {
                        $("#spanComboSubArea").css("display", "block");
                        $("#comboSubArea").html(data);
                    }
                    else {
                        $("#spanComboSubArea").css("display", "none");
                    }

                },
                beforeSend: function () {
                    $(".loader").css({display: "block"});
                },
                complete: function () {
                    $(".loader").css({display: "none"});
                }
            });
        } else {
            $("#spanComboSubArea").css("display", "none");
        }
    });
}

/*Recupera os profissionais selecionados para uma entrevista*/
function recuperaChecks() {
    $(document).ready(function () {

        if ($("idTiposPerguntas").val() == "") {
            alert('Selecione um checklist a ser respondido pelo profissional.');
            return false;
        }

        camposMarcados = new Array();
        $("input[type=checkbox][name='checkSel[]']:checked").each(function () {
            camposMarcados.push($(this).val());
        });

        document.getElementById("hprofissionais").value = camposMarcados;
        document.getElementById("formPerguntas").submit();
    });
}

/*Utilizado para validar o motivo informado ao profissional para não classificar*/
function vmotivo() {
    if (document.getElementById("feedback").value.trim() == "") {
        alert('Informe um motivo para não classificar o profissional.');
        return false;
    }
    else if ($("#feedback").val() == "Outro" && document.getElementById("feedbackDetalhado").value.trim() == "") {
        alert('Informe um motivo para não classificar o profissional.');
        return false;
    }
    else {
        return true;
    }
}

$(document).ready(function () {
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
});

$(document).ready(function () {

    $('.input-group.date').datepicker({
        format: 'dd/mm/yyyy',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

});

function alteraAtributo(caminho) {
    $("#iframeCandidato").attr('src', caminho);

}
function alteraAtributoChecklist(caminho) {
    $("#iframeChecklist").attr('src', caminho);

}

function alteraAtributoMessage(caminho) {
    $("#formMessage").attr('action', caminho);

}
function alteraAtributoMotivo(caminho) {
    $("#formMotivo").attr('action', caminho);

}

function alteraAtributoVideo(caminho) {
    var parte = "https://www.youtube.com/embed/";
    $("#frameVideo").attr('src', parte + caminho);
}
function alteraAtributoIndicacao(caminho) {
    $("#formIndicacaoAction").attr('action', caminho);

}

function validaFiltros() {

    var msg = "";

    var vaga = document.getElementById('vaga').value;

    if (vaga.trim() == "") {
        msg = "Selecione uma vaga para buscar indicações";
    }

    if (msg != "") {
        document.getElementById("alerta").innerHTML = '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' + msg + '</div>';
        return false;
    }
    else {
        document.getElementById("formIndicacao").submit();
    }

}

//Alterar valor da contratacao do plano no default-empresa
$(document).ready(function (e) {
    $("input[name=vagaAdicional]").change(function () {


        var valor = 79.90 + ($("input[name=vagaAdicional]").val() - 1) * 49.90;
        $("#valorAnuncio").html("R$ " + valor.toFixed(2));

    });
});

function validaDados() {

    var msg = "";

    var name = document.getElementById('nome').value;
    if (name.trim() == "") {
        msg = "Preencha o seu Nome Completo.  </br>";
    }
    var sexo = document.getElementById('sexo').value;
    if (sexo.trim() == "") {
        msg += "Selecione o seu Sexo. </br>";
    }
    var phone = document.getElementById('telefone').value;
    if (phone.trim() == "") {
        msg += "Preencha o seu Telefone. </br>";
    }
    if (document.getElementById('estado').value == "") {
        msg += "Selecione o seu Estado. </br>";
    }
    var cidade = document.getElementById('cidade').value;
    if (cidade.trim() == "") {
        msg += "Preencha a sua Cidade. </br>";
    }

    var pretensao = document.getElementById('pretensao').value;
    if (pretensao.trim() == "") {
        msg += "Preencha a sua Pretensão salarial. </br>";
    }

    var resumo = document.getElementById('resumo').value;
    if (resumo.trim() == "") {
        msg += "Preencha um resumo sobre você. </br>";
    }

    if (msg != "") {
        document.getElementById("alerta").innerHTML = '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' + msg + '</div>';
        //return false;
    }
    else {
        document.getElementById("formDadosPessoais").submit();
    }



}

function escondeMostrarCampo() {

    if ($('#campoDataFim').is(':visible')) {
        $("#campoDataFim").css({'display': "none"});
    } else {
        $("#campoDataFim").css({'display': "block"});
    }

}

function validarData() {
    var matchdata = new RegExp(/((0[1-9]|[12][0-9]|3[01])\/(0[13578]|1[02])\/[12][0-9]{3})|((0[1-9]|[12][0-9]|30)\/(0[469]|11)\/[12][0-9]{3})|((0[1-9]|1[0-9]|2[0-8])\/02\/[12][0-9]([02468][1235679]|[13579][01345789]))|((0[1-9]|[12][0-9])\/02\/[12][0-9]([02468][048]|[13579][26]))/gi);
    var data = $('#dataInicio').val();
    var dataFim = $('#dataFim').val();
    var msg = "";
    if (!data.match(matchdata)) {
        msg += '- Preencha corretamente o Data de início.\n';

    }
    if (dataFim != "" && !dataFim.match(matchdata)) {
        msg += '- Preencha corretamente o Data fim.\n';

    }

    if (msg != "") {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}

function validarDataFormacao() {
    var matchdata = new RegExp(/((0[1-9]|[12][0-9]|3[01])\/(0[13578]|1[02])\/[12][0-9]{3})|((0[1-9]|[12][0-9]|30)\/(0[469]|11)\/[12][0-9]{3})|((0[1-9]|1[0-9]|2[0-8])\/02\/[12][0-9]([02468][1235679]|[13579][01345789]))|((0[1-9]|[12][0-9])\/02\/[12][0-9]([02468][048]|[13579][26]))/gi);
    var data = $('#dataInicioFormacao').val();
    var dataFim = $('#dataFimFormacao').val();
    var msg = "";
    if (!data.match(matchdata)) {
        msg += '- Preencha corretamente o Data de início.\n';

    }
    if (dataFim != "" && !dataFim.match(matchdata)) {
        msg += '- Preencha corretamente o Data fim.\n';

    }

    if (msg != "") {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}

function escondeMostrarCampoResposta(value) {

    if (value == "Discursiva" || value == "Video") {
        $("#divResposta").css({'display': "none"});
    } else {
        $("#divResposta").css({'display': "block"});
    }

}


function gravarArquivoBanco(idvaga, arq, iditem, text) {
    $(document).ready(function () {


        $.ajax({
            url: 'http://www.gotalent.com.br/videoEntrevistaController/salvarRespostaAction/' + idvaga + '/' + arq + '/' + iditem + '/' + text,
            success: function (data) {
                if (data !== "") {
                    alert(data)
                    location.reload(true);
                }

            },
            beforeSend: function () {
                $(".loader").css({display: "block"});
            },
            complete: function () {
                $(".loader").css({display: "none"});
            }
        });

    });
}

function habitaCampoMotivoDetalhado() {

    if ($("#feedback").val() == "Outro") {
        $("#feedbackDetalhado").css({display: "block"});
    }
    else {
        $("#feedbackDetalhado").css({display: "none"});
    }

}

/*Grava a avaliaçao do video */
function gravaAvaliacaoVideo() {
    $(document).ready(function () {


        var avaliacaoVideo = $("#avaliacaoVideo").val();
        var idprofvaga = $("#idprofvaga").val();

        $.ajax({
            url: 'http://www.gotalent.com.br/checklistController/gravaAvaliacao/' + idprofvaga + '/' + avaliacaoVideo,
            success: function (data) {
                if (data == "sucesso") {
                    $("#resulAlert").html("<span style='color:green'>Avaliação salva com sucesso.</span>");
                }
                else {
                    $("#resulAlert").html("<span style='color:red'>Erro ao salvar avaliação, tente de novo.</span>");
                }
            },
            beforeSend: function () {
                $(".loader").css({display: "block"});
            },
            complete: function () {
                $(".loader").css({display: "none"});
            }
        });

    });
}

function setHidden(value, idHidden) {
    document.getElementById(idHidden).value = value;
}

/*Envia convite para participar de processo */
function convidarProfissional() {
    $(document).ready(function () {


        var idProfissional = $("#hidprofissional").val();
        var idVaga = $("#idvagaconvite").val();
        var mensagemConvite = $("#mensagemConvite").val();
        
        if(idVaga != ""){

        $.post("http://www.gotalent.com.br/bcController/convidar/", {idProfissional: idProfissional, idVaga: idVaga, mensagemConvite: mensagemConvite}, function (result) {
            if (result == "sucesso") {
                $("#resulAlert").html("<span style='color:green'>Convite enviado com sucesso.</span>");
            }
            else {
                $("#resulAlert").html("<span style='color:red'>Erro ao enviar convite, tente de novo.</span>");
            }
        });

        }else{
            $("#resulAlert").html("<span style='color:red'>Selecione a vaga para enviar o convite.</span>");
        }

    });
}

function limparResultado(){
    $("#resulAlert").html(" ");
}

function notificarProfissional(idProfissional){
     $(document).ready(function () {
        $.post("http://www.gotalent.com.br/bcController/notificarProfissional/", {idProfissional: idProfissional}, 
        function (result) {});
    });
}


/*Novas Funções*/

$(document).ready(function () {
   // document.getElementById("divsLocalVaga").style.display = "none";

    if($('#localVaga').is(':checked')){
        $("#divsLocalVaga").hide();
    }else{
        $("#divsLocalVaga").show();
    }
});

/*Funções Utilizadas para view addVaga*/
$('#localVaga').on('ifChecked', function () {
    $("#divsLocalVaga").hide();
});
$('#localVaga').on('ifUnchecked', function () {
    $("#divsLocalVaga").show();
});
$('#salarioVaga').on('ifChanged', function () {
    $("#salario").val("Salário a combinar").attr("readOnly", true);
});
$('#salarioVaga').on('ifUnchecked', function () {
    $("#salario").val("").attr("readOnly", false).attr("placeholder", "R$ 1.000,00");
});

$("#chkVideoEntrevista").on('ifChecked', function(){
    $('.newCheckModal').hide();
    $('#myModal').modal('show');
});

$('#btnNewChecklist').click(function(){
    $('#btnNewChecklist').hide();
    $('.newCheckModal').show();
    $('#btnCadChecklist').show();
});


$("#estado").change(function () {
    var estado = $("#estado").val();

    if($.isNumeric(estado)){
        $.ajax({
            url: 'http://www.gotalent.com.br/estadoCidadeController/preencheCidades/' + estado,
            success: function (data) {
                $("#cidade").html(data);
            },
            beforeSend: function () {
                $(".loader").css({display: "block"});
            },
            complete: function () {
                $(".loader").css({display: "none"});
            }
        });
    }else{
        $.ajax({
            url: 'http://www.gotalent.com.br/estadoCidadeController/preencheCidadesPorUF/' + estado,
            success: function (data) {
                $("#cidade").html(data);
            },
            beforeSend: function () {
                $(".loader").css({display: "block"});
            },
            complete: function () {
                $(".loader").css({display: "none"});
            }
        });
    }

});


$("#estadoAddVaga").change(function () {
    var uf = $("#estadoAddVaga").val();
    $.ajax({
        url: 'http://www.gotalent.com.br/estadoCidadeController/preencheCidadesPorUF/'+uf,
        success: function (data) {
            $("#cidadeAddVaga").html(data);
        },
        beforeSend: function () {
            $(".loaderAddVaga").css({display: "block"});
        },
        complete: function () {
            $(".loaderAddVaga").css({display: "block"});
        }
    });
});

function enableInputDataVigencia() {
    document.querySelector('#dataVigencia').removeAttribute('readonly');
    document.getElementById('dataVigencia').focus();
}

function funcSalarioVaga() {
    var btnInput = document.getElementById("btnSalAComb");
    var input = document.getElementById("salario");

    if (input.readOnly != true) {
        input.removeAttribute('placeholder');
        input.value = 'Salário a combinar';
        input.setAttribute('readonly', true);
        btnInput.value = "Inserir Valor do Salário"
    } else {
        input.removeAttribute('readonly', false);
        input.value = "";
        input.setAttribute("placeholder", 'R$ 1.000,00');
        btnInput.value = "Salário a combinar";
    }
}
