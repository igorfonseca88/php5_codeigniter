<?php

class AdministradorController extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata("tipo") != "Administrador") {
            redirect('/');
        }
    }

    /* Açoes do adm */

    function editEmpresa() {
        $this->load->model("Empresa_model", "empresa");
        $id = $this->input->post("id");

        $update = array(
            "razaosocial" => $this->input->post("razaosocial"),
            "email" => $this->input->post("email"),
            "telefone" => $this->input->post("telefone"),
            "cep" => $this->input->post("cep"),
            "endereco" => $this->input->post("endereco"),
            "numero" => $this->input->post("numero"),
            "bairro" => $this->input->post("bairro"),
            "cidade" => $this->input->post("cidade"),
            "estado" => $this->input->post("estado"),
            "facebook" => $this->input->post("facebook"),
            "contato" => $this->input->post("contato"),
            "sobre" => $this->input->post("sobre")
        );

        if ($this->empresa->update($id, $update) > 0) {
            $data["sucesso"] = "Salvo com sucesso.";
        } else {
            $data["erro"] = "Erro ao salvar.";
        }

        $this->editarEmpresaAction($id, $data);
    }

    function editarEmpresaAction($id, $mensagem = array()) {
        $this->load->model('Empresa_model', 'empresa');

        $data["empresa"] = $this->empresa->buscarPorId($id);

        $this->load->model('Usuario_model', 'usuario');

        $data["usuario"] = $this->usuario->buscarUsuarioPorIdEmpresa($id);

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        if ($data["empresa"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($data["empresa"][0]->estado);
        }

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/administrador/editEmpresa");
    }

    function novaEmpresaAction() {
        $this->load->view("priv/administrador/addEmpresa");
    }

    function editarProfissionalRecrutadorAction($id) {
        $this->load->model('Profissional_model', 'profissional');


        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
        $data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
        $data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional($id);

        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->buscarTodasSkillsDisponiveisPorIdProfissional($id);


        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        if ($data["profissional"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($data["profissional"][0]->estado);
        }

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);

        $this->load->view("priv/administrador/editProfissional");
    }

    function listarProfissionais() {
        $this->load->model('Profissional_model', 'profissional');
        $data["profissionais"] = $this->profissional->getAll();
        $this->load->vars($data);
        $this->load->view("priv/administrador/listProfissional");
    }

    function listarVagas() {
        $this->load->model('Vaga_model', 'vaga');
        $data["vagas"] = $this->vaga->buscarTodas();

        $this->load->vars($data);
        $this->load->view("priv/administrador/listVaga");
    }

    function editVaga() {
        $this->load->model('Vaga_model', 'vaga');
        $tipoProfissional = implode(",", $this->input->post("tipoProfissional"));

        $update = array(
            "vaga" => $this->input->post("vaga"),
            "descricao" => $this->input->post("descricao"),
            "quantidade" => $this->input->post("quantidade"),
            "tipo" => $this->input->post("tipo"),
            "tipoProfissional" => $tipoProfissional,
            "informacoesAdicionais" => $this->input->post("informacoesAdicionais"),
            "salario" => $this->input->post("salario") == "" ? "NÃO DIVULGADO" : $this->input->post("salario"),
            "cidade" => $this->input->post("cidade"),
            "estado" => $this->input->post("estado"),
            "diferenciais" => $this->input->post("diferenciais"),
            "idSubArea" => $this->input->post("comboSubArea") == "" ? null : $this->input->post("comboSubArea"),
            "idArea" => '2',
             "cep" => $this->input->post("cep"),
            "endereco" => $this->input->post("endereco"),
            "numero" => $this->input->post("numero"),
            "bairro" => $this->input->post("bairro")
        );

        $id = $this->vaga->update($this->input->post("idVaga"), $update);

        // delete beneficios por id vaga
        $this->vaga->deleteBeneficiosPorIdVaga($this->input->post("idVaga"));

        // inseri na tabela tb_profissional_skills		
        $beneficios = $this->input->post("beneficios");
        foreach ($beneficios as $beneficio) {
            $insertBeneficio = array(
                "idVaga" => $this->input->post("idVaga"),
                "idBeneficio" => $beneficio
            );
            $this->vaga->add_beneficio($insertBeneficio);
        }

        if ($id > 0) {
            $this->session->set_flashdata('sucesso', 'Vaga salva com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar salvar a vaga, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
        }
        redirect("administradorController/editarVagaAction/" . $this->input->post("idVaga"));
    }

    function editarVagaAction($id, $mensagem = array()) {

        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $this->load->model('Vaga_model', 'vaga');
        $data["vaga"] = $this->vaga->buscarVagaPorId($id);

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        if ($data["vaga"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($this->estado->buscarIdEstadoPorUF($data["vaga"][0]->estado)[0]->id);
        }

       $data["beneficios"] = $this->vaga->buscarTodosBeneficios();
        $data["beneficios_vaga"] = $this->vaga->buscarTodosBeneficiosPorIdVaga($id);
        $data["vaga_skills"] = $this->vaga->buscarTodasSkillsPorIdVaga($id);
        $data["profissionais_vaga"] = $this->vaga->buscarProfissionaisVagaPorIdVaga($id);

        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->buscarTodasSkillsDisponiveisPorIdVaga($id);


        $this->load->model('Profissional_model', 'profissional');
        
        foreach ($data["profissionais_vaga"] as $profissional) {
            $compatibilidade = 0;
            $compatibilidade = $this->vaga->retornaCompatibilidadeProfissional($profissional->id, $id);

            $compatibilidade_vaga[$profissional->id] = array("idProfissional" => $profissional->id, "compatibilidade" => $compatibilidade[0]->porcentagem);
        }
        $data["compatibilidade_vaga"] = $compatibilidade_vaga;
        
        $this->load->model('Area_model', 'area');
        #$data["areas"] = $this->area->buscarTodasAreas();
        $data["subareas"] = $this->area->buscarTodasSubAreas();
        $data["subareas"] = $this->area->buscarSubAreaPorIdArea('2');


        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/administrador/editVaga");
    }

    function addSkillVaga() {
        $this->load->model("Skills_model", "skill");
        $this->load->model("Vaga_model", "vaga");

        $skills = $this->input->post("lista");

        foreach ($skills as $sk) {
            $insertSkill = array(
                "idVaga" => $this->input->post("idVagah"),
                "idSkill" => $sk
            );
            $this->vaga->add_skill($insertSkill);
        }
        $this->session->set_flashdata('sucesso', 'Compentência(s) inserida(s) com sucesso.');

        redirect("administradorController/editarVagaAction/" . $this->input->post("idVagah"));
    }

    function excluirSkillVaga($id, $idVaga) {
        $this->load->model("Vaga_model", "vaga");

        if ($this->vaga->deleteVagaSkillPorId($id) > 0) {
            $this->session->set_flashdata('sucesso', 'Compentência removida com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao remover competência.');
        }
        redirect("administradorController/editarVagaAction/" . $idVaga);
    }

    function publicarVagaAction($idVaga, $idEmpresa) {

        $this->load->model('Vaga_model', 'vaga');
        $this->load->model('Extrato_model', 'extrato');


        $vaga = $this->vaga->buscarVagaPorId($idVaga);
        // buscar o ultimo credito da empresa
        //$creditoVagas = $this->extrato->buscarUltimoExtratoPorIdEmpresa($idEmpresa);
        // "dataVigencia" => $creditoVagas[0]->vigencia ,

        $this->load->model('Empresa_model', 'empresa');
        $empresa = $this->empresa->buscarPorId($idEmpresa);

        if($empresa[0]->dataFinalPlano < date('Y-m-d')) {
            $this->session->set_flashdata('error', 'Erro ao publicar vaga, o plano da empresa expirou.');
            redirect("administradorController/editarVagaAction/" . $idVaga);
        }

        $update = array(
            "dataPublicacao" => date("Y-m-d"),
            "dataVigencia" => $empresa[0]->dataFinalPlano,
            "situacao" => "Ativo"
        );



        if ($this->vaga->update($idVaga, $update) > 0) {

            $this->session->set_flashdata('sucesso', 'Vaga publicada com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao publicar vaga.');
        }
        redirect("administradorController/editarVagaAction/" . $idVaga);
    }

    function desaprovarVagaAction() {

        $this->load->model('Vaga_model', 'vaga');
        $this->load->model('Extrato_model', 'extrato');

        $idVaga =  $this->input->post("idVaga");
        $idEmpresa= $this->input->post("idEmpresa");

        $vaga = $this->vaga->buscarVagaPorId($idVaga);
        // buscar o ultimo credito da empresa
        //$creditoVagas = $this->extrato->buscarUltimoExtratoPorIdEmpresa($idEmpresa);
        // "dataVigencia" => $creditoVagas[0]->vigencia ,

        $this->load->model('Empresa_model', 'empresa');
        $empresa = $this->empresa->buscarPorId($idEmpresa);

        if($empresa[0]->dataFinalPlano < date('Y-m-d')) {
            $this->session->set_flashdata('error', 'Erro ao publicar vaga, o plano da empresa expirou.');
            redirect("administradorController/editarVagaAction/" . $idVaga);
        }

        $update = array(
            "dataDesaprovacao" => date("Y-m-d"),
            "motivoDesaprovacao" => $this->input->post("motivoFechamento"),
            "situacao" => "Desaprovado"
        );



        if ($this->vaga->update($idVaga, $update) > 0) {

            $this->session->set_flashdata('sucesso', 'Vaga desaprovada com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao publicar vaga.');
        }
        redirect("administradorController/editarVagaAction/" . $idVaga);
    }

    function addSkill() {
        $this->load->model('Skills_model', 'skill');

        $insert = array(
            "skill" => $this->input->post("skill"),
            "categoria" => $this->input->post("categoria")
        );
        $this->skill->add_record($insert);
        redirect("administradorController/listarSkills");
    }

    function novaSkillAction() {
        $this->load->view("priv/administrador/addSkill");
    }

    function editarSkillAction($idSkill) {
        
    }

    function listarSkills() {
        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->getAll();

        $this->load->vars($data);
        $this->load->view("priv/administrador/listSkill");
    }

    function imprimirProfissionaisAction() {
        // Incluimos a classe PHPExcel
        include 'Classes/PHPExcel.php';

        // Instanciamos a classe
        $objPHPExcel = new PHPExcel();

        // Definimos o estilo da fonte
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('G2')->getFont()->setBold(true);

        // Criamos as colunas
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Listagem de Profissionais')
                ->setCellValue('A2', "Nome ")
                ->setCellValue("B2", "E-mail")
                ->setCellValue("C2", "Telefone")
                ->setCellValue("D2", "Pretensão")
                ->setCellValue("E2", "Data de Nascimento")
                ->setCellValue("F2", "Cidade")
                ->setCellValue("G2", "Estado");

        // Podemos configurar diferentes larguras paras as colunas como padrão
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

        $this->load->model('Profissional_model', 'profissional');
        $arrayPessoas = $this->profissional->getAll();
        $i = 3;
        foreach ($arrayPessoas as $pessoa) {
            // Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $pessoa->nome);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $pessoa->email);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $pessoa->telefone);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $pessoa->pretensao);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, implode("/", array_reverse(explode("-", $pessoa->nascimento))));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $pessoa->cid);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $pessoa->uf);
            $i++;
        }

        // Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
        $objPHPExcel->getActiveSheet()->setTitle('Listagem de Profissionais');

        // Cabeçalho do arquivo para ele baixar
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="listagem_de_profissionais.xls"');
        header('Cache-Control: max-age=0');
        // Se for o IE9, isso talvez seja necessário
        header('Cache-Control: max-age=1');

        // Acessamos o 'Writer' para poder salvar o arquivo
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
        $objWriter->save('php://output');

        exit;
    }

    public function listarProfissionaisSkills() {
        $this->load->model('Profissional_model', 'profissional');
        $data["profissional_skills"] = $this->profissional->listarProfissionaisSkills();
        $this->load->vars($data);
        $this->load->view("priv/administrador/listProfissionalSkill");
    }

    public function indicacao() {
        $this->load->model("Vaga_model", "vaga");

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();

        $data["vagas"] = $this->vaga->getAll();
        $this->load->vars($data);
        $this->load->view("priv/administrador/indicacao");
    }

    public function listarEmpresas() {
        $this->load->model("Empresa_model", "empresa");
        $data["empresas"] = $this->empresa->getAll();
        $this->load->vars($data);
        $this->load->view('priv/administrador/listEmpresa');
    }

    public function buscarProfissionaisIndicados() {
        $vaga = $this->input->post("vaga");
        $estado = $this->input->post("estado");
        $pretensao = str_replace(",", ".", str_replace(".", "", $this->input->post("pretensao")));
        $tipoPretensao = $this->input->post("tipoPretensao");

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();

        $this->load->model("Vaga_model", "vaga");
        $data["profissionais"] = $this->vaga->retornaProfissionaisCompativeisPorFiltros($vaga, $estado, $pretensao, $tipoPretensao);
        $data["vagas"] = $this->vaga->getAll();
        $this->load->vars($data);
        $this->load->view("priv/administrador/indicacao");
    }

    public function enviarEmailMkt() {
        $this->load->view('priv/administrador/emailMarketing');
    }

    public function enviarEmail($lista, $texto, $assunto) {


        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = 'mail.gotalent.com.br';
        $config['smtp_user'] = 'noreply@gotalent.com.br';
        $config['smtp_pass'] = 'admingo1';
        $config['smtp_port'] = 587;

        $this->email->initialize($config);

        $this->email->from('noreply@gotalent.com.br', 'GoTalent');
        $this->email->to('contato@gotalent.com.br');
        $lista = $this->input->post("lista");
        $this->email->bcc($lista);
        $this->email->subject($this->input->post("assunto"));
        $msg = "&nbsp;";
        $msg = $msg . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
        $msg = $msg . $this->input->post("texto");
        $msg = $msg . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->email->message($msg);

        if ($this->email->send()) {
            redirect('administradorController/enviarEmailMkt');
        } else {
            echo $this->email->print_debugger();
        }
    }

    function indicarProfissionalAction($idProfissional, $idVaga) {

        $indicacao = $this->input->post("indicacao");

        $this->load->model("Vaga_model", "vaga");
        $this->load->model("Profissional_model", "profissional");
        $profissional_vaga = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);
        if (count($profissional_vaga) > 0) {
            $update = array("isIndicacao" => "SIM", "motivoIndicacao" => $indicacao);

            if ($this->vaga->updateProfissionalVaga($profissional_vaga[0]->id, $update) > 0) {

                $this->session->set_flashdata('sucesso', 'Profissional indicado com sucesso.');
            } else {
                $this->session->set_flashdata('error', 'Erro ao indicar profissional.');
            }
            redirect("administradorController/editarVagaAction/" . $idVaga);
        } else {
            $insert = array(
                "data" => date("Y-m-d"),
                "idVaga" => $idVaga,
                "idProfissional" => $idProfissional,
                "isIndicacao" => "SIM",
                "motivoIndicacao" => $indicacao
            );

            if ($this->profissional->add_candidatar_vaga($insert) > 0) {
                $this->session->set_flashdata('sucesso', 'Profissional indicado com sucesso.');
            } else {
                $this->session->set_flashdata('error', 'Erro ao indicar profissional.');
            }
            redirect("administradorController/indicacao/");
        }
    }

    function inserirLatitudeLongitudeVaga($idVaga, $latitude, $longitude) {

        $this->load->model("Vaga_model", "vaga");
        $result = $this->vaga->updateLatitudeLongitudeVaga($idVaga, $latitude, $longitude);
        if($result > 0){
            echo "<script>alert('Latitude e Longitude inseridos com sucesso para a vaga $idVaga.');</script>";
        }else{
            echo "<script>alert('Erro ao processar as informacoes. Por favor, verifique os valores e tente novamente.');</script>";
        }
        echo "<script>window.location='".base_url()."administradorController/listarVagas';</script>";
    }

}

?>