<?php

class CategoriaController extends CI_Controller {

    function __construct() {
        parent::__construct();
		if($this->session->userdata("tipo") != "Administrador" && $this->session->userdata("tipo") != "Conteudo"){
			redirect('/');
		}
    }

    function index() {
		$this->load->model('Categoria_model', 'categoria');
		
		$data["categoria"] = $this->categoria->getAll();
		$this->load->vars($data);
		$this->load->view("priv/blog/categoria/listCategoria");
    }
    
    function novaCategoriaAction() {
		$this->load->view("priv/blog/categoria/addCategoria");
    }

    function addCategoria() {
	   $this->load->model("Categoria_model", "categoria");

       $insert = array(
            "titulo" => $this->input->post("titulo"),
            "descricao" => $this->input->post("descricao"),
			"urlCat" => $this->input->post("urlCat"),
            "status" => $this->input->post("status")
           
        );
        $id = $this->categoria->add_record($insert);
		
        if ($id > 0) {
            $confere["categoria"] = $this->categoria->buscarPorId($id);
            if (!is_null($confere)) {
			$data["sucesso"] = "Categoria salva com sucesso.";
			
			$data["categoria"] = $this->categoria->getAll();
			$this->load->vars($data);
			$this->load->view("priv/blog/categoria/listCategoria");
            }
        }
    }

    function editCategoria() {
        $this->load->model("Categoria_model", "categoria");
        $id = $this->input->post("id");
        $update = array(            
            "titulo" => $this->input->post("titulo"),
            "descricao" => $this->input->post("descricao"),
            "urlCat" => $this->input->post("urlCat"),
            "status" => $this->input->post("status")
        );

        if ($this->categoria->update($id, $update) > 0) {
            $confere["categoria"] = $this->categoria->buscarPorId($id);
            if (!is_null($confere)) {
				$data["sucesso"] = "Salvo com sucesso.";
				
				$data["categoria"] = $this->categoria->getAll();
				$this->load->vars($data);
				$this->load->view("priv/blog/categoria/listCategoria");
            }
        }
    }
	
    function deleteCategoria($id) {
		$this->load->model("Categoria_model", "categoria");
		if ($this->categoria->delete($id) > 0) {	
			$data["sucesso"] = "Excluído com sucesso.";
			$data["categoria"] = $this->categoria->getAll();
			$this->load->vars($data);
			$this->load->view("priv/blog/categoria/listCategoria");
		}
    }
    
	function editarCategoriaAction($id, $mensagem) {
		$this->load->model('Categoria_model', 'categoria');
		$data["categoria"] = $this->categoria->buscarPorId($id);
		$data["error"] = $mensagem["erro"];
		$data["sucesso"] = $mensagem["sucesso"];
		
		if (!is_null($data)) {
			$this->load->vars($data);
			$this->load->view("priv/blog/categoria/editCategoria");
		}
	}
}
?>