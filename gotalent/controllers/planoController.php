<?php

require_once "PagSeguroLibrary/PagSeguroLibrary.php";
require_once "PagSeguroLibrary/PagSeguroNpi.php";
require_once("MercadoPagoLibrary/lib/mercadopago.php");

class PlanoController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function planoFree()
    {
        $this->load->model("Profissional_model", "profissional");
        $id = $this->session->userdata("idProfissional");

        $update = array(
            "idPlano" => 1,
            "dataInicioPlano" => null,
            "dataFinalPlano" => null,
            "valorPago" => 0,
            "idPlanoTemporario" => null
        );

        if ($this->profissional->update($id, $update) > 0) {
            $this->session->set_flashdata('error', 'Erro ao alterar seu plano, tente novamente.');
        } else {
            $this->session->set_flashdata('sucesso', 'Plano alterado com sucesso.');
        }
        redirect('area-restrita');
    }

    function planoPremium($idPlano)
    {
        $this->load->model("Plano_model", "planos");
        $plano = $this->planos->buscarPlanoProfissionalPorId($idPlano);

        if ($plano[0]->id == "" or $this->session->userdata("idProfissional") == "") {
            redirect("principal/plano");
        }

        $planoDesc = "Assinatura do plano " . $plano[0]->tipoPlano . " com acesso de " . $plano[0]->diasPlano . " dias no portal Go Talent";
        $valor = $plano[0]->valorPlano;

        $this->load->model('Profissional_model', 'profissional');
        $updateProfissional = array(
            "idPlanoTemporario" => $plano[0]->id,
            "valorPago" => $valor
        );
        $this->profissional->update($this->session->userdata("idProfissional"), $updateProfissional);
        $ref = "PRO/" . $this->session->userdata("idProfissional");

        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");
        $paymentRequest->addItem($ref, $planoDesc, 1, $valor);
        $paymentRequest->setReference($ref);
        $paymentRequest->setRedirectUrl(base_url() . "planoController/finalizacao");

        try {
            $credentials = new PagSeguroAccountCredentials("igorfonseca88@yahoo.com.br", "7E17B268E14B49BE913C42FE077EBD70");
            $url = $paymentRequest->register($credentials);
            header("Location: $url");
        } catch (PagSeguroServiceException $exp) {
            //print_r($exp->getHttpStatus());
            $this->session->set_flashdata('error', $exp->getMessage() . "Não foi possível concluir a transação pois houve algum erro no credenciamento com a PagSeguro, contate o administrador.");
            redirect('area-restrita');
        }
    }

    function planoEmpresa()
    {
        //Parametros recebidos
        $id = $this->input->get_post("idplano");
        //$destaque = $this->input->get_post("destaque");
        //$vagaAdicional = $this->input->get_post("vagaAdicional");

        //Busca plano
        $this->load->model("Plano_model", "planos");
        $plano = $this->planos->buscarPlanosEmpresasPorId($id);


        $valor = $plano[0]->valor;
        $quantidade = 30;

        // if ($vagaAdicional > 1) {
        //   $vagaAdicional = $vagaAdicional -1;
        // $valorAdicional = $vagaAdicional * $plano[0]->vagaAdicional;
        // $valor = $valor + $valorAdicional;
        // $quantidade = $quantidade + $vagaAdicional;
        // }
        //Dados do pagamento
        $descricao = "Assinatura mensal do plano " . $plano[0]->plano . " no portal Go Talent";
        //Verifica se houve selecao da destaque da vaga
        //$planoDestaque = $plano[0]->destaque;
        //if ($destaque == "SIM") {
        //	$valor = 138.00;
        //	$descricao = $descricao . " com destaque da vaga";
        //	$planoDestaque = 1;
        //}
        //Cria extrato
        $this->load->model('Extrato_model', 'extrato');
        $insert = array(
            "dataCriacao" => date("Y-m-d h:m:s"),
            "idEmpresa" => $this->session->userdata("idEmpresa"),
            "idPlanoEmpresa" => $id,
            "saldoAnuncio" => $quantidade,
            "saldoDestaque" => 0,
            "tipo" => "AGUARDANDO PAGAMENTO"
        );
        $idExtrato = $this->extrato->add_record($insert);

        //Referencia
        $ref = "EMP/" . $idExtrato;

        //Envia ao pagseguro
        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");
        $paymentRequest->addItem($ref, $descricao, 1, $valor);
        $paymentRequest->setReference($ref);
        $paymentRequest->setRedirectUrl(base_url() . "planoController/finalizacao");

        try {
            $credentials = new PagSeguroAccountCredentials("igorfonseca88@yahoo.com.br", "7E17B268E14B49BE913C42FE077EBD70");
            $url = $paymentRequest->register($credentials);
            header("Location: $url");
        } catch (PagSeguroServiceException $exp) {
            //print_r($exp->getHttpStatus());
            $this->session->set_flashdata('error', $exp->getMessage() . "Não foi possível concluir a transação pois houve algum erro no credenciamento com a PagSeguro, contate o administrador.");
            redirect('area-restrita-empresa');
        }
    }

    function destacarVaga($id)
    {
        $this->load->model("Vaga_model", "vaga");
        $dados["vaga"] = $this->vaga->buscarVagaPorId($id);

        $descricao = "Destaque da vaga " . $dados["vaga"][0]->vaga . " no portal Go Talent";
        $valor = 59.00;
        $ref = "DTQ/" . $dados["vaga"][0]->id;

        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");
        $paymentRequest->addItem($ref, $descricao, 1, $valor);
        $paymentRequest->setReference($ref);
        $paymentRequest->setRedirectUrl(base_url() . "planoController/finalizacao");

        try {
            $credentials = new PagSeguroAccountCredentials("igorfonseca88@yahoo.com.br", "7E17B268E14B49BE913C42FE077EBD70");
            $url = $paymentRequest->register($credentials);
            header("Location: $url");
        } catch (PagSeguroServiceException $exp) {
            //print_r($exp->getHttpStatus());
            $this->session->set_flashdata('error', $exp->getMessage() . "Não foi possível concluir a transação pois houve algum erro no credenciamento com a PagSeguro, contate o administrador.");
            redirect('area-restrita-empresa');
        }
    }

    function finalizacao()
    {
        if (isset($_GET["transaction_id"])) {

            $credentials = new PagSeguroAccountCredentials("igorfonseca88@yahoo.com.br", "7E17B268E14B49BE913C42FE077EBD70");
            $transaction_id = $_GET["transaction_id"];
            $transaction = PagSeguroTransactionSearchService::searchByCode(
                $credentials, $transaction_id
            );

            $status = $transaction->getStatus();

            if ($status->getTypeFromValue() === 'AVAILABLE') {
                $this->session->set_flashdata('sucesso', "A transação foi paga e chegou ao final de seu prazo de liberação.");
            } elseif ($status->getTypeFromValue() === 'WAITING_PAYMENT') {
                $this->session->set_flashdata('sucesso', "Estamos aguardando a comprovação de pagamento do PagSeguro. Quando o pagamento for liberado, você receberá um e-mail informando.");
            } elseif ($status->getTypeFromValue() === 'IN_ANALYSIS') {
                $this->session->set_flashdata('sucesso', "O PagSeguro está analisando a sua transação. Quando o pagamento for liberado, você receberá um e-mail informando.");
            } elseif ($status->getTypeFromValue() === 'PAID') {
                $this->session->set_flashdata('sucesso', "Pagamento efetuado com sucesso. Os detalhes do pagamento serão enviados para o seu e-mail!");
            } elseif ($status->getTypeFromValue() === 'IN_DISPUTE') {
                $this->session->set_flashdata('sucesso', "Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.");
            } elseif ($status->getTypeFromValue() === 'REFUNDED') {
                $this->session->set_flashdata('sucesso', "Devolvida: o valor da transação foi devolvido para o comprador.");
            } elseif ($status->getTypeFromValue() === 'CANCELLED') {
                $this->session->set_flashdata('error', "A transação não foi aprovada ou foi cancelada pelo PagSeguro, por favor tente novamente mais tarde ou entre em <a href='mailto:contato@gotalent.com.br'>contato</a> com o administrador.");
            }
        } else {
            $this->session->set_flashdata('error', "Nenhuma mensagem a exibir.");
        }

        //Redireciona
        if ($this->session->userdata("idEmpresa")) {
            redirect('area-restrita-empresa');
        } elseif ($this->session->userdata("idProfissional")) {
            redirect('area-restrita');
        }
    }

    public function notificacoes()
    {

        if (isset($_POST['notificationType']) && $_POST['notificationType'] == 'transaction') {

            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $_POST['notificationCode'] . '?email=igorfonseca88@yahoo.com.br&token=7E17B268E14B49BE913C42FE077EBD70';
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $transaction = curl_exec($curl);
            curl_close($curl);

            if ($transaction == 'Unauthorized') {
                echo "erro";
                exit;
            }

            //Busca dados do XML
            $transaction = simplexml_load_string($transaction);
            $ref = explode("/", $transaction->reference);
            $tipo = $ref[0];
            $idreference = $ref[1];
            $transactionstatus = $transaction->status;
            $transactionpagamento = $transaction->paymentMethod->type;

            //Configuracoes para envio de email				
            $this->load->model('AdminGotalent_model', 'admin_gotalent');
            $servidor = $this->admin_gotalent->getAll();
            $smtp = $servidor[0]->smtp;
            $usuariosmtp = $servidor[0]->usuario;
            $senha = $servidor[0]->senha;
            $porta = $servidor[0]->porta;

            //Profissional
            if ($tipo == "PRO") {

                //Busca profissional
                $this->load->model('Profissional_model', 'profissional');
                $profissional = $this->profissional->buscarPorId($idreference);

                //Busca plano temporario do profissional
                $this->load->model('Plano_model', 'planos');
                $plano = $this->planos->buscarPlanoProfissionalPorId($profissional[0]->idPlanoTemporario);
                $dias = $plano[0]->diasPlano;

                //Calcula novas datas inicial e final
                $dataInicioPlano = $profissional[0]->dataInicioPlano <> "" ? $profissional[0]->dataInicioPlano : date("Y-m-d");
                $dataFim = $profissional[0]->dataFinalPlano <> "" ? $profissional[0]->dataFinalPlano : date("Y-m-d");
                $dataFinalPlano = date('Y-m-d', strtotime("+$dias days", strtotime($dataFim)));

                //Busca usuario
                $this->load->model('Usuario_model', 'usuario');
                $usuario = $this->usuario->buscarUsuarioPorIdProfissional($profissional[0]->id);

                //Seleciona o status
                switch ($transactionstatus) {

                    case 1:
                        //PENDENTE
                        //Aguardando confirmacao de pagamento no PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $profissional[0]->nome . "</h2>";
                        $mensagem = $mensagem . "<p>Estamos aguardando confirmação de pagamento do seu cadastro por parte do PagSeguro, você receberá um e-mail assim que o mesmo for aprovado.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($profissional[0]->email);
                        $this->email->subject("Plano " . $plano[0]->plano . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();
                        break;

                    case 3:
                        //ATIVO
                        //Pagamento confirmado no PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $profissional[0]->nome . "</h2>";
                        $mensagem = $mensagem . "<p>O pagamento do seu cadastro foi <strong>aprovado</strong> pelo PagSeguro e todas as funcionalidades do plano foram liberadas para o seu perfil. Estamos felizes por fazer parte da Go Talent e desejamos boa sorte no mercado de trabalho!</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($profissional[0]->email);
                        $this->email->subject("Plano " . $plano[0]->plano . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();

                        //Atualiza plano do profissional
                        $update = array(
                            "idPlano" => $plano[0]->id,
                            "dataInicioPlano" => $dataInicioPlano,
                            "dataFinalPlano" => $dataFinalPlano,
                            "idPlanoTemporario" => null
                        );
                        $this->profissional->update($idreference, $update);

                        //Atualiza usuario
                        $update = array(
                            "situacao" => "ATIVO"
                        );
                        $this->usuario->update($usuario[0]->idUsuario, $update);

                        break;

                    case 7:
                        //CANCELADO
                        //Pagamento cancelado pelo PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $profissional[0]->nome . "!</h2>";
                        $mensagem = $mensagem . "<p>O pagamento pelo seu plano foi <strong>cancelado</strong> pelo PagSeguro, acesse sua área restrita do PagSeguro para mais detalhes e, no caso de duvidas, entre em contato conosco atraves do site.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($profissional[0]->email);
                        $this->email->subject("Plano " . $plano[0]->plano . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();
                        break;
                }

                //Pagamento
                switch ($transactionpagamento) {
                    case 1:
                        $pagamento = "Cartão de crédito";
                        break;

                    case 2:
                        $pagamento = "Boleto";
                        break;

                    case 3:
                        $pagamento = "Débito online (TEF)";
                        break;

                    case 4:
                        $pagamento = "Saldo PagSeguro";
                        break;

                    case 7:
                        $pagamento = "Depósito em conta";
                        break;
                }

                //Empresa
            } elseif ($tipo == "EMP") {

                //Busca extrato
                $this->load->model('Extrato_model', 'extrato');
                $extrato = $this->extrato->buscarPorId($idreference);

                //Busca empresa
                $this->load->model('Empresa_model', 'empresa');
                $empresa = $this->empresa->buscarPorId($extrato[0]->idEmpresa);

                //Busca usuario
                $this->load->model('Usuario_model', 'usuario');
                $usuario = $this->usuario->buscarUsuarioPorIdEmpresa($extrato[0]->idEmpresa);

                //Busca plano
                $this->load->model('Plano_model', 'plano');
                $plano = $this->plano->buscarPlanosEmpresasPorId($extrato[0]->idPlanoEmpresa);
                $diasVigencia = $plano[0]->diasVigencia;

                //Seleciona o status
                switch ($transactionstatus) {

                    case 1:
                        //PENDENTE
                        //Aguardando confirmacao de pagamento no PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $empresa[0]->razaosocial . "</h2>";
                        $mensagem = $mensagem . "<p>Estamos aguardando confirmação de pagamento do seu cadastro por parte do PagSeguro, você receberá um e-mail assim que o mesmo for provado.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($empresa[0]->email);
                        $this->email->subject("Plano " . $plano[0]->plano . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();

                        //Atualiza extrato
                        $update = array(
                            "dataConfirmacao" => date("Y-m-d h:m:s"),
                            "tipo" => "PENDENTE"
                        );
                        $this->extrato->update($idreference, $update);
                        break;

                    case 3:
                        //ATIVO
                        //Pagamento confirmado no PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $empresa[0]->razaosocial . "</h2>";
                        $mensagem = $mensagem . "<p>O pagamento do seu cadastro foi <strong>aprovado</strong> pelo PagSeguro e todas as funcionalidades do plano foram liberadas para o seu perfil. Estamos felizes por fazer parte da Go Talent e desejamos boa sorte no mercado de trabalho!</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($empresa[0]->email);
                        $this->email->subject("Plano " . $plano[0]->plano . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();

                        //Atualiza extrato
                        $update = array(
                            "vigencia" => date('Y-m-d', strtotime("+$diasVigencia days")),
                            "dataConfirmacao" => date("Y-m-d h:m:s"),
                            "tipo" => "ATIVO"
                        );
                        $this->extrato->update($idreference, $update);

                        //Atualiza usuario
                        $update = array(
                            "situacao" => "ATIVO"
                        );
                        $this->usuario->update($usuario[0]->idUsuario, $update);

                        break;

                    case 7:
                        //CANCELADO
                        //Pagamento cancelado pelo PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $empresa[0]->razaosocial . "!</h2>";
                        $mensagem = $mensagem . "<p>O pagamento pelo seu plano foi <strong>cancelado</strong> pelo PagSeguro, acesse sua área restrita do PagSeguro para mais detalhes e, no caso de duvidas, entre em contato conosco atraves do site.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($empresa[0]->email);
                        $this->email->subject("Plano " . $plano[0]->plano . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();

                        //Atualiza extrato
                        $update = array(
                            "dataConfirmacao" => date("Y-m-d h:m:s"),
                            "tipo" => "CANCELADO"
                        );
                        $this->extrato->update($idreference, $update);
                        break;
                }

                //Pagamento
                switch ($transactionpagamento) {
                    case 1:
                        $pagamento = "Cartão de crédito";
                        break;

                    case 2:
                        $pagamento = "Boleto";
                        break;

                    case 3:
                        $pagamento = "Débito online (TEF)";
                        break;

                    case 4:
                        $pagamento = "Saldo PagSeguro";
                        break;

                    case 7:
                        $pagamento = "Depósito em conta";
                        break;
                }

                //Destaque
            } elseif ($tipo == "DTQ") {

                //Busca vaga
                $this->load->model('Vaga_model', 'vaga');
                $vaga = $this->vaga->buscarPorId($idreference);

                //Busca empresa
                $this->load->model('Empresa_model', 'empresa');
                $empresa = $this->empresa->buscarPorId($vaga[0]->idEmpresa);

                //Seleciona o status
                switch ($transactionstatus) {

                    case 1:
                        //PENDENTE
                        //Aguardando confirmacao de pagamento no PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $empresa[0]->razaosocial . "</h2>";
                        $mensagem = $mensagem . "<p>Estamos aguardando confirmação de pagamento por parte do PagSeguro do destaque para a vaga " . $vaga[0]->vaga . ", você receberá um e-mail assim que o mesmo for provado.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($empresa[0]->email);
                        $this->email->subject("Destaque da vaga " . $vaga[0]->vaga . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();
                        break;

                    case 3:
                        //ATIVO
                        //Pagamento confirmado no PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $empresa[0]->razaosocial . "</h2>";
                        $mensagem = $mensagem . "<p>O pagamento do destaque para a vaga " . $vaga[0]->vaga . " foi aprovado e a partir deste momento esta vaga aparece como destaque no site.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($empresa[0]->email);
                        $this->email->subject("Destaque da vaga " . $vaga[0]->vaga . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();

                        //Atualiza extrato
                        $update = array(
                            "destaque" => "SIM"
                        );
                        $this->vaga->update($idreference, $update);

                        break;

                    case 7:
                        //CANCELADO
                        //Pagamento cancelado pelo PagSeguro

                        $mensagem = "&nbsp;";
                        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                        $mensagem = $mensagem . "<h2>Olá, " . $empresa[0]->razaosocial . "!</h2>";
                        $mensagem = $mensagem . "<p>O pagamento pelo destaque da vaga " . $vaga[0]->vaga . " foi <strong>cancelado</strong> pelo PagSeguro, acesse sua área restrita do PagSeguro para mais detalhes e, no caso de duvidas, entre em contato conosco atraves do site.</p>";
                        $mensagem = $mensagem . "<p>Atenciosamente,<br>";
                        $mensagem = $mensagem . "<b>Go Talent</b></p>";
                        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                        $this->load->library('email');
                        $config['smtp_host'] = $smtp;
                        $config['smtp_user'] = $usuariosmtp;
                        $config['smtp_pass'] = $senha;
                        $config['smtp_port'] = $porta;

                        $this->email->initialize($config);
                        $this->email->from($usuariosmtp, "Go Talent");
                        $this->email->to($empresa[0]->email);
                        $this->email->subject("Destaque da vaga " . $vaga[0]->vaga . " - Go Talent");
                        $this->email->message($mensagem);
                        $this->email->send();
                        break;
                }
            }
        }
    }





    /*Mercado Pago*/
    /*
     * Método que recebe todas as notificações de assinaturas do mercado pago.
     * - É preciso implementar o método de cancelamento.
     */
    public function  notifications()
    {
        $id = $this->input->get('id'); //Id da assinatura
        $topic = $this->input->get('topic'); //Tipo de assinatura

        if (isset($topic) && isset($id)) {
            /// Credenciais
            $mp = new MP("2372843291048079", "bD85KKctxK3qmu22TGOcviXPrOLw2Kk3");
            //$id = "1888346363";//Id teste
            $payment_info = $mp->get_payment_info($id);
            if (isset($payment_info)) {
                //tratando o status do pagamento para uma BR
                switch ($payment_info["response"]["collection"]["status"]) {

                    case "approved":
                        if ($this->session->userdata("idProfissional") != "" &&
                            $this->session->userdata("idProfissional") != null &&
                            $this->session->userdata("idProfissional") != 0 &&
                            $this->session->userdata("idProfissional") != "0"
                        ) {
                           $this->assinaturaProfissional($id, $topic);
                        } else if ($this->session->userdata("idEmpresa") != "" &&
                            $this->session->userdata("idEmpresa") != null &&
                            $this->session->userdata("idEmpresa") != 0 &&
                            $this->session->userdata("idEmpresa") != "0"
                        ) {
                            $this->assinaturaEmpresa($id, $topic);
                        }
                        break;
                    case "pending":
                        $status = "Aguardando Pagamento";    //O usuário não concluiu o processo de pagamento.
                        break;
                    case "in_process":
                        $status = "Aguardando Pagamento";    //O pagamento está sendo analisado.
                        break;
                    case "rejected"       :
                        $status = "Cancelado"; //O pagamento foi recusado. O usuário pode tentar novamente.
                        break;
                    case "refunded"    :
                        $status = "Devolvido";//(estado terminal)	O pagamento foi devolvido ao usuário.
                        break;
                    case "cancelled":
                        //validar se o usuário é profissional ou empresa
                        if ($this->session->userdata("idProfissional") != ""
                            && $this->session->userdata("idProfissional") != null
                        ) {

                            //Alterar os dados do profissional no banco de dados para free.
                            $this->load->model("Profissional_model", "profissional");
                            $idProfissional = $this->session->userdata("idProfissional");
                            $mp->cancel_payment($id);

                            $update = array(
                                "dataFinalPlano" => date(),
                                "idPlano" => 1
                            );

                            if ($this->profissional->update($id, $update) > 0) {
                                $data["profissional"] = $this->profissional->buscarPorId($id);
                                $msg = "Ocorreu um erro ao alterar o plano do profissional " . $data["profissional"][0]->nome . ", verifique por favor  !";
                                // contato@gotalent.com.br
                                $tipo = "profissional";
                                $this->enviarEmailProfissionalPremium("Suporte", "marcusbrunogm@gmail.com", $msg);
                            } else {

                                $msg = "Seu plano Premium, foi cancelado com sucesso !";
                                $data["profissional"] = $this->profissional->buscarPorId($id);
                                //Email de confirmaçao de cancelamento do plano
                                $this->enviarEmailProfissionalPremium($data["profissional"][0]->nome, $data["profissional"][0]->email, $msg);
                            }
                        } else {
                            if ($this->session->userdata("idEmpresa") != "" &&
                                $this->session->userdata("idEmpresa") != null
                            ) {

                            }
                        }
                        $status = "Cancelado";//(estado terminal)	O pagamento foi cancelado por superar o tempo necessário para ser efetuado ou por alguma das partes.
                        break;
                    case "in_mediation":
                        $status = "Disputa"; //	Foi iniciada uma disputa para o pagamento.
                        break;
                }
            }
        }
    }

    /*
     * Função que altera no banco de dados o plano do profissional e solicita o envio do email de confirmação ao profissional.
     */
    public function assinaturaProfissional($id, $topic)
    {

        $mp = new MP("2372843291048079", "bD85KKctxK3qmu22TGOcviXPrOLw2Kk3");
        $payment_info = $mp->get_payment_info($id);

        //Variaveis Locais
        $valor = null;
        $plano = null;

        //Alterar os dados do profissional no banco de dados para premium.
        $this->load->model("Profissional_model", "profissional");
        $idProfissional = $this->session->userdata("idProfissional");
        $data["profissional"] = $this->profissional->buscarPorId($idProfissional);

        if ($payment_info["response"]["collection"]["total_paid_amount"] == "0.5") {//Não esqucer de trocar o valor do if para 9.9;
            $valor = 9.90;
            $plano = "2";
        }
        //Atribuido valores.
        $update = array(
            "dataInicioPlano" => explode("T", $payment_info["response"]["collection"]["date_approved"])[0],
            "valorPago" => $valor,
            "idPlanoTemporario" => $plano
        );

        //Persistir dados
        if ($this->profissional->update($idProfissional, $update) != 0) {
            //Enviar um email de problemas na persistencia dos dados do profissional.
            $tipo = "profissional";
            $servico = "Plano Premium";
            $nome = $data["profissional"][0]->nome;
            $this->enviarEmailSuporte($nome, $tipo, $servico, "marcusbrunogm@gmail.com");//Email do suporte
        } else {
            //Enviar um email de assinatura
            $this->enviarEmailProfissionalPremium($data["profissional"][0]->nome, $data["profissional"][0]->email);

        }
    }

    /*
    * Função que valida qual é o plano do profissional para validar qual mensagem deve ser enviada ao mesmo.
    */
    public function envioMsgProfissional($idProfissional, $msg)
    {
        $this->load->model("Profissional_model", "profissional");
        $data["profissional"] = $this->profissional->buscarPorId($idProfissional);

        $this->load->model("Empresa_model", "empresa");
        $idEmpresa = $this->session->userdata("idEmpresa");
        $data["empresa"] = $this->empresa->buscarPorId($idEmpresa);

        if ($data["profissional"][0]->idPlano == 1) {
            $this->NotificacaoProfFree($data["profissional"][0], $data["empresa"][0]);
        } else {
            if ($data["profissional"][0]->idPlano != 1) {
                $this->MsgProfPremium($msg, $data["profissional"][0], $data["empresa"][0]);
            }
        }
    }


    /***Emails***/
    /*
     * Envia ao profissional um email de confirmaçao de assinatura
     */
    public function enviarEmailProfissionalPremium($nome, $email)
    {

        //Configuracoes para envio de email
        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();
        $smtp = $servidor[0]->smtp;
        $usuariosmtp = $servidor[0]->usuario;
        $senha = $servidor[0]->senha;
        $porta = $servidor[0]->porta;

        $mensagem = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
<meta content='width=device-width, initial-scale=1.0' name='viewport' />
<title></title>
<style type='text/css'>/* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}


         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}


         }
</style>
<center>
<div style='z-index:9998;position: absolute;top:0;bottom: 0;left: 50%;width: 700px; margin-left:-350px;'>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td width='100%'>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody><!-- Spacing --><!-- Spacing -->
								<tr>
									<td height='10' style='text-align: right;' width='100%'>
									<p>&nbsp; &nbsp;</p>
									</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>
                            <p><img alt='' height='77' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/64/original_logogo.png?1457386442' width='240' /></p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center; font-size: 16px;'><span style='font-size: 28px;'>Assinatura Profissional Premium</span></p>
<br/>
			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 40px; text-align: center;'><span style='font-size: 48px;'><strong>PROFISSIONAL PREMIUM
TEM MAIS VANTAGENS</strong></span></p>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td width='100%'>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody><!-- content -->
											<tr>
												<td style='font-family: Helvetica, arial, sans-serif; color: rgb(102, 102, 102); text-align: center; line-height: 30px;' colspan='2'>
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><span style='font-size: 24px;'><b>" . mb_strtoupper($nome, 'UTF-8') . "</b></span></p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'>&nbsp;</p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:justify;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>
												Sua assinatura do plano Premium foi aprovada. Conheça agora algumas das vantagens que você passa a ter acesso:</p>
												<p>&nbsp;</p>

												</td>
												<td></td>
											</tr>
											<tr>
												<td>
													<img src='http://www.gotalent.com.br/_imagens/email_1.png'/>
												</td>
												<td>
                                                    <p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: justify;'>Agora você tem acesso imediato às novas vagas, não precisa mais aguardar 48 horas para se candidatar, você na frente sempre!</p>
												</td>
											</tr>
											<tr>
												<td>
													<br/>
													<img src='http://www.gotalent.com.br/_imagens/email_2.png' style='padding-right:5px;'/>
												</td>
												<td>
                                                    <p>&nbsp;</p>
                                                    <p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: justify;'>Seu currículo em destaque! Empresas visualizam seu perfil com maior facilidade e podem entrar em contato diretamente por meio da nossa plataforma.</p>
												</td>
											</tr>
											<tr>
												<td style='font-family: Helvetica, arial, sans-serif; color: rgb(102, 102, 102); text-align: center; line-height: 30px;' colspan='2'>
                                                    <p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'>&nbsp;</p>
                                                    <p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: justify;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: justify;'>
                                                    Estamos felizes por fazer parte da GoTalent utilizando tudo que temos de melhor, desejamos boa sorte no mercado de trabalho e ficamos à disposição para auxilia-lo.</p>
												</td>
												<td></td>
											</tr>
											<!-- End of content -->
										</tbody>
									</table>
									</td>
								</tr>
								<!-- Spacing -->
								<tr>
									<td height='20' style='font-size:1px; line-height:1px; mso-line-height-rule: exactly;'>&nbsp;</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
								</tr>
								<tr>
									<td align='center' height='30' style='font-size:1px; line-height:1px;'>
									<p style='text-align: left;'>&nbsp;fdsfs</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' style='line-height: 1.6em;' width='100%'>
							<tbody>
								<tr>
									<td>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody>
											<tr>
												<td align='center' height='30' style='font-size:1px; line-height:1px;'><img alt='' class='banner' height='203' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/67/original_edinor.png?1457461288' style='display: block; border-width: 0px; border-style: solid;' width='594' /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div></center>
";

        $this->load->library('email');
        $config['smtp_host'] = $smtp;
        $config['smtp_user'] = $usuariosmtp;
        $config['smtp_pass'] = $senha;
        $config['smtp_port'] = $porta;

        $this->email->initialize($config);
        $this->email->from($usuariosmtp, "Go Talent");
        $this->email->to($email);//email
        $this->email->subject("Confirmação de Pagamento - Go Talent");
        $this->email->message($mensagem);
        $this->email->send();
    }

    /*
     *  Email enviado ao  suporte caso haja algum problema a persistencia de dados.
     */
    public function enviarEmailSuporte($nome, $tipo, $servico, $email)
    {
        $mensagem = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
<meta content='width=device-width, initial-scale=1.0' name='viewport' />
<title></title>
<style type='text/css'>/* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}


         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: default;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}


         }
</style>
<center>
<div style='z-index:9998;position: absolute;top:0;bottom: 0;left: 50%;width: 700px; margin-left:-350px;'>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td width='100%'>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody><!-- Spacing --><!-- Spacing -->
								<tr>
									<td height='10' style='text-align: right;' width='100%'>
									<p>&nbsp; &nbsp;</p>
									</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
                <table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
                    <tbody>
                        <tr>
                            <td align='center' height='30' style='font-size:1px; line-height:1px;'>
                                <p><img alt='' height='77' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/64/original_logogo.png?1457386442' width='240' /></p>
                                <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                                <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
		    </td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td width='100%'>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody><!-- content -->
											<tr>
												<td style='font-family: Helvetica, arial, sans-serif; color: rgb(102, 102, 102); text-align: center; line-height: 30px;' >
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><span style='font-size: 24px;'><b>Atenção Suporte</b></span></p>
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'>&nbsp;</p>
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:justify;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>
												Olá Suporte, houve um problema na alteração dos dados do(a) " . $tipo . " " . $nome . ", o(a) mesmo(a) contratou o " . $servico . ", contudo não foi possivel realizar a persistência dos dados deste usuário. Por favor verifique este problema.</p>
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:justify;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>Obrigado!</p>
												</td>
											</tr>
											<!-- End of content -->
										</tbody>
									</table>
									</td>
								</tr>
								<!-- Spacing -->
								<tr>
									<td height='20' style='font-size:1px; line-height:1px; mso-line-height-rule: exactly;'>&nbsp;</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
								</tr>
								<tr>
									<td align='center' height='30' style='font-size:1px; line-height:1px;'>
									<p style='text-align: left;'>&nbsp;fdsfs</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' style='line-height: 1.6em;' width='100%'>
							<tbody>
								<tr>
									<td>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody>
											<tr>
												<td align='center' height='30' style='font-size:1px; line-height:1px;'><img alt='' class='banner' height='203' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/67/original_edinor.png?1457461288' style='display: block; border-width: 0px; border-style: solid;' width='594' /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
</center>
";


        //Configuracoes para envio de email
        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();
        $smtp = $servidor[0]->smtp;
        $usuariosmtp = $servidor[0]->usuario;
        $senha = $servidor[0]->senha;
        $porta = $servidor[0]->porta;

        $this->load->library('email');
        $config['smtp_host'] = $smtp;
        $config['smtp_user'] = $usuariosmtp;
        $config['smtp_pass'] = $senha;
        $config['smtp_port'] = $porta;

        $this->email->initialize($config);
        $this->email->from($usuariosmtp, "Go Talent");
        $this->email->to("marcusbrunogm@gmail.com");//email
        $this->email->subject("Ajuda Suporte - Go Talent");
        $this->email->message($mensagem);
        $this->email->send();
    }

    /*
     * Envia um email contendo a mensagem que a empresa escrveu ao profissional.
     */
    public function MsgProfPremium($msg, $profissional, $empresa)
    {
        $mensagem = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
<meta content='width=device-width, initial-scale=1.0' name='viewport' />
<title></title>
<style type='text/css'>/* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}


         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}


         }
</style>
<center>
<div style='z-index:9998;position: absolute;top:0;bottom: 0;left: 50%;width: 700px; margin-left:-350px;'>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td width='100%'>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody><!-- Spacing --><!-- Spacing -->
								<tr>
									<td height='10' style='text-align: right;' width='100%'>
									<p>&nbsp; &nbsp;</p>
									</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>
                            <p><img alt='' height='77' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/64/original_logogo.png?1457386442' width='240' /></p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center; font-size: 16px;'><span style='font-size: 28px;'>Nova Mensagem</span></p>
<br/>
			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 50px; text-align: center;'><span style='font-size: 48px;'><strong>A $empresa->razaosocial ESTÁ INTERESSADA NO SEU CURRÍCULO</strong></span></p>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td width='100%'>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody><!-- content -->
											<tr>
												<td style='font-family: Helvetica, arial, sans-serif; color: rgb(102, 102, 102); text-align: center; line-height: 30px;>
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><span style='font-size: 24px;'><b></b></span></p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'>&nbsp;</p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>Olá $profissional->nome a $empresa->razaosocial enviou-lhe a seguinte mensagem: </p>
												<p>&nbsp;</p>
<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>$msg</p>
<p>&nbsp;</p>
<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>Qualquer dúvida estamos a sua disposição.<br/> Atenciosamente.</p>
												</td>
											</tr>
											<!-- End of content -->
										</tbody>
									</table>
									</td>
								</tr>
								<!-- Spacing -->
								<tr>
									<td height='20' style='font-size:1px; line-height:1px; mso-line-height-rule: exactly;'>&nbsp;</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
								</tr>
								<tr>
									<td align='center' height='30' style='font-size:1px; line-height:1px;'>
									<p style='text-align: left;'>&nbsp;</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' style='line-height: 1.6em;' width='100%'>
							<tbody>
								<tr>
									<td>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody>
											<tr>
												<td align='center' height='30' style='font-size:1px; line-height:1px;'><img alt='' class='banner' height='203' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/67/original_edinor.png?1457461288' style='display: block; border-width: 0px; border-style: solid;' width='594' /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
</center>
";

        //Configuracoes para envio de email
        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');
        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        //Construção do Email
        $this->email->initialize($config);
        $this->email->from($servidor[0]->usuario, "Go Talent");
        $this->email->to($profissional->email);//email
        $this->email->subject("Confirmação de Pagamento - Go Talent");
        $this->email->message($mensagem);
        $this->email->reply_to($empresa->email);//Define o email que irá receber a resposta do email enviado.
        $this->email->send();
    }

    /*
     * Envia um email ao profissional notificando que uma empresa lhe enviou
     * uma mensagem e que para ele visualizar ele terá que assinar o plano premium
     */
    public function NotificacaoProfFree($profissional, $empresa)
    {
        $mensagem = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
<meta content='width=device-width, initial-scale=1.0' name='viewport' />
<title></title>
<style type='text/css'>/* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}


         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}


         }
</style>
<center>
<div style='z-index:9998;position: absolute;top:0;bottom: 0;left: 50%;width: 700px; margin-left:-350px;'>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td width='100%'>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody><!-- Spacing --><!-- Spacing -->
								<tr>
									<td height='10' style='text-align: right;' width='100%'>
									<p>&nbsp; &nbsp;</p>
									</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>
                            <p><img alt='' height='77' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/64/original_logogo.png?1457386442' width='240' /></p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center; font-size: 16px;'><span style='font-size: 28px;'>Nova Possui Uma Mensagem</span></p>
<br/>
			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 50px; text-align: center;'><span style='font-size: 48px;'><strong>UMA EMPRSA INTERESSADA NO SEU CURRÍCULO!</strong></span></p>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td width='100%'>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody><!-- content -->
											<tr>
												<td style='font-family: Helvetica, arial, sans-serif; color: rgb(102, 102, 102); text-align: center; line-height: 30px;>
												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><span style='font-size: 24px;'><b></b></span></p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'>&nbsp;</p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>" . ucwords(mb_strtolower(explode(" ", $profissional->nome)[0])) . " a $empresa->razaosocial está interessada em seu currículo e enviou-lhe uma mensagem. Mas apenas profissionais premiums podem visualizar suas mensagens. Cl </p>
												<p>&nbsp;</p>
<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>$msg</p>
<p>&nbsp;</p>
<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align:center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: center;'>Qualquer dúvida estamos a sua disposição.<br/> Atenciosamente.</p>
												</td>
											</tr>
											<!-- End of content -->
										</tbody>
									</table>
									</td>
								</tr>
								<!-- Spacing -->
								<tr>
									<td height='20' style='font-size:1px; line-height:1px; mso-line-height-rule: exactly;'>&nbsp;</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
            <table bgcolor='#f7f7f7' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<div class='innerbg'>&nbsp;</div>

						<table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td width='100%'>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<thead>
											<tr>
												<th height='20' scope='col'>&nbsp;</th>
											</tr>
										</thead>
										<tbody><!-- Spacing --><!-- Spacing -->
											<tr>
												<td>
												<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidthinner' width='560'>
													<tbody>
														<tr>
															<td>
															<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidthinner' width='270'>
																<tbody><!-- button -->
																	<tr>
																		<td>
																		<table align='center' bgcolor='#d87b00' border='0' cellpadding='0' cellspacing='0' class='tablet-button' height='60' style='border-radius:3px;' valign='middle' width='270'>
																			<tbody>
																				<tr>
																					<td align='center' height='15' style='font-size:1px; line-height:1px;'>&nbsp;</td>
																				</tr>
																				<tr>
																					<td align='center' class='tablet-button' height='30' style='font-family: Helvetica, arial, sans-serif;  font-size: 24px;color: #ffffff; text-align:center;line-height: 30px;' valign='middle'>
																					<p><a href='http://www.gotalent.com.br/seja-premium' style='color: #ffffff; text-align:center;text-decoration: none;'>QUERO SER PREMIUM</a></p>
																					</td>
																				</tr>
																				<tr>
																					<td align='center' height='15' style='font-size:1px; line-height:1px;'>&nbsp;</td>
																				</tr>
																			</tbody>
																		</table>
																		</td>
																	</tr>
																	<!-- end of button -->
																</tbody>
															</table>
															<!-- end of right column --></td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<!-- Spacing -->
											<tr>
												<td height='20'>&nbsp;</td>
											</tr>
											<!-- Spacing -->
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
								</tr>
								<tr>
									<td align='center' height='30' style='font-size:1px; line-height:1px;'>
									<p style='text-align: left;'>&nbsp;</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' style='line-height: 1.6em;' width='100%'>
							<tbody>
								<tr>
									<td>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody>
											<tr>
												<td align='center' height='30' style='font-size:1px; line-height:1px;'><img alt='' class='banner' height='203' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/67/original_edinor.png?1457461288' style='display: block; border-width: 0px; border-style: solid;' width='594' /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
</center>
";
        //Configuracoes para envio de email
        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');
        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        //Construção do Email
        $this->email->initialize($config);
        $this->email->from($servidor[0]->usuario, "Go Talent");
        $this->email->to($profissional->email);//email
        $this->email->subject("Confirmação de Pagamento - Go Talent");
        $this->email->message($mensagem);
        $this->email->send();
    }

}
?>