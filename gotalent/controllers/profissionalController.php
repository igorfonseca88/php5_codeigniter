<?php

class ProfissionalController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model("Profissional_model", "profissional");
        $id = $this->session->userdata("idProfissional");

        if ($id == "" || $id == 0) {
            redirect('login/login');
        }
        $profissional = $this->profissional->buscarPorId($id);
    }

    function index()
    {
        redirect('profissionalController/editarProfissionalAction/');
    }

    function novoProfissionalAction()
    {
        $this->load->view("priv/profissional/addProfissional");
    }


    function editProfissional()
    {
        $this->load->model("Profissional_model", "profissional");
        $id = $this->session->userdata("idProfissional");

        $msgErro = "";
        if (trim($this->input->post("nome")) == "") {
            $msgErro .= "O campo Nome é obrigatório.<br>";
        }

        if ($this->input->post("sexo") == "") {
            $msgErro .= "O campo Sexo é obrigatório.<br>";
        }
        if ($this->input->post("telefone") == "") {
            $msgErro .= "O campo Telefone é obrigatório.<br>";
        }
        if ($this->input->post("estado") == "") {
            $msgErro .= "O campo Estado é obrigatório.<br>";
        }
        if ($this->input->post("cidade") == "") {
            $msgErro .= "O campo Cidade é obrigatório.<br>";
        }
        if ($this->input->post("pretensao") == "") {
            $msgErro .= "O campo Pretensão é obrigatório.<br>";
        }
        if ($this->input->post("resumo") == "") {
            $msgErro .= "O campo Resumo é obrigatório.";
        }
        if ($msgErro != "") {
            $this->session->set_flashdata('error', $msgErro);
            redirect('profissionalController/editarProfissionalAction/');
        }
        $nivelCarreira = implode(",", $this->input->post("nivelCarreira"));
        $update = array(
            "nome" => $this->input->post("nome"),
            "nascimento" => implode("-", array_reverse(explode("/", $this->input->post("nascimento")))),
            "telefone" => $this->input->post("telefone"),
            "cep" => $this->input->post("cep"),
            "endereco" => $this->input->post("endereco"),
            "numero" => $this->input->post("numero"),
            "bairro" => $this->input->post("bairro"),
            "cidade" => $this->input->post("cidade"),
            "estado" => $this->input->post("estado"),
            "facebook" => $this->input->post("facebook"),
            "linkedin" => $this->input->post("linkedin"),
            "observacao" => $this->input->post("observacao"),
            "sexo" => $this->input->post("sexo"),
            "resumo" => $this->input->post("resumo"),
            "repositorio" => $this->input->post("repositorio"),
            "pretensao" => str_replace(",", ".", str_replace(".", "", $this->input->post("pretensao"))),
            "nivelCarreira" => $nivelCarreira,
            "idArea" => "2" //Area TI
        );

        if ($this->profissional->update($id, $update) > 0) {
            $this->session->set_flashdata('sucesso', 'Salvo com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao salvar.');
        }


        redirect('profissionalController/editarProfissionalAction/');
    }

    function experiencia($idProfissional)
    {
        $this->load->model("Profissional_model", "profissional");

        $insert = array(
            "empresa" => $this->input->post("empresa"),
            "cargo" => $this->input->post("cargo"),
            "dataInicio" => implode("-", array_reverse(explode("/", $this->input->post("dataInicio")))),
            "dataFim" => implode("-", array_reverse(explode("/", $this->input->post("dataFim")))),
            "empregoAtual" => $this->input->post("empregoAtual"),
            "descricaoAtividade" => $this->input->post("descricaoAtividades"),
            "idProfissional" => $idProfissional
        );

        if ($this->profissional->add_experiencia($insert) > 0) {
            $this->session->set_flashdata('sucesso', 'Sua experiência foi cadastrada com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao cadastrar sua experiência, tente novamente.');
        }

        redirect('profissionalController/editarProfissionalAction/');
    }

    function editExperiencia()
    {
        $this->load->model("Profissional_model", "profissional");
        $idExperiencia = $this->input->post("idExp");
        $update = array(
            "empresa" => $this->input->post("empresa"),
            "cargo" => $this->input->post("cargo"),
            "dataInicio" => implode("-", array_reverse(explode("/", $this->input->post("dataInicio")))),
            "dataFim" => implode("-", array_reverse(explode("/", $this->input->post("dataFim")))),
            "empregoAtual" => $this->input->post("empregoAtual"),
            "descricaoAtividade" => $this->input->post("descricaoAtividades"),
            "idProfissional" => $this->session->userdata("idProfissional")
        );

        if ($this->profissional->update_experiencia($idExperiencia, $update) > 0) {
            $this->session->set_flashdata('sucesso', 'Sua experiência foi salva com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao salvar sua experiência, tente novamente.');
        }

        redirect('profissionalController/editarProfissionalAction/');
    }

    function formacao($idProfissional)
    {
        $this->load->model("Profissional_model", "profissional");

        $insert = array(
            "instituicao" => $this->input->post("instituicao"),
            "formacao" => $this->input->post("formacao"),
            "dataInicio" => implode("-", array_reverse(explode("/", $this->input->post("dataInicioFormacao")))),
            "dataFim" => implode("-", array_reverse(explode("/", $this->input->post("dataFimFormacao")))),
            "curso" => $this->input->post("curso"),
            "idProfissional" => $idProfissional
        );

        if ($this->profissional->add_formacao($insert) > 0) {
            $this->session->set_flashdata('sucesso', 'Sua formação educacional foi cadastrada com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao cadastrar sua formação educacional, tente novamente.');
        }

        redirect('profissionalController/editarProfissionalAction/');
    }

    function editFormacao()
    {
        $this->load->model("Profissional_model", "profissional");
        $idFormacao = $this->input->post("idForm");
        $update = array(
            "instituicao" => $this->input->post("instituicao"),
            "formacao" => $this->input->post("formacao"),
            "dataInicio" => implode("-", array_reverse(explode("/", $this->input->post("dataInicioFormacao")))),
            "dataFim" => implode("-", array_reverse(explode("/", $this->input->post("dataFimFormacao")))),
            "curso" => $this->input->post("curso"),
            "idProfissional" => $this->session->userdata("idProfissional")
        );

        if ($this->profissional->update_formacao($idFormacao, $update) > 0) {
            $this->session->set_flashdata('sucesso', 'Sua formação foi salva com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao salvar sua formaçao, tente novamente.');
        }

        redirect('profissionalController/editarProfissionalAction/');
    }

    function addSkillProfissional()
    {


        $this->load->model("Skills_model", "skill");
        $this->load->model("Profissional_model", "profissional");


        $skills = $this->input->post("lista");

        foreach ($skills as $sk) {
            $insertSkill = array(
                "idProfissional" => $this->session->userdata("idProfissional"),
                "idSkill" => $sk
            );
            $this->profissional->add_skill($insertSkill);
        }

        $this->session->set_flashdata('sucesso', 'Skill(s) inserida(s) com sucesso.');

        redirect("profissionalController/editarProfissionalAction/");
    }

    function excluirSkillProfissional($id, $idProfissional)
    {
        $this->load->model("Profissional_model", "vaga");

        if ($this->vaga->deleteProfissionalSkillPorId($id) > 0) {
            $this->session->set_flashdata('sucesso', 'Skill removida com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao remover Skill. Tente novamente e se o erro persistir entre em contato com o nosso suporte.');
        }

        redirect("profissionalController/editarProfissionalAction/");
    }

    /* function buscarSkills(){

      $q = strtolower($_GET["q"]);
      if (!$q) return;

      $this->load->model("Skills_model", "skill");

      $data = $this->skill->buscarSkills($q);

      foreach ($data as $d) {
      $cname = $d->skill;
      echo "$cname\n";
      }
      } */

    function editarProfissionalAction($id, $mensagem = array())
    {

        $this->load->model('Profissional_model', 'profissional');
        $id = $this->session->userdata("idProfissional");

        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
        $data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
        $data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional($id);

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        if ($data["profissional"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($data["profissional"][0]->estado);
        }
        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->buscarTodasSkillsDisponiveisPorIdProfissional($id);

        $data["porcentagemPreenchida"] = $this->curriculoCompleto($data["profissional"][0], $data["experiencias"], $data["formacoes"], $data["profissional_skills"]);

        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();

        if ($data["profissional"][0]->idArea != null) {
            $_SESSION["searchAreaInteresse"] = $data["profissional"][0]->idArea;
            $_SESSION["searchAreaInteresseArray"] = explode(",", $_SESSION["searchAreaInteresse"]);
        }

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/profissional/editProfissional");
    }

    function editarExperiencia($idExperiencia)
    {
        $this->load->model('Profissional_model', 'profissional');
        $id = $this->session->userdata("idProfissional");

        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
        $data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
        $data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional($id);

        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->buscarTodasSkillsDisponiveisPorIdProfissional($id);

        $data["porcentagemPreenchida"] = $this->curriculoCompleto($data["profissional"][0], $data["experiencias"], $data["formacoes"], $data["profissional_skills"]);

        $data["experiencia"] = $this->profissional->buscarExperienciaProfissionalPorId($idExperiencia);
        $data["editExperiencia"] = "Sim";
        $this->load->vars($data);
        $this->load->view("priv/profissional/editProfissional");
    }

    function editarFormacao($idFormacao)
    {
        $this->load->model('Profissional_model', 'profissional');
        $id = $this->session->userdata("idProfissional");

        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
        $data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
        $data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional($id);

        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->buscarTodasSkillsDisponiveisPorIdProfissional($id);

        $data["porcentagemPreenchida"] = $this->curriculoCompleto($data["profissional"][0], $data["experiencias"], $data["formacoes"], $data["profissional_skills"]);

        $data["formacao"] = $this->profissional->buscarFormacaoProfissionalPorId($idFormacao);
        $data["editFormacao"] = "Sim";
        $this->load->vars($data);
        $this->load->view("priv/profissional/editProfissional");
    }

    public function curriculoCompleto($profissional, $experiencias, $formacoes, $skills)
    {
        $curriculoPreenchido = 0;
        if ($profissional->nome != "") {
            $curriculoPreenchido = 3;
        }
        if ($profissional->email != "") {
            $curriculoPreenchido += 3;
        }
        if ($profissional->telefone != "") {
            $curriculoPreenchido += 3;
        }
        if ($profissional->estado != "") {
            $curriculoPreenchido += 3;
        }
        if ($profissional->cidade != "") {
            $curriculoPreenchido += 2;
        }
        if ($profissional->pretensao != "") {
            $curriculoPreenchido += 2;
        }
        if ($profissional->sexo != "") {
            $curriculoPreenchido += 2;
        }
        if ($profissional->resumo != "") {
            $curriculoPreenchido += 2;
        }
        if ($profissional->foto != "") {
            $curriculoPreenchido += 20;
        }
        if (count($experiencias) > 0) {
            $curriculoPreenchido += 20;
        }
        if (count($formacoes) > 0) {
            $curriculoPreenchido += 20;
        }
        if (count($skills) > 0) {
            $curriculoPreenchido += 20;
        }
        return $curriculoPreenchido;
    }

    function uploadArquivo($id)
    {

        $tipoAnexo = $this->input->post("tipoAnexo");
        //parametriza as preferências
        $config["upload_path"] = "./upload/profissional/";
        $config["allowed_types"] = "gif|jpg|png";
        $numeroRand = rand(00, 9999);
        $config["file_name"] = $tipoAnexo . $id . "_" . $numeroRand;
        $config["overwrite"] = TRUE;
        $config["remove_spaces"] = TRUE;
        $config['max_size'] = 1024;
        $this->load->library("upload", $config);


        //em caso de sucesso no upload
        if ($this->upload->do_upload()) {

            $this->load->model('Profissional_model', 'profissional');

            if ($tipoAnexo == "Curriculo") {
                $update = array(
                    "curriculo" => $this->upload->file_name
                );
            } else if ($tipoAnexo == "Foto") {
                $update = array(
                    "foto" => $this->upload->file_name
                );
            }

            $res = $this->profissional->update($id, $update);


            if ($res > 0) {
                //$foto["sucesso"] = "Arquivo cadastrado com sucesso";
                //$this->editarProfissionalAction($id, $foto);
                $this->session->set_flashdata('sucesso', 'Foto salva com sucesso.');

                redirect("profissionalController/editarProfissionalAction/" . $id);
            } else {
                //$foto["error"] = "Erro ao salvar arquivo no banco de dados.";
                //$this->editarProfissionalAction($id, $foto);
                $this->session->set_flashdata('error', 'Erro ao salvar foto no banco de dados. Tente novamente e se o erro persistir entre em contato com o nosso suporte.');

                redirect("profissionalController/editarProfissionalAction/" . $id);
            }
        } else {

            // $foto["error"] = "Erro ao fazer upload do arquivo.";
            //$this->editarProfissionalAction($id, $foto);
            $this->session->set_flashdata('error', 'Erro ao fazer upload do foto. Tente novamente e se o erro persistir entre em contato com o nosso suporte.');

            redirect("profissionalController/editarProfissionalAction/" . $id);
        }
    }

    function excluirExperiencia($id)
    {
        $this->load->model('Profissional_model', 'profissional');

        if ($this->profissional->deleteExperiencia($id) > 0) {

            $this->session->set_flashdata('sucesso', 'Experiência removida com sucesso.');

            redirect('profissionalController/editarProfissionalAction/');
        } else {

            $this->session->set_flashdata('error', 'Erro ao remover experiência.');
            redirect('profissionalController/editarProfissionalAction/');
        }
    }

    function excluirFormacao($id)
    {
        $this->load->model('Profissional_model', 'profissional');

        if ($this->profissional->deleteFormacao($id) > 0) {

            $this->session->set_flashdata('sucesso', 'Formação educacional removida com sucesso.');

            redirect('profissionalController/editarProfissionalAction/');
        } else {

            $this->session->set_flashdata('error', 'Erro ao remover formação educacional.');
            redirect('profissionalController/editarProfissionalAction/');
        }
    }

    function meusProcessosSeletivos()
    {
        $this->load->model("Profissional_model", "profissional");
        $data["vagas"] = $this->profissional->buscarTodasVagasPorIdProfissional($this->session->userdata("idProfissional"));
        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));
        $this->load->vars($data);
        $this->load->view("priv/profissional/meusProcessosSeletivos");
    }

    function editarUsuarioAction($id, $mensagem = array())
    {
        $this->load->model('Usuario_model', 'usuario');
        $this->load->model("Profissional_model", "profissional");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));
        $id = $this->session->userdata("idProfissional");
        $data["usuario"] = $this->usuario->buscarUsuarioPorIdProfissional($id);

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/profissional/editUsuario");
    }

    function editUsuario()
    {
        $this->load->model("Usuario_model", "usuario");
        //$id = $this->input->post("id");

        $user = $this->usuario->buscarUsuarioPorIdProfissional($this->session->userdata("idProfissional"));
        $id = $user[0]->idUsuario;
        $update = array(
            "senha" => $this->input->post("senha")
        );

        if ($this->usuario->update($id, $update) > 0) {
            $data["sucesso"] = "Salvo com sucesso.";
        } else {
            $data["erro"] = "Erro ao salvar.";
        }

        $this->editarUsuarioAction($id, $data);
    }

    public function detalhesVaga($idVaga)
    {

        if ($idVaga == "" || $idVaga == null || $idVaga == 0) {
            redirect('area-restrita');
        }

        $this->load->model("Profissional_model", "profissional");
        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

        $data["candidaturas"] = $this->profissional->buscarTodasCandidaturasPorIdProfissional($this->session->userdata("idProfissional"));
        $this->load->model("Vaga_model", "vaga");

        $data["vaga"] = $this->vaga->buscarVagaPorId($idVaga);

        if ($data["vaga"][0]->id == "" || $data["vaga"][0]->id == 0 || $data["vaga"][0]->id == null) {
            redirect('area-restrita');
        }

        $update = array(
            "visualizacao" => $data["vaga"][0]->visualizacao + 1
        );

        $this->vaga->update($idVaga, $update);

        //$data["beneficios"] = $this->vaga->buscarTodosBeneficios();
        $data["beneficios_vaga"] = $this->vaga->buscarTodosBeneficiosPorIdVaga($idVaga);

        $data["vaga_skills"] = $this->vaga->buscarTodasSkillsPorIdVaga($idVaga);
        $this->load->vars($data);
        $this->load->view('priv/profissional/detalhes-vaga');
    }

    public function buscar()
    {

        if ($this->session->userdata("idProfissional") == "" || $this->session->userdata("idProfissional") == 0) {
            redirect("login/login");
        }


        $this->load->model("Profissional_model", "profissional");
        $this->load->model("Vaga_model", "vaga");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

        $profissional_skills = $this->profissional->buscarTodasSkillsPorIdProfissional($this->session->userdata("idProfissional"));

        $data["candidaturas"] = $this->profissional->buscarTodasCandidaturasPorIdProfissional($this->session->userdata("idProfissional"));

        $searchVaga = $_POST["searchVaga"];
        $searchEstado = $_POST["searchEstado"];

        if ($searchVaga == "") {
            $_SESSION["searchVaga"] = null;
        } else {
            $_SESSION["searchVaga"] = $searchVaga;
        }


        $data["vagas"] = $this->vaga->buscarVagasPorFiltros($_SESSION["searchVaga"], null, $searchEstado, null);

        $data["estados"] = $this->vaga->buscarCidadesDasVagasAtivas();

        $data["totais"] = $this->profissional->buscarTotaisDashboard($this->session->userdata("idProfissional"));

        $data["limite"] = $this->profissional->verificarLimiteBotao($this->session->userdata("idProfissional"));

        $this->load->model("Plano_model", "planos");
        $data["planos_profissional"] = $this->planos->buscarPlanosPremiumProfissionais();

        foreach ($data["vagas"] as $vaga) {

            $compatibilidade = 0;
            $compatibilidade = $this->vaga->retornaCompatibilidadeProfissional($this->session->userdata("idProfissional"), $vaga->id);
            $compatibilidade_vaga[$vaga->id] = array("idVaga" => $vaga->id, "compatibilidade" => $compatibilidade[0]->porcentagem);
        }

        $data["compatibilidade_vaga"] = $compatibilidade_vaga;


        $this->load->vars($data);
        $this->load->view('priv/default-profissional');
    }

    function qualificacao()
    {
        $this->load->model("Profissional_model", "profissional");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));
        $this->load->vars($data);
        $this->load->view("priv/profissional/qualificacao");
    }

    function eventos()
    {
        $this->load->model("Profissional_model", "profissional");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));
        $this->load->vars($data);
        $this->load->view("priv/profissional/eventos");
    }

    function checklist($idVaga, $idChecklist)
    {
        $this->load->model("Profissional_model", "profissional");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

        $this->load->model('Checklist_model', 'checklist');
        $data["checklist"] = $this->checklist->buscarChecklistPorId($idChecklist);

        $this->load->model('Vaga_model', 'vaga');
        $data["vaga"] = $this->vaga->buscarVagaPorId($idVaga);

        if (count($data["checklist"]) == 0) {
            redirect("area-restrita");
        }

        $data["questoes_objetivas"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($data["checklist"][0]->id, "Objetiva");
        $data["questoes_discursivas"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($data["checklist"][0]->id, "Discursiva");

        $this->load->vars($data);
        $this->load->view("priv/profissional/checklist");
    }

    function salvarChecklistAction()
    {
        $this->load->model("Profissional_model", "profissional");
        $this->load->model("Checklist_model", "checklist");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

        $idChecklist = $this->input->post("ck");
        $idVaga = $this->input->post("v");

        // busca se o profissional já respondeu o checklist
        $resposta = $this->checklist->buscarRespostaChecklistPorIdProfissionalEIdChecklist($this->session->userdata("idProfissional"), $idChecklist);

        if (count($resposta) > 0) {
            $this->session->set_flashdata('error', 'O checklist já foi respondido anteriormente.');
            redirect('profissionalController/checklist/' . $idVaga . '/' . $idChecklist);
        }

        // salva as objetivas
        for ($i = 1; $i < 6; $i++) {
            $hcomponente = $this->input->post("hcomponente$i");
            $componente = $this->input->post("componente$i");

            if ($hcomponente != "" && $componente != "") {
                $insert = array(
                    "respostaObjetiva" => $componente,
                    "idChecklistItem" => $hcomponente,
                    "idVaga" => $idVaga,
                    "dataCriacao" => date("Y-m-d"),
                    "idProfissional" => $this->session->userdata("idProfissional")

                );

                $this->checklist->add_resposta($insert);
            }
        }

        // salva as discursivas
        for ($i = 1; $i < 3; $i++) {
            $hresposta = $this->input->post("hresposta$i");
            $resposta = $this->input->post("resposta$i");

            if ($hresposta != "" && $resposta != "") {
                $insert = array(
                    "respostaDiscursiva" => $resposta,
                    "idChecklistItem" => $hresposta,
                    "idVaga" => $idVaga,

                    "dataCriacao" => date("Y-m-d"),
                    "idProfissional" => $this->session->userdata("idProfissional")
                );

                $this->checklist->add_resposta($insert);
            }
        }

        $insert = array(
            "data" => date("Y-m-d"),
            "idVaga" => $idVaga,
            "idProfissional" => $this->session->userdata("idProfissional")
        );

        if ($this->profissional->add_candidatar_vaga($insert) > 0) {


            $this->load->model("Empresa_model", "empresa");
            $empresa = $this->empresa->buscarPorId($vaga[0]->idEmpresa);

            // busca a empresa e dispara email			
            $mensagem = "&nbsp;";
            $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
            $mensagem = $mensagem . "<p>Novo candidato a vaga " . $vaga[0]->vaga . "</p>";
            $mensagem = $mensagem . "<p>Para mais detalhes acesse o portal www.gotalent.com.br</p>";
            $mensagem = $mensagem . "<p>Estamos a disposição para o que for preciso!!</p>";
            $mensagem = $mensagem . "<p><b>Go Talent - Empregos e Vagas de TI </b></p>";
            $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

            $this->load->model('AdminGotalent_model', 'admin_gotalent');
            $servidor = $this->admin_gotalent->getAll();

            $this->load->library('email');
            $config['smtp_host'] = $servidor[0]->smtp;
            $config['smtp_user'] = $servidor[0]->usuario;
            $config['smtp_pass'] = $servidor[0]->senha;
            $config['smtp_port'] = $servidor[0]->porta;
            $this->email->initialize($config);

            $this->email->from('contato@gotalent.com.br', 'Go Talent - Empregos e Vagas de TI');
            $this->email->to($empresa[0]->email);
            $this->email->subject('[GoTalent] - Novo Candidato');
            $this->email->message($mensagem);
            $this->email->send();

            $this->session->set_flashdata('sucesso', 'Já registramos sua candidatura, suas respostas e notificamos a empresa anunciante. Aguarde o contato e boa sorte!');
        } else {
            $this->session->set_flashdata('error', 'Erro ao registrar candidatura, tente novamente ou entre em contato com o nosso suporte.');
        }

        redirect('area-restrita');


    }

    function autocompletarPorCargo()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Profissional_model", "pm");
        $data = $this->pm->autocompletarPorCargo($q);
        foreach ($data as $d) {
            $cname = $d->cargo;
            echo "$cname\n";
        }

    }

    function autocompletarPorEmpresa()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Profissional_model", "pm");
        $data = $this->pm->autocompletarPorEmpresa($q);
        foreach ($data as $d) {
            $cname = $d->empresa;
            echo "$cname\n";
        }

    }

    function autocompletarPorInstituicao()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Profissional_model", "pm");
        $data = $this->pm->autocompletarPorInstituicao($q);
        foreach ($data as $d) {
            $cname = $d->instituicao;
            echo "$cname\n";
        }

    }

    function autocompletarPorCurso()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Profissional_model", "pm");
        $data = $this->pm->autocompletarPorCurso($q);
        foreach ($data as $d) {
            $cname = $d->curso;
            echo "$cname\n";
        }

    }
}
?>