<?php
session_start();

class BcController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata("tipo") != 'Administrador' && $this->session->userdata("tipo") != 'Empresa') {
            redirect('login/login');
        }
    }

    function index()
    {
        $this->banco();
    }

    function autocompletarPorCargo()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Bc_model", "bc");
        $data = $this->bc->autocompletarPorCargo($q);
        foreach ($data as $d) {
            $cname = $d->cargo;
            echo "$cname\n";
        }
    }

    function autocompletarPorSkill()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Bc_model", "bc");
        $data = $this->bc->autocompletarPorSkill($q);
        foreach ($data as $d) {
            $cname = $d->skill;
            echo "$cname\n";
        }
    }

    public function banco()
    {
        $this->load->model("Bc_model", "bc");
        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        $this->load->model('Parametros_model', 'parametro');
        $data["p_qtd_bc"] = $this->parametro->buscarParametroPorNome("p_qtd_bc");


        if ($this->session->userdata("tipo") == 'Empresa') {
            $this->load->model('Empresa_model', 'empresa');
            $this->load->model('Vaga_model', 'vaga');
            $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

            if ($data["empresa"][0]->moduloBancoCurriculo == "NAO") {
                $data["msgPlano"] = 'Seu plano atual permite que tenha até ' . $data["p_qtd_bc"][0]->valor . ' resultados na busca de profissionais, faça UPGRADE para o plano Ideal e realize buscas ilimitadas. Basta acessar: <a href="' . base_url() . 'empresas#planos">Quero ver mais PROFISSIONAIS</a> e selecionar um dos planos.';
            }

            $data["vagas"] = $this->vaga->buscarTodasVagasAtivasPorIdEmpresa($this->session->userdata("idEmpresa"));
            $this->load->vars($data);
            $this->load->view("priv/empresa/indicacao");
        } else if ($this->session->userdata("tipo") == 'Administrador') {

            $this->load->vars($data);
            $this->load->view("priv/administrador/banco");
        }
    }

    public function limparFiltros(){
        $_SESSION["filtroEstado"] = "";
        $_SESSION["filtroCidade"] = "";
        $_SESSION["bc_palavra_chave"] = "";
        $_SESSION["idadeInicial"] = "";
        $_SESSION["idadeFinal"] = "";
        $_SESSION["bc_cargo"] = "";
        $_SESSION["sitProfissional"] = "";
        $_SESSION["tipoPretensao"] = "";
        $_SESSION["pretensao"] = "";
        $_SESSION["bc_skill"] = "";
        $_SESSION["sexo"] = "";
        $this->banco();
    }

    public function buscarProfissionais()
    {
        $estado = $this->input->get_post("estado");
        $cidade = $this->input->get_post("cidade");
        $palavraChave = $this->input->post("bc_palavra_chave");
        $idadeI = $this->input->get_post("idadeInicial");
        $idadeF = $this->input->get_post("idadeFinal");
        $sexo = $this->input->get_post("sexo");
        $pretensaoTemp = $this->input->post("pretensao");
        $pretensao = str_replace(",", ".", str_replace(".", "", $pretensaoTemp));
        $tipoPretensao = $this->input->get_post("tipoPretensao");
        $cargo = strtoupper($this->input->post("bc_cargo"));
        $sitProfissional = $this->input->post("sitProfissional");
        $skill = $this->input->post("bc_skill");

        if($estado != ""){
            $_SESSION["filtroEstado"] = $estado;
        }else{
            $estado =  $_SESSION["filtroEstado"];
        }
        if($cidade != ""){
            $_SESSION["filtroCidade"] = $cidade;
        }else{
            $cidade = $_SESSION["filtroCidade"];
        }
        if($palavraChave != ""){
            $_SESSION["bc_palavra_chave"] = $palavraChave;
        }else{
            $palavraChave = $_SESSION["bc_palavra_chave"];
        }
        if($idadeI != ""){
            $_SESSION["idadeInicial"] = $idadeI;
        }else{
            $idadeI = $_SESSION["idadeInicial"];
        }
        if($idadeF != ""){
            $_SESSION["idadeFinal"] = $idadeF;
        }else{
            $idadeF = $_SESSION["idadeFinal"];
        }
        if ($idadeI != "" && $idadeF != "") {
            $arrDate = getdate();
            $idadeInicial = intval($idadeF);
            $idadeFinal = intval($idadeI);
            $strDateI = strval(($arrDate["year"] - $idadeInicial) . "-" . $arrDate["mon"] . "-" . $arrDate["mday"]);
            $strDateF = strval(($arrDate["year"] - $idadeFinal) . "-" . $arrDate["mon"] . "-" . $arrDate["mday"]);
        }

        if ($cargo != "") {
            $_SESSION["bc_cargo"] = $cargo;
        } else {
            $cargo = $_SESSION["bc_cargo"];
        }
        if ($sitProfissional != "") {
            $_SESSION["sitProfissional"] = $sitProfissional;
        } else {
            $sitProfissional = $_SESSION["sitProfissional"];
        }
        if ($skill != "") {
            $_SESSION["bc_skill"] = $skill;
        } else {
            $skill = $_SESSION["bc_skill"];
        }
        if ($sexo != "") {
            $_SESSION["sexo"] = $sexo;
        } else {
            $sexo = $_SESSION["sexo"];
        }
        if ($pretensao != "") {
            $_SESSION["pretensao"] = $pretensaoTemp;
        } else {
            $pretensao = str_replace(",", ".", str_replace(".", "", $_SESSION["pretensao"]));
        }
        if ($tipoPretensao != "") {
            $_SESSION["tipoPretensao"] = $tipoPretensao;
        } else {
            $tipoPretensao = $_SESSION["tipoPretensao"];
        }
        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        if ($estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($estado);
        }

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->load->model('Empresa_model', 'empresa');
        $this->load->model('Parametros_model', 'parametro');
        $data["plano_empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
        $data["p_qtd_bc"] = $this->parametro->buscarParametroPorNome("p_qtd_bc");

        $this->load->model("Bc_model", "bc");

        if ($data["plano_empresa"][0]->moduloBancoCurriculo == "NAO") {
            $data["profissionais"] = $this->bc->retornaProfissionaisCompativeisSemPaginacao($estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill, $data["p_qtd_bc"][0]->valor, 0, $palavraChave, $strDateI, $strDateF);
            $data["msgPlano"] = 'Seu plano atual permite que tenha até ' . $data["p_qtd_bc"][0]->valor . ' resultados na busca de profissionais, faça UPGRADE para o plano Ideal e realize buscas ilimitadas. Basta acessar: <a href="' . base_url() . 'empresas#planos">Quero ver mais PROFISSIONAIS</a> e selecionar um dos planos.';
        } else {
            $data["profissionais"] = $this->bc->retornaProfissionaisCompativeisComPaginacao($estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill, $this->numRegister4PagePaginate(), $page, $palavraChave, $strDateI, $strDateF);
            $total = $this->bc->retornaTotalProfissionaisCompativeis($estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill, $palavraChave, $strDateI, $strDateF);
            $data['paginacao'] = $this->createPaginate('bcController', $total[0]->total);
        }

        if ($this->session->userdata("tipo") == 'Empresa') {
            $this->load->model('Vaga_model', 'vaga');
            $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
            $data["vagas"] = $this->vaga->buscarTodasVagasAtivasPorIdEmpresa($this->session->userdata("idEmpresa"));
            $this->load->vars($data);
            $this->load->view("priv/empresa/indicacao");
        } else if ($this->session->userdata("tipo") == 'Administrador') {
            $this->load->vars($data);
            $this->load->view("priv/administrador/banco");
        }
    }

    /**
     * Metodo que configura numero de registro por pagina
     */
    function numRegister4PagePaginate()
    {
        return 10;
    }

    /**
     * Metodo que cria link de paginacao
     */
    function createPaginate($_modulo, $_total)
    {

        $this->load->library('pagination');

        $config['base_url'] = base_url($_modulo . '/buscarProfissionais/');
        $config['total_rows'] = $_total;
        $config['per_page'] = $this->numRegister4PagePaginate();
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $config['next_link'] = 'Próximo';
        $config['prev_link'] = 'Anterior';

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    function convidar()
    {

        $idProfissional = $this->input->post("idProfissional");
        $idVaga = $this->input->post("idVaga");
        $mensagemConvite = $this->input->post("mensagemConvite");

        $this->load->model('Vaga_model', 'vaga');
        $vaga = $this->vaga->buscarVagaPorId($idVaga);

        $this->load->model('Profissional_model', 'profissional');
        $profissional = $this->profissional->buscarPorId($idProfissional);

        //$mensagemConvite = str_replace("@NOME_EMPRESA", $vaga[0]->razaosocial, $mensagemConvite);

        $mensagem = $mensagem . "<p><b>Saudações profissional! </b></p>";
        $mensagem = $mensagem . "<p>" . $mensagemConvite . " </p>";
        $mensagem = $mensagem . "<p><b>Dados da vaga:</b></p>";
        $mensagem = $mensagem . "<p><b style='text-transform:uppercase'> " . $vaga[0]->vaga . "</b></p>";
        $mensagem = $mensagem . "<p>Remuneração: " . $vaga[0]->salario . "</p>";
        $mensagem = $mensagem . "<p>Cidade/Estado: " . $vaga[0]->cidade . "/" . $vaga[0]->estado . "</p>";

        $mensagem = $mensagem . "<a style='background-color:#8cc63f; width: 300px; height: 50px; "
            . "line-height: 50px;font-size: 30px;  border-color: #8cc63f; color: #FFFFFF; padding: 10px 20px; border-radius: 5px; text-decoration: none;' "
            . " href='" . base_url() . "vaga/" . $vaga[0]->id . "-" . $this->url(str_replace(" ", "-", $vaga[0]->vaga)) . "'>VEJA MAIS</a>";

        $mensagem = $mensagem . "<p><b>Go Talent </b></p>";

        $assunto = "Convite para participar de processo de seleção.";
        $email = $profissional[0]->email;


        $this->load->model("Mensagem_model", "mensagens");
        $insert_mensagem = array(
            "data" => date("Y-m-d"),
            "idDestino" => $profissional[0]->id,
            "mensagem" => $mensagemConvite,
            "assunto" => $assunto,
            "lida" => 'N',
            "tipo" => 'PROFISSIONAL'
        );
        $this->mensagens->add_record($insert_mensagem);


        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'Go Talent');
        $this->email->to($email);
        $this->email->subject($assunto);
        $msg = "&nbsp;";
        $msg = $msg . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
        $msg = $msg . $mensagem;
        $msg = $msg . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->email->message($msg);

        if ($this->email->send()) {
            echo "sucesso";
        } else {
            echo "falha";
        }
    }

    //Transforma títulos em URL amigáveis
    function url($str)
    {
        $str = strtolower(utf8_decode($str));
        $i = 1;
        $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
        while ($i > 0)
            $str = str_replace('--', '-', $str, $i);
        if (substr($str, -1) == '-')
            $str = substr($str, 0, -1);
        return $str;
    }

    function notificarProfissional()
    {
        $idProfissional = $this->input->post("idProfissional");
        $this->load->model('Profissional_model', 'profissional');
        $profissional = $this->profissional->buscarPorId($idProfissional);

        $this->load->model('Empresa_model', 'empresa');
        $empresa = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $mensagemNotificacao = "<p>Notificamos que a empresa <b>" . $empresa[0]->razaosocial . "</b> visualizou seu currículo através do banco de currículos.</p>";
        $mensagemNotificacao .= "<p>Não perca tempo e mantenha seu currículo sempre atualizado.</p>";

        $this->load->model("Mensagem_model", "mensagens");

        if (count($this->mensagens->validaMensagemRecebidaNosUltimos3Dias($mensagemNotificacao, $idProfissional)) == 0) {

            $mensagem = $mensagem . "<p><b>Saudações profissional! </b></p>";
            $mensagem = $mensagem . $mensagemNotificacao;

            $mensagem = $mensagem . "<p><b>Go Talent - http://www.gotalent.com.br </b></p>";


            $assunto = $profissional[0]->nome . ", o seu perfil foi visualizado!";
            $email = $profissional[0]->email;


            $insert_mensagem = array(
                "data" => date("Y-m-d"),
                "idDestino" => $profissional[0]->id,
                "mensagem" => $mensagemNotificacao,
                "assunto" => $assunto,
                "lida" => 'N',
                "tipo" => 'PROFISSIONAL'
            );
            $this->mensagens->add_record($insert_mensagem);


            $this->load->model('AdminGotalent_model', 'admin_gotalent');
            $servidor = $this->admin_gotalent->getAll();

            $this->load->library('email');

            $config['smtp_host'] = $servidor[0]->smtp;
            $config['smtp_user'] = $servidor[0]->usuario;
            $config['smtp_pass'] = $servidor[0]->senha;
            $config['smtp_port'] = $servidor[0]->porta;

            $this->email->initialize($config);

            $this->email->from('contato@gotalent.com.br', 'Go Talent');
            $this->email->to($email);
            $this->email->subject($assunto);
            $msg = "&nbsp;";
            $msg = $msg . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
            $msg = $msg . $mensagem;
            $msg = $msg . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

            $this->email->message($msg);

            if ($this->email->send()) {
                echo "sucesso";
            } else {
                echo "falha";
            }
        }
    }

}

?>