<?php

session_start();
require_once("PagSeguroLibrary/PagSeguroLibrary.php");
require_once("PagSeguroLibrary/PagSeguroNpi.php");
require_once("dompdf/dompdf_config.inc.php");


class Principal extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function erro404()
    {
        $this->load->view('_paginas/erro404');
    }

    public function index()
    {
        $this->load->model("Vaga_model", "vaga");
        // buscar vagas em DESTAQUE
        $data["vagas"] = $this->vaga->listarVagasDestaqueHome();
        $data["vagas_estados"] = $this->vaga->listarNumVagasEstadosHome();

        $this->load->model("Empresa_model", "empresa");
        $data["empresas"] = $this->empresa->buscarEmpresasComLogo();
        $this->load->vars($data);
        $this->load->view('_paginas/index');
    }

    public function empresa($apelido)
    {
		
        $this->load->model("Empresa_model", "empresa");
        $this->load->model("Vaga_model", "vaga");
        $data["empresa"] = $this->empresa->buscarEmpresaComUrlPorApelido($apelido);
        
		if($data["empresa"] == "" || $data["empresa"] == null){
			redirect("/");
		}
		
		$data["vagas_empresa"] = $this->vaga->buscarTodasVagasAtivasPorIdEmpresa($data["empresa"][0]->id);
        $this->load->model("Galeria_model", "galeria");
        $data["galeria"] = $this->galeria->buscarPorIdEmpresa($data["empresa"][0]->id);
        $this->load->vars($data);
        $this->load->view('_paginas/empresa');
    }

    public function contato()
    {
        $numeros = array();
        $numeros[0] = rand(1, 50);
        $numeros[1] = rand(1, 50);
        $data["numeros"] = $numeros;
        $this->load->vars($data);
        $this->load->view('_paginas/contato');
    }

    public function premium()
    {
        $acessoAPP = false;
        if (isset($_GET["acessoAPP"])) {
            $acessoAPP = $_GET["acessoAPP"];
        }
        $this->load->model("Profissional_model", "profissional");
        if ($this->session->userdata("idProfissional") != null) {
            $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));
        }
        $data["acessoAPP"] = $acessoAPP;
        $this->load->vars($data);
        $this->load->view('_paginas/premium');
    }

    public function reativar()
    {
        $this->load->model("Plano_model", "planos");
        $data["planos"] = $this->planos->buscarPlanosProfissionais();
        $this->load->vars($data);
        $this->load->view('_paginas/reativar');
    }

    public function quemsomos()
    {
        $this->load->model("Equipe_model", "equipe");
        $this->load->model("Texto_model", "texto");

        $data["equipe"] = $this->equipe->getAll();

        $data["missao"] = $this->texto->buscarPorId(1);
        $data["valores"] = $this->texto->buscarPorId(2);
        $data["visao"] = $this->texto->buscarPorId(3);

        $this->load->vars($data);
        $this->load->view('_paginas/quemsomos');
    }

    public function texto($id)
    {
        $this->load->model("Texto_model", "texto");
        $data["texto"] = $this->texto->buscarPorId($id);

        if (count($data["texto"]) == 0) {
            redirect("/");
        }

        $this->load->vars($data);
        $this->load->view('_paginas/texto');
    }

    public function planosProfissionais()
    {
        $this->load->model("Plano_model", "planos");
        $data["planos_profissional"] = $this->planos->buscarPlanosProfissionais();

        $this->load->vars($data);
        $this->load->view('_paginas/planos-profissional');
    }

    public function planosEmpresa()
    {
        $this->load->model("Empresa_model", "empresa");
        $data["empresas"] = $this->empresa->buscarEmpresasComLogo();
        $this->load->vars($data);
        $this->load->view('_paginas/planos-empresa');
    }

    public function blog($urlCat)
    {
		
        $this->load->model("Categoria_model", "categoria");
        $this->load->model("Postagem_model", "posts");

        $data["categorias"] = $this->categoria->getAll();
		
		// Alterado para buscar categoria por URL
        if ($urlCat) {
			
			$cat = $this->categoria->buscarPorUrl($urlCat);
			if($cat == null || $cat == ""){
				redirect("/blog");
			}
            $data["posts"] = $this->posts->buscarPorCategoria($cat[0]->id);
            $data["categoria"] = $cat;
        } else {
            $data["posts"] = $this->posts->getLista();
        }

        $this->load->vars($data);
        $this->load->view('_paginas/blog');
    }

    public function post($urlCat, $urlPost)
    {
		
        $this->load->model('Postagem_model', 'post');
		$this->load->model("Categoria_model", "categoria");
		$cat = $this->categoria->buscarPorUrl($urlCat);
		
        $data["post"] = $this->post->buscarPorUrlEIdCategoria($urlPost, $cat[0]->id);
	
		if($data["post"] == null || $data["post"] == ""){
			redirect("/blog");
		}
		
        $data["relacionados"] = $this->post->getRelacionados($data["post"][0]->id, $cat[0]->id);

        
        $data["categoria"] = $cat;
        $data["categorias"] = $this->categoria->getAll();

        $this->load->model('Postagemgaleria_model', 'galeria');
        $data["galeria"] = $this->galeria->buscarPorIdPostagem($data["post"][0]->id);
        $this->load->vars($data);
        $this->load->view('_paginas/artigo');
    }

    public function cadastro($plano = null)
    {

        if ($plano != null) {
            //Busca plano
            $this->load->model("Plano_model", "planos");
            $data["planos"] = $this->planos->buscarPlanosEmpresasPorId($plano);

        }

        $this->load->vars($data);
        $this->load->view('_paginas/cadastro-empresa');
    }

    function planoEmpresa()
    {
        $id = $this->input->post("plano");

        //Busca plano
        $this->load->model("Plano_model", "planos");
        $plano = $this->planos->buscarPlanosEmpresasPorId($id);

        //Cria extrato
        $this->load->model('Extrato_model', 'extrato');
        $insert = array(
            "dataCriacao" => date("Y-m-d h:m:s"),
            "idEmpresa" => $_SESSION["idEmpresa"],
            "idPlanoEmpresa" => $id,
            "saldoAnuncio" => $plano[0]->quantidade,
            "saldoDestaque" => $plano[0]->destaque,
            "tipo" => "AGUARDANDO PAGAMENTO"
        );
        $idExtrato = $this->extrato->add_record($insert);

        //Dados do pagamento
        $descricao = "Aquisição do plano " . $plano[0]->plano . " com acesso de " . $plano[0]->diasVigencia . " dias no portal Go Talent";
        $valor = $plano[0]->valor;
        $ref = "EMP/" . $idExtrato;

        //Envia ao pagseguro
        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");
        $paymentRequest->addItem($ref, $descricao, 1, $valor);
        $paymentRequest->setReference($ref);
        $paymentRequest->setRedirectUrl(base_url() . "cadastro-empresa/passo-4");

        try {
            $credentials = new PagSeguroAccountCredentials("igorfonseca88@yahoo.com.br", "7E17B268E14B49BE913C42FE077EBD70");
            $url = $paymentRequest->register($credentials);
            header("Location: $url");
        } catch (PagSeguroServiceException $exp) {
            //print_r($exp->getHttpStatus());
            $message["erro"] = $exp->getMessage() . "Não foi possível concluir a transação pois houve algum erro no credenciamento com a PagSeguro, contate o administrador.";
            $this->feedback($message);
        }
    }

    public function cadastroCandidato()
    {
        $acessoAPP = false;
        if (isset($_GET["acessoAPP"])) {
            $acessoAPP = $_GET["acessoAPP"];
        }
        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();
        $data["acessoAPP"] = $acessoAPP;
        $this->load->vars($data);
        $this->load->view('_paginas/cadastro-profissional');
    }

    public function listarVagas()
    {
        $this->load->model("Vaga_model", "vaga");
        $_SESSION["searchVaga"] = null;
        $_SESSION["searchCidade"] = null;
        $_SESSION["searchNivel"] = null;
        //$data["vagas_destaque"] = $this->vaga->getAll("SIM");

        if ($_SESSION["searchAreaInteresse"] == null) {
            $_SESSION["searchAreaInteresse"] = $_COOKIE["areaInteresse"];
            $_SESSION["searchAreaInteresseArray"] = explode(",", $_COOKIE["areaInteresse"]);
        }

        $data["vagas"] = $this->vaga->buscarVagasPorFiltros(NULL, NULL, NULL, NULL, NULL, $_SESSION["searchAreaInteresse"]);

        $data["estados"] = $this->vaga->listarNumVagasEstados();

        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();

        $this->load->vars($data);
        $this->load->view('_paginas/vagas');
    }

    public function vagas($estado)
    {
        $this->load->model("Vaga_model", "vaga");
        $_SESSION["searchVaga"] = null;
        $_SESSION["searchCidade"] = null;
        $_SESSION["searchNivel"] = null;

        if ($_SESSION["searchAreaInteresse"] == null) {
            $_SESSION["searchAreaInteresse"] = $_COOKIE["areaInteresse"];
            $_SESSION["searchAreaInteresseArray"] = explode(",", $_COOKIE["areaInteresse"]);
        }
        //$data["vagas_destaque"] = $this->vaga->buscarVagasPorFiltros(null, null, $estado, "SIM");
        $data["vagas"] = $this->vaga->buscarVagasPorFiltros(null, null, $estado, NULL, NULL, $_SESSION["searchAreaInteresse"]);
        $data["estados"] = $this->vaga->listarNumVagasEstados();
        $data["estado"] = $estado;

        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();

        $this->load->vars($data);
        $this->load->view('_paginas/vagas');
    }

    public function vaga($vaga)
    {
        $idVaga = explode("-", $vaga);


        $this->load->model("Vaga_model", "vaga");
        $data["vaga"] = $this->vaga->buscarVagaPorId($idVaga[0]);
        if ($data["vaga"][0]->id == "" || $data["vaga"][0]->situacao != 'Ativo') {
            redirect('vagas');
        }
        $data["beneficios_vaga"] = $this->vaga->buscarTodosBeneficiosPorIdVaga($idVaga[0]);
        $data["vagas_empresa"] = $this->vaga->buscarVagasAtivasPorIdEmpresaParaPortal($data["vaga"][0]->idEmpresa, $idVaga[0]);

        $data["vaga_skills"] = $this->vaga->buscarTodasSkillsPorIdVaga($idVaga[0]);



        $update = array(
            "visualizacao" => $data["vaga"][0]->visualizacao + 1
        );

        $this->vaga->update($idVaga[0], $update);

        // verifica se tem profissional na sessão
        if ($this->session->userdata("idProfissional") != "") {
            $this->load->model("Profissional_model", "profissional");
            $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

            $data["candidaturas"] = $this->profissional->buscarTodasCandidaturasPorIdProfissional($this->session->userdata("idProfissional"));

            $compatibilidade = 0;
            $compatibilidade = $this->vaga->retornaCompatibilidadeProfissional($this->session->userdata("idProfissional"), $data["vaga"][0]->id);
            $data["compatibilidade_vaga"] = $compatibilidade[0]->porcentagem;
        }

        $this->load->vars($data);
        $this->load->view('_paginas/vaga');
    }

    public function buscar()
    {
        $this->load->model("Vaga_model", "vaga");
        $searchVaga = $this->input->post("searchVaga");
        $searchCidade = $this->input->post("searchCidade");
        $searchNivel = $this->input->post("searchNivel");
        $ckarea = $this->input->post("ckarea");

        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();

        if ($searchVaga == "") {
            $_SESSION["searchVaga"] = null;
        } else {
            $_SESSION["searchVaga"] = $searchVaga;
        }
        if ($searchCidade == "") {
            $_SESSION["searchCidade"] = null;
        } else {
            $_SESSION["searchCidade"] = $searchCidade;
        }
        if ($searchNivel == "Todos") {
            $_SESSION["searchNivel"] = null;
        } else {
            $_SESSION["searchNivel"] = $searchNivel;
        }

        if ($ckarea == "") {
            $_SESSION["searchAreaInteresse"] = null;
            $_SESSION["searchAreaInteresseArray"] = NULL;
        } else {
            $_SESSION["searchAreaInteresse"] = implode(",", $ckarea);
            $_SESSION["searchAreaInteresseArray"] = $ckarea;
        }

        //echo $_SESSION["searchAreaInteresse"];
        //$data["vagas_destaque"] = $this->vaga->buscarVagasPorFiltros($_SESSION["searchVaga"], $_SESSION["searchCidade"], null, "SIM", $_SESSION["searchNivel"]);
        $data["vagas"] = $this->vaga->buscarVagasPorFiltros($_SESSION["searchVaga"], $_SESSION["searchCidade"], null, null, $_SESSION["searchNivel"], $_SESSION["searchAreaInteresse"]);

        $data["estados"] = $this->vaga->buscarCidadesDasVagasAtivas();
        $this->load->vars($data);
        $this->load->view('_paginas/vagas');
    }

    function plano()
    {
        if ($this->session->userdata("idProfissional") == "" || $this->session->userdata("idProfissional") == 0) {
            redirect("login/login");
        }
        $this->load->model("Plano_model", "planos");
        $data["planos_profissional"] = $this->planos->buscarPlanosPremiumProfissionais();

        $this->load->model("Profissional_model", "profissional");
        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

        $this->load->vars($data);
        $this->load->view("priv/profissional/plano");
    }

    public function arearestrita()
    {

        if ($this->session->userdata("idProfissional") == "" || $this->session->userdata("idProfissional") == 0) {
            redirect("login/login");
        }


        $this->load->model("Profissional_model", "profissional");
        $this->load->model("Vaga_model", "vaga");

        $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

        // se a data de vigência do plano esgotou
        //if($data["profissional"][0]->dataFinalPlano < date("Y-m-d") && $data["profissional"][0]->tipoPlano <> 'FREE'){
        // redireciona para tela de seleção de planos
        //redirect("principal/plano");	
        //}

        $profissional_skills = $this->profissional->buscarTodasSkillsPorIdProfissional($this->session->userdata("idProfissional"));

        $data["candidaturas"] = $this->profissional->buscarTodasCandidaturasPorIdProfissional($this->session->userdata("idProfissional"));

        $data["vagas"] = $this->vaga->retornaVagasComCompatibilidadePorIdProfissional($this->session->userdata("idProfissional"));

        $data["estados"] = $this->vaga->buscarCidadesDasVagasAtivas();

        $data["totais"] = $this->profissional->buscarTotaisDashboard($this->session->userdata("idProfissional"));

        $data["limite"] = $this->profissional->verificarLimiteBotao($this->session->userdata("idProfissional"));

        $this->load->model("Plano_model", "planos");
        $data["planos_profissional"] = $this->planos->buscarPlanosPremiumProfissionais();

        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();

        if ($data["profissional"][0]->idArea != null) {
            $_SESSION["searchAreaInteresse"] = $data["profissional"][0]->idArea;
            $_SESSION["searchAreaInteresseArray"] = explode(",", $_SESSION["searchAreaInteresse"]);
        }


        //$this->load->model("Mensagem_model", "mensagem");
        //$data["mensagens"] = $this->mensagem->buscarPorIdDestino($this->session->userdata("idProfissional"), 'PROFISSIONAL');

        $this->load->vars($data);
        $this->load->view('priv/default-profissional');
    }

    public function arearestritaadmin()
    {
        if ($this->session->userdata("tipo") != "Administrador" && $this->session->userdata("tipo") != "Conteudo") {
            redirect('/');
        }

        $this->load->view('priv/default');
    }

    public function arearestritaempresa()
    {

        if ($this->session->userdata("idEmpresa") == "" || $this->session->userdata("idEmpresa") == 0) {
            redirect("login/login");
        }

        // carrega a foto
        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        // expirou o plano
        if ($data["empresa"][0]->dataFinalPlano < date('Y-m-d') && $data["empresa"][0]->idPlanoEmpresa <> 4 && $data["empresa"][0]->idPlanoEmpresa <> 1) {
            $update = array("idPlanoEmpresa" => 1, "dataFinalPlano" => '2016-12-31', "demonstracao" => 0);
            $this->empresa->update($this->session->userdata("idEmpresa"), $update);
        }

        //busca as vagas
        $this->load->model("Vaga_model", "vaga");
        $data["vagas"] = $this->vaga->buscarTodasVagasPorIdEmpresa($this->session->userdata("idEmpresa"));

        // buscar totais para widgets
        $data["totais"] = $this->empresa->buscarTotaisDashboard($this->session->userdata("idEmpresa"));

        //$this->load->model("Mensagem_model", "mensagem");
        //$data["mensagens"] = $this->mensagem->buscarPorIdDestino($this->session->userdata("idEmpresa"), 'EMPRESA');
        //Busca se existe extrato pendente
        $this->load->model("Extrato_model", "extrato");
        $data["extrato_pendente"] = $this->extrato->buscarUltimoExtratoPendentePorIdEmpresa($this->session->userdata("idEmpresa"));
        $this->load->model("Plano_model", "planos");
        $data["planos_empresa"] = $this->planos->buscarPlanosEmpresas();

        $this->load->vars($data);
        $this->load->view('priv/default-empresa');
    }

    public function candidatar($idVaga)
    {
        $filename = null; // Variavel que recebe o caminho do currículo do profissional após ele ser gerado pelo sistema.

        $this->load->model("Profissional_model", "profissional");
        $insert = array(
            "data" => date("Y-m-d"),
            "idVaga" => $idVaga,
            "idProfissional" => $this->session->userdata("idProfissional")
        );

        $this->load->model("Vaga_model", "vaga");
        $vaga = $this->vaga->buscarVagaPorId($idVaga);

        if ($vaga[0]->situacao != "Ativo") {
            redirect('area-restrita');
        }

        if ($vaga[0]->idChecklist != "") {
            $this->session->set_flashdata('sucesso', 'Para candidatar-se a esta vaga, primeiro responda este checklist!');
            redirect('profissionalController/checklist/' . $vaga[0]->id . '/' . $vaga[0]->idChecklist);
        }

        if ($this->profissional->add_candidatar_vaga($insert) > 0) {


            $this->load->model("Empresa_model", "empresa");
            $empresa = $this->empresa->buscarPorId($vaga[0]->idEmpresa);

            $profissional = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

            // busca a empresa e dispara email
            $mensagem = "<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
<meta content='width=device-width, initial-scale=1.0' name='viewport' />
<title></title>
<style type='text/css'>/* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration:  inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}
         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^='tel'], a[href^='sms'] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
         text-decoration: inherit;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}
         }
</style>
<div style='z-index:9998;position: absolute;top:0;bottom: 0;left: 50%;width: 700px; margin-left:-350px;'>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td width='100%'>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody><!-- Spacing --><!-- Spacing -->
								<tr>
									<td height='10' style='text-align: right;' width='100%'>
									<p>&nbsp; &nbsp;</p>
									</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>
                            <p><img alt='' height='77' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/64/original_logogo.png?1457386442' width='240' /></p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>

			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center; font-size: 16px;'><span style='font-size: 28px;'>Novo candidato &agrave; vaga</span></p>

			<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><span style='font-size: 48px;'>
                <strong style='margin-bottom: 5px;'>".$vaga[0]->vaga."</strong>
                <span  style='font-size: 28px;'>".$vaga[0]->tipoProfissional."</span>
			</p>
			</td>
		</tr>
	</tbody>
</table>

<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tbody>
		<tr>
			<td>
			<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
				<tbody>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
					</tr>
					<tr>
						<td align='center' height='30' style='font-size:1px; line-height:1px;'>&nbsp;</td>
					</tr>
				</tbody>
			</table>

			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td width='100%'>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody><!-- content -->
											<tr>
												<td style='font-family: Helvetica, arial, sans-serif; color: rgb(102, 102, 102); text-align: center; line-height: 30px;'>
									            <p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: justify;'></span></em></p>

												<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'>&nbsp;</p>";

            if($vaga[0]->receberCurriculoEmail == "SIM"){
                $mensagem.="<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: justify;'>Olá ".$empresa[0]->contato.", o profissional ".ucwords( mb_strtolower($profissional[0]->nome, 'UTF-8'))." candidatou-se à vaga de ".$vaga[0]->vaga.", anexo encontra-se o currículo do mesmo. Qualquer dúvida estamos à sua disposição!<br/> Atenciosamente.</span></em></p>";
            }else{

                $mensagem.="<p style='color: rgb(102, 102, 102); font-family: Helvetica, arial, sans-serif; line-height: 30px; text-align: center;'><em><span style='color: rgb(144, 144, 144); font-family: Lato, Calibri, Arial, sans-serif; font-size: 18px; line-height: normal; text-align: justify;'>Olá ".$empresa[0]->contato.", o profissional ".ucwords( mb_strtolower($profissional[0]->nome, 'UTF-8'))."  candidatou-se à vaga de ".$vaga[0]->vaga.", entre agora mesmo no portal <a href='www.gotalent.com.br'>GoTalent</a> e avalie este novo candidato. Qualquer dúvida estamos à sua disposição!<br/> Atenciosamente.</span></em></p>";
            }

            $mensagem .="</td>
                       </tr>
											<!-- End of content -->
										</tbody>
									</table>
									</td>
								</tr>
								<!-- Spacing -->
								<tr>
									<td height='20' style='font-size:1px; line-height:1px; mso-line-height-rule: exactly;'>&nbsp;</td>
								</tr>
								<!-- Spacing -->
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' width='100%'>
				<tbody>
					<tr>
						<td>
						<p>&nbsp;</p>

						<p>&nbsp;</p>

						<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
							<tbody>
								<tr>
									<td align='center' bgcolor='#d1d1d1' height='1' style='font-size:1px; line-height:1px;' width='550'>&nbsp;</td>
								</tr>
								<tr>
									<td align='center' height='30' style='font-size:1px; line-height:1px;'>
									<p style='text-align: left;'>&nbsp;fdsfs</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' style='line-height: 1.6em;' width='100%'>
							<tbody>
								<tr>
									<td>
									<table align='center' border='0' cellpadding='0' cellspacing='0' class='devicewidth' width='600'>
										<tbody>
											<tr>
												<td align='center' height='30' style='font-size:1px; line-height:1px;'><img alt='' class='banner' height='203' src='http://assets.mktnaweb.com/accounts/2015/04/27/52003/pictures/67/original_edinor.png?1457461288' style='display: block; border-width: 0px; border-style: solid;' width='594' /></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>";

            $this->load->model('AdminGotalent_model', 'admin_gotalent');
            $servidor = $this->admin_gotalent->getAll();

            $this->load->library('email');
            $config['smtp_host'] = $servidor[0]->smtp;
            $config['smtp_user'] = $servidor[0]->usuario;
            $config['smtp_pass'] = $servidor[0]->senha;
            $config['smtp_port'] = $servidor[0]->porta;
            $this->email->initialize($config);

            $this->email->from('contato@gotalent.com.br', 'Go Talent - Tecnologia em Recrutamento');
            $this->email->to("marcusbrunogm@gmail.com");//$empresa[0]->email
            $this->email->subject('Novo Candidato À Vaga');
            $this->email->message($mensagem);
            if($vaga[0]->receberCurriculoEmail == "SIM"){
                $filename = $this->gerarCurriculo();
                $this->email->attach($filename);
            }
            $this->email->send();

            $this->session->set_flashdata('sucesso', 'Já registramos sua candidatura e notificamos a empresa anunciante. Aguarde o contato e boa sorte!');
        } else {
            $this->session->set_flashdata('error', 'Erro ao registrar candidatura, tente novamente ou entre em contato com o nosso suporte.');
        }
        unlink($filename); //Exclui o currículo criado, após ele ser enviado a empresa.
        redirect('area-restrita');
    }

    public function gerarCurriculo()
    {

        $this->load->model('Profissional_model', 'profissional');
        $id = $this->session->userdata("idProfissional");

        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
        $data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
        $data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional("17513");

        $this->load->model("EstadoCidade_model", "estado");
        if ($data["profissional"][0]->estado != "" || $data["profissional"][0]->cidade != "") {
            $data["estado"] = $this->estado->buscarEstadosPorId($data["profissional"][0]->estado);
            $data["cidade"] = $this->estado->buscarCidadePorId($data["profissional"][0]->cidade);
        };
        //Inserir no html o conteudo dinâmico.
   $html = "
<html><head><link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
	<link href='font-awesome/css/font-awesome.css' rel='stylesheet' type='text/css'><style>
  body {margin: 0 auto;-webkit-print-color-adjust: exact;font-family: 'Times New Roman'}
    @page { margin: 10mm 0 0 0; padding: 0 0 10mm 0;}
    #content{width: 7in;margin:auto;display: block;}
    #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 80px; margin-top:25mm; font-family:'PT Sans' }
	#autor {z-index:999;margin-left:77.5%;margin-top:40px;font-size: 12px;color: #006886;margin-top:10px;}
	.clear {clear: both;}
	#conteudoFooter {position: fixed; left: 0px; bottom: 0px; right: 0px;height: 50px;width: 100%;background-color: #006886;font-family:'PT Sans'}
#logo {width: 150px;height: 48px;background-image: url('../../_imagens/curriculo/logo_rodape.jpg');float: left;margin-top: -10px;border-bottom-right-radius: 10px;
    border-bottom-left-radius: 5px;margin-left: 140px;box-shadow: -1px 0px 1px #ccc;}
#creditos {font-family:'sans-serif';float: right;color: #FFF;text-align:right;margin-top: -25px;margin-right:10px;font-weight: 500;line-height: 20px;}
.quebra-pagina{page-break-inside:avoid;margin-bottom: 35px;}
.quebra-conteudo{page-break-inside:avoid;margin-bottom: 5px;}
#hr_primary {width: 92%;border-width: 0;height: 4px;text-align: right;margin-right: 15%;background-color: #006886;border-radius: 5px;}
#boxResumo{float: left;}
#resumo {padding-left: 80px;text-align: justify;}
#containerFoto {width: 120px;height: 120px;border-radius: 60px;-webkit-border-radius: 60px;-moz-border-radius: 60px;border: 1px solid #999999;float: right;}
/*Parte Superior Dados de Contato*/
#Contato{width: 584px;display: block;margin: 0 auto;margin-left: 85px}
</style>
</head>
<body>
<div id='footer'>
	<small><p id='autor'>Currículo gerado por Go Talent</p></small>
	<div class='clear'></div>
	<div id='conteudoFooter'>
		<img src='_imagens/curriculo/logo_rodape.jpg' id='logo'>
		<p id='creditos'> WWW.GOTALENT.COM.BR</p>
	</div>
</div>
<div id='content'>
	<div>
		<table>
			<tr>
				<td width='530px' >
					<h1 style='font-size: 20pt'>" . mb_strtoupper($data['profissional'][0]->nome, 'UTF-8') . "</h1>";
        foreach ($data["experiencias"] as $experiencia) {
            if ($experiencia->empregoAtual == "1") {
                $html .= "<h4 style='margin-bottom: 0px;font-size:13px;margin-top: -30px'>" . mb_strtoupper($experiencia->cargo . " na " . $experiencia->empresa, 'UTF-8') . "</h4>";
                break;
            }
        }
        $html .= "
                    <h5 class='infoProf'>" . $data['profissional'][0]->email . "<br/>
                    " . $data['profissional'][0]->telefone . "<br/>
                    </h5>";
        $html .= "
                </td>
				<td width='130px' >";
        $foto = "_imagens/curriculo/default.png";
        if ($data['profissional'][0]->foto != '' && count(explode(':', $data['profissional'][0]->foto)) <= 1) {
            $foto = "upload/profissional/" . $data['profissional'][0]->foto;
        } else {
            if ($data['profissional'][0]->foto != "" && count(explode(':', $data['profissional'][0]->foto)) > 1) {
                $foto = "_imagens/curriculo/default.png";
            }
        }
        $html .= "
                    <img src=" . $foto . " id='containerFoto'>
                </td>
			</tr>
		</table>
	</div>
	<div>
        <div>
            <h3 style='color:#555'>Resumo Profissional</h3>
            <hr/>
        </div>
        <div id='boxResumo' style='page-break-inside:avoid;'>";
        $html .= "
            <p id='resumo'>" . $data['profissional'][0]->resumo . "<br>Eu tenho por interesse salárial R$" . $data['profissional'][0]->pretensao . ".</p>
        </div>
	</div>
    <div>
		<div>
			<h3 style='color:#555'>Histórico Educacional</h3>
			<hr>
		</div>
		<div clss='dadosEducacionais'>";
        foreach ($data["formacoes"] as $formacao) {
            $html .= "
            <div style='page-break-inside:avoid; margin-bottom:5mm;' >
			    <div class='boxDadosEducacionais' style='margin-left:80px;'>
				    <p class='dados'><strong style='color: #777'>" . mb_strtoupper($formacao->instituicao, 'UTF-8') . "</strong><p/>
					<p>
					    <span style='color:#555;margin-left:15px;'>" . $this->mes(explode("-", $formacao->dataInicio)[1]) . " de " . explode("-", $formacao->dataInicio)[0] . " até " . $this->mes(explode("-", $formacao->dataFim)[1]) . " de " . explode("-", $formacao->dataFim)[0] . "</span><br/>
					    <span style='margin-left:15px;'>$formacao->curso</span><br/>
					    <span style='margin-left:15px;'>$formacao->formacao</span>
					</p>
				</div>
			</div>";
        }
        $html .= "
        </div>
	</div>";
        $css = "";
        $div = "";

        if (count($data["formacoes"]) == 0 && count($data["experiencias"]) == 0 && strlen($data['profissional'][0]->resumo) >= 0) {
            $css = "page-break-inside:avoid";
        } else {
            if (count($data["formacoes"]) == 0 && count($data["experiencias"]) == 0 && strlen($data['profissional'][0]->resumo) >= 0) {

                $css = "page-break-inside:avoid";
            } else {
                if (count($data["formacoes"]) == 0 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) >= 0) {

                    $css = "page-break-inside:avoid";
                } else {
                    if (count($data["formacoes"]) >= 1 && count($data["experiencias"]) == 0 && strlen($data['profissional'][0]->resumo) >= 0) {

                        $css = "page-break-inside:avoid";
                    } else {
                        if (count($data["formacoes"]) == 1 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) <= 550) {

                            $css = "page-break-inside:avoid";
                        } else {
                            if (count($data["formacoes"]) == 1 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 550) {

                                $div = "page-break-before:always";
                            } else {
                                if (count($data["formacoes"]) <= 2 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) < 80) {

                                    $css = "page-break-inside:avoid";
                                } else {
                                    if (count($data["formacoes"]) <= 2 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 80) {

                                        $div = "page-break-before:always";
                                    } else {
                                        if (count($data["formacoes"]) < 4 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 0) {
                                            $div = "page-break-before:always";

                                        } else {
                                            if (count($data["formacoes"]) >= 4 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 0) {
                                                $css = "page-break-inside:avoid";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $html .= "
        <div style='. $div .'>
			<div>
				<h3  style='color:#555'>Experiência Profissional</h3>
				<hr>
			</div>
			<div class='dadosEducacionais'>";
        foreach ($data["experiencias"] as $experiencia) {
            $html .= "
                <div style=' margin-bottom:6mm;' >
				    <div class='boxDadosEducacionais' style='margin-left:80px;" . $css . "'>
					    <p class='dados'><strong style='color: #777'>" . mb_strtoupper($experiencia->cargo . " na " . $experiencia->empresa, 'UTF-8') . "</strong><p/>
                        <span style='color:#555;margin-left:15px;'>";
            if ($experiencia->empregoAtual == '1') {
                $periodo = $this->mes(explode("-", $experiencia->dataInicio)[1]) . " de " . explode("-", $experiencia->dataInicio)[0] . " até o momento";
            } else {
                $periodo = $this->mes(explode("-", $experiencia->dataInicio)[1]) . " de " . explode("-", $experiencia->dataInicio)[0] . " até " . $this->mes(explode
                    ("-", $experiencia->dataFim)[1]) . " de " . explode("-", $experiencia->dataFim)[0];
            };
            $html .= "$periodo</span><br/>
						 	<p style='margin-left:15px;text-align:justify'>" . $experiencia->descricaoAtividade . "</p>
						 	</div>
				</div>";
        }
        $html .= "
			</div>
	    </div>
	    <div>
	        <div>
    	        <h3  style='color:#555'>Skills</h3>
				<hr>
            </div>
            <div class='dadosEducacionais'>";

        $cont=0;

            $html .= "<ul style='margin-left:80px;'>";
            $elementos = count($data["profissional_skills"]);
            for ($j = 0; $j < $elementos; $j++) {

                if ($data["profissional_skills"][$j]->skill != null || $data["profissional_skills"][$j]->skill != "") {
                    $html .= "<li>" .$data["profissional_skills"][$j]->skill."</li>";
                }else{
                    break;
                }
            }
        $html.="</ul></div>
        </div>
    </body>
</html>
";

       $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $filename ="upload/profissional/curriculos/".$data["profissional"][0]->nome . "_Curriculo_GoTalent.pdf";
        file_put_contents($filename, $dompdf->output());
        return $filename;

    }

    public function mes($num)
    {
        $mes = "";
        switch ($num) {
            case "01":
                $mes = "Janeiro";
                break;
            case "02":
                $mes = "Fevereiro";
                break;
            case "03":
                $mes = "Março";
                break;
            case "04":
                $mes = "Abril";
                break;
            case "05":
                $mes = "Maio";
                break;
            case "06":
                $mes = "Junho";
                break;
            case "07":
                $mes = "Julho";
                break;
            case "08":
                $mes = "Agosto";
                break;
            case "09":
                $mes = "Setembro";
                break;
            case "10":
                $mes = "Outubro";
                break;
            case "11":
                $mes = "Novembro";
                break;
            case "12":
                $mes = "Dezembro";
                break;
        }
        return $mes;
    }

    public function cadastroProfissional()
    {

        // cria na tabela de profissional o cadastro.. cria o usuário e joga para tela de complementação do cadastro		
        $this->load->model("Profissional_model", "profissional");

        if (trim($this->input->post("senha")) == "") {
            $this->session->set_flashdata('error', 'Senha inválida.');
            redirect('cadastro-profissional');
        }

        // busca profissional pelo e-mail, caso já exista devolve erro
        $existe = $this->Usuario_model->buscarUsuarioPorEmail($this->input->post("emailLogin"));
        if (count($existe) == 0) {

            $this->load->model("Plano_model", "plano");
            //$plano = $this->plano->buscarPlanoProfissionalPorTipoEDias("PREMIUM", '30');
            $plano = $this->plano->buscarPlanoProfissionalPorId(1);
            $insert = array(
                "dataCadastro" => date("Y-m-d"),
                "nome" => $this->input->post("nome"),
                "email" => $this->input->post("emailLogin"),
                "telefone" => $this->input->post("telefone"),
                "idPlano" => $plano[0]->id,
                //"dataInicioPlano" => date("Y-m-d"),
                //"dataFinalPlano" => date("Y-m-d", strtotime("+15 days")),
                "valorPago" => 0,
                "idArea" => "2"//Area TI
            );

            $id = $this->profissional->add_record($insert);

            if ($id > 0) {

                // cria usuario
                $this->load->model('Usuario_model');

                $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->input->post("senha")); //remove palavras que contenham a sintaxe sql
                $seg = trim($seg); //limpa espaços vazios
                $seg = strip_tags($seg); // tira tags html e php
                $seg = addslashes($seg); //adiciona barras invertidas a uma string

                $insertUser = array(
                    "dataCriacao" => date("Y-m-d"),
                    "login" => $this->input->post("emailLogin"),
                    "tipo" => "Profissional",
                    "senha" => $seg,
                    //"situacao" => "CONFIRMAR",
					"situacao" => "ATIVO",
                    "idProfissional" => $id
                );
                $idUser = $this->Usuario_model->add_record($insertUser);


				$usuario = $this->Usuario_model->buscarUsuarioPorIdProfissional($id);

				if ($usuario[0]->idUsuario == "") {
					$this->session->set_flashdata('error', 'Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.');
					redirect("cadastro-profissional");
				}

				$data = array(
					'session_id' => $usuario[0]->idUsuario,
					'idUsuario' => $usuario[0]->idUsuario,
					'login' => $usuario[0]->login,
					'senha' => $usuario[0]->senha,
					'nome' => $usuario[0]->nome,
					"idProfissional" => $usuario[0]->idProfissional,
					"tipo" => "Profissional",
					'logged' => true
				);
				$this->session->set_userdata($data);
				$this->session->set_flashdata('sucesso', 'Olá ' . $usuario[0]->nome . ', seja bem vindo ao nosso portal, torcemos para que encontre o melhor emprego e o mais rápido possível. Lembramos que seu login para acesso é ' . $usuario[0]->login . '.Se tiver alguma dúvida entre em contato conosco através do e-mail suporte@gotalent.com.br, ficaremos felizes em lhe ajudar.');
				redirect("profissionalController/editarProfissionalAction");
			
            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.');
				redirect('cadastro-profissional');
            }
        } else {
            $this->session->set_flashdata('error', 'O e-mail utilizado já está cadastrado em nossa base.');
            redirect('cadastro-profissional');
        }
    }

    //Verificar se esta função está realmente sendo utilizada.
    public function cadastroProfissionalFacebook()
    {

        // cria na tabela de profissional o cadastro.. cria o usuário e joga para tela de complementação do cadastro
        $this->load->model("Profissional_model", "profissional");

        if (trim($this->input->post("senha_")) == "") {
            $this->session->set_flashdata('error', 'Senha inválida.');
            redirect('cadastro-profissional');
        }

        // busca profissional pelo e-mail, caso já exista devolve erro
        $existe = $this->Usuario_model->buscarUsuarioPorEmail($this->input->post("emailLogin"));
        if (count($existe) == 0) {

            $this->load->model("Plano_model", "plano");
            //$plano = $this->plano->buscarPlanoProfissionalPorTipoEDias("PREMIUM", '30');
            $plano = $this->plano->buscarPlanoProfissionalPorId(1);
            $insert = array(
                "dataCadastro" => date("Y-m-d"),
                "nome" => $this->input->post("nome"),
                "email" => $this->input->post("emailLogin"),
                "telefone" => $this->input->post("telefone"),
                "idPlano" => $plano[0]->id,
                //"dataInicioPlano" => date("Y-m-d"),
                //"dataFinalPlano" => date("Y-m-d", strtotime("+15 days")),
                "valorPago" => 0,
                "idArea" => $this->input->post("idArea")
            );

            $id = $this->profissional->add_record($insert);

            if ($id > 0) {

                // cria usuario
                $this->load->model('Usuario_model');

                $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->input->post("senha")); //remove palavras que contenham a sintaxe sql
                $seg = trim($seg); //limpa espaços vazios
                $seg = strip_tags($seg); // tira tags html e php
                $seg = addslashes($seg); //adiciona barras invertidas a uma string

                $insertUser = array(
                    "dataCriacao" => date("Y-m-d"),
                    "login" => $this->input->post("emailLogin"),
                    "tipo" => "Profissional",
                    "senha" => $seg,
                    //"situacao" => "CONFIRMAR",
                    "situacao" => "ATIVO",
                    "idProfissional" => $id
                );
                $idUser = $this->Usuario_model->add_record($insertUser);


                $usuario = $this->Usuario_model->buscarUsuarioPorIdProfissional($id);

                if ($usuario[0]->idUsuario == "") {
                    $this->session->set_flashdata('error', 'Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.');
                    redirect("cadastro-profissional");
                }

                $data = array(
                    'session_id' => $usuario[0]->idUsuario,
                    'idUsuario' => $usuario[0]->idUsuario,
                    'login' => $usuario[0]->login,
                    'senha' => $usuario[0]->senha,
                    'nome' => $usuario[0]->nome,
                    "idProfissional" => $usuario[0]->idProfissional,
                    "tipo" => "Profissional",
                    'logged' => true
                );
                $this->session->set_userdata($data);
                $this->session->set_flashdata('sucesso', 'Olá ' . $usuario[0]->nome . ', seja bem vindo ao nosso portal, torcemos para que encontre o melhor emprego e o mais rápido possível. Lembramos que seu login para acesso é ' . $usuario[0]->login . '.Se tiver alguma dúvida entre em contato conosco através do e-mail suporte@gotalent.com.br, ficaremos felizes em lhe ajudar.');
                redirect("profissionalController/editarProfissionalAction");

            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.');
                redirect('cadastro-profissional');
            }
        } else {
            $this->session->set_flashdata('error', 'O e-mail utilizado já está cadastrado em nossa base.');
            redirect('cadastro-profissional');
        }
    }

    public function preCadastroProfissional()
    {

        // cria na tabela de profissional o cadastro.. cria o usuário e joga para tela de complementação do cadastro		
        $this->load->model("Profissional_model", "profissional");

        $insert = array(

            "nome" => $this->input->post("nome"),
            "email" => $this->input->post("email"),
            "finalizou" => 'N'

        );
        $this->profissional->add_precadastro($insert);

        $this->session->set_flashdata('email', $this->input->post("email"));
        $this->session->set_flashdata('nome', $this->input->post("nome"));
        redirect("cadastro-profissional");


    }

    function confirmarCadastro($idProfissional)
    {

        $this->load->model("Profissional_model", "profissional");
        $usuario = $this->Usuario_model->buscarUsuarioPorIdProfissional($idProfissional);

        if ($usuario[0]->idUsuario == "") {
            $this->session->set_flashdata('error', 'Cadastro não foi confirmado. Por gentileza entre em contato com o nosso suporte.');
            redirect("cadastro-profissional");
        }

        $updateUser = array(
            "situacao" => "ATIVO",
            "ultimoAcesso" => date("Y-m-d")
        );
        if ($this->Usuario_model->update($usuario[0]->idUsuario, $updateUser) > 0) {

            $data = array(
                'session_id' => $usuario[0]->idUsuario,
                'idUsuario' => $usuario[0]->idUsuario,
                'login' => $usuario[0]->login,
                'senha' => $usuario[0]->senha,
                'nome' => $usuario[0]->nome,
                "idProfissional" => $usuario[0]->idProfissional,
                "tipo" => "Profissional",
                'logged' => true
            );
            $this->session->set_userdata($data);
            $this->session->set_flashdata('sucesso', 'Olá ' . $usuario[0]->nome . ', seja bem vindo ao nosso portal, torcemos para que encontre o melhor emprego e o mais rápido possível. Se tiver alguma dúvida entre em contato conosco, ficaremos felizes em lhe ajudar.');
            redirect("profissionalController/editarProfissionalAction");
        } else {
            $this->session->set_flashdata('error', 'Cadastro não foi confirmado. Por gentileza entre em contato com o nosso suporte.');
            redirect("cadastro-profissional");
        }
    }

    function confirmarEmpresa($idEmpresa)
    {

        $this->load->model("Empresa_model", "empresa");
        $usuario = $this->Usuario_model->buscarUsuarioPorIdEmpresa($idEmpresa);

        if ($usuario[0]->idUsuario == "") {
            $this->session->set_flashdata('error', 'Cadastro não foi confirmado. Por gentileza entre em contato com o nosso suporte.');
            redirect("cadastro-empresa");
        }

        $updateUser = array(
            "situacao" => "ATIVO"
        );
        if ($this->Usuario_model->update($usuario[0]->idUsuario, $updateUser) > 0) {

            $data = array(
                'session_id' => $usuario[0]->idUsuario,
                'idUsuario' => $usuario[0]->idUsuario,
                'login' => $usuario[0]->login,
                'senha' => $usuario[0]->senha,
                'nome' => $usuario[0]->nome,
                "idEmpresa" => $usuario[0]->idEmpresa,
                "tipo" => "Empresa",
                'logged' => true
            );
            $this->session->set_userdata($data);
            $this->session->set_flashdata('sucesso', 'Olá ' . $usuario[0]->nome . ', seja bem vindo ao nosso portal, torcemos para que encontre os melhores profissionais e o mais rápido possível. Se tiver alguma dúvida entre em contato conosco, ficaremos felizes em lhe ajudar.');
            redirect("area-restrita-empresa");
        } else {
            $this->session->set_flashdata('error', 'Cadastro não foi confirmado. Por gentileza entre em contato com o nosso suporte.');
            redirect("cadastro-empresa");
        }
    }

    function recuperarSenha()
    {
        $acessoAPP = false;
        if (isset($_GET["acessoAPP"])) {
            $acessoAPP = $_GET["acessoAPP"];
        }
        $numeros = array();
        $numeros[0] = rand(1, 50);
        $numeros[1] = rand(1, 50);
        $data["numeros"] = $numeros;
        $data["acessoAPP"] = $acessoAPP;
        $this->load->vars($data);
        $this->load->view("_paginas/recuperar-senha");
    }

    public function recuperar()
    {

        $resultCap = strip_tags(trim($this->input->post("respCap")));
        $hCap = $this->input->post("hCap");

        if ($resultCap != $hCap) {
            $this->session->set_flashdata('error', 'Erro ao recuperar senha. Número informado no captcha não está correto.');
            redirect("esqueci-minha-senha");
        }

        $email = strip_tags(trim($this->input->post("email")));

        if (trim(strtolower($email)) == "admin@gotalent.com.br" || trim(strtolower($email)) == "contato@gotalent.com.br" || trim(strtolower($email)) == "igor@gotalent.com.br") {
            $this->session->set_flashdata('error', 'E-mail não localizado. Por gentileza entre em contato com o nosso suporte.');
            redirect("esqueci-minha-senha");
        }

        $usuario = $this->Usuario_model->buscarUsuarioAtivoPorEmail($email);

        if ($usuario[0]->idUsuario == "") {
            $this->session->set_flashdata('error', 'E-mail não localizado. Por gentileza entre em contato com o nosso suporte.');
            redirect("esqueci-minha-senha");
        }

        $novaSenha = rand() . rand();

        $update = array("senha" => $novaSenha);

        $this->Usuario_model->update($usuario[0]->idUsuario, $update);

        $mensagem = "&nbsp;";
        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";

        $mensagem = $mensagem . "<p>Olá, você solicitou recentemente a recuperação de sua senha em nosso portal, por medidas de segurança geramos uma nova senha. Após o login, você pode alterá-la em Meus Dados de Acesso. </p><br />";
        $mensagem = $mensagem . "<p><b>Senha</b>: " . $novaSenha . "</p>";


        $mensagem = $mensagem . "<p>Atenciosamente,</p>";
        $mensagem = $mensagem . "<p><b>Go Talent</b></p>";
        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";


        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;
        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'GoTalent');
        $this->email->to($usuario[0]->login);
        $this->email->subject('Go Talent - Recuperação de senha');
        $this->email->message($mensagem);

        $this->email->send();

        $this->session->set_flashdata('sucesso', 'Já enviamos em seu e-mail, aguarde alguns instantes e confira.');
        redirect("esqueci-minha-senha");
    }

    public function cadastroEmpresa()
    {

        // cria na tabela de profissional o cadastro.. cria o usuário e joga para tela de complementação do cadastro		
        $this->load->model("Empresa_model", "empresa");

        // busca profissional pelo e-mail, caso já exista devolve erro
        $existe = $this->Usuario_model->buscarUsuarioPorEmail($this->input->post("emailEmpresa"));

        // busca empresa por cnpj
        $existeCNPJ = $this->empresa->buscarEmpresaPorCNPJ($this->input->post("cnpjEmpresa"));

        if (count($existeCNPJ) > 0) {
            $this->session->set_flashdata('error', 'O CNPJ utilizado já está cadastrado em nossa base. Entre em contato com o nosso suporte.');
            redirect("cadastro-empresa");
        }

        if (count($existe) == 0) {

            $plan = $this->input->post("plan");

            // SE FOR PLANO GRÁTIS, não tem data final
            if ($plan == 1) {
                $dataFinal = "2016-12-31";
                $demonstracao = 0;
            } else {
                $dataFinal = date("Y-m-d", strtotime("+15 days"));
                $demonstracao = 1;
            }

            $insert = array(
                "dataCadastro" => date("Y-m-d"),
                "razaosocial" => $this->input->post("nomeEmpresa"),
                "cnpj" => $this->input->post("cnpjEmpresa"),
                "email" => $this->input->post("emailEmpresa"),
                "contato" => $this->input->post("nomeContato"),
                "telefone" => $this->input->post("telefoneEmpresa"),
                "dataInicioPlano" => date("Y-m-d"),
                "dataFinalPlano" => $dataFinal,
                "demonstracao" => $demonstracao,
                "idPlanoEmpresa" => $plan
            );

            $id = $this->empresa->add_record($insert);

            if ($id > 0) {
                // formata senha segura
                $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->input->post("senhaEmpresa")); //remove palavras que contenham a sintaxe sql
                $seg = trim($seg); //limpa espaços vazios
                $seg = strip_tags($seg); // tira tags html e php
                $seg = addslashes($seg); //adiciona barras invertidas a uma string
                // cria usuario
                $this->load->model('Usuario_model');
                // antes era "situacao" => "AGUARDANDO PAGAMENTO",
                $insertUser = array(
                    "dataCriacao" => date("Y-m-d"),
                    "login" => $this->input->post("emailEmpresa"),
                    "tipo" => "Empresa",
                    "senha" => $seg,
                    "situacao" => "CONFIRMAR",
                    "idProfissional" => 0,
                    "idEmpresa" => $id,
                    "nome" => $this->input->post("nomeContato")
                );
                $idUser = $this->Usuario_model->add_record($insertUser);
                $data["sucesso"] = "Salvo com sucesso.";

                //salva id empresa na sessao
                $_SESSION["idEmpresa"] = $id;

                //email go talent
                $mensagem = "&nbsp;";
                $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                $mensagem = $mensagem . "<p>Novo cadastro de empresa realizado</p><br />";
                $mensagem = $mensagem . "<p><b>Nome da empresa</b>: " . $this->input->post("nomeEmpresa") . "</p>";
                $mensagem = $mensagem . "<p><b>E-mail</b>: " . $this->input->post("emailEmpresa") . "</p>";
                $mensagem = $mensagem . "<p><b>Nome do Contato</b>: " . $this->input->post("nomeContato") . "</p>";
                $mensagem = $mensagem . "<p><b>Telefone</b>: " . $this->input->post("telefoneEmpresa") . "</p><br /><br />";
                $mensagem = $mensagem . "<p>Atenciosamente,</p>";
                $mensagem = $mensagem . "<p><b>Go Talent</b></p>";
                $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                $this->load->model('AdminGotalent_model', 'admin_gotalent');
                $servidor = $this->admin_gotalent->getAll();

                $this->load->library('email');
                $config['smtp_host'] = $servidor[0]->smtp;
                $config['smtp_user'] = $servidor[0]->usuario;
                $config['smtp_pass'] = $servidor[0]->senha;
                $config['smtp_port'] = $servidor[0]->porta;
                $this->email->initialize($config);

                $this->email->from('contato@gotalent.com.br', 'GoTalent');
                $this->email->to('contato@gotalent.com.br,giovana@gotalent.com.br');
                $this->email->subject('Nova empresa registrada no portal GoTalent');
                $this->email->message($mensagem);
                $this->email->send();

                $mensagem = "&nbsp;";
                $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                $mensagem = $mensagem . "<h2>Olá, " . $this->input->post("nome") . "</h2>";
                $mensagem = $mensagem . "<p>A Go Talent te da boas vindas!</p>";
                $mensagem = $mensagem . "<p><b><a href='" . base_url() . "principal/confirmarEmpresa/" . $id . "'>Clique aqui</a> para confirmar o seu cadastro e ter acesso ao nosso portal </b></p><br /><br />";
                $mensagem = $mensagem . "<p>Após confirmar, você terá acesso a todas as funcionalidades e a possibilidade de encontrar os melhores profissionais!</p>";
                $mensagem = $mensagem . "<p><b>Sugerimos os seguintes passos: </b></p>";
                $mensagem = $mensagem . "<p>- Cadastre os dados da Empresa;</p>";
                $mensagem = $mensagem . "<p>- Faça upload da logo da Empresa;</p>";
                $mensagem = $mensagem . "<p>- Cadastre suas vagas e envie para aprovação e publicação;</p>";
                $mensagem = $mensagem . "<p>- E não deixe de conferir o banco de currículos;</p>";
                $mensagem = $mensagem . "<p>- BOA SORTE!!</p><br>";

                $mensagem = $mensagem . "<p><b>Seus dados de acesso ao portal:</b></p>";
                $mensagem = $mensagem . "<p>E-mail: " . $this->input->post("emailEmpresa") . " </p>";
                $mensagem = $mensagem . "<p>Senha: " . $this->input->post("senhaEmpresa") . " </p><br>";
                $mensagem = $mensagem . "<p>Estamos a disposição para o que precisar!</p>";
                $mensagem = $mensagem . "<p><b>Go Talent - Tecnologia em Recrutamento </b></p>";
                $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                $this->load->model('AdminGotalent_model', 'admin_gotalent');
                $servidor = $this->admin_gotalent->getAll();

                $this->load->library('email');

                $config['smtp_host'] = $servidor[0]->smtp;
                $config['smtp_user'] = $servidor[0]->usuario;
                $config['smtp_pass'] = $servidor[0]->senha;
                $config['smtp_port'] = $servidor[0]->porta;

                $this->email->from('contato@gotalent.com.br', 'GoTalent');
                $this->email->to($this->input->post("emailEmpresa"));
                $this->email->subject('Go Talent - Cadastro realizado com sucesso!');
                $this->email->message($mensagem);
                $this->email->send();

                $this->session->set_flashdata('sucesso', 'Cadastro efetuado com sucesso! Confirme o seu cadastro através do e-mail que enviamos para ' . $this->input->post("emailEmpresa"));
                redirect('cadastro-empresa');
                //redirect('cadastro-empresa/passo-2'); 
            } else {
                $this->session->set_flashdata('error', 'Algo de errado aconteceu, por gentileza, entre em contato com o nosso suporte.');
            }
        } else {
            $this->session->set_flashdata('error', 'O e-mail utilizado já está cadastrado em nossa base.');
        }
        redirect("cadastro-empresa");
    }

    public function enviarContato()
    {
        $resultCap = strip_tags(trim($this->input->post("respCap")));
        $hCap = $this->input->post("hCap");

        if ($resultCap != $hCap) {
            $this->session->set_flashdata('error', 'Erro ao enviar contato. Número informado no captcha não está correto.');
            redirect("contato");
        }

        $mensagem = "&nbsp;";
        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";

        $mensagem = $mensagem . "<p>Contato recebido pelo site <b>GoTalent</b></p><br />";
        $mensagem = $mensagem . "<p><b>Nome</b>: " . strip_tags($this->input->post("nome")) . "</p>";
        $mensagem = $mensagem . "<p><b>E-mail</b>: " . strip_tags($this->input->post("email")) . "</p>";
        $mensagem = $mensagem . "<p><b>Telefone</b>: " . strip_tags($this->input->post("telefone")) . "</p>";

        $mensagem = $mensagem . "<p><b>Mensagem</b>: " . strip_tags($this->input->post("mensagem")) . "</p><br /><br />";
        $mensagem = $mensagem . "<p>Atenciosamente,</p>";
        $mensagem = $mensagem . "<p><b>Go Talent</b></p>";
        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'GoTalent');
        $this->email->to('contato@gotalent.com.br');
        $this->email->subject('Novo e-mail recebido pelo site GoTalent');
        $this->email->message($mensagem);

        $message = array();
        if ($this->input->post("nome") <> "") {
            if (!$this->email->send()) {
                $this->session->set_flashdata('error', 'Erro ao enviar contato.');
            } else {
                $this->session->set_flashdata('sucesso', 'Contato enviado com sucesso.');
            }
        }
        redirect("contato");
    }

    function feedback($message = array())
    {
        $data["erro"] = $message["erro"];
        $data["sucesso"] = $message["sucesso"];

        $this->load->vars($data);
        $this->load->view('_paginas/cadastro-empresa');
    }

    function reativarAssinatura()
    {
        //$id = $this->input->post("plano");
        $email = $this->input->post("email");

        // busca usuário pelo email
        $usuario = $this->Usuario_model->buscarUsuarioPorEmail($email);

        // se retornar um usuário que seja profissional e situacao SUSPENSO
        if ($usuario[0]->idUsuario != "" && $usuario[0]->situacao == 'SUSPENSO' && $usuario[0]->idProfissional != "") {
            // busca profissional
            $this->load->model('Profissional_model', 'profissional');
            $profissional = $this->profissional->buscarPorId($usuario[0]->idProfissional);


            // email profissional
            $mensagem = "&nbsp;";
            $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
            $mensagem = $mensagem . "<h2>Olá, " . $profissional[0]->nome . "</h2>";
            $mensagem = $mensagem . "<p>A Go Talent te da boas vindas!</p>";
            $mensagem = $mensagem . "<p><b><a href=" . base_url() . "confirmar-cadastro/" . $profissional[0]->id . ">Clique aqui</a> para confirmar o seu cadastro e ter acesso ao nosso portal </b>.</p><br /><br />";
            $mensagem = $mensagem . "<p>Após confirmar, você poderá criar seu currículo e ter a possibilidade de encontrar a melhor oportunidade profissional!</p>";
            $mensagem = $mensagem . "<p><b>Sugerimos os seguintes passos: </b></p>";
            $mensagem = $mensagem . "<p>- Cadastre os seus dados pessoais (nome completo, telefone, cidade, estado, etc...);</p>";
            $mensagem = $mensagem . "<p>- Faça upload de uma foto;</p>";
            $mensagem = $mensagem . "<p>- Informe as SKILLS (Ex: PHP, Javascript, etc...);</p>";
            $mensagem = $mensagem . "<p>- Preencha os dados de experiências profissionais e formações escolares;</p>";
            $mensagem = $mensagem . "<p>- Candidate-se as vagas de interesse e principalmente as mais compatíveis com o seu perfil;</p>";
            $mensagem = $mensagem . "<p>- Aguarde o contato da Empresas e BOA SORTE!!</p><br>";

            $mensagem = $mensagem . "<p>Estamos a disposição para o que precisar!</p>";
            $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";
            $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

            $this->load->model('AdminGotalent_model', 'admin_gotalent');
            $servidor = $this->admin_gotalent->getAll();

            $this->load->library('email');

            $config['smtp_host'] = $servidor[0]->smtp;
            $config['smtp_user'] = $servidor[0]->usuario;
            $config['smtp_pass'] = $servidor[0]->senha;
            $config['smtp_port'] = $servidor[0]->porta;
            $this->email->initialize($config);

            $this->email->from('contato@gotalent.com.br', 'GoTalent');
            $this->email->to($email);
            $this->email->subject('Go Talent - Reativar perfil!');
            $this->email->message($mensagem);

            if ($this->email->send()) {

                //redirect('profissionalController/editarProfissionalAction/'.$id); 
                $this->session->set_flashdata('sucesso', 'Confirme o seu perfil através do e-mail que enviamos para ' . $email);
                redirect('principal/reativar');
            } else {
                echo $this->email->print_debugger();
            }


        } else {
            $this->session->set_flashdata('error', "E-mail não localizado, entre em contato com o nosso suporte. Ficaremos felizes em lhe ajudar!");
            redirect('principal/reativar');
        }
    }

    function finalizacao()
    {
        if (isset($_GET["transaction_id"])) {
            $credentials = new PagSeguroAccountCredentials("igorfonseca88@yahoo.com.br", "7E17B268E14B49BE913C42FE077EBD70");

            $transaction_id = $_GET["transaction_id"];
            $transaction = PagSeguroTransactionSearchService::searchByCode(
                $credentials, $transaction_id
            );

            $status = $transaction->getStatus();

            if ($status->getTypeFromValue() === 'AVAILABLE') {
                $this->session->set_flashdata('sucesso', "<h4>A transação foi paga e chegou ao final de seu prazo de liberação.</h4>");
            } elseif ($status->getTypeFromValue() === 'WAITING_PAYMENT') {
                $this->session->set_flashdata('sucesso', "<h4>Aguardando comprovação de pagamento</h4><p>Estamos aguardando a comprovação de pagamento do PagSeguro. Quando o pagamento for liberado, você receberá um e-mail informando seus dados de acesso ao portal.</p>");
            } elseif ($status->getTypeFromValue() === 'IN_ANALYSIS') {
                $this->session->set_flashdata('sucesso', "<h4>Transação em análise</h4><p>PagSeguro está analisando a sua transação. Quando o pagamento for liberado, você receberá um e-mail informando.</p>");
            } elseif ($status->getTypeFromValue() === 'PAID') {
                $this->session->set_flashdata('sucesso', "<h4>Pagamento efetuado com sucesso!</h4><p>Realize o seu login no portal.</p>");
            } elseif ($status->getTypeFromValue() === 'IN_DISPUTE') {
                $this->session->set_flashdata('sucesso', "<h4>Pagamento em disputa</h4><p>O comprador, dentro do prazo de liberação da transação, abriu uma disputa.</p>");
            } elseif ($status->getTypeFromValue() === 'REFUNDED') {
                $this->session->set_flashdata('sucesso', "<h4>Devolvida</h4><p>O valor da transação foi devolvido para o comprador.</p>");
            } elseif ($status->getTypeFromValue() === 'CANCELLED') {
                $this->session->set_flashdata('error', "<h4>Erro</h4><p>A transação não foi aprovada ou foi cancelada pelo PagSeguro, por favor tente novamente mais tarde ou entre em <a href='mailto:contato@gotalent.com.br'>contato</a> com o administrador.</p>");
            }
        } else {
            $this->session->set_flashdata('error', "<h4>Erro</h4><p>Nenhuma mensagem a exibir.</p>");
        }

        redirect('principal/reativar');
    }

    public function faleComConsultor()
    {

        $email = $this->input->post("email");
        $empresa = $this->input->post("nome");
        $telefone = $this->input->post("telefone");

        $this->load->model("Empresa_model", "empresa");


        $insert = array(
            "data" => date("Y-m-d"),
            "email" => $email,
            "nome" => $empresa,
            "telefone" => $telefone
        );

        $id = $this->empresa->add_contatoempresa($insert);
        if ($id > 0) {
            echo "sucesso";
        } else {
            echo "erro";
        }
    }

}
?>