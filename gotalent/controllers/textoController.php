<?php

class TextoController extends CI_Controller {

    function __construct() {
        parent::__construct();
		if($this->session->userdata("tipo") != "Administrador"){
			redirect('/');
		}
    }

    function index() {
		$this->load->model('Texto_model', 'texto');
		$data["textos"] = $this->texto->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/administrador/site/texto/list");
    }
    
	function editAction($id, $mensagem) {
		$this->load->model('Texto_model', 'texto');
		$data["texto"] = $this->texto->buscarPorId($id);
	
		$data["error"] = $mensagem["erro"];
		$data["sucesso"] = $mensagem["sucesso"];
		
		$this->load->vars($data);
		$this->load->view("priv/administrador/site/texto/edit");
	}

    function edit() {
        $this->load->model("Texto_model", "texto");
        $id = $this->input->post("id");
		
        $update = array( 
            "titulo" => $this->input->post("titulo"),
            "descricao" => $this->input->post("descricao"),
            "texto" => $this->input->post("texto")
        );

        if ($this->texto->update($id, $update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$this->editAction($id, $data);
        }
    }
	
	function addGaleria($id) {
        $this->load->model("Texto_model", "texto");
		$delete["delete"] = $this->texto->buscarPorId($id);
		
		if ($delete["delete"][0]->imagem) {		
			$filename = "./upload/texto/" . $delete["delete"][0]->imagem;
			unlink($filename);
		}
		
		$config["upload_path"] = "./upload/texto/";
		$config["allowed_types"] = "gif|jpg|png";
		$config["file_name"] = "texto_" . $id . "_" . rand(00, 9999);
		$config["overwrite"] = TRUE;
		$config["remove_spaces"] = TRUE;
		$this->load->library("upload", $config);

        if ($this->upload->do_upload()) {	            
	        $update = array(
				"imagem" => $this->upload->file_name
			);
			
			if ($this->texto->update($id, $update) > 0) {
				$data["sucesso"] = "Imagem salva com sucesso.";
			} else {
	        	$data["erro"] = "Erro ao salvar imagem no banco de dados.";
	        }
        } else {
            $data["erro"] = $this->upload->display_errors();
        }
		$this->editAction($id, $data);
    }
		
	function deleteGaleria($id) {
        $this->load->model("Texto_model", "texto");
		$delete["delete"] = $this->texto->buscarPorId($id);            
		$update = array(
			"imagem" => null
		);
		
		if ($this->texto->update($id, $update) > 0) {
			$filename = "./upload/texto/" . $delete["delete"][0]->imagem;
			unlink($filename);
			$data["sucesso"] = "Imagem removida com sucesso.";
		}
		
		$this->editAction($id, $data);
	}
}
?>