<?php

class VideoEntrevistaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        
    }

    function novaVideoEntrevistaAction($idVaga, $idProfissional) {
        $this->load->model("Vaga_model", "vaga");
        $this->load->model('VideoEntrevista_model', 'videoentrevista');
        $data["profissional_vaga"] = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);
        $data["idVaga"] = $idVaga;

        $this->load->model('Checklist_model', 'checklist');

        $idChecklist = $data["profissional_vaga"][0]->idChecklist;
        $data["checklist"] = $this->checklist->buscarChecklistPorId($idChecklist);

        $data["questoes_video"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($data["checklist"][0]->id, "Video");

        $this->load->model('Profissional_model', 'profissional');
        $data["profissional"] = $this->profissional->buscarPorId($idProfissional);

        foreach ($data["questoes_video"] as $questao) {

            $quest = 0;
            $quest = $this->videoentrevista->buscarNumeroRespostasPorIdProfissionalEItemChecklist($idProfissional, $questao->id);
            $questItem[$questao->id] = array("qtdRespostas" => $quest[0]->respostas, "respostaVideo" => $quest[0]->respostaVideo);
        }

        $data["videoentrevista"] = $questItem;

        
        $this->load->vars($data);
        $this->load->view("priv/videoentrevista/videoEntrevistaProfissional");
    }

    function salvarRespostaAction($idVaga, $arq, $itemCheck, $text) {
        $this->load->model("Profissional_model", "profissional");
        $this->load->model("Checklist_model", "checklist");
        $this->load->model("Empresa_model", "empresa");

        $respostas = $this->checklist->buscarRespostas($itemCheck, $idVaga, $this->session->userdata("idProfissional"));
        $empresas = $this->empresa->buscarEmpresaPorIdVaga($idVaga);

        if($respostas){
            
            foreach ($respostas as $resposta) {
                $idResposta = $resposta->id;                 
            }

            if ($arq != "" && $itemCheck != "") {
                $update = array(
                    "respostaVideo" => $arq,
                    "dataCriacao" => date("Y-m-d"),
                    "respostaVozTexto" => $text
                );

                $retUpdate = $this->checklist->updateResposta($idResposta,$update);

                if ($retUpdate > 0) {
                    echo "Vídeo armazenado com sucesso.";
                        $mensagem = $mensagem . "<p>O profissional ".$this->session->userdata("nome")." acabou de enviar uma vídeo entrevista para a vaga ".$empresas[0]->vaga.".</p>";
                        $mensagem = $mensagem . "<p>Estamos a disposição para o que for preciso!!</p>";
                        $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";
                        $assunto = "Um profissional acabou de enviar uma vídeo entrevista";
                        $this->enviarEmailMensagem($empresas[0]->email, $mensagem, $assunto);   
                } else {
                    echo "Erro ao salvar o vídeo, tente novamente.";
                }
            }
        }else{
            if ($arq != "" && $itemCheck != "") {
                $insert = array(
                    "respostaVideo" => $arq,
                    "idChecklistItem" => $itemCheck,
                    "idVaga" => $idVaga,
                    "dataCriacao" => date("Y-m-d"),
                    "idProfissional" => $this->session->userdata("idProfissional"),
                    "respostaVozTexto" => $text
                );

                $retAdd = $this->checklist->add_resposta($insert);

                if ($retAdd > 0) {   
                    echo "Vídeo armazenado com sucesso.";
                        $mensagem = $mensagem . "<p>O profissional ".$this->session->userdata("nome")." acabou de enviar uma vídeo entrevista para a vaga ".$empresas[0]->vaga.".</p>";
                        $mensagem = $mensagem . "<p>Estamos a disposição para o que for preciso!!</p>";
                        $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";
                        $assunto = "Um profissional acabou de enviar uma vídeo entrevista";
                        $this->enviarEmailMensagem($empresas[0]->email, $mensagem, $assunto);                       
                } else {
                    echo "Erro ao salvar o vídeo, tente novamente.";
                }
            }
        }
    }

    public function enviarEmailMensagem($email, $mensagem, $assunto) {


        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'Go Talent - Tecnologia em Recrutamento');
        $this->email->to($email);
        $this->email->subject($assunto);
        $msg = "&nbsp;";
        $msg = $msg . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
        $msg = $msg . $mensagem;
        $msg = $msg . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->email->message($msg);

        $this->email->send();
    }

    function buscarNumeroRespostasPorIdProfissionalEItemChecklist($idProfissional, $idItem){
        $this->load->model('Profissional_model', 'profissional');
        $this->load->model('VideoEntrevista_model', 'videoentrevista');
        $id = $this->session->userdata("idProfissional");

        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["videoentrevista"] = $this->videoentrevista->buscarNumeroRespostasPorIdProfissionalEItemChecklist($idProfissional, $idItem);
        $this->load->vars($data);
    }

}

?>