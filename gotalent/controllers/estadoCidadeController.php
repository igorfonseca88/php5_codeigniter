<?php

class EstadoCidadeController extends CI_Controller {

    function __construct() {
        parent::__construct();

    }

    function index() {
        
    }

   function preencheCidades($idEstado) {
        $this->load->model('EstadoCidade_model', 'cidade');
        if($idEstado != ""){
            $data = $this->cidade->buscarCidadesPorIdEstado($idEstado);
        }
        $ret = "";
        $ret .= "<option value=''> Selecione </option>";
        foreach ($data as $d) {

            $ret .= "<option value='$d->id'> $d->nome </option>";
        }
        echo $ret;
        
    }


    function preencheCidadesPorUF($uf) {


        $this->load->model('EstadoCidade_model', 'cidade');
        if($uf != ""){
            $data = $this->cidade->buscarCidadesPorIdEstado($this->cidade->buscarIdEstadoPorUF($uf)[0]->id);
        }
        $ret = "";
        $ret .= "<option value=''> Selecione </option>";
        foreach ($data as $d) {

            $ret .= "<option value='$d->nome'> $d->nome </option>";
        }
        echo $ret;

    }
}

?>