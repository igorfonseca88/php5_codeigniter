<?php

class AppController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    // arquivo vagas.php na pasta appgotalent
    function buscarTodasVagas() {

    	$idProfissional = $this->input->post('idProfissional');

		$this->load->model('App_model', 'app');
		$data = $this->app->getVagas($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'vaga'=>$row->vaga,'descricao'=>$row->descricao,'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'salario'=>$row->salario,'publicacao'=>$row->datapublicacao,'recebeCurriculoFora'=>$row->recebeCurriculoFora,'empresa'=>$row->razaosocial,'candidato'=>$row->candidato,'porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    function buscarVagasPorEstado() {

    	$idProfissional = $this->input->post('idProfissional');
    	$uf = $this->input->post('uf');

		$this->load->model('App_model', 'app');
		$data = $this->app->getVagasPorEstado($idProfissional,$uf);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'vaga'=>$row->vaga,'descricao'=>$row->descricao,'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'salario'=>$row->salario,'publicacao'=>$row->datapublicacao,'recebeCurriculoFora'=>$row->recebeCurriculoFora,'empresa'=>$row->razaosocial,'candidato'=>$row->candidato,'porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo buscaNovasSkills.php na pasta appgotalent
    function buscaNovasSkills() {

    	$idProfissional = $this->input->post('idProfissional');

		$this->load->model('App_model', 'app');
		$data = $this->app->getNovasSkills($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'categoria'=>$row->categoria, 'skill'=>$row->skill);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo buscaNovaVaga.php na pasta appgotalent
    function buscaNovaVaga() {

    	$idProfissional = $this->input->post('idProfissional');
    	$idVaga = $this->input->post('idVaga');

		$this->load->model('App_model', 'app');
		$data = $this->app->getVagaNova($idVaga, $idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'vaga'=>$row->vaga, 'descricao'=>$row->descricao, 'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'tipo_profissional'=>$row->tipo_profissional,'salario'=>$row->salario, 'razao_social'=>$row->razao_social_emp, 'quantidade'=>$row->quantidade_vagas, 'informacao_adicional'=>$row->informacoes_adicionais, 'requisitos'=>$row->requisitos, 'diferenciais'=>$row->diferenciais, 'data_publicacao'=>$row->data_publicacao, 'situacao'=>$row->situacao_vaga, 'candidato'=>$row->candidato, 'recebeCurriculoFora'=>$row->recebeCurriculoFora, 'porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo buscarAreas.php na pasta appgotalent
    function buscarAreas() {
		$this->load->model('App_model', 'app');
		$data = $this->app->getAreas();
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'area'=>$row->area);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo buscarNovasSkills.php na pasta appgotalent
    function buscarNovasSkills() {//Função não utilizada, poís as Skills ainda não possuem idArea.

		$idProfissional = $this->input->post('idProfissional');
		$idArea = $this->input->post('idArea');

		$this->load->model('App_model', 'app');
		$data = $this->app->getNovasSkillsArea($idProfissional, $idArea);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'categoria'=>$row->categoria, 'skill'=>$row->skill);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo cidades.php na pasta appgotalent
    function buscarCidades() {

    	$idEstado = $this->input->post('idEstado');

		$this->load->model('App_model', 'app');
		$data = $this->app->getCidades($idEstado);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'nome'=>$row->nome);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo contadores.php na pasta appgotalent
    function buscarContadores() {

    	$idProfissional = $this->input->post('idProfissional');

		$this->load->model('App_model', 'app');
		$data = $this->app->getContadores($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('vagas'=>$row->vagas,'processos'=>$row->processos,'skills'=>$row->skills);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo dataAtual.php na pasta appgotalent
    function buscarDataAtual() {
		$dataAtual = date('Y-m-d');
		$array[] = array('data'=>$dataAtual);
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo estados.php na pasta appgotalent
    function buscarEstados() {

		$this->load->model('App_model', 'app');
		$data = $this->app->getEstados();
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'nome'=>$row->nome,'uf'=>$row->uf);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo estados.php na pasta appgotalent
    function buscarEstadosVagas() {

		$this->load->model('App_model', 'app');
		$data = $this->app->getEstadosVagas();
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'nome'=>$row->nome,'uf'=>$row->uf);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo excluirSkills.php na pasta appgotalent
    function excluirSkills() {

		$idProfissional = $this->input->post('idProfissional');
		$idSkill = $this->input->post('idSkill');

		$this->load->model('App_model', 'app');
		$data = $this->app->deleteSkills($idProfissional,$idSkill);
		$array[] = array('resultado'=>$data);
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo opiniao.php na pasta appgotalent
    function addOpiniao() {

    	$idProfissional = $this->input->post('idProfissional');
		$opiniao = $this->input->post('opiniao');
		$data = $this->input->post('data');

		$this->load->model('App_model', 'app');
		$data = $this->app->setOpiniao($idProfissional,$opiniao,$data);
		$array[] = array('resultado'=>$data);
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo profissionalDados.php na pasta appgotalent
    function buscarDadosProfissional() {

    	$idProfissional = $this->input->post('idProfissional');

		$this->load->model('App_model', 'app');
		$data = $this->app->getDadosProfissional($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('id' => $row->id, 'nome' => $row->nome, 'email' => $row->email, 'telefone' => $row->telefone, 'nascimento' => $row->nascimento, 'sexo' => $row->sexo, 'pretensaoSal' => $row->pretensao, 'cidade' => $row->cidade, 'estado' => $row->estado, 'facebook' =>  $row->facebook, 'linkedin' => $row->linkedin, 'repositorio' => $row->repositorio, 'resumo' => $row->resumo, 'foto' => $row->foto);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo profissionalEditarDados.php na pasta appgotalent
    function editDadosProfissional() {//Verificar o encode do resumo.

    	$idProfissional = $this->input->post('idProfissional');
    	$telefone = $this->input->post('telefone');
    	$nascimento = $this->input->post('nascimento');
    	$sexo = $this->input->post('sexo');
    	$pretensao = $this->input->post('pretensao');
    	$cidade = $this->input->post('cidade');
    	$estado = $this->input->post('estado');


    	$facebook = $this->input->post('facebook');
		$facebook = urldecode(urldecode($facebook));

    	$linkedin = $this->input->post('linkedin');
    	$linkedin = urldecode(urldecode($linkedin));

    	$repositorio = $this->input->post('repositorio');
    	$repositorio = urldecode(urldecode($repositorio));

    	$resumo = $this->input->post('resumo');
    	$resumo = urldecode(urldecode($resumo));
		//$resumoNew = json_decode($resumo, true);
		$this->load->model('App_model', 'app');
		$data = $this->app->editDadosProfissional($idProfissional,$telefone,$nascimento,$sexo,$pretensao,$cidade,$estado,$facebook,$linkedin,$repositorio,$resumo);
		$array[] = array('resultado' => $data);
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo profissionalEditarFoto.php na pasta appgotalent
    function editFotoProfissional() {

    	$idProfissional = $this->input->post('idProfissional');
    	$foto = $this->input->post('foto');


		$foto = str_replace('data:image/jpg;base64,', '', $foto);
		$foto = str_replace(' ', '+', $foto);
		$data = base64_decode($foto);
		$numeroRand = rand(00, 9999);
		//../../upload/profissional/
		$file = 'upload/profissional/Foto' . $idProfissional . '_'. $numeroRand . '.jpg';
		$nomeFoto = 'Foto'.$idProfissional.'_'.$numeroRand.'.jpg';
		$success = file_put_contents($file, $data);


		$this->load->model('App_model', 'app');
		$data = $this->app->editFotoProfissional($idProfissional, $nomeFoto);
		$array[] = array('resultado' => $data);
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo profissionalEditarSkills.php na pasta appgotalent
    function editSkillsProfissional() {

    	$idProfissional = $this->input->post('idProfissional');
    	$skills = $this->input->post('skills');

		$this->load->model('App_model', 'app');
		$data = $this->app->editSkillsProfissional($idProfissional,$skills);
		$array[] = array('resultado' => $data);
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo profissionalProcessos.php na pasta appgotalent
    function buscarProfissionalProcessos() {

    	$idProfissional = $this->input->post('idProfissional');

		$this->load->model('App_model', 'app');
		$data = $this->app->getProfissionalProcessos($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('vaga' => $row->vaga, 'empresa' => $row->razaosocial, 'situacao' => $row->situacao, 'data' => $row->data, 'mensagem' => $row->msgClassificar.$row->msgNaoClassificar);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo profissionalVagas.php na pasta appgotalent
    function buscarProfissionalVagas($idUsuario) {
		$this->load->model('App_model', 'app');
		$data = $this->app->getProfissionalVagas($idUsuario);
		
		foreach($data as $row)
		{
			$array[] = array('idprofissional'=>$row->idprofissional,'idvaga'=>$row->idvaga,'data'=>$row->data,'situacao'=>$row->situacao);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }


	public function enviarEmailCandidatura($idProfissional, $idVaga) {
        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

		$this->load->model('App_model', 'app');
		$data["dados_email"] = $this->app->getDadosEmailCandidatura($idProfissional, $idVaga);

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        $this->email->initialize($config);

        $vaga = $data["dados_email"][0]->vaga;
        $profissional = $data["dados_email"][0]->nomeProfissional;
        $email = $data["dados_email"][0]->emailProfissional;
        $telefone = $data["dados_email"][0]->telefone;
        $pretensao = $data["dados_email"][0]->pretensao;
        $cidade = $data["dados_email"][0]->cidade;
        $estado = $data["dados_email"][0]->estado;
        $resumo = $data["dados_email"][0]->resumo;
        $idV = $data["dados_email"][0]->idVaga;
        $emailEmpresa = $data["dados_email"][0]->emailEmpresa;

        $this->email->from('contato@gotalent.com.br', 'Go Talent');
        $this->email->to($emailEmpresa);
        $this->email->subject("Novo Candidato");

        $mensagem = "&nbsp;";
        $mensagem .= "<img src='http://gotalent.com.br/_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
        $mensagem .= "<p>Novo candidato à vaga " . $vaga . "</p>";
                    
        $mensagem .= "<p><b>Dados do profissional</b></p>";
        $mensagem .= "<p><b style='text-transform:uppercase'> " . $profissional . "</b></p>";
        $mensagem .= "<p>E-mail: " . $email . "</p>";
        $mensagem .= "<p>Telefone: " . $telefone . "</p>";
        $mensagem .= "<p>Pretensão salarial de R$ " . $pretensao  . "</p>";
        $mensagem .= "<p>Cidade/Estado: " . $cidade . "/". $estado ."</p>";
        $mensagem .= "<p>Resumo: " . $resumo."</p>";
        $mensagem .= "<a style='background-color:#8cc63f; width: 300px; height: 50px; "
        . "line-height: 50px;font-size: 30px;  border-color: #8cc63f; color: #FFFFFF; padding: 10px 20px; border-radius: 5px; text-decoration: none;' "
        . " href='http://gotalent.com.br/empresaController/vagaCandidatos/".  $idV ."'>VEJA MAIS</a>";
        $mensagem .= "<p>Estamos a disposição para o que for preciso!!</p>";
        $mensagem .= "<p><b>Go Talent </b></p>";
        $mensagem .= "<br /><br /><img src='http://gotalent.com.br/_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->email->message($mensagem);
        $this->email->send();
    }

    // arquivo profissionalVagasCandidatura.php na pasta appgotalent
    function setProfissionalVagasCandidatura() {

		$idProfissional = $this->input->post('idProfissional');
		$idVaga = $this->input->post('idVaga');

		$this->load->model('App_model', 'app');
		$data = $this->app->setProfissionalVagasCandidatura($idProfissional, $idVaga);
		
		$array[] = array('resultado' => $data);
		$json["output"] = $array;

		$this->enviarEmailCandidatura($idProfissional, $idVaga);

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

	// profissionalVagasCandidaturaMapa
    function setProfissionalVagasCandidaturaMapa($lat,$lng,$idProfissional,$idVaga) {
		$this->load->model('App_model', 'app');
		$data = $this->app->setProfissionalVagasCandidatura($idProfissional,$idVaga);

		if($data == true){
			$var["output"] = "<br /><h2><img src='".base_url()."img/mapa/certo.png' width='20px' height='20px' /><font color='white'> Candidatou-se com sucesso.</font></h2><a class='link_vaga' href='".base_url()."mapaController/mapaApp/".$idProfissional."/".$lat."/".$lng."'>Voltar</a>";
		}else{
			$var["output"] = "<br /><h2><font color='white'>Erro ao candidatar-se, tente novamente.</font></h2><a class='link_vaga' href='".base_url()."mapaController/mapaApp/".$idProfissional."/".$lat."/".$lng."'>Voltar</a>";
		}

		$this->enviarEmailCandidatura($idProfissional, $idVaga);

		$this->load->vars($var);
		$this->load->view("priv/app/candidaturaMapaView");
    }

    // arquivo skills.php na pasta appgotalent
    function buscarSkillsProfissional() {

    	$idProfissional = $this->input->post("idProfissional");

		$this->load->model('App_model', 'app');
		$data = $this->app->getSkillsProfissional($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('nome' => $row->skill,'id' => $row->id);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo skills.php na pasta appgotalent
    function buscarTodasSkillsVagas() {
		$this->load->model('App_model', 'app');
		$data = $this->app->getTodasSkillsVagas();
		
		foreach($data as $row)
		{
			$array[] = array('nome' => $row->skill,'id' => $row->id);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo usuarios.php na pasta appgotalent
    function buscarUsuarios() {

    	$login = $this->input->post('login');
		$senha = $this->input->post('senha');
		
		$this->load->model('App_model', 'app');
		$data = $this->app->getUsuarios($login, $senha);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->idUsuario,'email'=>$row->login,'senha'=>$row->senha,'idProfissional'=>$row->idProfissional,'idEmpresa'=>$row->idEmpresa,'idPlano'=>$row->idPlano, 'idArea'=>$row->idArea, 'cidade'=>$row->cidade, 'estado'=>$row->estado);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo vagasNovas.php na pasta appgotalent
    function buscarVagasNovas($idProfissional, $dataReferencia) {
		$this->load->model('App_model', 'app');
		$data = $this->app->getVagasNovas($idProfissional, $dataReferencia);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'dataPublicacao'=>$row->datapublicacao, 'porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo vagasPorArea.php na pasta appgotalent
    function buscarVagasPorArea() {
    	$idProfissional = $this->input->post('idProfissional');
    	$idArea = $this->input->post('idArea');

		$this->load->model('App_model', 'app');
		$data = $this->app->getVagasPorArea($idProfissional, $idArea);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'vaga'=>$row->vaga,'descricao'=>$row->descricao,'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'salario'=>$row->salario,'publicacao'=>$row->datapublicacao,'recebeCurriculoFora'=>$row->recebeCurriculoFora,'empresa'=>$row->razaosocial,'candidato'=>$row->candidato,'porcentagem'=>$row->porcentagem, 'area'=>$row->idArea);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo vagasPorSkills.php na pasta appgotalent
    function buscarVagasPorSkills() {

    	$idProfissional = $this->input->post('idProfissional');
		$idSkill = $this->input->post('idSkill');
		$idArea = $this->input->post('idArea');

		$this->load->model('App_model', 'app');
		$data = $this->app->getVagasPorSkills($idProfissional, $idSkill, $idArea);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'vaga'=>$row->vaga,'descricao'=>$row->descricao,'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'salario'=>$row->salario,'publicacao'=>$row->datapublicacao,'recebeCurriculoFora'=>$row->recebeCurriculoFora,'empresa'=>$row->razaosocial,'candidato'=>$row->candidato,'porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo vagasSkill.php na pasta appgotalent
    function buscarVagasSkills($idProfissional,$idSkill) {
		$this->load->model('App_model', 'app');
		$data = $this->app->getVagasSkills($idProfissional,$idSkill);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id,'vaga'=>$row->vaga,'descricao'=>$row->descricao,'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'salario'=>$row->salario,'publicacao'=>$row->datapublicacao,'recebeCurriculoFora'=>$row->recebeCurriculoFora,'empresa'=>$row->razaosocial,'candidato'=>$row->candidato,'porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo validaOpiniao.php na pasta appgotalent
    function buscarValidaOpiniao() {

    	$idProfissional = $this->input->post('idProfissional');

		$this->load->model('App_model', 'app');
		$data = $this->app->getValidaOpiniao($idProfissional);
		
		foreach($data as $row)
		{
			$array[] = array('dias'=>$row->dias);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo verificaNotificacoes.php na pasta appgotalent
    function buscarVerificaNotificacoes() {
    	
    	$idProfissional = $this->input->post('idProfissional');
		$dataReferencia = $this->input->post('dataReferencia');

		$this->load->model('App_model', 'app');
		$data = $this->app->getVerificaNotificacoes($idProfissional, $dataReferencia);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'dataPublicacao'=>$row->datapublicacao, 'porcentagem'=>$row->porcentagem, 'vaga'=>$row->vaga, 'descricao'=>$row->descricao, 'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'tipo_profissional'=>$row->tipo_profissional,'salario'=>$row->salario, 'razao_social'=>$row->razao_social_emp, 'quantidade'=>$row->quantidade_vagas, 'informacao_adicional'=>$row->informacoes_adicionais, 'requisitos'=>$row->requisitos, 'diferenciais'=>$row->diferenciais, 'situacao'=>$row->situacao_vaga, 'tipo'=>'vaga','porcentagem'=>$row->porcentagem);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo notificacao.php na pasta appgotalent
    function buscarNovasVagasNotificacao() { // Testar

    	$idVagas= $this->input->post('idVagas');

		$this->load->model('App_model', 'app');
		$data = $this->app->getNovasVagasNotificacao($idVagas);
		
		foreach($data as $row)
		{
			$array[] = array('id'=>$row->id, 'vaga'=>$row->vaga, 'descricao'=>$row->descricao, 'cidade'=>$row->cidade,'estado'=>$row->estado,'tipo'=>$row->tipo,'tipo_profissional'=>$row->tipo_profissional,'salario'=>$row->salario, 'razao_social'=>$row->razao_social_emp, 'quantidade'=>$row->quantidade_vagas, 'informacao_adicional'=>$row->informacoes_adicionais, 'requisitos'=>$row->requisitos, 'diferenciais'=>$row->diferenciais, 'data_publicacao'=>$row->data_publicacao, 'situacao'=>$row->situacao_vaga, 'tipo'=>'vaga');
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

    // arquivo gotalentplus.php na pasta appgotalent
    function buscarClubeGoTalent() {
		$this->load->view("priv/app/gotalentplus");
    }

    function buscarPalavrasChaveNotificacoes() {
    	
		$this->load->model('App_model', 'app');
		$data = $this->app->getPalavrasChaveNotificacao();
		
		foreach($data as $row)
		{
			$array[] = array('palavraChave'=>$row->palavraChave);
		}
		$json["output"] = $array;

		$this->load->vars($json);
		$this->load->view("priv/app/output");
    }

}
?>