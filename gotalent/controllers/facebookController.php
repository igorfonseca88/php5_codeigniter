<?php
session_start();
require_once 'src/Facebook/autoload.php';

class FacebookController extends CI_Controller
{
    function entrarFacebook()
    {
        $param = $this->input->post("data");
        $json_decode = json_decode($param);

        $existe = $this->Usuario_model->buscarUsuarioPorEmail($json_decode->email);
        if (count($existe) == 0) {
            $this->cadastroProfissionalFacebook($json_decode);
        } else {
            $this->autenticar($json_decode->email, $json_decode->id);
        }
    }

        function validaUsuario($profile, $picture)
    {
        $this->load->model("Usuario_model", "usuario");
        $insert = array(
            "dataCriacao" => date("Y-m-d"),
            "login" => $profile['email'],
            "senha" => $profile['id'],
            "nome" => $profile['name'],
            "tipo" => "Profissional",
            "situacao" => "ATIVO"
        );

        if ($this->usuario->add_user($insert) > 0) {
            $data["sucesso"] = "Salvo com sucesso.";
        } else {
            $data["error"] = "Erro ao salvar.";
        }
    }

    public function cadastroProfissionalFacebook($profile)
    {
        $senhaTeste = $profile->id;
        // cria na tabela de profissional o cadastro.. cria o usuário e joga para tela de complementação do cadastro
        $this->load->model("Profissional_model", "profissional");

        // busca profissional pelo e-mail, caso já exista devolve erro
        $existe = $this->Usuario_model->buscarUsuarioPorEmail($profile->email);
        if (count($existe) == 0) {

            $this->load->model("Plano_model", "plano");
            $plano = $this->plano->buscarPlanoProfissionalPorId(1);

            $this->load->model("Estadocidade_model", "estadocidade");

            $estadoId = "";
            $cidadeId = "";
            if (isset($profile->location)) {

                $arrCidadeEstado = explode(",", $profile->location->name);
                $estadoId = $this->estadocidade->buscarEstadoPorNome(trim($arrCidadeEstado[1]));
                $cidadeId = $this->estadocidade->buscarCidadePorNome(trim($arrCidadeEstado[0]), $estadoId[0]->id);
            }

            $insert = array(
                "dataCadastro" => date("Y-m-d"),
                "nome" => $profile->name,
                "email" => $profile->email,
                "telefone" => "",
                "idPlano" => $plano[0]->id,
                "nascimento" => date("Y-m-d", strtotime($profile->birthday)),
                "facebook" => $profile->link,
                "valorPago" => 0,
                "cidade" => $cidadeId[0]->id,
                "estado" => $estadoId[0]->id,
                "idArea" => "2",
                "foto" => $profile->picture->data->url,
                "sexo" => ($profile->gender == "male") ? "Masculino" : (($profile->gender == "female") ? "Feminino" : $profile->gender),
            );

            $id = $this->profissional->add_record($insert);

            if (count($profile->work) > 0) {
                $this->experiencia($id, $profile);
            }
            if (count($profile->education) > 0) {
                $this->experienciaEducacional($id, $profile);
            }

            if (isset($id)) {
                // cria usuario
                $this->load->model('Usuario_model');

                $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $senhaTeste); //remove palavras que contenham a sintaxe sql
                $seg = trim($seg); //limpa espaços vazios
                $seg = strip_tags($seg); // tira tags html e php
                $seg = addslashes($seg); //adiciona barras invertidas a uma string

                $insertUser = array(
                    "dataCriacao" => date("Y-m-d"),
                    "login" => $profile->email,
                    "tipo" => "Profissional",
                    "senha" => $senhaTeste,
                    "situacao" => "ATIVO",
                    "idProfissional" => $id,
                    "nome" => $profile->name
                );

                $idUser = $this->Usuario_model->add_record($insertUser);
                $this->enviarEmailProfissional($profile, $id);
                $this->session->set_flashdata('sucesso', 'Cadastro efetuado com sucesso! Confirme o seu cadastro através do e-mail que enviamos para ' . $profile->email);
                $this->entrarFacebook();
            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.');
            }
        } else {
            $this->session->set_flashdata('error', 'O e-mail utilizado já está cadastrado em nossa base.');
            redirect('cadastro-profissional-facebook');
        }
    }

    public function enviarEmailProfissional($profile, $id)
    {
        // email profissional
        $mensagem = "&nbsp;";
        $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
        $mensagem = $mensagem . "<h2>Olá, " . $profile->name . "</h2>";
        $mensagem = $mensagem . "<p>A Go Talent te da boas vindas!</p>";
        $mensagem = $mensagem . "<p><b><a href=" . base_url() . "confirmar-cadastro/" . $id . ">Clique aqui</a> para confirmar o seu cadastro e ter acesso ao nosso portal </b>.</p><br /><br />";
        $mensagem = $mensagem . "<p>Após confirmar, você poderá criar seu currículo e ter a possibilidade de encontrar a melhor oportunidade profissional!</p>";
        $mensagem = $mensagem . "<p><b>Sugerimos os seguintes passos: </b></p>";
        $mensagem = $mensagem . "<p>- Cadastre os seus dados pessoais (nome completo, telefone, cidade, estado, etc...);</p>";
        $mensagem = $mensagem . "<p>- Fala upload de uma foto;</p>";
        $mensagem = $mensagem . "<p>- Informe as SKILLS (Ex: PHP, Javascript, etc...);</p>";
        $mensagem = $mensagem . "<p>- Preencha os dados de experiências profissionais e formações escolares;</p>";
        $mensagem = $mensagem . "<p>- Candidate-se as vagas de interesse e principalmente as mais compatíveis com o seu perfil;</p>";
        $mensagem = $mensagem . "<p>- Aguarde o contato da Empresas e BOA SORTE!!</p><br>";

        $mensagem = $mensagem . "<p>Estamos a disposição para o que precisar!</p>";
        $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";
        $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;
        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'GoTalent');
        $this->email->to($profile->email);
        $this->email->subject('Go Talent - Cadastro realizado com sucesso!');
        $this->email->message($mensagem);
        $this->email->send();

        // BUSCA NA TABELA DE PRE CADASTRO E CASO ENCONTRE, MARCA COMO FINALIZADO
        $existePreCad = $this->profissional->existePreCadastroPorEmail($profile->email);
        if (count($existePreCad) > 0) {
            $update = array(
                "finalizou" => 'S'
            );
            $this->profissional->updatePreCadastro($profile->email, $update);
        }
    }

    public function cadastroCandidatoFacebook()
    {
        $acessoAPP = false;
        if (isset($_GET["acessoAPP"])) {
            $acessoAPP = $_GET["acessoAPP"];
        }
        $this->load->model("Area_model", "area");
        $data["areas"] = $this->area->buscarTodasAreas();
        $data["acessoAPP"] = $acessoAPP;
        $this->load->vars($data);
        $this->load->view('_paginas/cadastro-profissional-facebook');
    }

    function autenticar($email, $senha)
    {
        $error = $this->validate($email, $senha);
        if ($error == 1) {
            echo "1";
            $error = array('error' => 'Usuário inativo.');
            $this->load->vars($error);
            $this->load->view('priv/login/login_view');
        }
        if ($error == 2) {
            echo "2";
            $error = array('error' => 'Usuário inválido.');
            $this->load->vars($error);
            $this->load->view('priv/login/login_view');
        }
        if ($error == 3) {
            echo "3";
            $error = array('error' => 'Seu perfil está suspenso. <a href="' . base_url() . 'principal/reativar">Reative aqui</a>.');
            $this->load->vars($error);
            $this->load->view('priv/login/login_view');
        }
    }

    function validate($email, $senha)
    {
        $this->load->model('Usuario_model');
        $this->Usuario_model->setLogin($email);
        $this->Usuario_model->setSenha($senha);
        $usuarios = $this->Usuario_model->validate();

        if ($usuarios) {
            foreach ($usuarios as $row) {
                $this->Usuario_model->setIdUsuario($row->idUsuario);
                $this->Usuario_model->setSituacao($row->situacao);
                $this->Usuario_model->setNome($row->nome);
                $this->Usuario_model->setIdProfissional($row->idProfissional);
                $this->Usuario_model->setTipo($row->tipo);
                $this->Usuario_model->setIdEmpresa($row->idEmpresa);
                $this->Usuario_model->setIdPlano($row->idPlano);
                $this->Usuario_model->setTipoPlano($row->tipoPlano);
                $this->Usuario_model->setDataFinalPlano($row->dataFinalPlano);
            }
            if ($this->Usuario_model->getSituacao() == "SUSPENSO") {
                return 3;
            }
            if ($this->Usuario_model->getSituacao() != "ATIVO") {
                return 1;
            }
            $data = array(
                'session_id' => $this->Usuario_model->getIdUsuario(),
                'idUsuario' => $this->Usuario_model->getIdUsuario(),
                'login' => $this->Usuario_model->getLogin(),
                'senha' => $this->Usuario_model->getSenha(),
                'nome' => $this->Usuario_model->getNome(),
                'idProfissional' => $this->Usuario_model->getIdProfissional(),
                'tipo' => $this->Usuario_model->getTipo(),
                'idEmpresa' => $this->Usuario_model->getIdEmpresa(),
                'idPlano' => $this->Usuario_model->getIdPlano(),
                'tipoPlano' => $this->Usuario_model->getTipoPlano(),
                'logged' => true
            );
            $this->session->set_userdata($data);

            $updateDataAcesso = array("ultimoAcesso" => date("Y-m-d"));
            $this->Usuario_model->update($this->Usuario_model->getIdUsuario(), $updateDataAcesso);

            if ($this->Usuario_model->getTipo() == 'Profissional') {
                redirect('area-restrita');
            } else if ($this->session->userdata("tipo") == 'Administrador' || $this->session->userdata("tipo") == 'Conteudo') {
                $_SESSION["test"] = true;
                redirect('principal/arearestritaadmin');
            } else if ($this->Usuario_model->getTipo() == 'Empresa') {
                redirect('area-restrita-empresa');
            }
        } else {
            return 2;
        }
    }

    function experiencia($id, $profile)
    {
        $lenght = count($profile->work);

        for ($i = 0; $i < $lenght; $i++) {
            $dataInicio = "";
            $dataFim = "";
            $empregoAtual = 0;

            if ($profile->work[$i]->start_date != "" && $profile->work[$i]->start_date != "0000-00" && $profile->work[0]->start_date != "0000-00-00") {
                $dataInicio = $profile->work[$i]->start_date;
                if ($profile->work[$i]->end_date == "" && $profile->work[$i]->end_date == "0000-00" && $profile->work[0]->end_date == "0000-00-00") {
                    $dataFim = $profile->work[$i]->end_date;
                } else {
                    $empregoAtual = 1;
                }

                $this->load->model("Profissional_model", "profissional");
                $insert = array(
                    "empresa" => $profile->work[$i]->employer->name,
                    "cargo" => $profile->work[$i]->position->name,
                    "dataInicio" => $dataInicio,
                    "dataFim" => $dataFim,
                    "empregoAtual" => $empregoAtual,
                    "descricaoAtividade" => $profile->work[$i]->description,
                    "idProfissional" => $id
                );
                $this->profissional->add_experiencia($insert);
            }
        }
    }

    function experienciaEducacional($id, $profile)
    {
        $lenght = count($profile->education);
        for ($i = 0; $i < $lenght; $i++) {
            $curso = "";
            $tipoFormacao = "";
            if ($profile->education[$i]->type == "High School") {
                $tipoFormacao = "Ensino Médio";
                $curso = "Ensino Médio";
            } else {
                if ($profile->education[$i]->type == "College") {
                    $tipoFormacao = "Graduação";
                    //Tratamento para caso o nome do curso esteja vazio
                    if (isset($profile->education[$i]->concentration[0]->name)) {
                        $curso = $profile->education[$i]->concentration[0]->name;
                    } else {
                        continue;
                    }
                } else {
                    if ($profile->education[$i]->type == "Graduate School") {
                        $tipoFormacao = "Especialização";
                        //Tratamento para caso o nome do curso esteja vazio
                        if (isset($profile->education[$i]->degree->name)) {
                            $curso = $profile->education[$i]->degree->name;
                        } else {
                            continue;
                        }
                    }
                }
            }
            $insert = array(
                "instituicao" => $profile->education[$i]->school->name,
                "formacao" => $tipoFormacao,
                "dataInicio" => "",
                "dataFim" => $profile->education[$i]->year->name . "-00-00",
                "curso" => $curso,
                "idProfissional" => $id);
            $this->load->model("Profissional_model", "profissional");
            $this->profissional->add_formacao($insert);
        }
    }
}