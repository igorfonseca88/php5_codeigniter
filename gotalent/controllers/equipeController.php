<?php

class EquipeController extends CI_Controller {

    function __construct() {
        parent::__construct();
		if($this->session->userdata("tipo") != "Administrador"){
			redirect('/');
		}
    }

    function index() {
		$this->load->model('Equipe_model', 'equipe');
		$data["equipes"] = $this->equipe->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/administrador/site/equipe/list");
    }
    
	function editAction($id, $mensagem) {
		$this->load->model('Equipe_model', 'equipe');
		$data["equipe"] = $this->equipe->buscarPorId($id);
	
		$data["error"] = $mensagem["erro"];
		$data["sucesso"] = $mensagem["sucesso"];
		
		$this->load->vars($data);
		$this->load->view("priv/administrador/site/equipe/edit");
	}

    function edit() {
        $this->load->model("Equipe_model", "equipe");
        $id = $this->input->post("id");
		
        $update = array(
            "nome" => $this->input->post("nome"),
            "cargo" => $this->input->post("cargo"),
           
            "texto" => $this->input->post("texto")
           
        );

        if ($this->equipe->update($id, $update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$this->editAction($id, $data);
        }
    }
	
	function addGaleria($id) {
        $this->load->model("Equipe_model", "equipe");
		$delete["delete"] = $this->equipe->buscarPorId($id);
		
		if ($delete["delete"][0]->imagem) {		
			$filename = "./upload/equipe/" . $delete["delete"][0]->imagem;
			unlink($filename);
		}
		
		$config["upload_path"] = "./upload/equipe/";
		$config["allowed_types"] = "gif|jpg|png";
		$config["file_name"] = "equipe" . $id . "_" . rand(00, 9999);
		$config["overwrite"] = TRUE;
		$config["remove_spaces"] = TRUE;
		$this->load->library("upload", $config);

        if ($this->upload->do_upload()) {	            
	        $update = array(
				"imagem" => $this->upload->file_name
			);			
			if ($this->equipe->update($id, $update) > 0) {
				$data["sucesso"] = "Imagem salva com sucesso.";
			} else {
	        	$data["erro"] = "Erro ao salvar imagem no banco de dados.";
	        }
        } else {
            $data["erro"] = $this->upload->display_errors();
        }
		$this->editAction($id, $data);
    }
		
	function deleteGaleria($id) {
        $this->load->model("Equipe_model", "equipe");
		$delete["delete"] = $this->equipe->buscarPorId($id);            
		$update = array(
			"imagem" => null
		);
		
		if ($this->equipe->update($id, $update) > 0) {
			$filename = "./upload/equipe/" . $delete["delete"][0]->imagem;
			unlink($filename);
			$data["sucesso"] = "Imagem removida com sucesso.";
		}
		
		$this->editAction($id, $data);
	}
}
?>