<?php

class NotificacaoController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function enviarEmailMkt() {
        $this->load->view('priv/administrador/emailMarketing');
    }

    public function notificarVagas() {
        
        $arrayProfissionais ="7,16";
           
        
        
        $this->load->model('Profissional_model', 'profissional');
        $profissionais = $this->profissional->buscarProfissionaisParaNotificacaoVagas('MS');
        echo count($profissionais);
       

        foreach($profissionais as $profissional){
            
            $this->load->model('Vaga_model', 'vaga');
            $vagas = $this->vaga->retornaVagasComCompatibilidadePorIdProfissionalParaNotificacao($profissional->id);

            if(count($vagas) == 0){
                continue;
            }
            $this->load->library('email');

            $config['smtp_host'] = 'mail.gotalent.com.br';
            $config['smtp_user'] = 'noreply@gotalent.com.br';
            $config['smtp_pass'] = 'admingo1';
            $config['smtp_port'] = 587;

            $this->email->initialize($config);

            $this->email->from('noreply@gotalent.com.br', 'Vagas de Emprego na Go Talent');
            $this->email->to($profissional->email);
           
            $this->email->subject("Oportunidades para você!");
            $msg = "&nbsp;";
            $msgVagas = "";
            $msgTopo = ' <table style="background:#fff;border: 1px solid #e9e9e9;" width="620" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                    <td>

                                            <table style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;margin-top:10px" width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                            <td><img class="img-responsive" src="http://www.gotalent.com.br/_imagens/email-topo.png" alt="Go Talent" width="600" style="display:block;border:none" /></td>
                                                    </tr>
                                                    <tr>
                                                            <td><h3 style="margin:30px 10px 0;color: #808080;font-size:18px;font-weight:300">%NOME%, temos vagas para o seu perfil!</h3></td>
                                                    </tr> ';
            $msgTopo = str_replace("%NOME%",$profissional->nome,$msgTopo);
            foreach ($vagas as $vaga){
            $msgVagas .= '<tr>
                                <td>
                                        <table style="border: 1px solid #e9e9e9;margin-top:30px" width="580" align="center" cellpadding="5" cellspacing="5" border="0">
                                                <tr>
                                                        
                                                        <td width="480">
                                                                <h2 style="margin:0;font-weight: 300"><a style="text-decoration: none;color: #1E89AF;font-size: 18px;" href="'.base_url().'vaga/'.$vaga->id.'-'.$this->url(str_replace(" ", "-", $vaga->vaga)).'">'.$vaga->vaga.'</a></h2>
                                                                <p style="color: #808080;  font-weight: 300;  margin: 5px 0;  font-size: 14px;"><strong>Local:</strong> '.$vaga->cidade.'/'.$vaga->estado.' </p>
                                                                <p style="color: #808080;  font-weight: 300;  margin: 5px 0;  font-size: 14px;"><strong>Salário:</strong> '.$vaga->salario.'</p>
                                                                <p style="color: #808080;  font-weight: 300;  margin: 5px 0;  font-size: 14px;"><strong>Regime de contratação:</strong> '.$vaga->tipo .'</p>
                                                        </td>
                                                        <td width="100"><a href="'.base_url().'vaga/'.$vaga->id.'-'.$this->url(str_replace(" ", "-", $vaga->vaga)).'"><img src="http://www.gotalent.com.br/_imagens/btn-maisdetalhes.jpg" alt="Mais detalhes" style="display:block;border:none" /></a></td>
                                                </tr>
                                        </table>
                                </td>
                        </tr>';
            }

            $msgFooter = ' <tr>
                              <td> <p style="color: #808080;  font-weight: 300;  margin:30px 10px 10px;  font-size: 14px;">Ainda não encontrou o que procurava? Temos mais anúncios em nosso portal.</p> </td>
                         </tr>
                            <tr>
                                    <td align="center"><a href="http://www.gotalent.com.br/vagas"><img src="http://www.gotalent.com.br/_imagens/btn-maisvagas.jpg" alt="Veja Mais Vagas" style="display:block;border:none" /></a><br /><br /></td>
                            </tr>
                            <tr>
                                    <td><img class="img-responsive" src="http://www.gotalent.com.br/_imagens/email-rodape.png" width="600" style="display:block;border:none;margin-bottom:10px" /></td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                    </table>
                    <table style="background:#fff;" width="620" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr><td align="center"><p style="color: #808080;  font-weight: 300;  margin: 10px 0;  font-size: 11px;font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif">
                            Siga a Go Talent nas redes sociais: <a target="_blank" href="http://www.twitter.com/portalgotalent" style="color:#1E89AF">Twitter</a>  | <a target="_blank" href="http://www.facebook.com/portalgotalent" style="color:#1E89AF">Facebook</a> </p></td></tr>
                    </table>
                    ';
            $msg .= $msgTopo.$msgVagas.$msgFooter;
           echo $profissional->email."<br>";
            $this->email->message($msg);

            if ($this->email->send()) {
               // redirect('administradorController/enviarEmailMkt');
            } else {
               echo $this->email->print_debugger();
            }
        }
        exit;
    }
    
function url($str) {
    $str = strtolower(utf8_decode($str));
    $i = 1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
    while ($i > 0)
        $str = str_replace('--', '-', $str, $i);
    if (substr($str, -1) == '-')
        $str = substr($str, 0, -1);
    return $str;
}

}

?>