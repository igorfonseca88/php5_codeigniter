<?php

class PostagensController extends CI_Controller {
	
    function __construct() {
        parent::__construct();
		
		if($this->session->userdata("tipo") != "Administrador" && $this->session->userdata("tipo") != "Conteudo"){
			redirect('/');
		}
    }
	
    function index() {
	
		$this->load->model('Postagem_model', 'postagem');
		$data["postagens"] = $this->postagem->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/blog/postagem/list");
    }
    
    function addAction() {
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/blog/postagem/add");
    }
	
    function add() {
		$this->load->model('Postagem_model', 'postagem');
		$insert = array(
            "idcategoria" => $this->input->post("idcategoria"),
            "titulo" => $this->input->post("titulo"),
            "descricao" => $this->input->post("descricao"),
            "texto" => $this->input->post("texto"),
            "tags" => $this->input->post("tags"),
            "data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"urlPost" => $this->input->post("urlPost"),
            "status" => $this->input->post("status")
        );
		$id = $this->postagem->add_record($insert);
        if ($id > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$this->editAction($id, $data);
        }
    }
    
	function editAction($id, $mensagem) {
		$this->load->model('Postagem_model', 'postagem');
		$data["postagem"] = $this->postagem->buscarPorId($id);
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Postagemgaleria_model', 'galeria');
		$data["galeria"] = $this->galeria->buscarPorIdPostagem($id);
		
		$data["error"] = $mensagem["erro"];
		$data["sucesso"] = $mensagem["sucesso"];
		
		$this->load->vars($data);
		$this->load->view("priv/blog/postagem/edit");
	}
	
    function edit() {
        $this->load->model("Postagem_model", "postagem");
        $id = $this->input->post("id");
		
        $update = array( 
            "idcategoria" => $this->input->post("idcategoria"),
            "titulo" => $this->input->post("titulo"),
            "descricao" => $this->input->post("descricao"),
            "texto" => $this->input->post("texto"),
            "tags" => $this->input->post("tags"),
            "data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"urlPost" => $this->input->post("urlPost"),
            "status" => $this->input->post("status")
        );
		
        if ($this->postagem->update($id, $update) > 0) {
			
			$this->session->set_flashdata('sucesso','Salvo com sucesso.');
			redirect('postagensController/editAction/'.$id);
        }
    }
	
	function delete($id) {
		//Delete galeria
        $this->load->model("Postagemgaleria_model", "galeria");
		$delete = $this->galeria->buscarPorIdPostagem($id); 		
		foreach ($delete as $row) { unlink("./upload/blog/" . $row->imagem); }
		
		//Delete postagem
        $this->load->model("Postagem_model", "postagem");
		if ($this->postagem->delete($id)) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
	        $data["erro"] = "Erro ao excluir.";
	    }
		
		//Seleciona lista atualizada de todos os postagens
		$data["postagens"] = $this->postagem->getAll();
		
		$this->load->vars($data);		
		$this->load->view("priv/blog/postagem/list");
	}
	
    function imagemPrincipal($id) {
		//Busca galeria a editar
        $this->load->model("Postagemgaleria_model", "galeria");
		$galeria["galeria"] = $this->galeria->buscarPorId($id);
		
		//Remove imagem principal deste postagem
		$this->galeria->removerImagemPrincipalPorPostagem($galeria["galeria"][0]->idPostagem);
				
        $update = array( 
            "principal" => "SIM"
        );		
        if ($this->galeria->update($id, $update) > 0) {
			
			$this->session->set_flashdata('sucesso','Imagem principal alterada.');
			redirect('postagensController/editAction/'.$galeria["galeria"][0]->idPostagem);
			
        }
    }
	
	function addGaleria($id) {
        $this->load->model("Postagemgaleria_model", "galeria");
		
		$config["upload_path"] = "./upload/blog/";
		$config["allowed_types"] = "gif|jpg|png";
		$config["file_name"] = "blog" . $id . "_" . rand(00, 9999);
		$config["overwrite"] = TRUE;
		$config["remove_spaces"] = TRUE;
		$this->load->library("upload", $config);
		
        if ($this->upload->do_upload()) {	            
	        $insert = array(
				"idpostagem" => $id,
				"imagem" => $this->upload->file_name
			);			
			if ($this->galeria->add_record($insert) > 0) {
				$this->session->set_flashdata('sucesso','Imagem salva com sucesso.');
			} else {
	        	$this->session->set_flashdata('error','Erro ao salvar imagem no banco de dados.');
	        }
        } else {
           $this->session->set_flashdata('error',$this->upload->display_errors());
        }
		//$this->editAction($id, $data);
		redirect('postagensController/editAction/'.$id);
		
    }
	
	function deleteGaleria($id) {
        $this->load->model("Postagemgaleria_model", "galeria");
		$delete = $this->galeria->buscarPorId($id);
		
		if ($this->galeria->delete($id) > 0) {
			$filename = "./upload/blog/" . $delete[0]->imagem;
			unlink($filename);
			$this->session->set_flashdata('sucesso','Imagem removida com sucesso.');
		}
		redirect('postagensController/editAction/'.$delete[0]->idPostagem);
	}
}
?>