<?php
session_start();

class Login extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	
		if ($this->Usuario_model->logged() == TRUE && $this->Usuario_model->usuarioExiste() == TRUE  ) { 
			if($this->session->userdata("tipo") == 'Profissional'){
				redirect('area-restrita'); 
			}
			else if($this->session->userdata("tipo") == 'Administrador' || $this->session->userdata("tipo") == 'Conteudo'){
				redirect('principal/arearestritaadmin'); 
			}
			else if($this->session->userdata("tipo") == 'Empresa'){
				redirect('area-restrita-empresa'); 
			}
			else{
				$this->load->view('priv/login/login_view');
			}
		}
		
		$this->load->view('priv/login/login_view');
	}
	
	function erroUsuarioInvalido(){
		$data["error"] = "Usuário inválido.";
		$this->load->vars($error);
		$this->load->view('priv/login/login_view');
	}
	
	function autenticar() {
		$this->form_validation->set_rules('usuarioEmail', 'Usuario', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$error = array('error' => 'Usuário e senha obrigatórios.');
			$this->load->vars($error);
			$this->load->view('priv/login/login_view');
		} else {
			$error = $this->validate();
		
			if($error == 1){
				$error = array('error' => 'Usuário inativo.');
				$this->load->vars($error);
			}
			if($error == 2){
				$error = array('error' => 'Usuário inválido.');
				$this->load->vars($error);
			}
			if($error == 3){
				$error = array('error' =>'Seu perfil está suspenso. <a href="'.base_url().'principal/reativar">Reative aqui</a>.');
				$this->load->vars($error);
			}
				
			$this->load->view('priv/login/login_view');
		}
	}

	function autenticarFacebook() {
		$this->form_validation->set_rules('usuarioEmail', 'Usuario', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');

		if ($this->form_validation->run() == FALSE) {
			$error = array('error' => 'Usuário e senha obrigatórios.');
			$this->load->vars($error);
			$this->load->view('priv/login/login_view');
		} else {
			$error = $this->validate();

			if($error == 1){
				$error = array('error' => 'Usuário inativo.');
				$this->load->vars($error);
			}
			if($error == 2){
				$error = array('error' => 'Usuário inválido.');
				$this->load->vars($error);
			}
			if($error == 3){
				$error = array('error' =>'Seu perfil está suspenso. <a href="'.base_url().'principal/reativar">Reative aqui</a>.');
				$this->load->vars($error);
			}

			$this->load->view('priv/login/login_view');
		}
	}
	
	function validate() {
		$this->load->model('Usuario_model');
		$this->Usuario_model->setLogin($this->input->post("usuarioEmail"));
		$this->Usuario_model->setSenha($this->input->post("senha"));	
		
		$usuarios = $this->Usuario_model->validate();
		
		if ($usuarios) {	
			foreach ($usuarios as $row) {
				$this->Usuario_model->setIdUsuario($row->idUsuario);
				$this->Usuario_model->setSituacao($row->situacao);
				$this->Usuario_model->setNome($row->nome);
				$this->Usuario_model->setIdProfissional($row->idProfissional);
				$this->Usuario_model->setTipo($row->tipo);
				$this->Usuario_model->setIdEmpresa($row->idEmpresa);
				$this->Usuario_model->setIdPlano($row->idPlano);
				$this->Usuario_model->setTipoPlano($row->tipoPlano);
				$this->Usuario_model->setDataFinalPlano($row->dataFinalPlano);
			}
			if ($this->Usuario_model->getSituacao() == "SUSPENSO") {
				return 3;
			}
			if ($this->Usuario_model->getSituacao() != "ATIVO") {
				return 1;
			}
	
			$data = array(
				'session_id' => $this->Usuario_model->getIdUsuario(),
				'idUsuario' => $this->Usuario_model->getIdUsuario(),
				'login' => $this->Usuario_model->getLogin(),
				'senha' => $this->Usuario_model->getSenha(),
				'nome' => $this->Usuario_model->getNome(),
				'idProfissional' => $this->Usuario_model->getIdProfissional(),
				'tipo' => $this->Usuario_model->getTipo(),
				'idEmpresa' => $this->Usuario_model->getIdEmpresa(),
				'idPlano' => $this->Usuario_model->getIdPlano(),
				'tipoPlano' => $this->Usuario_model->getTipoPlano(),
				'logged' => true
			);
			$this->session->set_userdata($data);
			
			$updateDataAcesso = array("ultimoAcesso" => date("Y-m-d"));
			$this->Usuario_model->update($this->Usuario_model->getIdUsuario(),$updateDataAcesso );
			
			if($this->Usuario_model->getTipo() == 'Profissional'){
				redirect('area-restrita');
			}else if($this->session->userdata("tipo") == 'Administrador' || $this->session->userdata("tipo") == 'Conteudo'){
				$_SESSION["test"] = true;
				redirect('principal/arearestritaadmin');
			}	
			else if($this->Usuario_model->getTipo() == 'Empresa'){
				redirect('area-restrita-empresa');
			}
		} else {
			return 2;
		}
	}

	function logoff() {		
		$_SESSION["test"] = null;
		$_SESSION["FBRLH_state"] = null;
		$_SESSION["localhost_app_token"] = null;

		$this->session->sess_destroy();
		redirect('/');
	}

}