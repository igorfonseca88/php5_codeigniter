<?php

class UsuarioController extends CI_Controller {

    function __construct() {
        parent::__construct();
		if($this->session->userdata("tipo") != "Administrador"){
			redirect('/');
		}
    }
	
	function index()
	{
		
		
		$this->load->model('Usuario_model', 'usuario');
		$data["usuario"] = $this->usuario->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/usuario/listUsuario");
	}
	
	function novoUsuarioAction()
	{
		$this->load->view("priv/usuario/addUsuario");
	}
	
	function addUsuario()
	{
		$this->load->model("Usuario_model", "usuario");		
		$insert = array(
			"dataCriacao" => date("Y-m-d"),
			"login" => $this->input->post("login"),
			"senha" => $this->input->post("senha"),
			"nome" => $this->input->post("nome"),
			"tipo" => $this->input->post("tipo"),
			"situacao" => $this->input->post("situacao")
		);
		
		if ($this->usuario->add_record($insert) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$this->editarUsuarioAction($id, $data);
	}
	
	function deleteUsuario($id)
	{
		$this->load->model("Usuario_model", "usuario");		

		if ($this->usuario->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["usuario"] = $this->usuario->getAll();
		$this->load->vars($data);
		$this->load->view("priv/usuario/listUsuario");
	}

	function editUsuario() {
		$this->load->model("Usuario_model", "usuario");
		$id = $this->input->post("id");
		
		$update = array(
			"nome" => $this->input->post("nome"),
			"login" => $this->input->post("login"),
			"senha" => $this->input->post("senha"),
			"tipo" => $this->input->post("tipo"),
			"situacao" => $this->input->post("situacao")
		);
		
		if ($this->usuario->update($id,$update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			
			$mensagem = "&nbsp;";
			$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/logo.png' alt='Sistema' title='Sistema' /><br /><br />";
			$mensagem = $mensagem . "<h1>Alteração de dados de acesso</h1><br /><br />";
			$mensagem = $mensagem . "<p>Confira os novos dados de acesso ao sistema:</p>";
			$mensagem = $mensagem . "<p><b>Nome</b>: " . $this->input->post("nome") . "<br />";
			$mensagem = $mensagem . "<b>Login</b>: " . $this->input->post("login") . "<br />";
			$mensagem = $mensagem . "<b>Senha</b>: " .  $this->input->post("senha") . "</p><br /><br />";
			$mensagem = $mensagem . "<p>Guarde este e-mail para o caso de esquecer sua senha.</p><br /><br />";
			$mensagem = $mensagem . "<p>Atenciosamente.</p>";
			
			$this->load->model('Empresa_model', 'empresa');
			$servidor["servidor"] = $this->empresa->getAll();	
	
			$this->load->library('email');
			
			$config['smtp_host'] = $servidor["servidor"][0]->smtp;
			$config['smtp_user'] = $servidor["servidor"][0]->usuario;
			$config['smtp_pass'] = $servidor["servidor"][0]->senha;
			$config['smtp_port'] = $servidor["servidor"][0]->porta;
			$this->email->initialize($config);
			
			$this->email->from($servidor["servidor"][0]->usuario, 'Sistema');
			$this->email->to($servidor["servidor"][0]->email);
			$this->email->subject('Senha de usuário alterada | Sistema');
			$this->email->message($mensagem);
	
			$message = array();
			//$this->email->send();
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$this->editarUsuarioAction($id,$data);
    }
    
    function editarUsuarioAction($id, $mensagem = array()) {   
        $this->load->model('Usuario_model', 'usuario');
        $data["usuario"] = $this->usuario->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/usuario/editUsuario");
    } 
}

?>