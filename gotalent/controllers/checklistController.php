<?php

session_start();

class ChecklistController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function listarChecklists() {
        if ($this->session->userdata("idEmpresa") == "") {
            redirect("area-restrita-empresa");
        }

        $this->load->model("Checklist_model", "checklist");
        $data["checklists"] = $this->checklist->buscarChecklistPorIdEmpresa($this->session->userdata("idEmpresa"));
        $this->load->vars($data);
        $this->load->view("priv/empresa/listChecklist");
    }

    function novoChecklistAction($msg = "") {
        $this->load->view("priv/empresa/addChecklist");
    }

    function addChecklist() {
        $this->load->model('Checklist_model', 'checklist');


        $msgErro = "";
        if ($this->input->post("checklist") == "") {
            $msgErro.="O campo Descrição do Checklist é obrigatório.<br>";
        }


        if ($msgErro != "") {
            $this->novoChecklistAction($msgErro);
        } else {
            $insert = array(
                "dataCriacao" => date("Y-m-d"),
                "checklist" => $this->input->post("checklist"),
                "idEmpresa" => $this->session->userdata("idEmpresa"),
                "situacao" => 'Ativo'
            );



            $id = $this->checklist->add_record($insert);

            if ($id > 0) {

                $this->session->set_flashdata('sucesso', 'Checklist salvo com sucesso.');
            } else {
                $this->session->set_flashdata('error', 'Erro ao tentar salvar o checklist, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
            }

            //$this->editarVagaAction($id, $data);
            redirect("checklistController/editarChecklistAction/" . $id);
        }
    }

    function editChecklist() {
        $this->load->model('Checklist_model', 'checklist');



        $update = array(
            "checklist" => $this->input->post("checklist"),
            "situacao" => $this->input->post("situacao")
        );



        $id = $this->checklist->update($this->input->post("idChecklist"), $update);

        if ($id > 0) {

            $this->session->set_flashdata('sucesso', 'Checklist salvo com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar salvar o checklist, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
        }

        //$this->editarVagaAction($id, $data);
        redirect("checklistController/editarChecklistAction/" . $this->input->post("idChecklist"));
    }

    function editarChecklistAction($id, $mensagem = array()) {

        $this->load->model('Checklist_model', 'checklist');
        $data["checklist"] = $this->checklist->buscarChecklistPorIdEIdEmpresa($this->session->userdata("idEmpresa"), $id);

        if (count($data["checklist"]) == 0) {
            redirect("checklistController/listarChecklists");
        }

        $data["questoes_objetivas"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($data["checklist"][0]->id, "Objetiva");

        $data["questoes_discursivas"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($data["checklist"][0]->id, "Discursiva");

        $data["questoes"] = $this->checklist->buscarQuestoesPorIdChecklist($data["checklist"][0]->id);

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/empresa/editChecklist");
    }

    function addItemChecklist() {

        $this->load->model('Checklist_model', 'checklist');

        if (count($this->checklist->buscarChecklistEmUsoPorIdChecklist($this->input->post("idCk"))) > 0) {
            $this->session->set_flashdata('error', 'O checklist já está em uso e não pode ser alterado.');
            redirect("checklistController/editarChecklistAction/" . $this->input->post("idCk"));
        }

        $data["questoes_objetivas"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($this->input->post("idCk"), "Objetiva");

        $data["questoes_discursivas"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($this->input->post("idCk"), "Discursiva");
        $data["questoes_video"] = $this->checklist->buscarQuestoesPorIdChecklistEComponente($this->input->post("idCk"), "Video");

        if ($this->input->post("componente") == "Objetiva") {
            if (count($data["questoes_objetivas"]) >= 5) {
                $this->session->set_flashdata('error', 'Já foram adicionadas as 5 questões objetivas.');
                redirect("checklistController/editarChecklistAction/" . $this->input->post("idCk"));
            }
        } else if ($this->input->post("componente") == "Discursiva") {
            if (count($data["questoes_discursivas"]) >= 2) {
                $this->session->set_flashdata('error', 'Já foram adicionadas as 2 questões discursivas.');
                redirect("checklistController/editarChecklistAction/" . $this->input->post("idCk"));
            }
        }
        else if ($this->input->post("componente") == "Video") {
            if (count($data["questoes_video"]) >= 5) {
                $this->session->set_flashdata('error', 'Já foram adicionadas as 5 questões.');
                redirect("checklistController/editarChecklistAction/" . $this->input->post("idCk"));
            }
        }
        $resEsperada = "";
        if($this->input->post("componente") == "Objetiva"){
            $resEsperada = "SIM";
        }

        $insert = array(
            "questao" => $this->input->post("questao"),
            "respostaEsperada" => $resEsperada,
            "componente" => $this->input->post("componente"),
            "idChecklist" => $this->input->post("idCk")
        );



        $id = $this->checklist->add_questao($insert);

        if ($id > 0) {

            $this->session->set_flashdata('sucesso', 'Questão salva com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar salvar a questão, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
        }

        //$this->editarVagaAction($id, $data);
        redirect("checklistController/editarChecklistAction/" . $this->input->post("idCk"));
    }

    function excluirItemChecklist($idItem, $idChecklist) {

        $this->load->model('Checklist_model', 'checklist');

        if (count($this->checklist->buscarChecklistEmUsoPorIdChecklist($idChecklist)) > 0) {
            $this->session->set_flashdata('error', 'O checklist já está em uso e não pode ser alterado.');
            redirect("checklistController/editarChecklistAction/" . $idChecklist);
        }

        if ($this->checklist->delete_item($idItem) > 0) {
            $this->session->set_flashdata('sucesso', 'Questão excluída com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao excluir a questão, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
        }

        redirect("checklistController/editarChecklistAction/" . $idChecklist);
    }

    // tela de vagas
    function visualizarChecklistAction($idProfissional, $idVaga, $componente) {

        $this->load->model('Checklist_model', 'checklist');

        $data["respostas"] = $this->checklist->buscaRespostaPorIdProfissionalEIdVagaEComponente($idProfissional, $idVaga, $componente);
        $data["checklist"] = $this->checklist->buscaChecklistPorIdVaga($idVaga);
        
        if($componente == "Video"){
            $data["habilitaAvaliacao"] = true;
            $this->load->model('Vaga_model', 'vaga');
            $data["profissional_vaga"] = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);
        }
        
        $this->load->vars($data);
        $this->load->view("priv/empresa/viewChecklist");
    }
    
    function gravaAvaliacao($idProfissionalVaga, $avaliacao){
        
        
        $this->load->model('Vaga_model', 'vaga');
        $update = array(
            "avaliacaoVideo" => $avaliacao
        );
        
        if ($this->vaga->updateProfissionalVaga($idProfissionalVaga, $update) > 0) {
            echo "sucesso";
        }else{
            echo "falha";
        }
    }

}

?>