<?php


class EntrevistaController extends CI_Controller {

    function __construct() {
        parent::__construct();
		if($this->session->userdata("tipo") != "Administrador"){
			redirect('/');
		}
    }
	
	function index()
	{
		$this->load->model("Profissional_model", "profissional");
		$data["profissionais"] = $this->profissional->buscarProfissionalComVideo();
		$this->load->vars($data);
		$this->load->view("priv/entrevista/entrevista");
	}
	
	function visualizarDadosProfissionalAction($id){
		// habilitar aba de avaliação do recrutador
		$this->load->model('Profissional_model', 'profissional');
		
		$this->load->model('Empresa_model', 'empresa');
		$this->load->model('Vaga_model', 'vaga');

        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
		
        $id = $id/1000;
		$data["profissional"] = $this->profissional->buscarPorId($id);
		$data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
		$data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional($id);
		$data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
		
		
        	   
        $this->load->vars($data);
        $this->load->view("priv/profissional/viewProfissional");
	}
	
	
	 
	
}

?>