<?php
require_once("dompdf/dompdf_config.inc.php");
class PdfController extends CI_Controller {

    function __construct() {
        parent::__construct();

	}

	public function pdf($idProfissional){

		 // habilitar aba de avaliação do recrutador
		$this->load->model('Profissional_model', 'profissional');
		$profissional = $this->profissional->buscarPorId($idProfissional);

		$experiencias = $this->profissional->buscarTodasExperienciasPorIdProfissional($idProfissional);
		foreach ($experiencias as $e) {
			$emprego = $e->empregoAtual == 1 ? " (Emprego Atual)" : "";
			$experiencia .= '<p>' . implode("/",array_reverse(explode("-",$e->dataInicio))) . '|' . implode("/",array_reverse(explode("-",$e->dataFim))) . ' - ' . $e->empresa . ' - ' . $e->cargo .  $emprego . ' - ' . $e->descricaoAtividade . '</p>';
		}

		$formacoes = $this->profissional->buscarTodasFormacoesPorIdProfissional($idProfissional);
		foreach ($formacoes as $f) {
			$formacao .= '<p>' .  $f->formacao . ' - ' . $f->curso . ' - ' . $f->instituicao . ' - ' . implode("/",array_reverse(explode("-",$f->dataInicio))) . '|' . implode("/",array_reverse(explode("-",$f->dataFim))) . '</p>';
		}

		$profissional_skills = $this->profissional->buscarTodasSkillsPorIdProfissional($idProfissional);
		foreach ($profissional_skills as $p) { $skill = $skill . $p->skill . "; "; }

		$dompdf = new DOMPDF();
		$html = '
		<!DOCTYPE html> <html lang="pt-br"> <head> <meta http-equiv="Content-Type" content="charset=utf-8" /> <title></title> </head>
		<body style="font-family:sans-serif;color:#333">
		Currículo gerado por Go Talent - www.gotalent.com.br
		<hr />
		<h1 style="color:#1D80A2;font-weight:300">'.$profissional[0]->nome.'</h1>
		<p>'.$profissional[0]->email.'</p>
		<p>'.$profissional[0]->resumo.'</p>
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Dados pessoais</h3>
		<p>Telefone: '.$profissional[0]->telefone.'</p>
		<p>Data de nascimento: '. implode("/",array_reverse(explode("-",$profissional[0]->nascimento))) . '</p>
		<p>Local: '.$profissional[0]->cid.'/'.$profissional[0]->uf.'</p>
		<p>Pretensão salarial: R$ '.$profissional[0]->pretensao.'</p>
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Redes Sociais</h3>
		<p><a href="' . $profissional[0]->facebook . '">Facebook</a></p>
		<p><a href="' . $profissional[0]->linkedin . '">Linkedin</a></p>
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Formação</h3> ' . $formacao . '
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Experiências profissionais</h3> ' . $experiencia . '
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Skills</h3>
		<p>' . $skill . '</p>
		</body>
		</html>';

		$dompdf->load_html($html);
		//$dompdf->set_paper('A4','portrait');
		$dompdf->render();
		$filename = "GoTalent_".$profissional[0]->nome.".pdf";
		$dompdf->stream($filename, array("Attachment" => true));
		exit(0);
	}

	public function gerar(){

		$idProfissional = $this->session->userdata("idProfissional");
		 // habilitar aba de avaliação do recrutador
		$this->load->model('Profissional_model', 'profissional');
		$profissional = $this->profissional->buscarPorId($idProfissional);

		$experiencias = $this->profissional->buscarTodasExperienciasPorIdProfissional($idProfissional);
		foreach ($experiencias as $e) {
			$emprego = $e->empregoAtual == 1 ? " (Emprego Atual)" : "";
			$experiencia .= '<p>' . implode("/",array_reverse(explode("-",$e->dataInicio))) . '|' . implode("/",array_reverse(explode("-",$e->dataFim))) . ' - ' . $e->empresa . ' - ' . $e->cargo .  $emprego . ' - ' . $e->descricaoAtividade . '</p>';
		}

		$formacoes = $this->profissional->buscarTodasFormacoesPorIdProfissional($idProfissional);
		foreach ($formacoes as $f) {
			$formacao .= '<p>' .  $f->formacao . ' - ' . $f->curso . ' - ' . $f->instituicao . ' - ' . implode("/",array_reverse(explode("-",$f->dataInicio))) . '|' . implode("/",array_reverse(explode("-",$f->dataFim))) . '</p>';
		}

		$profissional_skills = $this->profissional->buscarTodasSkillsPorIdProfissional($idProfissional);
		foreach ($profissional_skills as $p) { $skill = $skill . $p->skill . "; "; }

		$dompdf = new DOMPDF();
		$html = '
		<!DOCTYPE html> <html lang="pt-br"> <head> <meta http-equiv="Content-Type" content="charset=utf-8" /> <title></title> </head>
		<body style="font-family:sans-serif;color:#333">
		Currículo gerado por Go Talent - www.gotalent.com.br
		<hr />
		<h1 style="color:#1D80A2;font-weight:300">'.$profissional[0]->nome.'</h1>
		<p>'.$profissional[0]->email.'</p>
		<p>'.$profissional[0]->resumo.'</p>
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Dados pessoais</h3>
		<p>Telefone: '.$profissional[0]->telefone.'</p>
		<p>Data de nascimento: '. implode("/",array_reverse(explode("-",$profissional[0]->nascimento))) . '</p>
		<p>Local: '.$profissional[0]->cid.'/'.$profissional[0]->uf.'</p>
		<p>Pretensão salarial: R$ '.$profissional[0]->pretensao.'</p>
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Redes Sociais</h3>
		<p><a href="' . $profissional[0]->facebook . '">Facebook</a></p>
		<p><a href="' . $profissional[0]->linkedin . '">Linkedin</a></p>
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Formação</h3> ' . $formacao . '
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Experiências profissionais</h3> ' . $experiencia . '
		<hr />
		<h3 style="color:#1D80A2;font-weight:300">Skills</h3>
		<p>' . $skill . '</p>
		</body>
		</html>';

		$dompdf->load_html($html);
		//$dompdf->set_paper('A4','portrait');
		$dompdf->render();
		$filename = "GoTalent_".$profissional[0]->nome.".pdf";
		$dompdf->stream($filename, array("Attachment" => true));
		exit(0);
	}

	public function gerarCurriculo(){

		//Busca dos Dados
		$this->load->model('Profissional_model', 'profissional');
		$id = $this->session->userdata("idProfissional");

		$data["profissional"] = $this->profissional->buscarPorId($id);
		$data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
		$data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);
		$data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional("17513");

		$this->load->model("EstadoCidade_model", "estado");
		if ($data["profissional"][0]->estado != "" || $data["profissional"][0]->cidade != "") {
			$data["estado"] = $this->estado->buscarEstadosPorId($data["profissional"][0]->estado);
			$data["cidade"] = $this->estado->buscarCidadePorId($data["profissional"][0]->cidade);
		};

		//Documento
		$html = "
		<!DOCTYPE html>
		<html lang='pt-br'>
			<head>
			 	<meta http-equiv='Content-Type' content='charset=utf-8'/>
				<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
				<link href='font-awesome/css/font-awesome.css' rel='stylesheet' type='text/css'>
				<style>
  					body {margin: 0 auto;-webkit-print-color-adjust: exact;font-family: 'Times New Roman'}
    				@page { margin: 10mm 0 0 0; padding: 0 0 10mm 0;}
    				#content{width: 7in;margin:auto;display: block;}
    				#header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    				#footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 80px; margin-top:25mm; font-family:'PT Sans' }
					#autor {z-index:999;margin-left:77.5%;margin-top:40px;font-size: 12px;color: #006886;margin-top:10px;}
					.clear {clear: both;}
					#conteudoFooter {position: fixed; left: 0px; bottom: 0px; right: 0px;height: 50px;width: 100%;background-color: #006886;font-family:'PT Sans'}
					#logo {width: 150px;height: 48px;background-image: url('../../_imagens/curriculo/logo_rodape.jpg');float: left;margin-top: -10px;border-bottom-right-radius: 10px;
    						border-bottom-left-radius: 5px;margin-left: 140px;box-shadow: -1px 0px 1px #ccc;}
					#creditos {font-family:'sans-serif';float: right;color: #FFF;text-align:right;margin-top: -25px;margin-right:10px;font-weight: 500;line-height: 20px;}
					.quebra-pagina{page-break-inside:avoid;margin-bottom: 35px;}
					.quebra-conteudo{page-break-inside:avoid;margin-bottom: 5px;}
					#hr_primary {width: 92%;border-width: 0;height: 4px;text-align: right;margin-right: 15%;background-color: #006886;border-radius: 5px;}
					#boxResumo{float: left;}
					#resumo {padding-left: 80px;text-align: justify;}
					#containerFoto {width: 120px;height: 120px;border-radius: 60px;-webkit-border-radius: 60px;-moz-border-radius: 60px;border: 1px solid #999999;float: right;}
					#Contato{width: 584px;display: block;margin: 0 auto;margin-left: 85px}
				</style>
			</head>
			<body>
				<div id='footer'>
					<small><p id='autor'>Currículo gerado por Go Talent</p></small>
					<div class='clear'></div>
					<div id='conteudoFooter'>
						<img src='_imagens/curriculo/logo_rodape.jpg' id='logo'>
						<p id='creditos'> WWW.GOTALENT.COM.BR</p>
					</div>
				</div>
				<div id='content'>
					<div>
						<table>
							<tr>
								<td width='530px' >
									<h1 style='font-size: 20pt'>" . mb_strtoupper($data['profissional'][0]->nome, 'UTF-8') . "</h1>";
										foreach ($data["experiencias"] as $experiencia) {
											if ($experiencia->empregoAtual == "1") {
												$html .= "<h4 style='margin-bottom: 0px;font-size:13px;margin-top: -30px'>" . mb_strtoupper($experiencia->cargo . " na " . 												$experiencia->empresa, 'UTF-8') . "</h4>";
												break;
											}
										}
									$html .= "
									<h5 class='infoProf'>" . $data['profissional'][0]->email . "<br/>
                    					" . $data['profissional'][0]->telefone . "<br/>
                    				</h5>";
									$html .= "
								</td>
								<td width='130px' >";
									$foto = "http://192.168.25.185/oficial/gotalent/_imagens/curriculo/default.png";
									/*if ($data['profissional'][0]->foto != '' && count(explode(':', $data['profissional'][0]->foto)) <= 1) {
										$foto = "upload/profissional/" . $data['profissional'][0]->foto;
									} else {
										if ($data['profissional'][0]->foto != "" && $data['profissional'][0]->foto != "NULL"  && count(explode(':', $data['profissional'][0]->foto)) > 1) {
											$foto = "_imagens/curriculo/default.png";
										}
									}*/
									$html .= "
                    					<img src='.$foto.' id='containerFoto'>
                				</td>
							</tr>
						</table>
					</div>
					<div>
        				<div>
            				<h3 style='color:#555'>Resumo Profissional</h3>
            				<hr/>
        				</div>
        				<div id='boxResumo' style='page-break-inside:avoid;'>";
							$html .= "
            				<p id='resumo'>" . $data['profissional'][0]->resumo . "<br>Eu tenho por interesse salárial R$" . $data['profissional'][0]->pretensao . ".</p>
        				</div>
					</div>
    				<div>
						<div>
							<h3 style='color:#555'>Histórico Educacional</h3>
							<hr>
						</div>
						<div clss='dadosEducacionais'>";
							foreach ($data["formacoes"] as $formacao) {
								$html .= "
            					<div style='page-break-inside:avoid; margin-bottom:5mm;' >
									<div class='boxDadosEducacionais' style='margin-left:80px;'>
										<p class='dados'><strong style='color: #777'>" . mb_strtoupper($formacao->instituicao, 'UTF-8') . "</strong><p/>
										<p><span style='color:#555;margin-left:15px;'>" . $this->mes(explode("-", $formacao->dataInicio)[1]) . " de " . explode("-", $formacao->dataInicio)[0] . " até " . $this->mes									(explode("-", $formacao->dataFim)[1]) . " de " . explode("-", $formacao->dataFim)[0] . "</span><br/>
											<span style='margin-left:15px;'>$formacao->curso</span><br/>
											<span style='margin-left:15px;'>$formacao->formacao</span>
										</p>
									</div>
								</div>";
							}
							$html .= "
        				</div>
					</div>";
					$css = "";
					$div = "";

					if (count($data["formacoes"]) == 0 && count($data["experiencias"]) == 0 && strlen($data['profissional'][0]->resumo) >= 0) {
						$css = "page-break-inside:avoid";
					} else {
						if (count($data["formacoes"]) == 0 && count($data["experiencias"]) == 0 && strlen($data['profissional'][0]->resumo) >= 0) {

							$css = "page-break-inside:avoid";
						} else {
							if (count($data["formacoes"]) == 0 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) >= 0) {

								$css = "page-break-inside:avoid";
							} else {
								if (count($data["formacoes"]) >= 1 && count($data["experiencias"]) == 0 && strlen($data['profissional'][0]->resumo) >= 0) {

									$css = "page-break-inside:avoid";
								} else {
									if (count($data["formacoes"]) == 1 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) <= 550) {

										$css = "page-break-inside:avoid";
									} else {
										if (count($data["formacoes"]) == 1 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 550) {

											$div = "page-break-before:always";
										} else {
											if (count($data["formacoes"]) <= 2 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) < 80) {

												$css = "page-break-inside:avoid";
											} else {
												if (count($data["formacoes"]) <= 2 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 80) {

													$div = "page-break-before:always";
												} else {
													if (count($data["formacoes"]) < 4 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 0) {
														$div = "page-break-before:always";

													} else {
														if (count($data["formacoes"]) >= 4 && count($data["experiencias"]) >= 1 && strlen($data['profissional'][0]->resumo) > 0) {
															$css = "page-break-inside:avoid";
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					$html .= "
					<div>
						<div style='. $div .'>
							<div>
								<h3  style='color:#555'>Experiência Profissional</h3>
								<hr>
							</div>
						<div class='dadosEducacionais'>";
							foreach ($data["experiencias"] as $experiencia) {
								$html .= "
									<div style=' margin-bottom:6mm;' >
										<div class='boxDadosEducacionais' style='margin-left:80px;" . $css . "'>
											<p class='dados'><strong style='color: #777'>" . mb_strtoupper($experiencia->cargo . " na " . $experiencia->empresa, 'UTF-8') . "</strong><p/>
											<span style='color:#555;margin-left:15px;'>";
								if ($experiencia->empregoAtual == '1') {
									$periodo = $this->mes(explode("-", $experiencia->dataInicio)[1]) . " de " . explode("-", $experiencia->dataInicio)[0] . " até o momento";
								} else {
									$periodo = $this->mes(explode("-", $experiencia->dataInicio)[1]) . " de " . explode("-", $experiencia->dataInicio)[0] . " até " . $this->mes(explode
										("-", $experiencia->dataFim)[1]) . " de " . explode("-", $experiencia->dataFim)[0];
								};
								$html .= "$periodo</span><br/>
												<p style='margin-left:15px;text-align:justify'>" . $experiencia->descricaoAtividade . "</p>
												</div>
									</div>";
							}
						$html .= "
						</div>
	    			</div>
	    			<div>
	        			<div>
    	        			<h3  style='color:#555'>Skills</h3>
							<hr>
            			</div>
            			<div class='dadosEducacionais'>";
							$cont=0;
							$html .= "
							<ul style='margin-left:80px;'>";
								$elementos = count($data["profissional_skills"]);
								for ($j = 0; $j < $elementos; $j++) {

									if ($data["profissional_skills"][$j]->skill != null || $data["profissional_skills"][$j]->skill != "") {
										$html .= "<li>" .$data["profissional_skills"][$j]->skill."</li>";
									}else{
										break;
									}
								}
							$html.="
							</ul>
						</div>
       				</div>
       			</div>
    		</body>
		</html>";

		//Exportar
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->render();
		$filename = "GoTalent_".$data['profissional'][0]->nome.".pdf";
		$dompdf->stream($filename, array("Attachment" => true));
		exit(0);
	}

    public function mes($num)
    {
        $mes = "";
        switch ($num) {
            case "01":
                $mes = "Janeiro";
                break;
            case "02":
                $mes = "Fevereiro";
                break;
            case "03":
                $mes = "Março";
                break;
            case "04":
                $mes = "Abril";
                break;
            case "05":
                $mes = "Maio";
                break;
            case "06":
                $mes = "Junho";
                break;
            case "07":
                $mes = "Julho";
                break;
            case "08":
                $mes = "Agosto";
                break;
            case "09":
                $mes = "Setembro";
                break;
            case "10":
                $mes = "Outubro";
                break;
            case "11":
                $mes = "Novembro";
                break;
            case "12":
                $mes = "Dezembro";
                break;
        }
        return $mes;
    }

}

?>