<?php

class VerificarAnunciosController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		//Carrega dados para envio de e-mail
		$this->load->model('AdminGotalent_model', 'admin_gotalent');
		$servidor["servidor"] = $this->admin_gotalent->getAll();			
		$this->load->library('email');		
		$config['smtp_host'] = $servidor["servidor"][0]->smtp;
		$config['smtp_user'] = $servidor["servidor"][0]->usuario;
		$config['smtp_pass'] = $servidor["servidor"][0]->senha;
		$config['smtp_port'] = $servidor["servidor"][0]->porta;
		$this->email->initialize($config);
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//Zerar variáveis
		$data["vencer"] = $arrayVencer = $emailVencer = $data["vencido"] = $arrayVencidos = $emailVencido = $arrayIdVencidos = null;
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//PROFISSIONAIS
		$this->load->model('Profissional_model', 'profissional');
		
		//Profissional - Seleciona email de profissionais com plano a vencer em 5 dias
		$data["vencer"] = $this->profissional->verificarLimite("5");
		foreach ($data["vencer"] as $vencer) {$arrayVencer[] = $vencer->email;}
		$emailVencer = implode(",",$arrayVencer);
		
		//Profissional - Seleciona email de profissionais com plano vencido há 1 dia
		$data["vencido"] = $this->profissional->verificarLimite("-1");
		
		foreach ($data["vencido"] as $vencido) {$arrayVencidos[] = $vencido->email;}
		$emailVencido = implode(",",$arrayVencidos);	
			
		//Profissional - Setar planos vencidos para FREE	
		foreach ($data["vencido"] as $vencidos) { $arrayIdVencidos[] = $vencidos->id; }
		
		if (count($data["vencido"])>0) { $this->profissional->updateStatusParaFree(implode(",",$arrayIdVencidos)); }
		
		//Enviar e-mails para profissionais com cadastros a vencer em 5 dias
		$mensagem = "&nbsp;";
		$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
		$mensagem = $mensagem . "<h2>Assinatura Premium disponível para pagamento</h2>";
		$mensagem = $mensagem . "<p>";
		$mensagem = $mensagem . "Saudações profissional de TI!<br>";
		$mensagem = $mensagem . "Sua assinatura vai expirar e para continuar tendo todas as vantagens do plano PREMIUM, você precisa renová-lo! Caso não o faça, seu cadastro será alterado para FREE automaticamente.<br>"; 
                $mensagem = $mensagem . "Uma novidade que estamos lançando para os assinantes do plano PREMIUM é o Club Go Talent, além de você ter as melhores oportunidades de TI e candidatar às vagas com antecedência, você terá uma área com descontos em cursos, indicação de cursos de inglês, testes comportamentais, calendário dos maiores eventos de tecnologia, etc e você poderá adquirir mentoria profissional também! <br>";
		$mensagem = $mensagem . "Não perca essa oportunidade, acesse sua área restrita através do endereço http://www.gotalent.com.br/login e escolha um dos planos disponíveis.<br>";
		
		$mensagem = $mensagem . "Agradecemos por fazer parte da Go Talent e desejamos sempre boa sorte com seu crescimento profissional!<br><br>";
		$mensagem = $mensagem . "Atenciosamente,<br>";
		$mensagem = $mensagem . "<b>Go Talent</b>";		
		$mensagem = $mensagem . "</p>";
		$mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";
		
		$this->email->from($servidor["servidor"][0]->usuario,"Go Talent");
		$this->email->to("contato@gotalent.com.br");
		$this->email->bcc($emailVencer);
		$this->email->subject("Renove seu plano PREMIUM - Go Talent");
		$this->email->message($mensagem);
		//$this->email->send();
				
		//Enviar e-mails para profissionais com cadastros vencidos há 1 dia
		$mensagem = "&nbsp;";
		$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
		$mensagem = $mensagem . "<h2>Não contabilizamos o pagamento do seu plano.</h2>";
		$mensagem = $mensagem . "<p>";
		$mensagem = $mensagem . "Saudações profissional de TI!<br>";
		$mensagem = $mensagem . "Informamos que não contabilizamos o pagamento do seu plano PREMIUM no portal Go Talent e o seu perfil a partir de agora será migrado para FREE.<br>";
		$mensagem = $mensagem . "Para voltar a ser um assinante PREMIUM, ter acesso a ótimas oportunidades de TI e participar do Club Go Talent basta acessar sua área restrita " . base_url() . "login e renovar sua assinatura através do menu Planos.<br>";
		$mensagem = $mensagem . "Atenciosamente,<br>";
		$mensagem = $mensagem . "<b>Go Talent</b>";		
		$mensagem = $mensagem . "</p>";
		$mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";
		$this->email->from($servidor["servidor"][0]->usuario, "Go Talent");
		$this->email->to("contato@gotalent.com.br");
                $this->email->bcc($emailVencido);
		$this->email->subject("Pagamento não efetuado - Go Talent");
		$this->email->message($mensagem);
		//$this->email->send();
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		//Zerar variáveis
		//$data["vencer"] = $arrayVencer = $emailVencer = $data["vencido"] = $arrayVencidos = $emailVencido = null;
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		//EMPRESAS PELO EXTRATO
		$this->load->model('Extrato_model', 'extrato');
		
		//Empresa - Seleciona email de empresas com plano a vencer em 10 dias
		$data["vencer10"] = $this->extrato->verificarLimite("10");
	
		foreach ($data["vencer10"] as $vencer) {$arrayVencer10[] = $vencer->email;}
		$emailVencer10 = implode(",",$arrayVencer10);
		
		//Empresa - Seleciona email de empresas com plano a vencer em 5 dias
		$data["vencer"] = $this->extrato->verificarLimite("5");
		foreach ($data["vencer"] as $vencer) {$arrayVencer[] = $vencer->email;}
		$emailVencer = implode(",",$arrayVencer);
		
		//Empresa - Seleciona email de empresas com plano vencido há 1 dia
		$data["vencido"] = $this->extrato->verificarLimite("-1");
		foreach ($data["vencido"] as $vencido) {$arrayVencido[] = $vencido->email;}
		$emailVencido = implode(",",$arrayVencido);
		
		//Enviar e-mails para empresas com planos a vencer em 10 dias
		$mensagem = "&nbsp;";
		$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
		$mensagem = $mensagem . "<h2>Sua empresa ainda tem mais 10 dias para anunciar vagas</h2>";
		$mensagem = $mensagem . "<p>";
		$mensagem = $mensagem . "Viemos informar que o seu plano contratado para anunciar vagas no portal Go Talent vencerá em 10 dias.<br>";
		$mensagem = $mensagem . "Aproveite para utilizar seu saldo e anunciar novas vagas no portal.";
		$mensagem = $mensagem . " Agradecemos por fazer parte da Go Talent e desejamos que encontre muitos talentos para somar à sua equipe!<br><br>";
		$mensagem = $mensagem . "Atenciosamente,<br>";
		$mensagem = $mensagem . "<b>Go Talent</b>";		
		$mensagem = $mensagem . "</p>";
		$mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";
		
		$this->email->from($servidor["servidor"][0]->usuario, "Go Talent");
		$this->email->bcc($emailVencer10);
		$this->email->subject("10 dias para encerrar seu plano - Go Talent");
		$this->email->message($mensagem);
		//$this->email->send();
		
		//Enviar e-mails para empresas com planos a vencer em 5 dias
		$mensagem = "&nbsp;";
		$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
		$mensagem = $mensagem . "<h2>Plano contratado prestes a vencer</h2>";
		$mensagem = $mensagem . "<p>";
		$mensagem = $mensagem . "Viemos informar que o seu plano contratado para anunciar vagas no portal Go Talent vencerá em 5 dias.<br>";
		$mensagem = $mensagem . "Após este prazo iremos disponibilizar em sua área restrita todas as opções de contratação para que possa continuar a anunciar suas vagas no portal.";
		$mensagem = $mensagem . " Agradecemos por fazer parte da Go Talent e desejamos que encontre muitos talentos para somar à sua equipe!<br><br>";
		$mensagem = $mensagem . "Atenciosamente,<br>";
		$mensagem = $mensagem . "<b>Go Talent</b>";		
		$mensagem = $mensagem . "</p>";
		$mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";
		
		$this->email->from($servidor["servidor"][0]->usuario, "Go Talent");
		$this->email->bcc($emailVencer);
		$this->email->subject("5 dias para encerrar seu plano - Go Talent");
		$this->email->message($mensagem);
		//$this->email->send();
				
		//Empresa - Zerar planos vencidos
		foreach ($data["vencido"] as $vencidos) {
			$arrayVencidos[] = $vencidos->id;			
		}
		if (count($data["vencido"])>0) {
			//$this->extrato->updateStatus(implode(",",$arrayVencidos));	
		}
		
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		//Zerar variáveis
		$data["vencer"] = $arrayVencer = $emailVencer = $data["vencido"] = $arrayVencidos = $emailVencido = null;
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		//EMPRESAS PELO EXTRATO
		$this->load->model('Vaga_model', 'vaga');
		
			
		//Empresa - Seleciona email de empresas com plano a vencer em 5 dias
		$data["vencer"] = $this->vaga->verificarLimite("2");
		foreach ($data["vencer"] as $vencer) {$arrayVencer[] = $vencer->email;}
		$emailVencer = implode(",",$arrayVencer);
		
		//Empresa - Seleciona email de empresas com plano vencido há 1 dia
		$data["vencido"] = $this->vaga->verificarLimite("-1");
		foreach ($data["vencido"] as $vencido) {$arrayVencido[] = $vencido->email;}
		$emailVencido = implode(",",$arrayVencido);
		
		
		//Enviar e-mails para empresas com planos a vencer em 5 dias
		$mensagem = "&nbsp;";
		$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
		$mensagem = $mensagem . "<h2>Vaga prestes a atingir a vigência</h2>";
		$mensagem = $mensagem . "<p>";
		$mensagem = $mensagem . "Viemos informar que a vaga anunciada no portal Go Talent vencerá em 2 dias.<br>";
		$mensagem = $mensagem . "Após este prazo não ficará mais disponível os dados dos candidatos. Sugerimos que avalie o quanto antes.";
		$mensagem = $mensagem . " Agradecemos por fazer parte da Go Talent e desejamos que encontre muitos talentos para somar à sua equipe!<br><br>";
		$mensagem = $mensagem . "Atenciosamente,<br>";
		$mensagem = $mensagem . "<b>Go Talent - Tecnologia em Recrutamento</b>";		
		$mensagem = $mensagem . "</p>";
		$mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";
		
		$this->email->from($servidor["servidor"][0]->usuario, "Go Talent");
		$this->email->bcc($emailVencer);
		$this->email->subject("2 dias para encerrar sua vaga");
		$this->email->message($mensagem);
		$this->email->send();
				
		//Empresa - Zerar vagas vencidas
		foreach ($data["vencido"] as $vencidos) {
			$arrayVencidos[] = $vencidos->id;			
		}
		if (count($data["vencido"])>0) {
			$this->vaga->updateStatus(implode(",",$arrayVencidos));	
		}
		
		
	}
}
?>

