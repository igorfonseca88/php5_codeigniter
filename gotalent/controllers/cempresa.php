<?php

session_start();

class Cempresa extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function erro404() {
        $this->load->view('_paginas/erro404');
    }

    public function job($vaga) {

        $dadosVaga = explode("-", $vaga);


        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($dadosVaga[0]);

        $this->load->model("Vaga_model", "vaga");
        $data["vaga"] = $this->vaga->buscarVagaPorId($dadosVaga[1]);
        if ($data["vaga"][0]->id == "" || $data["vaga"][0]->situacao != 'Ativo') {
            redirect('vagas');
        }
        $data["beneficios_vaga"] = $this->vaga->buscarTodosBeneficiosPorIdVaga($dadosVaga[1]);
        $data["vagas_empresa"] = $this->vaga->buscarVagasAtivasPorIdEmpresaParaPortal($data["vaga"][0]->idEmpresa, $dadosVaga[1]);

        $data["vaga_skills"] = $this->vaga->buscarTodasSkillsPorIdVaga($dadosVaga[1]);

        $update = array(
            "visualizacao" => $data["vaga"][0]->visualizacao + 1
        );

        $this->vaga->update($dadosVaga[1], $update);

        // verifica se tem profissional na sessão
        if ($this->session->userdata("idProfissional") != "") {
            $this->load->model("Profissional_model", "profissional");
            $data["profissional"] = $this->profissional->buscarPorId($this->session->userdata("idProfissional"));

            $data["candidaturas"] = $this->profissional->buscarTodasCandidaturasPorIdProfissional($this->session->userdata("idProfissional"));

            $compatibilidade = 0;
            $compatibilidade = $this->vaga->retornaCompatibilidadeProfissional($this->session->userdata("idProfissional"), $data["vaga"][0]->id);
            $data["compatibilidade_vaga"] = $compatibilidade[0]->porcentagem;
        }

        $this->load->vars($data);
        $this->load->view('_paginas/vaga-empresa');
    }

    public function cadastroProfissionalParceiro() {

        // cria na tabela de profissional o cadastro.. cria o usuário e joga para tela de complementação do cadastro		
        $this->load->model("Profissional_model", "profissional");

        // busca profissional pelo e-mail, caso já exista devolve erro
        $existe = $this->Usuario_model->buscarUsuarioPorEmail($this->input->post("emailLogin"));
        if (count($existe) == 0) {

            $this->load->model("Plano_model", "plano");
            //$plano = $this->plano->buscarPlanoProfissionalPorTipoEDias("PREMIUM", '30');
            $plano = $this->plano->buscarPlanoProfissionalPorId(1);
            $insert = array(
                "dataCadastro" => date("Y-m-d"),
                "nome" => $this->input->post("nome"),
                "email" => $this->input->post("emailLogin"),
                "telefone" => $this->input->post("phone"),
                "idPlano" => $plano[0]->id,
                //"dataInicioPlano" => date("Y-m-d"),
                //"dataFinalPlano" => date("Y-m-d", strtotime("+15 days")),
                "valorPago" => 0
            );

            $id = $this->profissional->add_record($insert);

            if ($id > 0) {

                // cria usuario
                $this->load->model('Usuario_model');

                $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->input->post("senha")); //remove palavras que contenham a sintaxe sql
                $seg = trim($seg); //limpa espaços vazios
                $seg = strip_tags($seg); // tira tags html e php
                $seg = addslashes($seg); //adiciona barras invertidas a uma string

                $insertUser = array(
                    "dataCriacao" => date("Y-m-d"),
                    "login" => $this->input->post("emailLogin"),
                    "tipo" => "Profissional",
                    "senha" => $seg,
                    "situacao" => "CONFIRMAR",
                    "idProfissional" => $id
                );
                $idUser = $this->Usuario_model->add_record($insertUser);

                //email go talent
                $mensagem = "&nbsp;";
                $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                $mensagem = $mensagem . "<h1>Novo cadastro de profissional realizado</h1>";
                $mensagem = $mensagem . "<p><b>Nome</b>: " . $this->input->post("nome") . "</p>";
                $mensagem = $mensagem . "<p><b>E-mail</b>: " . $this->input->post("emailLogin") . "</p>";
                $mensagem = $mensagem . "<p><b>Telefone</b>: " . $this->input->post("phone") . "</p><br /><br />";

                $mensagem = $mensagem . "<p>Atenciosamente,</p>";
                $mensagem = $mensagem . "<p><b>Go Talent</b></p>";
                $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                $this->load->model('AdminGotalent_model', 'admin_gotalent');
                $servidor = $this->admin_gotalent->getAll();

                $this->load->library('email');

                $config['smtp_host'] = $servidor[0]->smtp;
                $config['smtp_user'] = $servidor[0]->usuario;
                $config['smtp_pass'] = $servidor[0]->senha;
                $config['smtp_port'] = $servidor[0]->porta;
                $this->email->initialize($config);

                $this->email->from('contato@gotalent.com.br', 'Go Talent');
                $this->email->to('contato@gotalent.com.br');
                $this->email->subject('Novo Profissional Cadastrado ');
                $this->email->message($mensagem);

                $this->email->send();

                // email profissional
                $mensagem = "&nbsp;";
                $mensagem = $mensagem . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
                $mensagem = $mensagem . "<h2>Olá, " . $this->input->post("nome") . "</h2>";
                $mensagem = $mensagem . "<p>A Go Talent te da boas vindas!</p>";
                $mensagem = $mensagem . "<p><b><a href=" . base_url() . "confirmar-cadastro/" . $id . ">Clique aqui</a> para confirmar o seu cadastro e ter acesso ao nosso portal </b>.</p><br /><br />";
                $mensagem = $mensagem . "<p>Após confirmar, você poderá criar seu currículo e ter a possibilidade de encontrar a melhor oportunidade profissional!</p>";
                $mensagem = $mensagem . "<p><b>Sugerimos os seguintes passos: </b></p>";
                $mensagem = $mensagem . "<p>- Cadastre os seus dados pessoais (nome completo, telefone, cidade, estado, etc...);</p>";
                $mensagem = $mensagem . "<p>- Faça upload de uma foto;</p>";
                $mensagem = $mensagem . "<p>- Informe as SKILLS (Ex: PHP, Javascript, etc...);</p>";
                $mensagem = $mensagem . "<p>- Preencha os dados de experiências profissionais e formações escolares;</p>";
                $mensagem = $mensagem . "<p>- Candidate-se as vagas de interesse e principalmente as mais compatíveis com o seu perfil;</p>";
                $mensagem = $mensagem . "<p>- Aguarde o contato da Empresas e BOA SORTE!!</p><br>";

                $mensagem = $mensagem . "<p>Estamos a disposição para o que precisar!</p>";
                $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";
                $mensagem = $mensagem . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

                $this->load->model('AdminGotalent_model', 'admin_gotalent');
                $servidor = $this->admin_gotalent->getAll();

                $this->load->library('email');

                $config['smtp_host'] = $servidor[0]->smtp;
                $config['smtp_user'] = $servidor[0]->usuario;
                $config['smtp_pass'] = $servidor[0]->senha;
                $config['smtp_port'] = $servidor[0]->porta;

                $this->email->from('contato@gotalent.com.br', 'Go Talent');
                $this->email->to($this->input->post("emailLogin"));
                $this->email->subject('Go Talent - Cadastro realizado com sucesso!');
                $this->email->message($mensagem);
                $this->email->send();

                //redirect('profissionalController/editarProfissionalAction/'.$id); 
                //$this->session->set_flashdata('sucesso', 'Cadastro efetuado com sucesso! Confirme o seu cadastro através do e-mail que enviamos para ' . $this->input->post("emailLogin"));
                //redirect('cadastro-profissional');
                echo "sucesso";
            } else {
                //$this->session->set_flashdata('error', 'Ocorreu um erro inesperado ao realizar seu cadastro, tente novamente e se o problema persistir entre em contato conosco.');

                echo "erro";
            }
        } else {
            //$this->session->set_flashdata('error', 'O e-mail utilizado já está cadastrado em nossa base.');
            //redirect('cadastro-profissional');
            echo "erro-email-cadastrado";
        }
    }

    public function cadNews() {


        $email = $this->input->post("email");
        $empresa = $this->input->post("empresa");

        $this->load->model("News_model", "news");


        $insert = array(
            "data" => date("Y-m-d"),
            "email" => $email,
            "idEmpresa" => $empresa
        );

        $id = $this->news->add_record($insert);
        if ($id > 0) {
            echo "sucesso";
        } else {
            echo "erro";
        }
    }

    
    public function empresa($apelido) {
       
        $this->load->model("Empresa_model", "empresa");
        $this->load->model("Vaga_model", "vaga");
        $data["empresa"] = $this->empresa->buscarEmpresaComUrlPorApelido($apelido);
        if(count($data["empresa"]) == 0){
            redirect("/");
        }
        $data["vagas_empresa"] = $this->vaga->buscarTodasVagasAtivasPorIdEmpresa($data["empresa"][0]->id);
        $this->load->model("Galeria_model", "galeria");
        $data["galeria"] = $this->galeria->buscarPorIdEmpresa($data["empresa"][0]->id);
        $this->load->vars($data);
        $this->load->view('_paginas/view-empresa');
    }

}

?>