<?php

session_start();
require_once "PagSeguroLibrary/PagSeguroLibrary.php";
require_once "PagSeguroLibrary/PagSeguroNpi.php";

class EmpresaController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata("idEmpresa") == "") {
            redirect('login/login');
        }
    }

    function index()
    {
        if ($this->session->userdata("idEmpresa") == "") {
            redirect('login/login');
        }
        redirect("area-restrita-empresa");
    }

    function editEmpresa()
    {
        $this->load->model("Empresa_model", "empresa");
        $id = $this->session->userdata("idEmpresa");

        $update = array(
            "razaosocial" => $this->input->post("razaosocial"),
            "email" => $this->input->post("email"),
            "cnpj" => $this->input->post("cnpj"),
            "telefone" => $this->input->post("telefone"),
            "cep" => $this->input->post("cep"),
            "endereco" => $this->input->post("endereco"),
            "numero" => $this->input->post("numero"),
            "bairro" => $this->input->post("bairro"),
            "cidade" => $this->input->post("cidade"),
            "estado" => $this->input->post("estado"),
            "facebook" => $this->input->post("facebook"),
            "contato" => $this->input->post("contato"),
            "sobre" => $this->input->post("sobre"),
            "site" => $this->input->post("site"),
            "twitter" => $this->input->post("twitter"),
            "instagram" => $this->input->post("instagram"),
            "slogan" => $this->input->post("slogan"),
            //"apelido" => $this->input->post("url"),
            "numeroColaboradores" => $this->input->post("numeroColaboradores") == "" ? null : $this->input->post("numeroColaboradores"),
            "fundacao" => $this->input->post("fundacao"),
            "mapa" => $this->input->post("mapa"),
            "pontosPositivos" => $this->input->post("pontosPositivos"),
            "googleplus" => $this->input->post("googleplus"),
            "youtube" => $this->input->post("youtube")
        );

        if ($this->empresa->update($id, $update) > 0) {
            $this->session->set_flashdata('sucesso', 'Os dados da empresa foram salvos com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar salvar, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
        }

        redirect("empresaController/editarEmpresaAction/");
    }

    function editarEmpresaAction($id, $mensagem = array())
    {
        $this->load->model('Empresa_model', 'empresa');
        $id = $this->session->userdata("idEmpresa");
        $data["empresa"] = $this->empresa->buscarPorId($id);

        $this->load->model("Galeria_model", "galeria");
        $data["galeria"] = $this->galeria->buscarPorIdEmpresa($id);

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();

        if ($data["empresa"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($data["empresa"][0]->estado);
        }

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/empresa/editEmpresa");
    }

    function editarUsuarioAction($id, $mensagem = array())
    {
        $this->load->model('Usuario_model', 'usuario');
        //$id = $this->session->userdata("idEmpresa");
        $data["usuario"] = $this->usuario->buscarUsuarioPorIdEIdEmpresa($id, $this->session->userdata("idEmpresa"));

        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
        if (count($data["usuario"]) == 0) {
            $this->session->set_flashdata('error', 'Usuário não encontrado. Caso o erro persista, entre em contato com nosso suporte.');
            redirect("empresaController/listarUsuarios/");
        }

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        $this->load->vars($data);
        $this->load->view("priv/empresa/editUsuario");
    }

    function editUsuario()
    {
        $this->load->model("Usuario_model", "usuario");
        $id = $this->input->post("id");
        $update = array(
            "senha" => $this->input->post("senha"),
            "nome" => $this->input->post("nome"),
            "situacao" => $this->input->post("situacao")
        );

        if ($this->usuario->update($id, $update) > 0) {
            $data["sucesso"] = "Salvo com sucesso.";
        } else {
            $data["erro"] = "Erro ao salvar.";
        }

        $this->editarUsuarioAction($id, $data);
    }

    function listarVagas()
    {
        if ($this->session->userdata("idEmpresa") == "") {
            redirect("area-restrita-empresa");
        }


        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $id = $this->session->userdata("idEmpresa");

        $this->load->model('Vaga_model', 'vaga');
        $data["vagas"] = $this->vaga->buscarTodasVagasPorIdEmpresa($id);


        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();
        if ($data["empresa"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($data["empresa"][0]->estado);
        }

        for ($var = 0; $var < count($data["vagas"]); $var++) {

            if ($data["vagas"][$var]->id != null && $data["vagas"][$var]->situacao == 'Ativo') {
                $valor = $data["vagas"][$var]->recebeCurriculoFora; //$this->vaga->recebeCurriculoFora($data["vagas"][$var]->id);


                if ($valor == "SIM") {
                    $data["profissionais"][$var] = $this->vaga->buscarIndicacaoPorVagaDeFora($data["vagas"][$var]->id, $valor);
                } else {
                    $data["profissionais"][$var] = $this->vaga->buscarIndicacaoPorVagaDaMesmaCidade($data["vagas"][$var]->id, $valor);
                }
            } else {
                continue;
            }
        }
        $this->load->vars($data);
        $this->load->view("priv/empresa/listVaga");
    }

    function novaVagaAction($msg = "")
    {
        $this->load->model('Vaga_model', 'vaga');
        //$this->load->model('Extrato_model', 'extrato');
        $this->load->model('Area_model', 'area');
        $this->load->model('Parametros_model', 'parametro');
        //$creditoVagas = $this->extrato->buscarUltimoExtratoPorIdEmpresa($this->session->userdata("idEmpresa"));
        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
        $data["p_qtd_vaga"] = $this->parametro->buscarParametroPorNome("p_qtd_vaga");
        $data["qtd_vaga"] = $this->vaga->buscarTotalVagasAtivasPorEmpresa($data["empresa"][0]->id);

        if ($data["empresa"][0]->moduloAnuncio == "NAO" && $data["qtd_vaga"][0]->total >= $data["p_qtd_vaga"][0]->valor) {
            $this->session->set_flashdata('error', 'Seu plano atual permite que tenha até ' . $data["p_qtd_vaga"][0]->valor . ' vagas ativas, faça UPGRADE para o plano Básico e continue anunciando de forma ilimitada. Basta acessar: <a href="' . base_url() . 'empresas#planos">Quero anunciar mais VAGAS</a> e selecionar um dos planos.');
            redirect("empresaController/listarVagas/");
        } else {
            if ($data["empresa"][0]->dataFinalPlano < date('Y-m-d')) {
                $this->session->set_flashdata('error', 'Seu plano expirou, renove seu plano e continue a anunciar vagas. Basta acessar a <a href="' . base_url() . 'empresaController/plano">Quero mais VAGAS</a>, selecionar o número de vagas e realizar o pagamento.');
                redirect("empresaController/listarVagas/");
            }
        }
        //if ($creditoVagas[0]->saldoAnuncio <= 0) {
        //$this->session->set_flashdata('error', 'O número de vagas do seu plano já atingiu o limite, entre em contato conosco e adquira uma vaga adicional.');
        // redirect("empresaController/listarVagas/");
        //}

        $data["beneficios"] = $this->vaga->buscarTodosBeneficios();
        #$data["areas"] = $this->area->buscarTodasAreas();
        $data["subareas"] = $this->area->buscarSubAreaPorIdArea('2');

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();

        if ($data["empresa"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($data["empresa"][0]->estado);
        }


        $data["error"] = $msg;
        $this->load->vars($data);
        $this->load->view("priv/empresa/addVaga");
    }

    function addVaga()
    {

        $this->load->model('Vaga_model', 'vaga');

        $msgErro = "";
        if ($this->input->post("vaga") == "") {
            $msgErro .= "O campo Título da Vaga é obrigatório.<br>";
        };
        if ($this->input->post("cep") == "") {
            $msgErro .= "O campo CEP é obrigatório.<br>";
        }
        if ($this->input->post("endereco") == "") {
            $msgErro .= "O campo Endereço é obrigatório.<br>";
        }
        if ($this->input->post("numero") == "") {
            $msgErro .= "O campo Número é obrigatório.<br>";
        }
        if ($this->input->post("bairro") == "") {
            $msgErro .= "O campo Bairro é obrigatório.<br>";
        }
        if ($this->input->post("cidade") == "") {
            $msgErro .= "O campo Número é obrigatório.<br>";
        }
        if ($this->input->post("estado") == "") {
            $msgErro .= "O campo Bairro é obrigatório.<br>";
        }
        if ($this->input->post("descricao") == "") {
            $msgErro .= "O campo Descrição é obrigatório.<br>";
        }
        if ($this->input->post("tipo") == "") {
            $msgErro .= "O campo Tipo de contratação é obrigatório.<br>";
        }
        if ($this->input->post("salario") == "") {
            $msgErro .= "O campo Salário é obrigatório.<br>";
        }


        $receberCurriculoFora = $this->input->post("receberCurriculoFora");
        if ($receberCurriculoFora == "0" || $receberCurriculoFora == "off") {
            $receberCurriculoFora = "NAO";
        } else {
            $receberCurriculoFora = "SIM";
        }
        $receberCurriculoEmail = $this->input->post("receberCurriculoEmail");
        if ($receberCurriculoEmail == "0" || $receberCurriculoEmail == "off") {
            $receberCurriculoEmail = "NAO";
        } else {
            $receberCurriculoEmail = "SIM";
        }
        $local = $this->input->post("localVaga");
        if ($local == "0" || $local == "off") {
            $local = "NAO";
        } else {
            $local = "SIM";
        }

        if ($msgErro != "") {
            $this->novaVagaAction($msgErro);
        } else {

            $insert = array(
                "dataEnvioAprovacao" => date('Y-m-d'),
                "dataCadastro" => date("Y-m-d"),
                "vaga" => $this->input->post("vaga"),
                "descricao" => $this->input->post("descricao"),
                "quantidade" => $this->input->post("quantidade"),
                "tipo" => $this->input->post("tipo"),
                "receberCurriculoFora" => $receberCurriculoFora,
                "receberCurriculoEmail" => $receberCurriculoEmail,
                "salario" => $this->input->post("salario") == "" ? "NÃO DIVULGADO" : $this->input->post("salario"),
                "cidade" => $this->input->post("cidade"),
                "estado" => $this->input->post("estado"),
                "idEmpresa" => $this->session->userdata("idEmpresa"),
                "idArea" => '2',//T.I.
                "idSubArea" => $this->input->post("comboSubArea") == "" ? null : $this->input->post("comboSubArea"), // Área de atuação na T.I.
                "situacao" => "Aguardando Aprovação",
                "destaque" => 'NAO',
                "cep" => $this->input->post("cep"),
                "endereco" => $this->input->post("endereco"),
                "numero" => $this->input->post("numero"),
                "bairro" => $this->input->post("bairro"),
                "empresaMatriz" => $local
            );
            $id = $this->vaga->add_record($insert);
            if ($id > 0) {
                // salva os beneficios
                $beneficios = $this->input->post("beneficios");
                // inseri na tabela tb_profissional_skills
                foreach ($beneficios as $beneficio) {
                    $insertBeneficio = array(
                        "idVaga" => $id,
                        "idBeneficio" => $beneficio
                    );
                    $this->vaga->add_beneficio($insertBeneficio);
                }
                $this->session->set_flashdata('sucesso', 'Vaga publicada com sucesso, daqui poucos minutos sua vaga estará no ar!');
            } else {
                $this->session->set_flashdata('error', 'Erro ao tentar salvar a vaga, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
            }
            redirect("empresaController/listarVagas");
        }
    }

    function editVaga()
    {


        $msgErro = "";
        /*if ($this->input->post("cep") == "") {
            $msgErro .= "O campo CEP é obrigatório.<br>";
        }
        if ($this->input->post("endereco") == "") {
            $msgErro .= "O campo Endereço é obrigatório.<br>";
        }
        if ($this->input->post("numero") == "") {
            $msgErro .= "O campo Número é obrigatório.<br>";
        }
        if ($this->input->post("bairro") == "") {
            $msgErro .= "O campo Bairro é obrigatório.<br>";
        }
        if ($this->input->post("cidadeAddVaga") == "") {
            $msgErro .= "O campo Número é obrigatório.<br>";
        }
        if ($this->input->post("estadoAddVaga") == "") {
            $msgErro .= "O campo Bairro é obrigatório.<br>";
        }
        if ($this->input->post("descricao") == "") {
            $msgErro .= "O campo Descrição é obrigatório.<br>";
        }
        if ($this->input->post("tipo") == "") {
            $msgErro .= "O campo Tipo de contratação é obrigatório.<br>";
        }
        if ($this->input->post("salario") == "") {
            $msgErro .= "O campo Salário é obrigatório.<br>";
        }*/

        $this->load->model('Vaga_model', 'vaga');
        $data["vaga"] = $this->vaga->buscarVagaPorId($this->input->post("idVaga"));
        //$tipoProfissional = implode(",", $this->input->post("tipoProfissional"));

        $receberCurriculoFora = $this->input->post("receberCurriculoFora");
        if ($receberCurriculoFora == "0" || $receberCurriculoFora == "off") {
            $receberCurriculoFora = "NAO";
        } else {
            $receberCurriculoFora = "SIM";
        }

        $receberCurriculoEmail = $this->input->post("receberCurriculoEmail");
        if ($receberCurriculoEmail == "0" || $receberCurriculoEmail == "off") {
            $receberCurriculoEmail = "NAO";
        } else {
            $receberCurriculoEmail = "SIM";
        }

        $local = $this->input->post("localVaga");
        if ($local == "0" || $local == "off") {
            $local = "NAO";
        } else {
            $local = "SIM";
        }
        if ($msgErro != "") {
            $this->session->set_flashdata('error', $msgErro);
            //$this->novaVagaAction($msgErro);
        } else {
            $update = array(
                "vaga" => $this->input->post("vaga"),
                "descricao" => $this->input->post("descricao"),
                "quantidade" => $this->input->post("quantidade"),
                "tipo" => $this->input->post("tipo"),
                "salario" => $this->input->post("salario") == "" ? "NÃO DIVULGADO" : $this->input->post("salario"),
                "cidade" => $this->input->post("cidade"),
                "estado" => $this->input->post("estado"),
                //"idChecklist" => $this->input->post("idChecklist") == "" ? null : $this->input->post("idChecklist"),
                "idSubArea" => $this->input->post("comboSubArea") == "" ? null : $this->input->post("comboSubArea"),
                "idArea" => "2",
                "cep" => $this->input->post("cep"),
                "endereco" => $this->input->post("endereco"),
                "numero" => $this->input->post("numero"),
                "bairro" => $this->input->post("bairro"),
                "empresaMatriz" => $local,
                "receberCurriculoEmail" => $receberCurriculoEmail,
                "receberCurriculoFora" => $receberCurriculoFora
            );

            if ($data["vaga"][0]->situacao == "Desaprovado") {
                $update += array("situacao" => "Aguardando Aprovação");
            }


            //Calculo da diferença entre dias.
            $dataAtual = new DateTime(date('y-m-d'));
            $dataVigencia = new DateTime($this->input->post("dataVigencia"));
            $dateInterval = $dataAtual->diff($dataVigencia);
            $data["dateInterval"] = $dateInterval->days;

            //IF responsável por validar se o intevalo já é menor ou igual a 15. E
            if ($dateInterval <= 15) {
                $update += array("dataVigencia" => $this->input->post("dataVigencia"));
            }

            /*Executa o update na tabela*/
            $id = $this->vaga->update($this->input->post("idVaga"), $update);

            /*Se UPDATE dos dados da vaga ocorrer, então é feito o UPDATE dos beneficios*/
            if($id > 0){
                // delete beneficios por id vaga
                $this->vaga->deleteBeneficiosPorIdVaga($this->input->post("idVaga"));

                // insere na tabela tb_profissional_skills
                $beneficios = $this->input->post("beneficios");
                foreach ($beneficios as $beneficio) {
                    $insertBeneficio = array(
                        "idVaga" => $this->input->post("idVaga"),
                        "idBeneficio" => $beneficio
                    );
                    $this->vaga->add_beneficio($insertBeneficio);
                }
            }

            if ($id > 0) {
                if ($data["vaga"][0]->situacao == "Ativo" || $data["vaga"][0]->situacao == "Aguardando Aprovação") {
                    $this->session->set_flashdata('sucesso', 'As alterações foram salvas com sucesso.');
                }else {
                    $this->session->set_flashdata('sucesso', 'As alterações foram salvas com sucesso. Adicione competências e clique em PUBLICAR.');
                }
            } else {
                $this->session->set_flashdata('error', 'Erro ao tentar salvar a vaga, tente novamente. Se o erro persistir entre em contato com o nosso suporte.');
            }

        }

        redirect("empresaController/editarVagaAction/" . $this->input->post("idVaga"));
    }

    function editarVagaAction($id, $mensagem = array())
    {

        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $this->load->model('Vaga_model', 'vaga');
        $data["vaga"] = $this->vaga->buscarVagaPorId($id);

        if ($data["vaga"][0]->idEmpresa != $data["empresa"][0]->id) {
            redirect("area-restrita-empresa");
        }
        //$this->load->model('Extrato_model', 'extrato');
        //$data["saldos"] = $this->extrato->buscarUltimoExtratoPorIdEmpresa($this->session->userdata("idEmpresa"));

        $data["beneficios"] = $this->vaga->buscarTodosBeneficios();
        $data["beneficios_vaga"] = $this->vaga->buscarTodosBeneficiosPorIdVaga($id);
        $data["vaga_skills"] = $this->vaga->buscarTodasSkillsPorIdVaga($id);
        $data["profissionais_vaga"] = $this->vaga->buscarProfissionaisVagaPorIdVaga($id);

        $this->load->model('Skills_model', 'skill');
        $data["skills"] = $this->skill->buscarTodasSkillsDisponiveisPorIdVaga($id);

        $this->load->model('Profissional_model', 'profissional');

        $this->load->model('Checklist_model', 'checklist');
        $data["checklists"] = $this->checklist->buscarChecklistPorIdEmpresa($this->session->userdata("idEmpresa"));

        $this->load->model("EstadoCidade_model", "estado");
        $data["estados"] = $this->estado->buscarEstados();

        //Verifica se o estado na vaga é diferente de vazio.
        if ($data["vaga"][0]->estado != "") {
            $data["cidades"] = $this->estado->buscarCidadesPorIdEstado($this->estado->buscarIdEstadoPorUF($data["vaga"][0]->estado)[0]->id);//Busca as cidades a partir da localização da vaga.
        }

        $this->load->model('Area_model', 'area');
        #$data["areas"] = $this->area->buscarTodasAreas();

        $data["subareas"] = $this->area->buscarSubAreaPorIdArea('2');

        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];

        /*
         * Calculo da vigencia da vaga
         */
        if ($data["vaga"][0]->dataVigencia != null && $data["vaga"][0]->dataVigencia != "" && $data["vaga"][0]->dataVigencia != "0000-00-00") {
            //Calcula a diferença de dias entre a data atual e a data final da vigencia da vaga.
            $dataAtual = new DateTime(date('y-m-d'));
            $dataFinal = new DateTime($data["vaga"][0]->dataVigencia);
            $dateInterval = $dataAtual->diff($dataFinal);
            $data["dateInterval"] = $dateInterval->days;


            //Se a diferença calculada for menor que 15 dias, notifaca o usuário.
            if ($dateInterval->days <= 15) {
                $mensagemVigencia=null;
                switch ($dateInterval->days) {
                    case 0:
                        $mensagemVigencia = "A vigência desta vaga finaliza hoje !";
                        break;
                    case 1:
                        $mensagemVigencia = "Resta apenas 1 dia para finalizar a vigência desta vaga.";
                        break;
                    default:
                        $mensagemVigencia = "Restam ".$dateInterval->days." dias para finalizar a vigência desta vaga.";
                        break;
                }
                $this->session->set_flashdata('sucesso',$mensagemVigencia);

                //Contador de 15 dias a partir da data de vigência, para  delimmitar os dias que o usuário pode selecionar.
                $date = new DateTime($data["vaga"][0]->dataVigencia);
                $date->add(new DateInterval('P15D'));
                $data["minDiasCalendario"] = $date->format('Y-m-d');
                //Data maxima para estender a vigência da vaga.
                $date->add(new DateInterval('P2M'));
                $data["maxDiasCalendario"] = $date->format('Y-m-d');
            }
        }

        $this->load->vars($data);
        $this->load->view("priv/empresa/editVaga");
    }

    // publicarVagaAction
    function enviarVagaParaAprovacaoAction($idVaga)
    {

        $this->load->model('Empresa_model', 'empresa');
        $this->load->model('Vaga_model', 'vaga');
        //$this->load->model('Extrato_model', 'extrato');

        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        //$creditoVagas = $this->extrato->buscarUltimoExtratoPorIdEmpresa($this->session->userdata("idEmpresa"));

        if ($data["empresa"][0]->dataFinalPlano < date('Y-m-d')) {
            $this->session->set_flashdata('error', 'Seu plano expirou, renove seu plano e continue a anunciar vagas. Basta acessar a <a href="' . base_url() . 'empresas#planos">Quero mais VAGAS</a>, selecionar o número de vagas e realizar o pagamento.');
            redirect("empresaController/listarVagas/");
        }
        //if ($creditoVagas[0]->saldoAnuncio <= 0) {
        // $this->session->set_flashdata('error', 'O número de vagas do seu plano já atingiu o limite, entre em contato conosco através do e-mail contato@gotalent.com.br e adquira uma vaga adicional.');
        // redirect("empresaController/listarVagas/");
        // }


        $update = array(
            "dataEnvioAprovacao" => date('Y-m-d'),
            "situacao" => "Aguardando Aprovação"
        );
        if ($this->vaga->update($idVaga, $update) > 0) {
            //$saldoAnuncio = $creditoVagas[0]->saldoAnuncio - 1;
            //$update = array(
            //  "saldoAnuncio" => $saldoAnuncio
            //);
            //$this->extrato->update($creditoVagas[0]->id, $update);
            // busca saldo do extrato novamente e se estiver zerado, coloca como encerrado
            //$ext = $this->extrato->buscarUltimoExtratoPorIdEmpresa($this->session->userdata("idEmpresa"));
            //if ($ext[0]->saldoAnuncio <= 0 && $ext[0]->saldoDestaque <= 0) {
            //  $update = array(
            //    "tipo" => 'ENCERRADO'
            //);
            //$this->extrato->update($ext[0]->id, $update);
            //}

            $this->session->set_flashdata('sucesso', 'Vaga enviada para aprovação com sucesso. Em breve sua vaga estará revisada e publicada');
        } else {
            $this->session->set_flashdata('error', 'Erro ao tentar enviar para aprovação. Tente novamente e caso persista entre em contato com o nosso suporte.');
        }

        redirect("empresaController/editarVagaAction/" . $idVaga);
    }

    function fecharVagaAction($idVaga)
    {
        $this->load->model('Empresa_model', 'empresa');
        $this->load->model('Vaga_model', 'vaga');

        $empresa = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
        $vaga = $this->vaga->buscarVagaPorId($idVaga);
        $update = array(
            "situacao" => "Fechado",
            "dataFechamento" => date("Y-m-d"),
            "motivoFechamento" => $this->input->post("motivoFechamento")
        );

        if ($this->vaga->update($idVaga, $update) > 0) {

            $data["sucesso"] = "Vaga fechada com sucesso.";
        } else {
            $data["error"] = "Erro ao fechar a vaga.";
        }

        $this->editarVagaAction($idVaga, $data);
    }

    function visualizarDadosProfissionalAction($id)
    {

        // habilitar aba de avaliação do recrutador
        $this->load->model('Profissional_model', 'profissional');

        $this->load->model('Empresa_model', 'empresa');
        $this->load->model('Vaga_model', 'vaga');

        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        //$id = $id/1000;
        $data["profissional"] = $this->profissional->buscarPorId($id);
        $data["experiencias"] = $this->profissional->buscarTodasExperienciasPorIdProfissional($id);
        $data["profissional_skills"] = $this->profissional->buscarTodasSkillsPorIdProfissional($id);
        $data["formacoes"] = $this->profissional->buscarTodasFormacoesPorIdProfissional($id);

        $this->load->vars($data);
        $this->load->view("priv/empresa/viewProfissional");
    }

    function addSkillVaga()
    {
        $this->load->model("Skills_model", "skill");
        $this->load->model("Vaga_model", "vaga");

        $skills = $this->input->post("lista");

        foreach ($skills as $sk) {
            $insertSkill = array(
                "idVaga" => $this->input->post("idVagah"),
                "idSkill" => $sk
            );
            $this->vaga->add_skill($insertSkill);
        }
        $this->session->set_flashdata('sucesso', 'Compentência(s) inserida(s) com sucesso.');

        redirect("empresaController/editarVagaAction/" . $this->input->post("idVagah"));
    }

    function excluirSkillVaga($id, $idVaga)
    {
        $this->load->model("Vaga_model", "vaga");

        if ($this->vaga->deleteVagaSkillPorId($id) > 0) {
            $this->session->set_flashdata('sucesso', 'Compentência removida com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao remover competência.');
        }
        redirect("empresaController/editarVagaAction/" . $idVaga);
    }

    function buscarSkills()
    {
        $q = strtolower($_GET["q"]);
        if (!$q)
            return;

        $this->load->model("Skills_model", "skill");
        $data = $this->skill->buscarSkills($q);
        foreach ($data as $d) {
            $cname = $d->skill;
            echo "$cname\n";
        }
    }

    function destacarVagaSaldo($id)
    {
        $this->load->model("Vaga_model", "vaga");
        $dados["vaga"] = $this->vaga->buscarVagaPorId($id);

        $this->load->model("Extrato_model", "extrato");
        $data["extrato"] = $this->extrato->buscarUltimoExtratoPorIdEmpresa($dados["vaga"][0]->idEmpresa);
        $saldoAtual = $dados["extrato"][0]->saldoDestaque;

        if ($saldoAtual > 0) {
            //Torna vaga destaque
            $update = array(
                "destaque" => "SIM"
            );
            $this->vaga->update($id, $update);
            //Diminui um do saldo
            $saldoNovo = $saldoAtual - 1;
            $update = array(
                "saldoDestaque" => $saldoNovo
            );
            $this->extrato->update($id, $update);

            // busca saldo do extrato novamente e se estiver zerado, coloca como encerrado
            $ext = $this->extrato->buscarUltimoExtratoPorIdEmpresa($dados["vaga"][0]->idEmpresa);
            if ($ext[0]->saldoAnuncio <= 0 && $ext[0]->saldoDestaque <= 0) {
                $update = array(
                    "tipo" => 'ENCERRADO'
                );
                $this->extrato->update($ext[0]->id, $update);
            }

            $this->session->set_flashdata('sucesso', 'Vaga destacada com sucesso.');
        } else {
            $this->session->set_flashdata('error', 'Erro ao destacar vaga.');
        }
        redirect("empresaController/editarVagaAction/" . $id);
    }

    function uploadArquivo($id)
    {
        //parametriza as preferências
        $config["upload_path"] = "./upload/empresas/";
        $config["allowed_types"] = "gif|jpg|png|doc|docx|pdf|txt";
        $numeroRand = rand(00, 9999);
        $config["file_name"] = $id . "_" . $numeroRand;
        $config["overwrite"] = TRUE;
        $config["remove_spaces"] = TRUE;
        $this->load->library("upload", $config);
        //em caso de sucesso no upload
        if ($this->upload->do_upload()) {

            $this->load->model('Empresa_model', 'empresa');


            $update = array(
                "logo" => $this->upload->file_name
            );


            $res = $this->empresa->update($id, $update);


            if ($res > 0) {
                $foto["sucesso"] = "Arquivo cadastrado com sucesso";
                $this->editarEmpresaAction($id, $foto);
            } else {
                $foto["error"] = "Erro ao salvar arquivo no banco de dados.";
                $this->editarEmpresaAction($id, $foto);
            }
        } else {

            $foto["error"] = "Erro ao fazer upload do arquivo.";
            $this->editarEmpresaAction($id, $foto);
        }
    }

    public
    function classificarCanditado($idProfissional, $idVaga)
    {

        $this->load->model('Vaga_model', 'vaga');
        $this->load->model('Profissional_model', 'profissional');
        $this->load->model('Empresa_model', 'empresa');


        $retorno = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);
        $vaga = $this->vaga->buscarVagaPorId($idVaga);


        $assunto = "Go Talent - Currículo classificado";


        $update = array(
            "situacao" => "Classificado",
            "msgClassificar" => $assunto
        );

        $empresa = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $mensagem = "Seu currículo foi classificado para a vaga " . $vaga[0]->vaga . ".";
        $mensagem = $mensagem . "<p>Aguarde o  contato da empresa " . $empresa[0]->razaosocial . ".</p>";
        $mensagem = $mensagem . "<p>Desejamos sorte!!</p>";
        $mensagem = $mensagem . "<p>Estamos à disposição para o que for preciso!!</p>";
        $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";

        if ($this->vaga->updateProfissionalVaga($retorno[0]->id, $update) > 0) {

            // inserir na tabela de mensagens
            $this->load->model("Mensagem_model", "mensagens");
            $insert_mensagem = array(
                "data" => date("Y-m-d"),
                "idDestino" => $idProfissional,
                "mensagem" => $mensagem,
                "assunto" => $assunto,
                "lida" => 'N',
                "tipo" => 'PROFISSIONAL'
            );
            $this->mensagens->add_record($insert_mensagem);

            $message["sucesso"] = "Candidato classificado com sucesso.";

            $profissional = $this->profissional->buscarPorId($idProfissional);

            //$this->enviarEmail($profissional[0]->email, $mensagem);
            $this->enviarEmailMensagem($profissional[0]->email, $mensagem, $assunto, $empresa[0]->email);
        } else {
            $message["error"] = "Um erro inesperado aconteceu. Tente novamente e caso persista entre em contato com o nosso suporte.";
        }


        //$this->editarVagaAction($idVaga, $message);
        $this->vagaCandidatos($idVaga);
    }

    public
    function naoClassificarCanditado($idProfissional, $idVaga)
    {

        $this->load->model('Vaga_model', 'vaga');
        $this->load->model('Profissional_model', 'profissional');
        $this->load->model('Empresa_model', 'empresa');

        $feedback = $this->input->post("feedback");
        $feedbackDetalhado = $this->input->post("feedbackDetalhado");

        if ($feedback == "Outro") {
            $feedback = $feedbackDetalhado;
        }

        // altera situacao
        $retorno = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);
        $vaga = $this->vaga->buscarVagaPorId($idVaga);

        $assunto = "Currículo não classificado";

        $update = array(
            "situacao" => "Não Classificado",
            "msgNaoClassificar" => $assunto . '. Motivo: ' . $feedback
        );

        $empresa = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));


        $mensagem = "Seu currículo não foi classificado para a vaga " . $vaga[0]->vaga . ".";
        $mensagem = $mensagem . "<p> Motivo: $feedback </p>";
        $mensagem = $mensagem . "<p>Para mais detalhes entre em contato com a empresa " . $empresa[0]->razaosocial . " através do e-mail " . $empresa[0]->email . " </p>";
        $mensagem = $mensagem . "<p>Estamos à disposição para o que for preciso!!</p>";
        $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";


        if ($this->vaga->updateProfissionalVaga($retorno[0]->id, $update) > 0) {

            $message["sucesso"] = "Candidato não classificado com sucesso.";

            $profissional = $this->profissional->buscarPorId($idProfissional);

            // inserir na tabela de mensagens
            $this->load->model("Mensagem_model", "mensagens");
            $insert_mensagem = array(
                "data" => date("Y-m-d"),
                "idDestino" => $idProfissional,
                "mensagem" => $mensagem,
                "assunto" => $assunto,
                "lida" => 'N',
                "tipo" => 'PROFISSIONAL'
            );
            $this->mensagens->add_record($insert_mensagem);

            //$this->enviarEmail($profissional[0]->email, $mensagem);
            $this->enviarEmailMensagem($profissional[0]->email, $mensagem, $assunto, $empresa[0]->email);
        } else {
            $message["error"] = "Um erro inesperado aconteceu. Tente novamente e caso persista entre em contato com o nosso suporte.";
        }


        //$this->editarVagaAction($idVaga, $message);
        $this->vagaCandidatos($idVaga);
    }

    public
    function entrevistar($idVaga)
    {

        $this->load->model('Vaga_model', 'vaga');
        $this->load->model('Profissional_model', 'profissional');
        $this->load->model('Empresa_model', 'empresa');


        $arrayProf = explode(',', $this->input->post("hprofissionais"));
        print_r($arrayProf);

        foreach ($arrayProf as $prof) {

            $idProfissional = $prof;
            // altera situacao
            $retorno = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);
            $vaga = $this->vaga->buscarVagaPorId($idVaga);

            $assunto = "Você foi classificado para entrevista";

            $update = array(
                "situacao" => "Entrevista",
                "idChecklist" => $this->input->post("idTiposPerguntas")
            );


            $mensagem = "Você foi selecionado para a fase de vídeo entrevista do processo de recrutamento da vaga " . $vaga[0]->vaga . ".";
            $mensagem = $mensagem . "<p> Acesse sua <a href='" . base_url() . "login'>área restrita</a>, vá até Meus Processos e clique no botão Fazer Entrevista. </p>";


            if ($this->vaga->updateProfissionalVaga($retorno[0]->id, $update) > 0) {

                $message["sucesso"] = "Candidato classificado para ser entrevistado com sucesso.";


                // inserir na tabela de mensagens
                $this->load->model("Mensagem_model", "mensagens");
                $insert_mensagem = array(
                    "data" => date("Y-m-d"),
                    "idDestino" => $idProfissional,
                    "mensagem" => $mensagem,
                    "assunto" => $assunto,
                    "lida" => 'N',
                    "tipo" => 'PROFISSIONAL'
                );
                $this->mensagens->add_record($insert_mensagem);

                $mensagem = $mensagem . "<p>Desejamos boa sorte!!</p>";
                $mensagem = $mensagem . "<p>Qualquer dúvida, estamos à disposição para o que for preciso!!</p>";
                $mensagem = $mensagem . "<p><b>Go Talent - www.gotalent.com.br </b></p>";

                $profissional = $this->profissional->buscarPorId($idProfissional);
                $this->enviarEmailMensagem($profissional[0]->email, $mensagem, $assunto);
            } else {
                $message["error"] = "Um erro inesperado aconteceu. Tente novamente e caso persista entre em contato com o nosso suporte.";
            }
        }

        $this->vagaCandidatos($idVaga);
    }

    public
    function contratar($idProfissional, $idVaga)
    {

        $this->load->model('Vaga_model', 'vaga');
        $this->load->model('Profissional_model', 'profissional');
        $this->load->model('Empresa_model', 'empresa');

        // altera situacao
        $retorno = $this->vaga->buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga);

        $update = array(
            "situacao" => "Contratado"
        );


        if ($this->vaga->updateProfissionalVaga($retorno[0]->id, $update) > 0) {

            $message["sucesso"] = "Salvo com sucesso.";
        } else {
            $message["error"] = "Um erro inesperado aconteceu. Tente novamente e caso persista entre em contato com o nosso suporte.";
        }

        $this->vagaCandidatos($idVaga);
    }

    public
    function vagaCandidatos($idVaga, $situacao = "")
    {

        $this->load->model('Vaga_model', 'vaga');
        $data["vaga"] = $this->vaga->buscarVagaPorIdETotais($idVaga, $this->session->userdata("idEmpresa"));

        $this->load->model('Checklist_model', 'checklist');
        $data["checklists"] = $this->checklist->buscarChecklistPorIdEmpresaEQuestoesPorComponente($this->session->userdata("idEmpresa"), 'Video');

        if (count($data["vaga"]) == 0) {
            redirect("empresaController/listarVagas");
        }


        if ($situacao == "Nao_Classificado") {
            $situacao = "Não Classificado";
            $sit_tela = "Não Classificados";
        } else if ($situacao == "Classificado") {
            $sit_tela = "Classificados";
        } else if ($situacao == "Entrevista") {
            $sit_tela = "para Entrevista";
        } else if ($situacao == "Contratado") {
            $sit_tela = "Contratados";
        } else {

            $situacao = 'Em análise';
            $sit_tela = 'Em análise';
        }

        $data["profissionais_vaga"] = $this->vaga->retornaProfissionaisComCompatibilidadePorIdVagaESituacao($idVaga, $situacao);
        $data["vaga_skills"] = $this->vaga->buscarTodasSkillsPorIdVaga($idVaga);

        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));


        $this->load->model("VideoEntrevista_model", "video");
        $data["qtd_video"] = $this->video->buscarTotalEntrevistasEmpresa($this->session->userdata("idEmpresa"));

        $this->load->model("Parametros_model", "parametro");
        $data["p_qtd_video"] = $this->parametro->buscarParametroPorNome("p_qtd_video");

        if ($data["empresa"][0]->moduloVideoEntrevista == "NAO" && $data["qtd_video"][0]->total >= $data["p_qtd_video"][0]->valor) {
            $data["msgPlano"] = 'Seu plano atual permite que tenha até ' . $data["p_qtd_video"][0]->valor . ' vídeo-entrevistas, faça UPGRADE para o plano Go Talent e continue realizando vídeo-entrevistas de forma ilimitada. Basta acessar: <a href="' . base_url() . 'empresas#planos">Quero continuar entrevistando</a> e selecionar um dos planos.';
        }
        $data["sit_tela"] = $sit_tela;


        $this->load->vars($data);
        $this->load->view("priv/empresa/vaga-candidatos");
    }

    public
    function extrato()
    {
        $this->load->model("Extrato_model", "extrato");
        $data["extratos"] = $this->extrato->buscarExtratoPorIdEmpresa($this->session->userdata("idEmpresa"));

        $this->load->vars($data);
        $this->load->view("priv/empresa/extrato");
    }

    public
    function enviarMensagemAction($idProfissional)
    {
        $this->load->model("Profissional_model", "profissional");
        $this->load->model("Empresa_model", "empresa");
        $profissional = $this->profissional->buscarPorId($idProfissional);

        $empresa = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $mensagem = "";//
        $mensagem = $mensagem . "<p> Contato enviado pela empresa " . $empresa[0]->razaosocial . " e deve ser respondido para o e-mail " . $empresa[0]->email . " </p>";

        $mensagem = $mensagem . "<p><strong>Mensagem: </strong></p>";
        $mensagem = $mensagem . $this->input->post("mensagem");


        $assunto = $this->input->post("assunto");


        // inserir na tabela de mensagens
        $this->load->model("Mensagem_model", "mensagens");
        $insert_mensagem = array(
            "data" => date("Y-m-d"),
            "idDestino" => $idProfissional,
            "mensagem" => $mensagem,
            "assunto" => "Mensagem automática: " . $assunto,
            "lida" => 'N',
            "tipo" => 'PROFISSIONAL'
        );
        $this->mensagens->add_record($insert_mensagem);


        $this->enviarEmailMensagem($profissional[0]->email, $mensagem, $assunto, $empresa[0]->email);
        $idVaga = $this->input->post("idVagaRetorno");
        $this->vagaCandidatos($idVaga);
    }

    public
    function enviarEmailMensagem($email, $mensagem, $assunto, $replyTo = "")
    {


        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;

        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'Go Talent');
        $this->email->to($email);
        $this->email->subject($assunto);
        if ($replyTo != "") {
            $this->email->reply_to($replyTo);
        }
        $msg = "&nbsp;";
        $msg = $msg . "<img src='" . base_url() . "_imagens/email-topo.png' alt='Go Talent' title='Go Talent' /><br /><br />";
        $msg = $msg . $mensagem;
        $msg = $msg . "<br /><br /><img src='" . base_url() . "_imagens/email-rodape.png' alt='Go Talent' title='Go Talent' />";

        $this->email->message($msg);

        $this->email->send();
    }

    public
    function enviarEmail($email, $feedback)
    {
        // email profissional
        $mensagem = "";
        $mensagem = "&nbsp;";

        $mensagem = $mensagem . "<p>Você possui uma nova atualização em seus processos seletivos.</p>";

        $mensagem = $mensagem . $feedback;

        $mensagem = $mensagem . "<p>Para mais detalhes acesse o portal www.gotalent.com.br</p>";

        $mensagem = $mensagem . "<p>Estamos a disposição para o que for preciso!!</p>";

        $mensagem = $mensagem . "<p><b>GoTalent</b></p>";


        $this->load->model('AdminGotalent_model', 'admin_gotalent');
        $servidor = $this->admin_gotalent->getAll();

        $this->load->library('email');

        $config['smtp_host'] = $servidor[0]->smtp;
        $config['smtp_user'] = $servidor[0]->usuario;
        $config['smtp_pass'] = $servidor[0]->senha;
        $config['smtp_port'] = $servidor[0]->porta;
        $this->email->initialize($config);

        $this->email->from('contato@gotalent.com.br', 'GoTalent');
        $this->email->to($email);
        $this->email->subject('Go Talent - Atualização nos processos seletivos');
        $this->email->message($mensagem);

        $this->email->send();
    }

    function autocompletarPorVaga()
    {
        $q = strtolower($_GET["q"]);
        if (!$q)
            return;
        $this->load->model("Vaga_model", "vaga");
        $data = $this->vaga->autocompletarPorVaga($q);
        foreach ($data as $d) {
            $vaga = $d->vaga;
            echo "$vaga\n";
        }
    }

    function autocompletarPorEstado()
    {
        $q = strtolower($_GET["q"]);

        if (!$q)
            return;
        $this->load->model("Vaga_model", "vaga");
        $data = $this->vaga->autocompletarPorEstado($q);
        foreach ($data as $d) {
            $estado = $d->estado;
            echo "$estado\n";
        }
    }

    function buscarPerguntas($id)
    {
        $this->load->model('Checklist_model', 'checklist');
        $data = $this->checklist->buscarQuestoesPorIdChecklist($id);
        $i = 1;
        foreach ($data as $d) {
            $questao = $d->questao;
            echo "$i) $questao <br>";

            $i++;
        }
    }

    function buscarSubAreas($idArea)
    {
        $this->load->model('Area_model', 'area');
        $data = $this->area->buscarSubAreaPorIdArea($idArea);
        $ret = "";
        foreach ($data as $d) {

            $ret .= "<option value='$d->id'> $d->subarea </option>";
        }
        echo $ret;
        exit;
    }

    function plano()
    {

        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $this->load->model("Plano_model", "planos");
        $data["planos_empresa"] = $this->planos->buscarPlanosEmpresas();

        $this->load->vars($data);
        $this->load->view("priv/empresa/plano");
    }

    function planoHotlink()
    {

        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $this->load->model("Plano_model", "planos");
        $data["planos_empresa"] = $this->planos->buscarPlanosEmpresas();

        $this->load->vars($data);
        $this->load->view("priv/empresa/planos-hotlinks");
    }

    function addGaleria()
    {
        $this->load->model("Galeria_model", "galeria");

        $config["upload_path"] = "./upload/galeria/";
        $config["allowed_types"] = "gif|jpg|png";
        $config["file_name"] = "empresa_" . $this->session->userdata("idEmpresa") . "_" . rand(00, 9999);
        $config["overwrite"] = TRUE;
        $config["remove_spaces"] = TRUE;
        $this->load->library("upload", $config);

        if ($this->upload->do_upload()) {
            $insert = array(
                "idEmpresa" => $this->session->userdata("idEmpresa"),
                "imagem" => $this->upload->file_name
            );

            if ($this->galeria->add_record($insert) > 0) {
                $data["sucesso"] = "Imagem salva com sucesso.";
            } else {
                $data["erro"] = "Erro ao salvar imagem no banco de dados.";
            }
        } else {
            $data["erro"] = $this->upload->display_errors();
        }
        $this->editarEmpresaAction($this->session->userdata("idEmpresa"), $data);
    }

    function deletarGaleria($id)
    {
        $this->load->model("Galeria_model", "galeria");
        $delete["delete"] = $this->galeria->buscarPorId($id);

        if (count($delete["delete"]) > 0) {
            $filename = "./upload/galeria/" . $delete["delete"][0]->imagem;
            unlink($filename);
            if ($this->galeria->delete($id) > 0) {
                $data["sucesso"] = "Imagem excluída com sucesso.";
            }
        }

        $this->editarEmpresaAction($this->session->userdata("idEmpresa"), $data);
    }

    function faq()
    {

        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));
        $this->load->vars($data);
        $this->load->view("priv/empresa/faq");
    }

    function listarUsuarios()
    {
        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $data["usuarios"] = $this->Usuario_model->buscarUsuarioPorIdEmpresa($this->session->userdata("idEmpresa"));
        $this->load->vars($data);
        $this->load->view("priv/empresa/listUsuarios");
    }

    function novoUsuarioAction()
    {
        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($this->session->userdata("idEmpresa"));

        $this->load->vars($data);
        $this->load->view("priv/empresa/addUsuario");
    }

    function addUsuario()
    {

        // busca profissional pelo e-mail, caso já exista devolve erro
        $existe = $this->Usuario_model->buscarUsuarioPorEmail(trim($this->input->post("login")));

        if (count($existe) == 0) {
            // formata senha segura
            $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->input->post("senha")); //remove palavras que contenham a sintaxe sql
            $seg = trim($seg); //limpa espaços vazios
            $seg = strip_tags($seg); // tira tags html e php
            $seg = addslashes($seg); //adiciona barras invertidas a uma string
            // cria usuario
            $this->load->model('Usuario_model');

            $insertUser = array(
                "dataCriacao" => date("Y-m-d"),
                "login" => trim($this->input->post("login")),
                "tipo" => "Empresa",
                "senha" => $seg,
                "situacao" => "ATIVO",
                "idProfissional" => 0,
                "idEmpresa" => $this->session->userdata("idEmpresa"),
                "nome" => $this->input->post("nome")
            );
            $idUser = $this->Usuario_model->add_record($insertUser);

            if ($idUser > 0) {

                $data["sucesso"] = "Usuário salvo com sucesso.";
                $this->editarUsuarioAction($idUser, $data);
            } else {
                $data["error"] = "Erro ao salvar usuário. Tente novamente e caso o erro continue, entre em contato com o nosso suporte.";
                $this->load->vars($data);
                $this->load->view("priv/empresa/addUsuario");
            }
        } else {
            $data["error"] = "O e-mail utilizado já está cadastrado em nossa base.";
            $this->load->vars($data);
            $this->load->view("priv/empresa/addUsuario");
        }
    }

}

?>