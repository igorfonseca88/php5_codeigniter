<?php

class MapaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {}

    function getMapaApp($idProfissional) {
        $this->load->model("Mapa_model", "mapa");
        $data["mapa"] = $this->mapa->getPontosMapaApp($idProfissional);

        header('Content-Type: text/xml; charset=UTF-8');
        echo '<?xml version="1.0" encoding="UTF-8" ?>';
        echo '<markers>';

        foreach ($data["mapa"] as $row){
          echo '<marker ';
          $arr = array("&" => "&amp;");
          $name = strtr($row->vaga,$arr);
          echo 'name="' . $this->parseToXML($name) . '" ';
          echo 'address="' . $this->parseToXML($row->endereco) . '" ';
          echo 'cidade="' . $this->parseToXML($row->cidade) . '" ';
          echo 'empresa="' . $this->parseToXML($row->razaosocial) . '" ';
          echo 'id="' . $this->parseToXML($row->id) . '" ';
          echo 'numero="' . $this->parseToXML($row->numero) . '" ';
          echo 'salario="' . $this->parseToXML($row->salario) . '" ';
          echo 'id_empresa="' . $this->parseToXML($row->id_empresa) . '" ';
          echo 'qtd_vagas="' . $this->parseToXML($row->qtd_vagas) . '" ';
          echo 'dias_publicacao="' . $this->parseToXML($row->dias_publicacao) . '" ';
          echo 'porcentagem="' . $this->parseToXML($row->porcentagem) . '" ';

          $link = str_replace(" ","-",$name); 
          $linkUrl = $this->url($link);
          echo 'link="' .$link. '" ';
          echo 'lat="'.$row->latitude .'" ';
          echo 'lng="'.$row->longitude .'" ';
          echo 'type="' . $row->type . '" ';
          echo '/>';
        }

        echo '</markers>';
    }

    function getMapaWeb() {
        $this->load->model("Mapa_model", "mapa");
        $data["mapa"] = $this->mapa->getPontosMapaWeb();

        header('Content-Type: text/xml; charset=UTF-8');
        echo '<?xml version="1.0" encoding="UTF-8" ?>';
        echo '<markers>';

        foreach ($data["mapa"] as $row){
          echo '<marker ';
          $arr = array("&" => "&amp;");
          $name = strtr($row->vaga,$arr);
          echo 'name="' . $this->parseToXML($name) . '" ';
          echo 'address="' . $this->parseToXML($row->endereco) . '" ';
          echo 'cidade="' . $this->parseToXML($row->cidade) . '" ';
          echo 'empresa="' . $this->parseToXML($row->razaosocial) . '" ';
          echo 'id="' . $this->parseToXML($row->id) . '" ';
          echo 'numero="' . $this->parseToXML($row->numero) . '" ';
          echo 'salario="' . $this->parseToXML($row->salario) . '" ';
          echo 'id_empresa="' . $this->parseToXML($row->id_empresa) . '" ';
          echo 'qtd_vagas="' . $this->parseToXML($row->qtd_vagas) . '" ';
          echo 'dias_publicacao="' . $this->parseToXML($row->dias_publicacao) . '" ';

          $link = str_replace(" ","-",$name); 
          $linkUrl = $this->url($link);
          echo 'link="' .$linkUrl. '" ';
          echo 'lat="'.$row->latitude .'" ';
          echo 'lng="'.$row->longitude .'" ';
          echo 'type="' . $row->type . '" ';
          echo '/>';
        }

        echo '</markers>';
    }


    function parseToXML($htmlStr) 
    { 
      $xmlStr=str_replace('<','&lt;',$htmlStr); 
      $xmlStr=str_replace('>','&gt;',$xmlStr); 
      $xmlStr=str_replace('"','&quot;',$xmlStr); 
      $xmlStr=str_replace("'",'&#39;',$xmlStr); 
      $xmlStr=str_replace("&",'&amp;',$xmlStr); 
      return $xmlStr; 
    } 

    function mapaWeb() {
        $this->load->view("mapa/index_web");
    }

    function mapaApp($idProfissional, $latitude, $longitude) {
        $data["idProfissional"] = $idProfissional;
        $data["latitude"] = $latitude;
        $data["longitude"] = $longitude;

        $this->load->model("Mapa_model", "mapa");
        $data["candidaturas"] = $this->mapa->getProfissionalVagasCandidatura($idProfissional);

        $this->load->vars($data);
        $this->load->view("mapa/index_app");
    }

    function url($str) {
        $str = strtolower(utf8_decode($str));
        $i = 1;
        $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
        while ($i > 0)
            $str = str_replace('--', '-', $str, $i);
        if (substr($str, -1) == '-')
            $str = substr($str, 0, -1);
        return $str;
    }

}

?>