<?php

class VideoEntrevista_model extends CI_Model {

    function getAll() {
        
    }


    function addVideoEntrevista($options = array()) {
        
    }

    function updateVideoEntrevista($id, $options = array()) {
       
    }

    function buscarPorId($id) {
        
    }

    function buscarNumeroRespostasPorIdProfissionalEItemChecklist($idProfissional, $idItem) {
        $sql = " SELECT count(id) respostas, respostaVideo FROM `tb_checklist_resposta` WHERE idProfissional = ? and idChecklistItem = ? group by respostaVideo ";
        $query = $this->db->query($sql, array($idProfissional,$idItem));
        return $query->result();
    }

    function deleteVideoEntrevista($id) {
       
    }

    function buscarTotalEntrevistasEmpresa($idEmpresa){
        $sql = " select count(*) as total from tb_vaga v join tb_profissional_vaga pv on pv.idVaga = v.id where pv.situacao = 'Entrevista' and v.situacao = 'Ativo' and v.idEmpresa = ? ";
        $query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }

}
?>