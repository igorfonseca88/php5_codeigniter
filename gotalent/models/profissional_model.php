<?php

class Profissional_model extends CI_Model
{

    function getAll()
    {
        $query = $this->db->query("select p.*, u.login, u.senha, pp.tipoPlano, c.nome as cid, e.uf  
		from tb_profissional p 
                left join cidade c on c.id = p.cidade
                left join estado e on e.id = p.estado
		join tb_usuario u on u.idProfissional = p.id 
		join tb_plano_profissional pp on pp.id = p.idPlano
		order by dataCadastro desc");
        return $query->result();
    }

    function buscarProfissionalComVideo()
    {
        $query = $this->db->query("select * from tb_profissional where video is not null order by nome ");
        return $query->result();
    }

    function buscarProfissionalPorEmail($email)
    {
        $sql = "select * from tb_profissional where email = ? ";
        $query = $this->db->query($sql, array($email));
        return $query->result();
    }

    function buscarTodasExperienciasPorIdProfissional($idProfissional)
    {
        $sql = "select * from tb_experiencia_profissional where idProfissional = ? order by dataFim ";
        $query = $this->db->query($sql, array($idProfissional));
        return $query->result();
    }

    function buscarExperienciaProfissionalPorId($id)
    {
        $sql = "select * from tb_experiencia_profissional where id = ?  ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function buscarFormacaoProfissionalPorId($id)
    {
        $sql = "select * from tb_formacaoeducacional_profissional where id = ?  ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function buscarTodasSkillsPorIdProfissional($idProfissional)
    {
        $sql = "select ps.*, s.skill from tb_profissional_skills ps join tb_skills s on s.id = ps.idSkill where ps.idProfissional = ? ";
        $query = $this->db->query($sql, array($idProfissional));
        return $query->result();
    }

    function buscarTodasCandidaturasPorIdProfissional($idProfissional)
    {
        $sql = "select * from tb_profissional_vaga where idProfissional = ? ";
        $query = $this->db->query($sql, array($idProfissional));
        return $query->result();
    }

    function buscarTodasVagasPorIdProfissional($idProfissional)
    {
        $sql = "select pv.*, v.vaga, e.razaoSocial, v.dataVigencia 
		from tb_profissional_vaga pv 
		join tb_vaga v on v.id = pv.idVaga 
		join tb_empresa e on e.id = v.idEmpresa
		where idProfissional = ? ";
        $query = $this->db->query($sql, array($idProfissional));
        return $query->result();
    }

    function buscarTodasFormacoesPorIdProfissional($idProfissional)
    {
        $sql = "select * from tb_formacaoeducacional_profissional where idProfissional = ? order by dataFim ";
        $query = $this->db->query($sql, array($idProfissional));
        return $query->result();
    }

    function add_skill($options = array())
    {
        $this->db->insert('tb_profissional_skills', $options);
        return $this->db->insert_id();
    }

    function add_candidatar_vaga($options = array())
    {
        $this->db->insert('tb_profissional_vaga', $options);
        return $this->db->insert_id();
    }

    function add_record($options = array())
    {
        $this->db->insert('tb_profissional', $options);
        return $this->db->insert_id();
    }

    function add_experiencia($options = array())
    {
        $this->db->insert('tb_experiencia_profissional', $options);
        return $this->db->insert_id();
    }

    function update_experiencia($id, $options = array())
    {
        $this->db->where('id', $id);
        $this->db->update('tb_experiencia_profissional', $options);
        return $this->db->affected_rows();
    }

    function update_formacao($id, $options = array())
    {
        $this->db->where('id', $id);
        $this->db->update('tb_formacaoeducacional_profissional', $options);
        return $this->db->affected_rows();
    }

    function add_formacao($options = array())
    {
        $this->db->insert('tb_formacaoeducacional_profissional', $options);
        return $this->db->insert_id();
    }

    function update($id, $options = array())
    {
        $this->db->where('id', $id);
        $this->db->update('tb_profissional', $options);
        return $this->db->affected_rows();
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tb_profissional', $options);
        return $this->db->affected_rows();
    }

    function deleteSkills($id)
    {
        $this->db->where('idProfissional', $id);
        $this->db->delete('tb_profissional_skills', $options);
        return $this->db->affected_rows();
    }

    function deleteProfissionalSkillPorId($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tb_profissional_skills', $options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id)
    {
        $sql = "select p.*, pp.tipoPlano, c.nome as cid, e.uf, e.nome as descEstado
		from tb_profissional p join tb_plano_profissional pp on p.idPlano = pp.id 
                left join cidade c on c.id = p.cidade 
                left join estado e on e.id = p.estado  where p.id = ? ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function buscarProfissionaisPorId($ids)
    {
        $sql = "select p.*, pp.tipoPlano, c.nome as cid, e.uf
		from tb_profissional p join tb_plano_profissional pp on p.idPlano = pp.id 
                left join cidade c on c.id = p.cidade 
                left join estado e on e.id = p.estado  where p.id in ($ids) ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function buscarProfissionaisParaNotificacaoVagas($uf)
    {
        $sql = "select p.*, pp.tipoPlano, c.nome as cid, e.uf
		from tb_profissional p join tb_plano_profissional pp on p.idPlano = pp.id 
                left join cidade c on c.id = p.cidade 
                left join estado e on e.id = p.estado  where e.uf = '$uf' and p.receberEmail is null ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function deleteExperiencia($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tb_experiencia_profissional', $options);
        return $this->db->affected_rows();
    }

    function deleteFormacao($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tb_formacaoeducacional_profissional', $options);
        return $this->db->affected_rows();
    }

    function buscarSkillPorIdProfissionalIdSkill($idProfissional, $idSkill)
    {
        $sql = "select ps.idSkill 
		from tb_profissional_skills ps 
		where ps.idProfissional = ? and ps.idSkill = ? ";
        $query = $this->db->query($sql, array($idProfissional, $idSkill));
        return $query->result();
    }

    function listarProfissionaisSkills()
    {
        $query = $this->db->query("select * 
		from tb_profissional_skills ps 
		join tb_profissional p on ps.idProfissional = p.id
		join tb_skills s on s.id = ps.idSkill
		order by p.nome ");
        return $query->result();
    }

    function buscarTotaisDashboard($id)
    {

        $sql = "select  
		(select count(id) from tb_vaga where situacao = 'Ativo'  ) as total, 
		(select count(id) from tb_vaga where estado = e.uf and situacao = 'Ativo' ) as estado,
		(select count(id) from tb_vaga where upper(cidade) = upper(c.nome) and situacao = 'Ativo' ) as cidade
		from tb_profissional p left join estado e on e.id = p.estado left join cidade c on c.id = p.cidade where p.id = ? ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function verificarLimite($limite)
    {
        $sql = "select id, email from tb_profissional 
		where dataFinalPlano is not null and TO_DAYS(dataFinalPlano) - TO_DAYS(NOW()) = ? ";
        $query = $this->db->query($sql, array($limite));
        return $query->result();
    }

    function verificarLimiteBotao($id)
    {
        $sql = "select TO_DAYS(dataFinalPlano) - TO_DAYS(NOW()) as limite from tb_profissional 
		where dataFinalPlano is not null and id = ? ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function updateStatusParaSuspenso($ids)
    {
        $sql = " update tb_usuario set situacao = 'SUSPENSO' where idProfissional in ($ids) ";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

    function updateStatusParaFree($ids)
    {
        $sql = " update tb_profissional set idPlano = 1, dataFinalPlano = null, dataInicioPlano = null where id in ($ids) ";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

    function autocompletarPorCargo($cargo)
    {
        $sql = "select distinct(cargo) as cargo from tb_experiencia_profissional
		where cargo like '$cargo%' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function autocompletarPorEmpresa($empresa)
    {
        $sql = "select distinct(empresa) as empresa from tb_experiencia_profissional
		where empresa like '$empresa%' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function autocompletarPorInstituicao($instituicao)
    {
        $sql = "select distinct(instituicao) as instituicao from tb_formacaoeducacional_profissional
		where instituicao like '$instituicao%' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function autocompletarPorCurso($curso)
    {
        $sql = "select distinct(curso) as curso from tb_formacaoeducacional_profissional
		where curso like '$curso%' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*PRE CADASTRO*/
    function add_precadastro($options = array())
    {
        $this->db->insert('tb_precadastro', $options);
        return $this->db->insert_id();
    }

    function existePreCadastroPorEmail($email)
    {
        $sql = "select count(*) as existe from tb_precadastro 
		where email = ? ";
        $query = $this->db->query($sql, array($email));
        return $query->result();
    }

    function updatePreCadastro($email, $options = array())
    {
        $this->db->where('email', $email);
        $this->db->update('tb_precadastro', $options);
        return $this->db->affected_rows();
    }

    function salvarNomeFoto($nome, $id)
    {
        //Persiste no banco
        $sql = " update tb_profissional set foto = '" . $nome . "' where id = $id ";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }
}

?>