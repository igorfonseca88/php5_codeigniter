<?php
class Postagem_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_postagens order by data desc, id desc");
        return $query->result();
    }
	
    function getLista() {
        $query = $this->db->query("select a.*, (select g.imagem from tb_postagensgaleria g where g.idPostagem = a.id and principal = 'SIM') as imgprincipal , (select c.titulo from tb_categorias c where c.id = a.idcategoria) as categoria,
		(select c.urlCat from tb_categorias c where c.id = a.idcategoria) as urlCat
		from tb_postagens a where a.status = 'ATIVO' order by data desc");
        return $query->result();
    }
	
    function getRelacionados($id,$idcategoria) {
        $query = $this->db->query("select a.*, (select g.imagem from tb_postagensgaleria g where g.idPostagem = a.id and principal = 'SIM') as imgprincipal , (select c.titulo from tb_categorias c where c.id = a.idcategoria) as categoria,
		(select c.urlCat from tb_categorias c where c.id = a.idcategoria) as urlCat		
		from tb_postagens a where a.status = 'ATIVO' and a.idCategoria = $idcategoria and a.id not in ($id) order by rand() limit 0,3");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_postagens', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_postagens',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_postagens',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
         $query = $this->db->query("select a.*, (select g.imagem from tb_postagensgaleria g where g.idPostagem = a.id and principal = 'SIM') as imgprincipal 
		 from tb_postagens a where a.id = $id ");
        return $query->result();
    }
	
	function buscarPorUrlEIdCategoria($urlPost, $id) {
         $query = $this->db->query("select a.*, (select g.imagem from tb_postagensgaleria g where g.idPostagem = a.id and principal = 'SIM') as imgprincipal,
		  (select c.urlCat from tb_categorias c where c.id = a.idcategoria) as urlCat	
		 from tb_postagens a where a.urlPost = '$urlPost' and a.idCategoria = $id ");
        return $query->result();
    }
	
	
    function buscarPorCategoria($id) {
        $query = $this->db->query("select a.*, (select g.imagem from tb_postagensgaleria g where g.idpostagem = a.id and principal = 'SIM') as imgprincipal , (select c.titulo from tb_categorias c where c.id = a.idcategoria) as categoria, 
		(select c.urlCat from tb_categorias c where c.id = a.idcategoria) as urlCat
		from tb_postagens a where a.status = 'ATIVO' and a.idCategoria = $id order by a.data desc");
        return $query->result();
    }
}
?>