<?php

class Vaga_model extends CI_Model
{

    function getAll($destaque = "")
    {

        if ($destaque <> "") {
            $args = " AND v.destaque = '$destaque'";
        }
        $sql = "select v.*, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total, e.razaosocial, e.logo  
		from tb_vaga v join tb_empresa e on v.idEmpresa = e.id where v.situacao = 'Ativo' $args 
		
		order by v.destaque desc, v.dataPublicacao desc";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function listarVagasDestaqueHome()
    {
        $sql = "select v.*  
		from tb_vaga v where v.situacao = 'Ativo'  
		
		order by v.dataPublicacao desc limit 0,6";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function buscarCidadesDasVagasAtivas()
    {
        $query = $this->db->query("select count(estado) as contador, estado 
		from tb_vaga where situacao = 'Ativo' group by estado order by estado ");
        return $query->result();
    }

    function listarNumVagasEstados()
    {
        $query = $this->db->query("select count(estado) as contador, estado 
		from tb_vaga where situacao = 'Ativo' group by estado order by estado ");
        return $query->result();
    }

    function listarNumVagasEstadosHome()
    {
        $query = $this->db->query("select count(estado) as contador, estado 
		from tb_vaga where situacao = 'Ativo' group by estado order by count(estado) desc ");
        return $query->result();
    }

    function buscarVagasPorFiltros($searchVaga, $searchCidade, $seachEstado, $destaque, $searchNivel = "", $searchAreaInteresse)
    {

        $argVaga = "";
        if ($searchVaga != "") {
            $searchVaga = strtoupper($searchVaga);
            $argVaga = $argVaga . " AND ( UPPER(v.vaga) like '%$searchVaga%' )";
        }
        if ($searchCidade != "") {
            $argVaga = $argVaga . " AND ( v.cidade like '%$searchCidade%' )";
        }
        if ($seachEstado != "") {
            $argVaga = $argVaga . " AND ( v.estado = '$seachEstado' )";
        }
        if ($searchNivel != "") {
            $argVaga = $argVaga . " AND ( v.tipoProfissional like '%$searchNivel%' )";
        }

        if ($destaque != "") {
            $argDestaque = " AND ( v.destaque = '$destaque' )";
        }

        if ($searchAreaInteresse != "") {
            $argDestaque = " AND ( v.idArea in ($searchAreaInteresse) )";
        }

        $select = "";
        $order = "";
        if ($this->session->userdata("idProfissional") > 0) {
            $idProfissional = $this->session->userdata("idProfissional");
            $select = ", (((select count(idSkill) from tb_profissional_skills 
			where idSkill in ( select idSkill from tb_vaga_skills  where idVaga = v.id) and idProfissional = $idProfissional) *100)/  
			(select count(idSkill) from tb_vaga_skills where idVaga = v.id)) as porcentagem";
            $order = " porcentagem desc,";
        }

        $sql = "select v.*, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total, e.razaosocial, e.logo, sa.imagem as imgSub
                $select
            
		from tb_vaga v 
                join tb_empresa e on v.idEmpresa = e.id 
                left join tb_subarea sa on sa.id = v.idSubArea
		where v.situacao = 'Ativo' 
		$argVaga  $argDestaque
		order by v.destaque desc, $order v.dataPublicacao desc";
//echo $sql;

        $query = $this->db->query($sql);
        return $query->result();
    }

    function buscarTodas()
    {
        $query = $this->db->query("select v.*, e.razaosocial, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total  
		from tb_vaga v join tb_empresa e on v.idEmpresa = e.id  order by v.dataPublicacao desc");
        return $query->result();
    }

    function buscarVagaPorId($idVaga)
    {
        $sql = "select v.*, e.razaosocial, e.sobre, e.logo, e.site, e.apelido from tb_vaga v join tb_empresa e on v.idEmpresa = e.id  where v.id = ? ";
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }

    function buscarVagaPorIdETotais($idVaga, $idEmpresa)
    {

        $sql = "select v.*, 
		(select count(id) from tb_profissional_vaga where idVaga = v.id ) as total, 
		(select count(id) from tb_profissional_vaga where idVaga = v.id and situacao = 'Em Análise') as em_analise,
		(select count(id) from tb_profissional_vaga where idVaga = v.id and situacao = 'Classificado') as classificados,
		(select count(id) from tb_profissional_vaga where idVaga = v.id and situacao = 'Não Classificado') as nao_classificados,
                (select count(id) from tb_profissional_vaga where idVaga = v.id and situacao = 'Entrevista') as entrevista,
                (select count(id) from tb_profissional_vaga where idVaga = v.id and situacao = 'Contratado') as contratado,
		e.razaosocial  
		from tb_vaga v join tb_empresa e on v.idEmpresa = e.id where v.id = $idVaga and v.situacao in ('Ativo','Finalizado', 'Fechado')
		and e.id = ? ";
        $query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }

    function buscarProfissionaisVagaPorIdVaga($idVaga)
    {
        $sql = "select p.*, pf.data, pf.situacao as sit, pf.isIndicacao, pf.motivoIndicacao from tb_vaga v 
		join tb_profissional_vaga pf on v.id = pf.idVaga  
		join tb_profissional p on p.id = pf.idProfissional
		where v.id = ? ";
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }

    function buscarProfissionaisVagaPorIdVagaEClasssificados($idVaga)
    {
        $sql = "select p.*, pf.data, pf.situacao as sit from tb_vaga v 
		join tb_profissional_vaga pf on v.id = pf.idVaga  
		join tb_profissional p on p.id = pf.idProfissional
		where v.id = ? and pf.situacao in ('Em análise', 'Classificado', 'Aprovado' ) ";
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }

    function buscarTodasVagasPorIdEmpresa($idEmpresa)
    {
        $sql = "select v.*, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total,
                (select count(id) from tb_profissional_vaga where idVaga = v.id and situacao = 'Em análise' ) as analise    
		from tb_vaga v 
		join tb_empresa e on e.id = v.idEmpresa
		where e.id = ? AND v.situacao in ('Ativo', 'Em Elaboração', 'Aguardando Aprovação', 'Finalizado', 'Fechado', 'Desaprovado') order by v.dataCadastro desc ";

        $query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }

    function buscarTodasVagasAtivasPorIdEmpresa($idEmpresa)
    {
        $sql = "select v.*, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total
		from tb_vaga v 
		join tb_empresa e on e.id = v.idEmpresa
		where e.id = ? AND v.situacao in ('Ativo') ";
        $query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }

    function buscarVagasAtivasPorIdEmpresaParaPortal($idEmpresa, $idVaga)
    {
        $sql = "select v.*
		from tb_vaga v 
		join tb_empresa e on e.id = v.idEmpresa
		where e.id = ? AND v.situacao = 'Ativo' and v.dataVigencia >= NOW() and v.id <> ? ";
        $query = $this->db->query($sql, array($idEmpresa, $idVaga));
        return $query->result();
    }

    function buscarTodosBeneficios()
    {
        $query = $this->db->query("select * 
		from tb_beneficios order by beneficio ");
        return $query->result();
    }

    function add_record($options = array())
    {
        $this->db->insert('tb_vaga', $options);
        return $this->db->insert_id();
    }

    function update($id, $options = array())
    {
        $this->db->where('id', $id);
        $this->db->update('tb_vaga', $options);
        return $this->db->affected_rows();
    }

    function add_beneficio($options = array())
    {
        $this->db->insert('tb_vaga_beneficios', $options);
        return $this->db->insert_id();
    }

    function deleteBeneficiosPorIdVaga($id)
    {
        $this->db->where('idVaga', $id);
        $this->db->delete('tb_vaga_beneficios', $options);
        return $this->db->affected_rows();
    }

    function buscarTodosBeneficiosPorIdVaga($idVaga)
    {
        $sql = "select vb.*, b.beneficio
		from tb_vaga_beneficios vb 
		join tb_beneficios b on b.id = vb.idBeneficio
		where vb.idVaga = ? ";
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }

    function add_skill($options = array())
    {
        $this->db->insert('tb_vaga_skills', $options);
        return $this->db->insert_id();
    }

    function deleteSkillsPorIdVaga($id)
    {
        $this->db->where('idVaga', $id);
        $this->db->delete('tb_vaga_skills', $options);
        return $this->db->affected_rows();
    }

    function deleteVagaSkillPorId($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tb_vaga_skills', $options);
        return $this->db->affected_rows();
    }

    function buscarTodasSkillsPorIdVaga($idVaga)
    {
        $sql = "select vs.*, s.skill 
		from tb_vaga_skills vs 
		join tb_skills s on s.id = vs.idSkill
		where idVaga = ? ";
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }

    function buscarSkillPorIdVagaIdSkill($idVaga, $idSkill)
    {
        $sql = "select vs.idSkill 
		from tb_vaga_skills vs 
		where vs.idVaga = ? and vs.idSkill = ? ";
        $query = $this->db->query($sql, array($idVaga, $idSkill));
        return $query->result();
    }

    function retornaTotalSkillsPorIdVaga($idVaga)
    {
        $query = $this->db->query("select count(*) as total 
		from tb_vaga_skills 
		where idVaga = $idVaga ");
        return $query->result();
    }

    function buscarProfissionalVagaPorIdProfissionalEIdVaga($idProfissional, $idVaga)
    {
        $sql = "select *
		from tb_profissional_vaga
		where idVaga = ? and idProfissional = ? ";
        $query = $this->db->query($sql, array($idVaga, $idProfissional));
        return $query->result();
    }

    function updateProfissionalVaga($id, $options = array())
    {
        $this->db->where('id', $id);
        $this->db->update('tb_profissional_vaga', $options);
        return $this->db->affected_rows();
    }

    function retornaCompatibilidadeProfissional($idProfissional, $idVaga)
    {

        $query = $this->db->query("
			select ((count(ps.idSkill)*100)/  
			(select count(idSkill) from tb_vaga_skills where idVaga = $idVaga)) as porcentagem

			from tb_profissional p 
			join tb_profissional_skills ps on p.id = ps.idProfissional 
			where ps.idSkill in (
			select idSkill 
				from tb_vaga_skills  where idVaga = $idVaga
			)
			and p.id = $idProfissional
			 
					");
        return $query->result();
    }

    function retornaProfissionaisCompativeisPorFiltros($vaga, $estado, $pretensao, $tipoPretensao)
    {

        if ($estado <> "") {
            $filtroEstado = " and p.estado = $estado ";
        }
        if ($pretensao <> "") {
            $filtroPretensao = " and p.pretensao  $tipoPretensao $pretensao";
        }
        $sql = "select p.nome, ((count(ps.idSkill)*100)/  
			(select count(idSkill) from tb_vaga_skills where idVaga = $vaga)) as porcentagem,
			p.email, p.telefone,  c.nome as cidade, e.uf as estado, p.pretensao, p.id, p.foto, pla.tipoPlano

			from tb_profissional p 
			join tb_plano_profissional pla on pla.id = p.idPlano
			join tb_profissional_skills ps on p.id = ps.idProfissional 
			join tb_usuario u on u.idProfissional = p.id
                        left join estado e on e.id = p.estado
                        left join cidade c on c.id = p.cidade
			where ps.idSkill in (
			select idSkill 
				from tb_vaga_skills  where idVaga = $vaga
			)
			and p.id not in (select idProfissional from tb_profissional_vaga where idVaga = $vaga )
				$filtroEstado $filtroPretensao 
			and u.situacao = 'Ativo'
			group by p.nome, p.email, p.telefone, p.pretensao, p.id
			order by pla.tipoPlano desc, porcentagem  desc limit 0,40
			 
			 ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function retornaProfissionaisComCompatibilidadePorIdVagaESituacao($vaga, $situacao)
    {

        $sql = "
		    select p.nome, 
			
			(((select count(idSkill) from tb_profissional_skills 
			where idSkill in ( select idSkill from tb_vaga_skills  where idVaga = $vaga) and idProfissional = p.id) *100)/  
			(select count(idSkill) from tb_vaga_skills where idVaga = $vaga)) as porcentagem,
			
			p.email, p.telefone, c.nome as cidade, e.uf as estado, p.pretensao, p.id, p.foto, p.resumo, p.video,
			
			(select situacao 
				from tb_profissional_vaga  where idVaga = $vaga and situacao = '$situacao' and idProfissional = p.id ) as situacao,
                                    
                        (select isIndicacao 
				from tb_profissional_vaga  where idVaga = $vaga and idProfissional = p.id ) as isIndicacao,            
                        (select motivoIndicacao 
				from tb_profissional_vaga  where idVaga = $vaga and idProfissional = p.id ) as motivoIndicacao,            
                        (select case when msgNaoClassificar is not null then msgNaoClassificar else null end 
				from tb_profissional_vaga  where idVaga = $vaga and idProfissional = p.id ) as feedback,
                                    
                        (select count(*) from tb_checklist_resposta cr join tb_checklist_item ci on ci.id = cr.idChecklistItem  
                        where idVaga = $vaga and idProfissional = p.id and ci.componente in ('Objetiva', 'Discursiva')) as checklist,
                        
                        (select count(*) from tb_checklist_resposta cr join tb_checklist_item ci on ci.id = cr.idChecklistItem  
                        where idVaga = $vaga and idProfissional = p.id and ci.componente in ('Video')) as checklist_video,
                            
                        (select avaliacaoVideo 
				from tb_profissional_vaga  where idVaga = $vaga and idProfissional = p.id ) as avaliacaoVideo 
                                
			FROM tb_profissional p left join estado e on e.id = p.estado left join cidade c on c.id = p.cidade
			WHERE  p.id in (select idProfissional 
				from tb_profissional_vaga  where idVaga = $vaga and situacao = '$situacao')
				
			group by p.nome, p.email, p.telefone, p.pretensao, p.id, p.foto
	        order by isIndicacao desc, porcentagem desc";


        $query = $this->db->query($sql);
        return $query->result();
    }

    function retornaVagasComCompatibilidadePorIdProfissional($idProfissional)
    {

        $sql = "
		    select 
                    v.*, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total, e.razaosocial, e.logo, 
			
			(((select count(idSkill) from tb_profissional_skills 
			where idSkill in ( select idSkill from tb_vaga_skills  where idVaga = v.id) and idProfissional = $idProfissional) *100)/  
			(select count(idSkill) from tb_vaga_skills where idVaga = v.id)) as porcentagem
			         
			FROM tb_vaga v 
                        join tb_empresa e on v.idEmpresa = e.id where v.situacao = 'Ativo' 
		
	        order by v.destaque desc, porcentagem desc, v.dataPublicacao desc";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function autocompletarPorVaga($vaga)
    {
        $query = $this->db->query("select vaga from tb_vaga where situacao = 'Ativo' and dataVigencia >= NOW() and vaga like '$vaga%' group by vaga order by vaga");
        return $query->result();
    }

    function autocompletarPorEstado($estado)
    {
        $query = $this->db->query("select estado from tb_vaga where situacao = 'Ativo' and dataVigencia >= NOW() and estado like '$estado%'  group by estado order by estado");
        return $query->result();
    }

    function verificarLimite($limite)
    {
        $sql = "select v.id, v.idEmpresa, e.email, v.vaga 
		from tb_vaga v 
		join tb_empresa e on v.idEmpresa = e.id 
		where v.situacao = 'Ativo' and TO_DAYS(dataVigencia) - TO_DAYS(NOW()) = $limite ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function updateStatus($ids)
    {
        /*$sql1 = " update tb_profissional_vaga set situacao = 'Não Classificado', msgNaoClassificar = 'Vaga encerrada.' 
		where idVaga in ($ids) and situacao = 'Em análise' ";
        $query = $this->db->query($sql1);*/

        $sql = " update tb_vaga set situacao = 'Fechado', motivoFechamento = 'Atingiu sua vigência de publicação no site.', dataFechamento = NOW() where id in ($ids) ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function buscarVagasPorId($arrayVagas)
    {
        $sql = "select v.*, e.razaosocial, e.sobre, e.logo, e.site, e.apelido from tb_vaga v join tb_empresa e on v.idEmpresa = e.id  where v.id in ($arrayVagas) ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function buscarTotalVagasAtivasPorEmpresa($empresa)
    {
        $query = $this->db->query("select count(*) as total from tb_vaga where idEmpresa = $empresa and situacao = 'Ativo'");
        return $query->result();
    }

    function retornaVagasComCompatibilidadePorIdProfissionalParaNotificacao($idProfissional)
    {

        $sql = "
                select * from (
		    select 
                    v.*, (select count(id) from tb_profissional_vaga where idVaga = v.id ) as total, e.razaosocial, e.logo, 
			
			(((select count(idSkill) from tb_profissional_skills 
			where idSkill in ( select idSkill from tb_vaga_skills  where idVaga = v.id) and idProfissional = $idProfissional) *100)/  
			(select count(idSkill) from tb_vaga_skills where idVaga = v.id)) as porcentagem
			         
			FROM tb_vaga v 
                        join tb_empresa e on v.idEmpresa = e.id 
                        where v.situacao = 'Ativo'  and v.id not in (select idvaga from tb_profissional_vaga where idProfissional = $idProfissional)
                        and v.estado = 'MS'  
		
	         ) as x 
                WHERE porcentagem > 33 order by destaque desc, porcentagem desc limit 0,5";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function updateLatitudeLongitudeVaga($idVaga, $latitude, $longitude)
    {
        $sql = " update tb_vaga set latitude = '$latitude', longitude = '$longitude' where id in ($idVaga) ";
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    function buscarIndicacaoPorVagaDeFora($id)
    {
        $sql = "SELECT 
				(SELECT cd.nome FROM cidade cd WHERE cd.id = p.cidade ) as cidade,
				(SELECT e.uf FROM estado e WHERE e.id = p.estado) as estado, 
				COUNT(tps.idSkill) as skills,
				(((SELECT count(ps.idSkill) FROM tb_profissional_skills as ps WHERE ps.idSkill IN (SELECT vs.idSkill FROM tb_vaga_skills as vs WHERE idVaga = $id) AND ps.idProfissional = p.id) *100)/ (SELECT count(tvs.idSkill) FROM tb_vaga_skills as tvs WHERE tvs.idVaga = $id)) AS porcentagem, 
				(SELECT CONCAT(cargo, ' - Empresa: ', empresa) FROM tb_experiencia_profissional where idprofissional = p.id and (empregoAtual =1 or dataFim is not null) order by empregoAtual desc, dataFim desc, id desc limit 0,1) as ultCargo,
				p.id, 
				p.nome, 
				p.email, 
				p.foto, 
				p.telefone, 
				p.pretensao 
				FROM tb_profissional p 
				JOIN cidade c ON p.cidade = c.id 
				JOIN tb_profissional_skills tps ON p.id=tps.idProfissional  
				WHERE
				p.id not in (SELECT pv.idProfissional FROM tb_profissional_vaga pv WHERE pv.idVaga = $id )

				GROUP BY p.id HAVING porcentagem >= 60 ORDER BY  porcentagem DESC, pretensao ASC LIMIT 10";


        $query = $this->db->query($sql);
        return $query->result();
    }

    function buscarIndicacaoPorVagaDaMesmaCidade($id)
    {
        $sql = "SELECT (SELECT cd.nome FROM cidade cd WHERE cd.id = p.cidade ) as cidade,(SELECT e.uf FROM estado e WHERE e.id = p.estado) as estado, COUNT(tps.idSkill) as skills,(((SELECT count(ps.idSkill) FROM tb_profissional_skills as ps WHERE ps.idSkill IN (SELECT vs.idSkill FROM tb_vaga_skills as vs WHERE idVaga = $id) AND ps.idProfissional = p.id) *100)/ (SELECT count(tvs.idSkill) FROM tb_vaga_skills as tvs WHERE tvs.idVaga = $id)) AS porcentagem, (SELECT CONCAT(cargo, ' - Empresa: ', empresa) FROM tb_experiencia_profissional where idprofissional = p.id and (empregoAtual =1 or dataFim is not null) order by empregoAtual desc, dataFim desc, id desc limit 0,1) as ultCargo,p.id, p.nome, p.email, p.foto, p.telefone, p.pretensao FROM tb_profissional p JOIN cidade c ON p.cidade = c.id JOIN tb_profissional_skills tps ON p.id=tps.idProfissional  
		WHERE
		p.id not in (SELECT pv.idProfissional FROM tb_profissional_vaga pv WHERE pv.idVaga = $id ) 
		AND (SELECT c.id FROM tb_vaga v WHERE v.id = $id AND v.cidade LIKE c.nome) = p.cidade GROUP BY p.id HAVING porcentagem >= 30 
		ORDER BY  porcentagem DESC, pretensao ASC, skills DESC LIMIT 10";


        $query = $this->db->query($sql);
        return $query->result();
    }

    function recebeCurriculoFora($id)
    {
        $sql = "SELECT v.recebeCurriculoFora as resultado FROM tb_vaga v where v.id = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }
}

?>