<?php
class App_model extends CI_Model {

    function getDadosEmailCandidatura($idProfissional, $idVaga) {
        $query = $this->db->query("SELECT v.vaga,p.nome as nomeProfissional,p.email emailProfissional,p.telefone,p.pretensao,c.nome as cidade,es.nome as estado,p.resumo,v.id as idVaga,e.email as emailEmpresa FROM tb_profissional_vaga pv join tb_profissional p on pv.idprofissional = p.id join tb_vaga v on v.id = pv.idvaga join tb_empresa e on e.id = v.idempresa join cidade c on c.id = p.cidade join estado es on es.id = p.estado where p.id = ".$idProfissional." and v.id = ".$idVaga);
        return $query->result();
    }

    function getVagas($idProfissional) {
        $query = $this->db->query("SELECT v.id,v.vaga,v.descricao,v.cidade,v.estado,v.tipo,v.salario,v.datapublicacao, e.razaosocial, (SELECT count(id) FROM tb_profissional_vaga WHERE idVaga = v.id AND idProfissional = $idProfissional) AS candidato, (((SELECT count(idSkill) FROM tb_profissional_skills WHERE idSkill IN ( SELECT idSkill FROM tb_vaga_skills  WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT count(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem, v.recebeCurriculoFora FROM `tb_vaga` v join tb_empresa e ON e.id = v.idempresa WHERE v.situacao = 'ativo' ORDER BY porcentagem DESC");
        return $query->result();
    }

        function getVagasPorEstado($idProfissional,$uf) {
        $query = $this->db->query("SELECT v.id,v.vaga,v.descricao,v.cidade,v.estado,v.tipo,v.salario,v.datapublicacao, e.razaosocial, (SELECT count(id) FROM tb_profissional_vaga WHERE idVaga = v.id AND idProfissional = $idProfissional) AS candidato, (((SELECT count(idSkill) FROM tb_profissional_skills WHERE idSkill IN ( SELECT idSkill FROM tb_vaga_skills  WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT count(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem, v.recebeCurriculoFora FROM `tb_vaga` v join tb_empresa e ON e.id = v.idempresa WHERE v.situacao = 'ativo' AND v.estado = '$uf' ORDER BY porcentagem DESC");
        return $query->result();
    }

    function getNovasSkills($idProfissional) {
        $query = $this->db->query("SELECT ts.id, ts.categoria, ts.skill FROM tb_skills ts WHERE ts.id NOT IN(SELECT idSkill FROM tb_profissional_skills WHERE idProfissional = $idProfissional) ORDER BY ts.skill");
        return $query->result();
    }

    function getVagaNova($idV, $idP) {
        $query = $this->db->query("SELECT IF(vg.id IS NULL, '', vg.id) AS id,
            IF (vg.vaga IS NULL,'',vg.vaga)AS vaga,
            IF(vg.descricao IS NULL, '' ,vg.descricao) AS descricao,
            IF(vg.cidade IS NULL,'' ,vg.cidade) AS cidade,
            IF(vg.estado IS NULL, '' ,vg.estado) AS estado,
            IF(vg.tipo IS NULL, '', vg.tipo) AS tipo,
            IF(vg.tipoProfissional IS NULL, '', vg.tipoProfissional) AS tipo_profissional,
            IF(vg.salario IS NULL, '0.0' , vg.salario) AS salario,
            IF(emp.razaosocial IS NULL, '',emp.razaosocial) AS razao_social_emp,
            IF(vg.quantidade IS NULL, '', vg.quantidade) quantidade_vagas,
            IF(vg.informacoesAdicionais IS NULL, '', vg.informacoesAdicionais) AS informacoes_adicionais,
            IF(vg.requisitos IS NULL, '',vg.requisitos) AS requisitos,
            IF(vg.diferenciais IS NULL, '',vg.diferenciais) AS diferenciais,
            IF(vg.dataPublicacao IS NULL, '',vg.dataPublicacao) AS data_publicacao,
            IF(vg.situacao IS NULL, '',vg.situacao) AS situacao_vaga,
            (SELECT count(id) FROM tb_profissional_vaga pv WHERE pv.idVaga = vg.id AND idProfissional =$idP) as candidato,
            IF(vg.recebeCurriculoFora IS NULL, '',vg.recebeCurriculoFora) as recebeCurriculoFora,
            (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills WHERE idVaga = vg.id) AND idProfissional =$idP) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = vg.id)) AS porcentagem
            FROM tb_vaga vg JOIN tb_empresa emp ON vg.idEmpresa =  emp.id  WHERE vg.situacao = 'Ativo' AND vg.id = $idV");
        return $query->result();
    }

    function getAreas() {
        $query = $this->db->query("SELECT a.id, concat(a.area,' (',count(v.id),')') as area FROM tb_area AS a JOIN tb_vaga AS v on a.id = v.idArea where v.situacao = 'Ativo' group by a.id,a.area ORDER BY a.area ASC");
        return $query->result();
    }

    function getNovasSkillsArea($idProfissional, $idArea) {
        $query = $this->db->query("SELECT ts.id, ts.categoria, ts.skill FROM tb_skills AS ts WHERE ts.id NOT IN(SELECT idSkill FROM tb_profissional_skills where idProfissional = $idProfissional) AND ts.idArea = $idArea ORDER BY ts.skill");
        return $query->result();
    }

    function getCidades($idEstado) {
        $query = $this->db->query("SELECT c.id,c.nome FROM cidade AS c WHERE c.estado = $idEstado ORDER BY c.nome");
        return $query->result();
    }

    function getContadores($idProfissional) {
        $query = $this->db->query("SELECT (SELECT COUNT(*) AS vagas FROM tb_vaga AS v WHERE v.situacao = 'Ativo') AS vagas,(SELECT COUNT(*) AS processos FROM tb_profissional_vaga AS pv WHERE pv.idprofissional = $idProfissional) AS processos,(SELECT COUNT(*) AS skills FROM tb_profissional_skills AS ps WHERE ps.idprofissional = $idProfissional) AS skills FROM tb_profissional p WHERE p.id = $idProfissional");
        return $query->result();
    }

    function getEstados() {
        $query = $this->db->query("SELECT e.id,e.nome,e.uf FROM estado AS e ORDER BY e.nome");
        return $query->result();
    }

    function getEstadosVagas() {
        $query = $this->db->query("SELECT e.id, e.nome, concat(e.uf,' (',(SELECT count(v.id) FROM tb_vaga as v where v.estado = e.uf and v.situacao = 'Ativo'),')') as uf FROM estado AS e group by e.id,e.nome,e.uf ORDER BY e.nome");
        return $query->result();
    }

    function deleteSkills($idProfissional,$idSkill) {
        $this->db->where('idProfissional', $idProfissional);
        $this->db->where('idSkill', $idSkill);
        $this->db->delete('tb_profissional_skills',$options);
     //   return $this->db->affected_rows();
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    function setOpiniao($idProfissional,$opiniao,$data) {
        $data = array(
           'idProfissional'=>$idProfissional,
           'opiniao'=>$opiniao,
           'data' =>$data
        );
        $this->db->insert('tb_opiniao_profissional', $data);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    function getDadosProfissional($id) {
        $query = $this->db->query("SELECT IF(prof.id IS NULL, '', prof.id) AS id,
           IF(prof.nome IS NULL,'', prof.nome) AS nome,
           IF(prof.email IS NULL,'', prof.email) AS email,
           IF(prof.telefone IS NULL,'', prof.telefone) AS telefone,
           IF(prof.nascimento IS NULL,'', prof.nascimento) AS nascimento,
           IF(prof.sexo IS NULL,'', prof.sexo) AS sexo,
           IF(prof.pretensao IS NULL, '0.0' , prof.pretensao) AS pretensao,
           IF(c.nome IS NULL,'', c.nome) AS cidade,
           IF(e.nome IS NULL,'', e.nome) AS estado,
           IF(prof.facebook IS NULL,'', prof.facebook) AS facebook,
           IF(prof.linkedin IS NULL,'', prof.linkedin) AS linkedin,
           IF(prof.repositorio IS NULL, '', prof.repositorio) AS repositorio,
           IF(prof.resumo IS NULL, '', prof.resumo) AS resumo,
           IF(prof.foto IS NULL, '', prof.foto) AS foto
           FROM tb_profissional prof LEFT JOIN cidade c ON prof.cidade = c.id LEFT JOIN estado e ON prof.estado = e.id WHERE prof.id =$id");
        return $query->result();
    }

    function editDadosProfissional($idProfissional,$telefone,$nascimento,$sexo,$pretensao,$cidade,$estado,$facebook,$linkedin,$repositorio,$resumo) {
        $data = array(
           'telefone'=>$telefone,
           'nascimento'=>$nascimento,
           'sexo' =>$sexo,
           'pretensao' =>$pretensao,
           'cidade' =>$cidade,
           'estado' =>$estado,
           'facebook' =>$facebook,
           'linkedin' =>$linkedin,
           'repositorio' =>$repositorio,
           'resumo' =>$resumo
        );

        $this->db->where('id', $idProfissional);
        $this->db->update('tb_profissional', $data);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    function editFotoProfissional($idProfissional,$foto) {
        $data = array(
           'foto'=>$foto
        );

        $this->db->where('id', $idProfissional);
        $this->db->update('tb_profissional', $data);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    function editSkillsProfissional($idProfissional,$skills) {
        $values = split("-",$skills);
        $valuesSplit = "";

        for($i = 0;$i < count($values);$i++){
            if(count($values) == ($i+1)){
                $valuesSplit .= "(".$idProfissional.",".$values[$i].")";
            }else{
                $valuesSplit .= "(".$idProfissional.",".$values[$i]."),";
            }
        }

        $this->db->query("INSERT INTO tb_profissional_skills (idProfissional, idSkill) VALUES $valuesSplit");
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    function getProfissionalProcessos($id) {
        $query = $this->db->query("SELECT vaga.vaga, emp.razaosocial, profVaga.situacao, profVaga.data, 
            IF(profVaga.msgClassificar IS NULL,'', profVaga.msgClassificar) AS msgClassificar, 
            IF(profVaga.msgNaoClassificar IS NULL,'', profVaga.msgNaoClassificar) AS msgNaoClassificar 
            FROM tb_profissional_vaga AS profVaga INNER JOIN tb_profissional AS prof ON profVaga.idProfissional = prof.id INNER JOIN tb_vaga AS vaga ON profVaga.idVaga = vaga.id INNER JOIN tb_empresa AS emp ON vaga.idEmpresa = emp.id WHERE profVaga.idProfissional = $id AND profVaga.idVaga = vaga.id ORDER BY profVaga.data DESC");
        return $query->result();
    }

    function getProfissionalVagas($idUsuario) {
        $query = $this->db->query("SELECT pv.idprofissional,pv.idvaga,pv.data,pv.situacao FROM tb_profissional_vaga AS pv JOIN tb_usuario AS u ON u.idprofissional = pv.idprofissional WHERE u.idusuario = $idUsuario");
        return $query->result();
    }

    function setProfissionalVagasCandidatura($idProfissional,$idVaga) {
        $this->db->query("INSERT INTO tb_profissional_vaga (idProfissional, idVaga, data) values ($idProfissional, $idVaga, CURRENT_DATE)");
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    function getSkillsProfissional($id) {
        $query = $this->db->query("SELECT sk.skill,sk.id FROM tb_skills AS sk JOIN tb_profissional_skills AS proSk ON sk.id = proSk.idSkill WHERE  proSk.idProfissional = $id ORDER BY sk.skill ASC");
        return $query->result();
    }

    function getTodasSkillsVagas() {
        $query = $this->db->query("SELECT s.id, concat(s.skill,' (',count(v.id),')') as skill FROM tb_skills AS s JOIN tb_vaga_skills AS vs ON s.id = vs.idskill JOIN tb_vaga AS v ON v.id = vs.idvaga WHERE v.situacao = 'Ativo' GROUP BY s.id,s.skill ORDER BY s.skill");
        return $query->result();
    }

    function getUsuarios($login, $senha) {
        $query = $this->db->query("SELECT u.idUsuario,u.login,u.senha,u.idProfissional,u.idEmpresa,p.idPlano, p.idArea, (SELECT c.nome FROM cidade c WHERE c.id = p.cidade) as cidade, (SELECT e.nome FROM estado e WHERE e.id = p.estado) as estado FROM tb_usuario AS u JOIN tb_profissional AS p on u.idProfissional = p.id WHERE u.login = '$login' AND u.senha = '$senha'");
        return $query->result();
    }

    function getVagasNovas($idProfissional, $dataReferencia) {
        $query = $this->db->query("SELECT v.id, v.datapublicacao, (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills  WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem FROM tb_vaga v join tb_empresa e ON e.id = v.idempresa WHERE v.situacao = 'Ativo' AND (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) > 0.0000 AND v.datapublicacao >= '$dataReferencia' ORDER BY vaga");
        return $query->result();
    }

    function getVagasPorArea($idProfissional, $idArea) {
        $query = $this->db->query("SELECT v.id,v.vaga,v.descricao,v.cidade,v.estado,v.tipo,v.salario,v.datapublicacao,e.razaosocial, (SELECT COUNT(id) FROM tb_profissional_vaga WHERE idVaga = v.id AND idProfissional = $idProfissional) AS candidato, (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem, v.idArea, v.recebeCurriculoFora FROM tb_vaga AS v JOIN tb_empresa AS e ON e.id = v.idempresa WHERE v.situacao = 'Ativo' AND v.idArea = $idArea ORDER BY porcentagem DESC");
        return $query->result();
    }

    function getVagasPorSkills($idProfissional, $idSkill, $idArea) {
        $query = $this->db->query("SELECT v.id,v.vaga,v.descricao,v.cidade,v.estado,v.tipo,v.salario,v.datapublicacao, e.razaosocial, (SELECT COUNT(id) FROM tb_profissional_vaga WHERE idVaga = v.id AND idProfissional = $idProfissional) AS candidato, (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem, v.recebeCurriculoFora FROM tb_vaga AS v JOIN tb_empresa AS e ON e.id = v.idempresa JOIN tb_vaga_skills AS vs ON vs.idvaga = v.id JOIN tb_skills AS s ON s.id = vs.idskill WHERE v.situacao = 'Ativo' AND s.id = $idSkill AND v.idArea = $idArea ORDER BY porcentagem DESC");
        return $query->result();
    }

    function getVagasSkills($idProfissional,$idSkill) {
        $query = $this->db->query("SELECT v.id,v.vaga,v.descricao,v.cidade,v.estado,v.tipo,v.salario,v.datapublicacao,e.razaosocial, (SELECT COUNT(id) FROM tb_profissional_vaga WHERE idVaga = v.id AND idProfissional = $idProfissional) AS candidato, (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills  WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem, v.recebeCurriculoFora FROM tb_vaga AS v JOIN tb_empresa AS e ON e.id = v.idempresa JOIN tb_vaga_skills AS vs ON vs.idvaga = v.id JOIN tb_skills AS s ON s.id = vs.idskill WHERE v.situacao = 'Ativo' AND s.id = $idSkill ORDER BY porcentagem DESC");
        return $query->result();
    }

    function getValidaOpiniao($idProfissional) {
        $query = $this->db->query("SELECT DATEDIFF(CURDATE(),data) dias FROM tb_opiniao_profissional WHERE idProfissional = $idProfissional ORDER BY data DESC LIMIT 1");
        return $query->result();
    }



    function getPalavrasChaveNotificacao() {
        $query = $this->db->query("SELECT palavraChave FROM tb_palavras_chave_notificacao ORDER BY palavraChave");
        return $query->result();
    }

    function getVerificaNotificacoes($idProfissional, $dataReferencia) {
        $query = $this->db->query("SELECT 
            IF(v.id IS NULL, '', v.id) AS id,
            IF (v.vaga IS NULL,'',v.vaga)AS vaga,
            IF(v.descricao IS NULL, '' ,v.descricao) AS descricao,
            IF(v.cidade IS NULL,'' ,v.cidade) AS cidade,
            IF(v.estado IS NULL, '' ,v.estado) AS estado,
            IF(v.tipo IS NULL, '', v.tipo) AS tipo,
            IF(v.tipoProfissional IS NULL, '', v.tipoProfissional) AS tipo_profissional,
            IF(v.salario IS NULL, '0.0' , v.salario) AS salario,
            IF(e.razaosocial IS NULL, '',e.razaosocial) AS razao_social_emp,
            IF(v.quantidade IS NULL, '', v.quantidade) quantidade_vagas,
            IF(v.informacoesAdicionais IS NULL, '', v.informacoesAdicionais) AS informacoes_adicionais,
            IF(v.requisitos IS NULL, '',v.requisitos) AS requisitos,
            IF(v.diferenciais IS NULL, '',v.diferenciais) AS diferenciais,
            IF(v.dataPublicacao IS NULL, '',v.dataPublicacao) AS data_publicacao,
            IF(v.situacao IS NULL, '',v.situacao) AS situacao_vaga,
             (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) AS porcentagem FROM tb_vaga AS v JOIN tb_empresa AS e ON e.id = v.idempresa WHERE v.situacao = 'Ativo' AND (((SELECT COUNT(idSkill) FROM tb_profissional_skills WHERE idSkill IN (SELECT idSkill FROM tb_vaga_skills WHERE idVaga = v.id) AND idProfissional = $idProfissional) *100)/ (SELECT COUNT(idSkill) FROM tb_vaga_skills WHERE idVaga = v.id)) > 0.0000 AND v.datapublicacao >= '$dataReferencia' ORDER BY vaga");
        return $query->result();
    }

    function getNovasVagasNotificacao($idVagas) {
        $query = $this->db->query("SELECT IF(vg.id IS NULL, '', vg.id) AS id,
            IF (vg.vaga IS NULL,'',vg.vaga)AS vaga,
            IF(vg.descricao IS NULL, '' ,vg.descricao) AS descricao,
            IF(vg.cidade IS NULL,'' ,vg.cidade) AS cidade,
            IF(vg.estado IS NULL, '' ,vg.estado) AS estado,
            IF(vg.tipo IS NULL, '', vg.tipo) AS tipo,
            IF(vg.tipoProfissional IS NULL, '', vg.tipoProfissional) AS tipo_profissional,
            IF(vg.salario IS NULL, '0.0' , vg.salario) AS salario,
            IF(emp.razaosocial IS NULL, '',emp.razaosocial) AS razao_social_emp,
            IF(vg.quantidade IS NULL, '', vg.quantidade) quantidade_vagas,
            IF(vg.informacoesAdicionais IS NULL, '', vg.informacoesAdicionais) AS informacoes_adicionais,
            IF(vg.requisitos IS NULL, '',vg.requisitos) AS requisitos,
            IF(vg.diferenciais IS NULL, '',vg.diferenciais) AS diferenciais,
            IF(vg.dataPublicacao IS NULL, '',vg.dataPublicacao) AS data_publicacao,
            IF(vg.situacao IS NULL, '',vg.situacao) AS situacao_vaga
            FROM tb_vaga vg JOIN tb_empresa emp ON vg.idEmpresa =  emp.id  WHERE vg.situacao = 'Ativo' AND vg.id IN (".$idVagas.")");
        return $query->result();
    }

}
?>