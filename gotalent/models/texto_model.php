<?php
class Texto_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_site_texto");
        return $query->result();
    }
	
	function buscarPorTitulo($titulo) {
        $query = $this->db->query("select * from tb_site_texto where titulo = '$titulo'");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_site_texto', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_site_texto',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_site_texto',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_site_texto");
        return $query->result();
    }
}
?>