<?php

class Extrato_model extends CI_Model {

    function buscarExtratoPorIdEmpresa($idEmpresa) {
        $query = $this->db->query("select * from tb_extrato_empresa where idEmpresa = $idEmpresa order by dataCriacao desc ");
        return $query->result();
    }

    function buscarSaldosAnuncioEDestaque($idEmpresa) {
        $query = $this->db->query("select ((select sum(saldoAnuncio) from tb_extrato_empresa where idEmpresa = e.id and tipo = 'Crédito' AND vigencia >= NOW() )
		- (select IFNULL(sum(saldoAnuncio),0) from tb_extrato_empresa where idEmpresa = e.id and tipo = 'Débito' AND vigencia >= NOW() ))
		as saldo, 
		((select IFNULL(sum(saldoDestaque),0) from tb_extrato_empresa where idEmpresa = e.id and tipo = 'Crédito' AND vigencia >= NOW() )
		- (select IFNULL(sum(saldoDestaque),0) from tb_extrato_empresa where idEmpresa = e.id and tipo = 'Débito' AND vigencia >= NOW() ))
		as destaque 
		from tb_empresa e where e.id = $idEmpresa  ");
        return $query->result();
    }

    function buscarUltimoExtratoPorIdEmpresa($idEmpresa) {
        $query = $this->db->query("select * from tb_extrato_empresa 
		where idEmpresa = $idEmpresa and tipo ='ATIVO' AND vigencia >= NOW() order by dataCriacao desc limit 0,1 ");
        return $query->result();
    }

    function buscarUltimoExtratoPendentePorIdEmpresa($idEmpresa) {
        $query = $this->db->query("select * from tb_extrato_empresa where idEmpresa = $idEmpresa and tipo ='AGUARDANDO PAGAMENTO' limit 0,1 ");
        return $query->result();
    }

    function add_record($options = array()) {
        $this->db->insert('tb_extrato_empresa', $options);
        return $this->db->insert_id();
    }

    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_extrato_empresa', $options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id) {
        $query = $this->db->query("select * from tb_extrato_empresa where id = $id ");
        return $query->result();
    }

    function verificarLimite($limite) {
        $sql = "select ee.id, ee.idEmpresa, e.email 
		from tb_extrato_empresa ee 
		join tb_empresa e on ee.idEmpresa = e.id 
		where tipo = 'ATIVO' and saldoAnuncio > 0 and TO_DAYS(vigencia) - TO_DAYS(NOW()) = $limite ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function updateStatus($ids) {
        $sql = " update tb_extrato_empresa set tipo = 'ENCERRADO' where id in ($ids) ";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

}

?>