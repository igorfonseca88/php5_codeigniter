<?php

class Mapa_model extends CI_Model {

    function getProfissionalVagasCandidatura($idProfissional) {
        $query = $this->db->query("select pv.idvaga from tb_profissional_vaga pv where pv.idprofissional = ".$idProfissional);
        return $query->result();
    }

    function getPontosMapaApp($idProfissional) {
        $query = $this->db->query("SELECT v.id, v.vaga, e.razaosocial, e.endereco, c.nome cidade, e.numero, 'bar' type, v.salario, e.id id_empresa, (select count(va.id) from tb_vaga va where va.idempresa = e.id and va.situacao = 'Ativo') qtd_vagas, DATEDIFF(NOW(), v.datapublicacao) dias_publicacao,(((select count(idSkill) from tb_profissional_skills where idSkill in ( select idSkill from tb_vaga_skills  where idVaga = v.id) and idProfissional = ".$idProfissional.") *100)/ (select count(idSkill) from tb_vaga_skills where idVaga = v.id)) as porcentagem, v.latitude, v.longitude FROM tb_vaga v join tb_empresa e on v.idempresa = e.id join cidade c on c.id = e.cidade WHERE v.situacao = 'Ativo' and e.endereco != '' order by e.id");
        return $query->result();
    }

    function getPontosMapaWeb() {
        $query = $this->db->query("SELECT v.id, v.vaga, e.razaosocial, e.endereco, c.nome cidade, e.numero, 'bar' type, v.salario, e.id id_empresa, (select count(va.id) from tb_vaga va where va.idempresa = e.id and va.situacao = 'Ativo') qtd_vagas, DATEDIFF(NOW(), v.datapublicacao) dias_publicacao, v.latitude, v.longitude FROM tb_vaga v join tb_empresa e on v.idempresa = e.id join cidade c on c.id = e.cidade WHERE v.situacao = 'Ativo' and e.endereco != '' order by e.id");
        return $query->result();
    }

}
?>