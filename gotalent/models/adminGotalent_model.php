<?php
class AdminGotalent_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_admin_gotalent");
        return $query->result();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_admin_gotalent',$options);
        return $this->db->affected_rows();
    }
        
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_admin_gotalent");
        return $query->result();
    }
}
?>