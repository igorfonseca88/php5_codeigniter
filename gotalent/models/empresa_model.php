<?php

class Empresa_model extends CI_Model {

    function getAll() {
        $query = $this->db->query("select e.*, u.senha from tb_empresa e join tb_usuario u on u.idEmpresa = e.id ");
        return $query->result();
    }

    function add_record($options = array()) {
        $this->db->insert('tb_empresa', $options);
        return $this->db->insert_id();
    }

    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_empresa', $options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id) {
        $sql = "select e.*, est.uf, p.moduloBancoCurriculo, p.moduloVideoEntrevista, p.moduloAnuncio from tb_empresa e join tb_plano_empresa p on p.id = e.idPlanoEmpresa left join estado est on est.id = e.estado where e.id = ? ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function buscarEmpresaPorEmail($email) {
        $sql = "select * from tb_empresa where email = ? ";
        $query = $this->db->query($sql, array($email));
        return $query->result();
    }

    function buscarEmpresaComUrlPorApelido($apelido) {
        $sql = "select * from tb_empresa where apelido = ? ";
        $query = $this->db->query($sql, array($apelido));
        return $query->result();
    }

    function buscarEmpresaPorCNPJ($cnpj) {
        $sql = "select * from tb_empresa where cnpj = ? ";
        $query = $this->db->query($sql, array($cnpj));
        return $query->result();
    }

    function buscarTotaisDashboard($id) {
        $sql = "select  
		(select count(id) from tb_profissional ) as total, 
		(select count(id) from tb_profissional where estado = es.id) as estado,
		(select count(id) from tb_profissional where cidade = c.nome) as cidade,
                e.dataInicioPlano, e.dataFinalPlano, e.demonstracao, p.plano
		from tb_empresa e 
                join tb_plano_empresa p on p.id = e.idPlanoEmpresa
                left join estado es on es.id = e.estado 
                left join cidade c on c.id = e.cidade where e.id = ?";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function buscarEmpresaPorIdVaga($idVaga) {
        $sql = "select e.*,v.vaga from tb_empresa e join tb_vaga v on v.idEmpresa = e.id where v.id = ? ";
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }
    
    function buscarEmpresasComLogo() { 
        $query = $this->db->query("select distinct e.* from tb_empresa e 
        left join tb_vaga v on e.id = v.idEmpresa and v.situacao = 'Ativo'
        where logo is not null
        order by v.id desc");
        return $query->result();
    }

    
    /* CONTATO EMPRESA*/
    function add_contatoempresa($options = array()) {
        $this->db->insert('tb_contato_empresa', $options);
        return $this->db->insert_id();
    }
}

?>