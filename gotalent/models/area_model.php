<?php
class Area_model extends CI_Model {
    
    function buscarTodasAreas() {
        $query = $this->db->query("select * from tb_area order by area");
        return $query->result();
    }
    
    function buscarSubAreaPorIdArea($idArea){
        $query = $this->db->query("select * from tb_subarea where idArea = $idArea  order by subarea");
        return $query->result();
    }
    
    function buscarTodasSubAreas(){
        $query = $this->db->query("select s.*, a.area from tb_subarea s join tb_area a on a.id = s.idArea order by a.area");
        return $query->result();
    }
	
    
}
?>