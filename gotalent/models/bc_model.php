<?php

class Bc_model extends CI_Model {

    function autocompletarPorCargo($cargo) {
        $sql = "select distinct(cargo) as cargo from tb_experiencia_profissional
		where cargo like '$cargo%' ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function autocompletarPorSkill($skill) {
       $query = $this->db->query("select skill from tb_skills where skill like '%$skill%' order by skill ");
        return $query->result();
    }

    function retornaProfissionaisCompativeis( $estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill) {

        if ($estado <> "") {
            $filtroEstado = " and p.estado = $estado ";
            
            if($cidade <> ""){
                $filtroCidade = " and p.cidade = $cidade";
            }
        }
        
        if ($pretensao <> "") {
            $filtroPretensao = " and p.pretensao  $tipoPretensao $pretensao";
        }
        if ($cargo <> "") {
            $filtroCargo = " and p.id in (select idProfissional from tb_experiencia_profissional where upper(cargo) like '%$cargo%')  ";
            $filtroUltimoCargo = " and cargo like '%$cargo%'";
        }
        
        
        if($sexo <> ""){
            $filtroSexo = " and p.sexo = '$sexo'";
        }
        
        if($sitProfissional != ""){
            if($sitProfissional == "Empregado"){
                $filtroEmpregado = " and p.id in ( select idProfissional from tb_experiencia_profissional where p.id = idProfissional and empregoAtual = 1 )";
            }else{
                $filtroEmpregado = " and p.id in ( select idProfissional from 
                        tb_experiencia_profissional ex where p.id = ex.idProfissional and ex.empregoAtual <> 1 and ex.idProfissional  not in (select idProfissional  from tb_experiencia_profissional
                        where idProfissional  = ex.idProfissional and  empregoAtual = 1)) ";
            }
        }
        
        if($skill <> ""){
            $filtroSkill = " and p.id in ( select idProfissional from tb_profissional_skills ps join tb_skills s on s.id = ps.idSkill where s.skill like '%$skill%')  ";
        }
        
        $sql = "select p.nome, 0 as porcentagem,
			p.email, p.telefone, c.nome as cidade, e.uf as estado, p.pretensao, p.id, p.foto, pla.tipoPlano,
                        (SELECT CONCAT(cargo, ' - Empresa: ', empresa) 
                        FROM tb_experiencia_profissional 
                        where idprofissional = p.id $filtroUltimoCargo and (empregoAtual =1 or dataFim is not null ) 
                        order by empregoAtual desc, dataFim desc, id desc limit 0,1) as ultCargo

			from tb_profissional p 
			join tb_plano_profissional pla on pla.id = p.idPlano
			join tb_usuario u on u.idProfissional = p.id
                        left join estado e on e.id = p.estado
                        left join cidade c on c.id = p.cidade
			where u.situacao = 'Ativo' 
				$filtroEstado $filtroPretensao $filtroCargo $filtroCidade $filtroSexo $filtroEmpregado $filtroSkill
			
			group by p.nome, p.email, p.telefone, p.cidade, p.estado, p.pretensao, p.id
			order by pla.tipoPlano desc, p.pretensao
			
			 ";

        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function retornaProfissionaisCompativeisComPaginacao( $estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill, $_limit = 100, $_start = 0, $palavraChave, $strDateI, $strDateF) {

        if ($estado <> "") {
            $filtroEstado = " and p.estado = $estado";

            if($cidade <> ""){
                $filtroCidade = " and p.cidade = $cidade";
            }
        }
        
        if ($pretensao <> "") {
            $filtroPretensao = " and p.pretensao  $tipoPretensao $pretensao";
        }
        if ($cargo <> "") {
            $filtroCargo = " and p.id in (select idProfissional from tb_experiencia_profissional where upper(cargo) like '%$cargo%') ";
                $filtroUltimoCargo = " and cargo like '%$cargo%'";
        }

        if($sexo <> ""){
            $filtroSexo = " and p.sexo = '$sexo'";
        }
        
        if($sitProfissional != ""){
            if($sitProfissional == "Empregado"){
                $filtroEmpregado = " and p.id in ( select idProfissional from tb_experiencia_profissional where p.id = idProfissional and empregoAtual = 1 )";
            }else{
                $filtroEmpregado = " and p.id in ( select idProfissional from 
                        tb_experiencia_profissional ex where p.id = ex.idProfissional and ex.empregoAtual <> 1 and ex.idProfissional  not in (select idProfissional  from tb_experiencia_profissional
                        where idProfissional  = ex.idProfissional and  empregoAtual = 1)) ";
            }
        }
        
        if($skill <> ""){
            $filtroSkill = " and p.id in ( select idProfissional from tb_profissional_skills ps join tb_skills s on s.id = ps.idSkill where s.skill like '%$skill%')  ";
        }

        if($palavraChave != ""){
            $filtroPalavraChave = " and (p.nome LIKE '%{$palavraChave}%' OR p.email LIKE '%{$palavraChave}%' OR p.telefone LIKE '%{$palavraChave}%' OR p.facebook LIKE '%{$palavraChave}%' OR p.linkedin LIKE '%{$palavraChave}%' OR p.curriculo LIKE  '%{$palavraChave}%' OR p.endereco LIKE '%{$palavraChave}%' OR p.cep LIKE '%{$palavraChave}%' OR p.cidade LIKE '%{$palavraChave}%'
OR p.estado LIKE '%{$palavraChave}%' OR p.bairro LIKE '%{$palavraChave}%' OR p.sexo LIKE '%{$palavraChave}%' OR p.observacao LIKE '%{$palavraChave}%' OR p.resumo LIKE '%{$palavraChave}%' OR p.repositorio LIKE '%{$palavraChave}%' OR p.nivelCarreira LIKE '%{$palavraChave}%')";
        }

       if($strDateI != "" && $strDateF != ""){
         $filtroFaixaEtaria = " and p.nascimento BETWEEN '{$strDateF}' AND '{$strDateI}'";
       }
        
        $sql = "select p.nome, 0 as porcentagem,
			p.email, p.telefone, c.nome as cidade, e.uf as estado, p.pretensao, p.id, p.foto, pla.tipoPlano,
                        (SELECT CONCAT(cargo, ' - Empresa: ', empresa)
                        FROM tb_experiencia_profissional 
                        where idprofissional = p.id $filtroUltimoCargo and (empregoAtual =1 or dataFim is not null ) 
                        order by empregoAtual desc, dataFim desc, id desc limit 0,1) as ultCargo

			from tb_profissional p 
			join tb_plano_profissional pla on pla.id = p.idPlano
			join tb_usuario u on u.idProfissional = p.id
                        left join estado e on e.id = p.estado
                        left join cidade c on c.id = p.cidade
			where u.situacao = 'Ativo' 
				$filtroEstado $filtroPretensao $filtroCargo $filtroCidade $filtroSexo $filtroEmpregado $filtroSkill $filtroPalavraChave $filtroFaixaEtaria
			
			group by p.nome, p.email, p.telefone, p.cidade, p.estado, p.pretensao, p.id
			order by pla.tipoPlano desc, p.pretensao, p.nome
			limit $_start, $_limit 
			 ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function retornaProfissionaisCompativeisSemPaginacao( $estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill, $_limit, $_start, $palavraChave, $strDateI, $strDateF) {

        if ($estado <> "") {
            $filtroEstado = " and p.estado = $estado";

            if($cidade <> ""){
                $filtroCidade = " and p.cidade = $cidade";
            }
        }
        
        if ($pretensao <> "") {
            $filtroPretensao = " and p.pretensao  $tipoPretensao $pretensao";
        }
        if ($cargo <> "") {
            $filtroCargo = " and p.id in (select idProfissional from tb_experiencia_profissional where upper(cargo) like '%$cargo%')  ";
            $filtroUltimoCargo = " and cargo like '%$cargo%'";
        }

        if($sexo <> ""){
            $filtroSexo = " and p.sexo = '$sexo'";
        }
        
        if($sitProfissional != ""){
            if($sitProfissional == "Empregado"){
                $filtroEmpregado = " and p.id in ( select idProfissional from tb_experiencia_profissional where p.id = idProfissional and empregoAtual = 1 )";
            }else{
                $filtroEmpregado = " and p.id in ( select idProfissional from 
                        tb_experiencia_profissional ex where p.id = ex.idProfissional and ex.empregoAtual <> 1 and ex.idProfissional  not in (select idProfissional  from tb_experiencia_profissional
                        where idProfissional  = ex.idProfissional and  empregoAtual = 1)) ";
            }
        }
        
        if($skill <> ""){
            $filtroSkill = " and p.id in ( select idProfissional from tb_profissional_skills ps join tb_skills s on s.id = ps.idSkill where s.skill like '%$skill%')  ";
        }
        
        if($palavraChave !=  ""){
            $filtroPalavraChave = "and (p.nome LIKE '%{$palavraChave}%' OR p.email LIKE '%{$palavraChave}%' OR p.telefone LIKE '%{$palavraChave}%' OR p.facebook LIKE '%{$palavraChave}%' OR p.linkedin LIKE '%{$palavraChave}%' OR p.curriculo LIKE  '%{$palavraChave}%' OR p.endereco LIKE '%{$palavraChave}%' OR p.cep LIKE '%{$palavraChave}%' OR p.cidade LIKE '%{$palavraChave}%'
OR p.estado LIKE '%{$palavraChave}%' OR p.bairro LIKE '%{$palavraChave}%' OR p.sexo LIKE '%{$palavraChave}%' OR p.observacao LIKE '%{$palavraChave}%' OR p.resumo LIKE '%{$palavraChave}%' OR p.repositorio LIKE '%{$palavraChave}%' OR p.nivelCarreira LIKE '%{$palavraChave}%')";
        }

        if($strDateI != "" && $strDateF != ""){
            $filtroFaixaEtaria = " and p.nascimento BETWEEN '{$strDateF}' AND '{$strDateI}'";
        }


        $sql = "select p.nome, 0 as porcentagem,
            p.email, p.telefone, c.nome as cidade, e.uf as estado, p.pretensao, p.id, p.foto, pla.tipoPlano,
                        (SELECT CONCAT(cargo, ' - Empresa: ', empresa) 
                        FROM tb_experiencia_profissional 
                        where idprofissional = p.id $filtroUltimoCargo and (empregoAtual =1 or dataFim is not null ) 
                        order by empregoAtual desc, dataFim desc, id desc limit 0,1) as ultCargo

            from tb_profissional p 
            join tb_plano_profissional pla on pla.id = p.idPlano
            join tb_usuario u on u.idProfissional = p.id
                        left join estado e on e.id = p.estado
                        left join cidade c on c.id = p.cidade
            where u.situacao = 'Ativo' 
                $filtroEstado $filtroPretensao $filtroCargo $filtroCidade $filtroSexo $filtroEmpregado $filtroSkill $filtroPalavraChave $filtroFaixaEtaria
            
            group by p.nome, p.email, p.telefone, p.cidade, p.estado, p.pretensao, p.id
            order by pla.tipoPlano desc, p.pretensao, p.nome
            limit $_start, $_limit 
             ";

        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function retornaTotalProfissionaisCompativeis( $estado, $pretensao, $tipoPretensao, $cargo, $cidade, $sexo, $sitProfissional, $skill, $palavraChave, $strDateI, $strDateF) {

        if ($estado <> "") {
            $filtroEstado = " and p.estado = $estado";
            
            if($cidade <> ""){
                $filtroCidade = " and p.cidade = $cidade";
            }
        }
        
        if ($pretensao <> "") {
            $filtroPretensao = " and p.pretensao  $tipoPretensao $pretensao";
        }
        if ($cargo <> "") {
            $filtroCargo = " and p.id in (select idProfissional from tb_experiencia_profissional where upper(cargo) like '%$cargo%')  ";
            $filtroUltimoCargo = " and cargo like '%$cargo%'";
        }
        
        
        if($sexo <> ""){
            $filtroSexo = " and p.sexo = '$sexo'";
        }
        
        if($sitProfissional != ""){
            if($sitProfissional == "Empregado"){
                $filtroEmpregado = " and p.id in ( select idProfissional from tb_experiencia_profissional where p.id = idProfissional and empregoAtual = 1 )";
            }else{
                $filtroEmpregado = " and p.id in ( select idProfissional from 
                        tb_experiencia_profissional ex where p.id = ex.idProfissional and ex.empregoAtual <> 1 and ex.idProfissional  not in (select idProfissional  from tb_experiencia_profissional
                        where idProfissional  = ex.idProfissional and  empregoAtual = 1)) ";
            }
        }
        
        if($skill <> ""){
            $filtroSkill = " and p.id in ( select idProfissional from tb_profissional_skills ps join tb_skills s on s.id = ps.idSkill where s.skill like '%$skill%')  ";
        }

        if($palavraChave !=  ""){
            $filtroPalavraChave = "and (p.nome LIKE '%{$palavraChave}%' OR p.email LIKE '%{$palavraChave}%' OR p.telefone LIKE '%{$palavraChave}%' OR p.facebook LIKE '%{$palavraChave}%' OR p.linkedin LIKE '%{$palavraChave}%' OR p.curriculo LIKE  '%{$palavraChave}%' OR p.endereco LIKE '%{$palavraChave}%' OR p.cep LIKE '%{$palavraChave}%' OR p.cidade LIKE '%{$palavraChave}%'
OR p.estado LIKE '%{$palavraChave}%' OR p.bairro LIKE '%{$palavraChave}%' OR p.sexo LIKE '%{$palavraChave}%' OR p.observacao LIKE '%{$palavraChave}%' OR p.resumo LIKE '%{$palavraChave}%' OR p.repositorio LIKE '%{$palavraChave}%' OR p.nivelCarreira LIKE '%{$palavraChave}%')";
        }

        if($strDateI != "" && $strDateF != ""){
            $filtroFaixaEtaria = " and p.nascimento BETWEEN '{$strDateF}' AND '{$strDateI}'";
        }
        
        $sql = "select count(p.id) as total
			from tb_profissional p 
			join tb_plano_profissional pla on pla.id = p.idPlano
			join tb_usuario u on u.idProfissional = p.id
                        left join estado e on e.id = p.estado
                        left join cidade c on c.id = p.cidade
			where u.situacao = 'Ativo' 
				$filtroEstado $filtroPretensao $filtroCargo $filtroCidade $filtroSexo $filtroEmpregado $filtroSkill $filtroPalavraChave $filtroFaixaEtaria
			
			
			 ";

        $query = $this->db->query($sql);
        return $query->result();
    }

}

?>