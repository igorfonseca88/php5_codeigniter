<?php

class Usuario_model extends CI_Model {

    private $nome;
    private $login;
    private $senha;
    private $idUsuario;
    private $situacao;
    private $idProfissional;
    private $idEmpresa;
    private $tipo;
    // plano do profissional
    private $idPlano;
    private $tipoPlano;
    private $dataFimPlano;

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    function getLogin() {
        return $this->login;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }

    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    public function getSituacao() {
        return $this->situacao;
    }

    public function setSituacao($situacao) {
        $this->situacao = $situacao;
    }

    public function getIdProfissional() {
        return $this->idProfissional;
    }

    public function setIdProfissional($idProfissional) {
        $this->idProfissional = $idProfissional;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function getIdEmpresa() {
        return $this->idEmpresa;
    }

    public function setIdEmpresa($idEmpresa) {
        $this->idEmpresa = $idEmpresa;
    }

    // Planos do profissional

    public function getIdPlano() {
        return $this->idPlano;
    }

    public function setIdPlano($idPlano) {
        $this->idPlano = $idPlano;
    }

    public function getTipoPlano() {
        return $this->tipoPlano;
    }

    public function setTipoPlano($tipoPlano) {
        $this->tipoPlano = $tipoPlano;
    }

    public function getDataFinalPlano() {
        return $this->dataFinalPlano;
    }

    public function setDataFinalPlano($dataFinalPlano) {
        $this->dataFinalPlano = $dataFinalPlano;
    }

    function validate() {


        $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->getSenha()); //remove palavras que contenham a sintaxe sql
        $seg = trim($seg); //limpa espaços vazios
        $seg = strip_tags($seg); // tira tags html e php
        $seg = addslashes($seg); //adiciona barras invertidas a uma string

        $sql = "select idUsuario, login, situacao, IFNULL(p.nome,u.nome) as nome, idProfissional, tipo, idEmpresa, 
		p.idPlano, pp.tipoPlano, p.dataFinalPlano  
		from tb_usuario u 
		left join tb_profissional p ON u.idProfissional = p.id 
		left join tb_plano_profissional pp on pp.id = p.idPlano
		left join tb_empresa e ON e.id = u.idEmpresa
		where login = ? and senha = ?";

        $query = $this->db->query($sql, array($this->getLogin(), $seg));
        return $query->result();
    }

    function logged() {
        $logged = $this->session->userdata('logged');
        if (!isset($logged) || $logged != true) {
            return false;
        }
        return true;
    }

    function nomeLogin() {
        return $this->session->userdata('nome');
    }

    function usuarioExiste() {

        $seg = preg_replace(sql_regcase("/(\\\\)/"), "", $this->session->userdata('senha')); //remove palavras que contenham a sintaxe sql
        $seg = trim($seg); //limpa espaços vazios
        $seg = strip_tags($seg); // tira tags html e php
        $seg = addslashes($seg); //adiciona barras invertidas a uma string

        $sql = "select idUsuario, login, situacao, IFNULL(p.nome,u.nome) as nome, idProfissional, tipo, idEmpresa 
		from tb_usuario u 
		left join tb_profissional p ON u.idProfissional = p.id 
		left join tb_empresa e ON e.id = u.idEmpresa
		where login = ? and senha = ? ";

        $query = $this->db->query($sql, array($this->session->userdata('login'), $seg));
        if ($query->num_rows() > 0) {
            return TRUE;
        } else
            return FALSE;
    }

    function get_all() {
        $query = $this->db->query("select * from tb_usuario");
        return $query->result();
    }

    function get_nome($login) {
        $this->db->where('login', $login);
        $query = $this->db->get("tb_usuario");
        return $query->result();
    }

    function buscarUsuarioAtivoPorEmail($login) {
        $sql = "select * from tb_usuario where login = ? and situacao = 'ATIVO'";
        $query = $this->db->query($sql, array($login));
        return $query->result();
    }

    function buscarUsuarioPorEmail($login) {
        $sql = "select * from tb_usuario where login = ? ";
        $query = $this->db->query($sql, array($login));
        return $query->result();
    }

    function getAll() {
        $query = $this->db->query("select * from tb_usuario");
        return $query->result();
    }

    function buscarUsuarioPorIdEmpresa($idEmpresa) {
        $sql = "select * from tb_usuario where idEmpresa = ?";
        $query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }
    
    function buscarUsuarioPorIdEIdEmpresa($idUsuario,$idEmpresa) {
        $sql = "select * from tb_usuario where idUsuario = ? and idEmpresa = ?";
        $query = $this->db->query($sql, array($idUsuario,$idEmpresa));
        return $query->result();
    }

    function buscarUsuarioPorIdProfissional($idProfissional) {
        $sql = "select u.*, p.nome from tb_usuario u join tb_profissional p on p.id = u.idProfissional where u.idProfissional = ? ";
        $query = $this->db->query($sql, array($idProfissional));
        return $query->result();
    }

    function add_record($options = array()) {
        $this->db->insert('tb_usuario', $options);
        return $this->db->insert_id();
    }

    function update($id, $options = array()) {
        $this->db->where('idUsuario', $id);
        $this->db->update('tb_usuario', $options);
        return $this->db->affected_rows();
    }

    function delete($id) {
        $this->db->where('idUsuario', $id);
        $this->db->delete('tb_usuario', $options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id) {
        $sql = "select * from tb_usuario where idUsuario = ?";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    /*Facebook*/
    function add_user($options = array()) {
        $this->db->insert('tb_usuario', $options);
        return $this->db->insert_id();
    }
}

?>