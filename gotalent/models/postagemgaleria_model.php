<?php
class Postagemgaleria_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_postagensgaleria");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_postagensgaleria', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_postagensgaleria',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_postagensgaleria',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_postagensgaleria");
        return $query->result();
    }
	
    function removerImagemPrincipalPorPostagem($id) {
        $query = $this->db->query("update tb_postagensgaleria set principal = 'NAO' where idpostagem = $id");
        return $this->db->affected_rows();
    }
    
    function buscarPorIdPostagem($id) {
        $query = $this->db->query("select * from tb_postagensgaleria where idPostagem = $id");
        return $query->result();
    }
	
}
?>