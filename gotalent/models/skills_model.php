<?php
class Skills_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_skills order by skill asc");
        return $query->result();
    }
	
	
	
	function buscarTodasSkillsPorCategoria($categoria){
		$query = $this->db->query("select * from tb_skills where categoria = '$categoria' order by skill ");
        return $query->result();
	}
    
    
    function add_record($options = array()) {
        $this->db->insert('tb_skills', $options);
        return $this->db->insert_id();
    }
	

    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_skills',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_skills',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_skills");
        return $query->result();
    }
	
	function buscarPorNome($skill) {
        $this->db->where('skill', $skill);
        $query = $this->db->get("tb_skills");
        return $query->result();
    }
	
	function buscarSkills($skill) {
       $query = $this->db->query("select skill from tb_skills where skill like '%$skill%' order by skill ");
        return $query->result();
    }
	
	function buscarTodasSkillsDisponiveisPorIdProfissional($idProfissional){
		$query = $this->db->query("select * 
		from tb_skills  
		where id not in (select idSkill from tb_profissional_skills where idProfissional = $idProfissional) order by skill ");
        return $query->result();
	}
	
	function buscarTodasSkillsDisponiveisPorIdVaga($idVaga){
		$query = $this->db->query("select * 
		from tb_skills  
		where id not in (select idSkill from tb_vaga_skills where idVaga = $idVaga) order by skill ");
        return $query->result();
	}
}
?>