<?php
class Plano_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_planos order by id ");
        return $query->result();
    }
	   
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_planos");
        return $query->result();
	}	
    	
	function buscarPlanosProfissionais(){
		$query = $this->db->query("select * from tb_plano_profissional order by id ");
        return $query->result();
	}
	function buscarPlanosPremiumProfissionais(){
		$query = $this->db->query("select * from tb_plano_profissional where plano != 'FREE' order by id ");
        return $query->result();
	}
	
	function buscarPlanoProfissionalPorTipoEDias($tipo, $dias){
		$query = $this->db->query("select * from tb_plano_profissional where tipoPlano = '$tipo' and diasPlano = $dias order by id ");
        return $query->result();
	}
	
	function buscarPlanoProfissionalPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_plano_profissional");
        return $query->result();
	}	
		
	function buscarPlanosEmpresas(){
		$query = $this->db->query(" select * from tb_plano_empresa where plano not in('Básico','Grátis') order by id ");
        return $query->result();
	}
	
	function buscarPlanosEmpresasPorId($id){
		$query = $this->db->query(" select * from tb_plano_empresa where id = $id ");
        return $query->result();
	}
	
	
}
?>