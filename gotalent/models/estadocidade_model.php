<?php
class EstadoCidade_model extends CI_Model {
    
    function buscarEstados() {
        $query = $this->db->query("select * from estado order by uf");
        return $query->result();
    }
    
    function buscarCidadesPorIdEstado($idEstado){
        $query = $this->db->query("select * from cidade where estado = $idEstado  order by nome");
        return $query->result();
    }
    
    function buscarTodasSubAreas(){
        $query = $this->db->query("select s.*, a.area from tb_subarea s join tb_area a on a.id = s.idArea order by a.area");
        return $query->result();
    }
	
    function buscarCidadePorId($idCidade){
        $query = $this->db->query("select * from cidade where id = $idCidade ");
        return $query->result();
    }
    function buscarEstadosPorId($idEstado){
        $query = $this->db->query("select * from cidade where id = $idEstado ");
        return $query->result();
    }


    function buscarCidadePorNome($cidade, $idEstado){
        $query = $this->db->query("select c.id from cidade c where c.nome LIKE '%$cidade%' AND c.estado = '$idEstado'");
        return $query->result();
    }
    /*Busca o ID do estado pelo nome do estado*/
    function buscarEstadoPorNome($estado) {
        $query = $this->db->query("select id from estado where nome LIKE '%$estado%'");
        return $query->result();
    }

    /*Busca o ID do estado pelo uf do estado*/
    function buscarIdEstadoPorUF($estado) {
        $query = $this->db->query("select id from estado where uf LIKE '$estado'");
        return $query->result();
    }


    function buscarEstadoPorId($idEstado){
        $query = $this->db->query("select * from estado where id = $idEstado ");
        return $query->result();
    }
}
?>