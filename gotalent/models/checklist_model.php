<?php

class Checklist_model extends CI_Model {

    function getAll() {
        $query = $this->db->query("select * from tb_categorias order by titulo asc");
        return $query->result();
    }

    function buscarChecklistPorId($id) {
        $sql = "select * from tb_checklist where id = ? ";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function buscarChecklistPorIdEmpresa($idEmpresa) {
        $sql = "select * from tb_checklist where idEmpresa = ? ";
        $query = $this->db->query($sql, array($idEmpresa));
        return $query->result();
    }
    
    function buscarChecklistPorIdEmpresaEQuestoesPorComponente($idEmpresa,$componente) {
        $sql = "select * from tb_checklist c where idEmpresa = ? and id in (select idChecklist from tb_checklist_item where idChecklist = c.id and componente = ?) ";
        $query = $this->db->query($sql, array($idEmpresa, $componente));
        return $query->result();
    }

    function buscarChecklistPorIdEIdEmpresa($idEmpresa, $id) {
        $sql = "select * from tb_checklist where idEmpresa = ? and id = ? ";
        $query = $this->db->query($sql, array($idEmpresa, $id));
        return $query->result();
    }

    function buscarQuestoesPorIdChecklist($idChecklist) {
        $sql = "select * from tb_checklist_item where idChecklist = ? ";
        $query = $this->db->query($sql, array($idChecklist));
        return $query->result();
    }

    function buscarQuestoesPorIdChecklistEComponente($idChecklist, $tipo) {
        $sql = "select * from tb_checklist_item where idChecklist = ? and componente = ?";
        $query = $this->db->query($sql, array($idChecklist, $tipo));
        return $query->result();
    }
    
    function buscarRespostaChecklistPorIdProfissionalEIdChecklist($idProfissional, $idChecklist){
        $sql = "select * from tb_checklist_resposta where idProfissional = ? and idChecklistItem in (select id from tb_checklist_item where idChecklist = ?)";
        $query = $this->db->query($sql, array($idProfissional, $idChecklist));
        return $query->result();
    }
    
    function buscarChecklistEmUsoPorIdChecklist($idChecklist) {
        $sql = "select c.* from tb_checklist c join tb_vaga v on v.idChecklist = c.id where c.id = ? 
                UNION 
                select c.* from tb_checklist c join tb_profissional_vaga v on v.idChecklist = c.id where c.id = ?";
        $query = $this->db->query($sql, array($idChecklist,$idChecklist));
        return $query->result();
    }
    
    function buscaRespostaPorIdProfissionalEIdVaga($idProfissional, $idVaga){
        $sql = "select * from tb_checklist_resposta cr join tb_checklist_item ci on cr.idChecklistItem = ci.id "
                . "where idProfissional = ? "
                . "and idVaga = ? order by ci.componente";
        $query = $this->db->query($sql, array($idProfissional, $idVaga));
        return $query->result();
    }
    
    function buscaRespostaPorIdProfissionalEIdVagaEComponente($idProfissional, $idVaga, $componente){
        if($componente != ""){
            $args= " and ci.componente = '$componente' ";
        }
        else{
            $args= " and ci.componente in ('Objetiva', 'Discursiva') ";
        }
        $sql = "select * from tb_checklist_resposta cr join tb_checklist_item ci on cr.idChecklistItem = ci.id "
                . "where idProfissional = ? "
                . "and idVaga = ?  $args order by ci.componente";
       
        $query = $this->db->query($sql, array($idProfissional, $idVaga));
        return $query->result();
    }
    
    function buscaChecklistPorIdVaga($idVaga){
        $sql = "select * from tb_checklist c  "
                . "where id in (select ci.idChecklist from tb_checklist_item ci "
                . "join tb_checklist_resposta cr on cr.idChecklistItem = ci.id where idVaga = ? ) ";
                
        $query = $this->db->query($sql, array($idVaga));
        return $query->result();
    }

    function add_record($options = array()) {
        $this->db->insert('tb_checklist', $options);
        return $this->db->insert_id();
    }

    function add_questao($options = array()) {
        $this->db->insert('tb_checklist_item', $options);
        return $this->db->insert_id();
    }
    
    function add_resposta($options = array()) {
        $this->db->insert('tb_checklist_resposta', $options);
        return $this->db->insert_id();
    }

    function buscarRespostas($idChecklistItem, $idVaga, $idProfissional) {
        $sql = "select * from tb_checklist_resposta where idChecklistItem = ? and idVaga = ? and idProfissional = ? ";
        $query = $this->db->query($sql, array($idChecklistItem, $idVaga, $idProfissional));
        return $query->result();
    }

    function updateResposta($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_checklist_resposta', $options);
        return $this->db->affected_rows();
    }

    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_checklist', $options);
        return $this->db->affected_rows();
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_checklist', $options);
        return $this->db->affected_rows();
    }
    
    function delete_item($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_checklist_item', $options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_categorias");
        return $query->result();
    }

}

?>