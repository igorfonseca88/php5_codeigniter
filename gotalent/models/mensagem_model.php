<?php

class Mensagem_model extends CI_Model {

    function getAll() {
        $query = $this->db->query("select * from tb_mensagem  ");
        return $query->result();
    }

    function add_record($options = array()) {
        $this->db->insert('tb_mensagem', $options);
        return $this->db->insert_id();
    }

    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_mensagem', $options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_mensagem");
        return $query->result();
    }

    function buscarPorIdOrigem($id, $tipo) {
        $query = $this->db->query("select * from tb_mensagem where idOrigem = $id and tipo = '$tipo'  ");
        return $query->result();
    }

    function buscarPorIdDestino($id, $tipo) {
        $query = $this->db->query("select * from tb_mensagem where idDestino = $id and tipo = '$tipo'  ");
        return $query->result();
    }
    
    function validaMensagemRecebidaNosUltimos3Dias($mensagem, $idDestino) {
        $query = $this->db->query("select * from tb_mensagem where idDestino = $idDestino "
                . " and mensagem = '$mensagem' and TO_DAYS(NOW()) - TO_DAYS(data) <= 3 ");
        return $query->result();
    }

}

?>