<?php
class Equipe_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_site_equipe order by nome");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_site_equipe', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_site_equipe',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_site_equipe',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_site_equipe");
        return $query->result();
    }
}
?>