<?php
class Galeria_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select * from tb_galeria ");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_galeria', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_galeria',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_galeria',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_galeria");
        return $query->result();
    }
	
    function buscarPorIdEmpresa($id) {
        $query = $this->db->query("select * from tb_galeria where idEmpresa = $id");
        return $query->result();
    }
}
?>