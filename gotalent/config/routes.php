<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "principal";
$route['/'] = "principal";
$route['login'] = 'login/login';
$route['404_override'] = 'principal/erro404';

$route['area-restrita'] = "principal/arearestrita";
$route['area-restrita-empresa'] = "principal/arearestritaempresa";

$route['profissionais'] = "principal/planosProfissionais";
$route['empresas'] = "principal/planosEmpresa";

$route['profissional'] = "principal/cadastroProfissional";
$route['empresa'] = "principal/cadastroEmpresa";

$route['contato'] = "principal/contato";
$route['enviar-contato'] = "principal/enviarContato";

$route['quem-somos'] = "principal/quemsomos";
$route['texto/(:any)'] = "principal/texto/$1";
$route['quem-somos'] = "principal/quemsomos";
$route['termo-de-uso'] = "principal/texto/4";

$route['blog/(:any)/(:any)'] = "principal/post/$1/$2";
$route['blog/(:any)'] = "principal/blog/$1";
$route['blog'] = "principal/blog";


$route['cadastro-empresa'] = "principal/cadastro";
$route['cadastro-empresa/passo-(:any)'] = "principal/cadastro/$1";
$route['cadastro-profissional'] = "principal/cadastroCandidato";

$route['profissional-facebook'] = "facebookController/cadastroProfissionalFacebook";
$route['cadastro-profissional-facebook'] = "facebookController/cadastroCandidatoFacebook";
$route['entrar-facebook'] = "facebookController/loginFacebook";
$route['entrarFacebook'] = "facebookController/loginFacebook";

$route['planos'] = "principal/planos";
$route['buscar'] = "principal/buscar";

$route['vagas'] = "principal/listarVagas";
$route['vagas/(:any)'] = "principal/vagas/$1";
$route['vaga/(:any)'] = "principal/vaga/$1";

$route['detalhes-vaga/(:num)'] = "profissionalController/detalhesVaga/$1";
$route['detalhes-vaga'] = "profissionalController/detalhesVaga";

$route['candidatar/(:num)'] = "principal/candidatar/$1";

$route['confirmar-cadastro/(:num)'] = "principal/confirmarCadastro/$1";
$route['esqueci-minha-senha'] = "principal/recuperarSenha";
$route['recuperar'] = "principal/recuperar";


$route['empresa/(:any)'] = "principal/empresa/$1";
$route['seja-premium'] = "principal/premium";

$route['pre-cadastro'] = "principal/preCadastroProfissional";
$route['fale-com-consultor'] = "principal/faleComConsultor";

$route['cadastro-empresa/plano-(:num)'] = "principal/cadastro/$1";

/*URLS*/


$route['trabalhe-na/(:any)'] = "cempresa/empresa/$1";
$route['job/(:any)/(:any)'] = "cempresa/job/$2";

?>
