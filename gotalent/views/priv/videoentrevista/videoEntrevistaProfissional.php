<?
$this->load->view('priv/videoentrevista/_inc/superior');
?>

<head>

    <!-- script used for audio/video/gif recording -->
    <script src="<?echo base_url()?>js/RecordRTC.js">
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js"></script> 
        <script type="text/javascript">
            $(document).ready(function(){
                $("a[rel=modal]").click( function(ev){
                    ev.preventDefault();

                    var id = $(this).attr("href");

                    var alturaTela = $(document).height();
                    var larguraTela = $(window).width();
    
                    //colocando o fundo preto
                    $('#mascara').css({'width':larguraTela,'height':alturaTela/1.5});
                    $('#mascara').fadeIn(1000); 
                    $('#mascara').fadeTo("slow",0.8);

                    var left = ($(window).width() /4) - ( $(id).width() / 4 );
                    //var top = ($(window).height() / 4) - ( $(id).height() / 4 );
                    var top = 10;
                    
                    $(id).css({'top':top,'left':left});
                    $(id).show();   
                    $(".windowR").css({'top':top*10,'left':left});
                    $(".windowR").show(); 
                });

                $("#mascara").click( function(){
                    $(this).hide();
                    $(".window").hide();
                    $(".windowR").hide();
                    limparAudioVideo();
                });

                $('.fechar').click(function(ev){
                    ev.preventDefault();
                    $("#mascara").hide();
                    $(".window").hide();
                    $(".windowR").hide();
                    limparAudioVideo();
                });
            });
        </script>

        
</head>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Vídeo entrevista</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li class="active">
                <a href="<? echo base_url() ?>profissionalController/meusProcessosSeletivos">Meus processos</a>
            </li>
            <li class="active">
                <strong>Vídeo entrevista</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Vídeo entrevista - </h5>
                    <h5><a target='_blank' href='<?echo base_url()?>videoentrevista/index.html'><img src="<?echo base_url()?>img/testvideo.png" width="25px" height="25px" />Testar áudio e vídeo</a></h5>
                </div>
            <div class="ibox-content">  
                <table>
                <? foreach ($questoes_video as $row) { ?>

                <?
                    /*$file = "videoentrevista/audios/".$row->id.".mp3";
                    $message = $row->questao;

                    $fd = fopen($file, 'wb');
                    $ch = curl_init("http://translate.google.com/translate_tts?ie=UTF-8&tl=pt-br&q=" . urlencode($message));
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla');
                    curl_setopt($ch, CURLOPT_FILE, $fd);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);*/
                ?>

                <tr>
                <td width="450px" height="70px"><h3 id="descricao<?= $row->id?>"><?= $row->questao ?>  </td>
                <td>
                    <? if($videoentrevista[$row->id]["qtdRespostas"] > 0){ ?>
                        <p id="resp<?= $row->id ?>"> <a class="link_ver" target='_blank' href='<?echo base_url()?>videoentrevista/review.php?arq=<?= $videoentrevista[$row->id]['respostaVideo']?>'>Ver resposta</a> <a class="link_regravar" onclick="javascript:carregaInfos(<?= $row->id ?>, '<?= $row->questao ?>');" id="<?= $row->id ?>" href="#pergunta" rel="modal">Responder novamente</a></p>
                    <? }else { ?>
                        <p id="resp<?= $row->id ?>"><a class="link_video" onclick="javascript:carregaInfos(<?= $row->id ?>, '<?= $row->questao ?>');" id="<?= $row->id ?>" href="#pergunta" rel="modal">Responder</a></p>
                    <? } ?>
                </td>
                </tr>
                <iframe id="frame" hidden="true" height="0px" width="0px" src=""></iframe>
                <!--<audio id='playAudio<?= $row->id ?>'></audio>-->
                <? } ?>
                </table>
                <br /><br /><br />
                Browsers compatíveis: <img src="<?echo base_url()?>img/firefox.png" width="25px" height="25px" /><img src="<?echo base_url()?>img/chrome.png" width="25px" height="25px" />
                <div align="center" class="window" id="pergunta">
                    <a href="#" class="fechar">X Fechar</a>
                    <h4 class="titulo" id="titulo"></h4>
                    <section class="experiment">
                        <p style="text-align: center;">
                            <video id="preview" controls style="border: 1px solid rgb(15, 158, 238); height: 100px; vertical-align: top; width: 300px;"></video>
                            <!--<audio id="previewAudio"></audio>-->
                        </p>

                        <button class="buttonVideo" id="record">Gravar Resposta</button>
                        <button class="buttonVideo" id="stop" disabled>Parar</button>
                        <br /><br />
                        <div id="container" align="center">
                        </div>
                    </section>
                     
                </div>

                <div align="center" class="windowR" id="recruiter">
                    <video id="recrutadora" autoplay></video>                   
                </div>

                <!-- mascara para cobrir o site --> 
                <div id="mascara"></div>
               
            </div>
            </div>
        </div>  

    </div>

</div>
                
    <script>
        var question, idQuestion;
        var playAudio;
        var recrutadora;
        function carregaInfos(idQ, q){
            idQuestion = idQ;
            question = q;
            //document.getElementById("titulo").innerHTML = "<a href='javascript:darPlayAudio();'><img src='<?echo base_url()?>img/som.png' height='25px' width='25px' /></a>    "+question;
            document.getElementById("titulo").innerHTML = question;
            var txt = question.replace(/ /gi, "-")
            document.getElementById("frame").src = "http://translate.google.com/translate_tts?ie=UTF-8&tl=pt-br&q="+txt;
            //darPlayAudio();
            recrutadora = document.getElementById("recrutadora");
            recrutadora.innerHTML = '<source src="<?echo base_url()?>videoentrevista/audios/recrutadora.mp4" type="video/mp4">';
                        var temp = question.length;
            setTimeout(stopRec, temp*100);
        }

        function stopRec() {
            recrutadora = document.getElementById("recrutadora");
            recrutadora.source = "";
            document.getElementById("recruiter").style.display = "none";
        }

        /*function darPlayAudio() {
            playAudio = document.getElementById("playAudio"+idQuestion);
            recrutadora = document.getElementById("recrutadora");
            playAudio.innerHTML = '<source src="<?echo base_url()?>videoentrevista/audios/'+idQuestion+'.mp3" type="audio/mpeg">'
            playAudio.playbackRate = 1.5;
            setTimeout(timeAudio, 1000);
            playAudio.play();
            recrutadora.innerHTML = '<source src="<?echo base_url()?>videoentrevista/audios/recrutadora.mp4" type="video/mp4">';
        }

        var time = 0;
        var duration;
        var intervalo;
        function timeAudio(){
            var audioDuration = Math.round(playAudio.duration/1.5);
            duration = audioDuration;
            time = 1;
            intervalo = setInterval('timeVideo()',983);
        }

        function timeVideo(){
            time++;
            if(time >= duration){
                playAudio.source = '';
                recrutadora.source = '';                
                duration = '';
                document.getElementById("recruiter").style.display = "none";
                clearInterval(intervalo);
                time = 0;
                intervalo = '';
            } 
        }

        function limparAudioVideo(){
            playAudio.source = '';
            recrutadora.source = '';                
            duration = '';
            document.getElementById("recruiter").style.display = "none";
            time = 0;
            intervalo = '';
        }*/

    </script>

    <script>

    function PostBlobFirefox(blob, fileType, fileName) {
            // FormData
            var formData = new FormData();
            formData.append(fileType + '-filename', fileName + '.webm');
            formData.append(fileType + '-blob', blob);

            // POST the Blob using XHR2
            xhr('<?echo base_url()?>videoentrevista/savefirefox.php', formData, function(fileURL) {
                var mediaElement = document.createElement(fileType);
                var source = document.createElement('source');
                var href = location.href.substr(0, location.href.lastIndexOf('/') + 1);
                source.src = href + fileURL;
                if (fileType == 'video') source.type = 'video/webm; codecs="vp8, vorbis"';
                if (fileType == 'audio') source.type = !!navigator.mozGetUserMedia ? 'audio/ogg' : 'audio/wav';
                mediaElement.appendChild(source);
                mediaElement.controls = true;
                container.innerHTML = "";
                document.getElementById('titulo').innerHTML = 'Resposta enviada com sucesso.';
                preview.poster = '<?echo base_url()?>img/logovideocapa.png';
                //document.getElementById('resp'+idQuestion).innerHTML = "<a class='link_video' target='_blank' href='<?echo base_url()?>videoentrevista/review.php?arq="+fileName+"'>Ver resposta</a>";

                gravarArquivoBanco(<?echo $idVaga?>,fileName,idQuestion,speechText);

            });
        }
    
    function PostBlob(audioBlob, videoBlob, fileName) {
        var formData = new FormData();
        formData.append('filename', fileName);
        formData.append('audio-blob', audioBlob);
        formData.append('video-blob', videoBlob);
        xhr('<?echo base_url()?>videoentrevista/savechrome.php', formData, function() {
            document.getElementById('titulo').innerHTML = 'Resposta enviada com sucesso.';
            preview.poster = '<?echo base_url()?>img/logovideocapa.png';
            //document.getElementById('resp'+idQuestion).innerHTML = "<a class='link_video' target='_blank' href='<?echo base_url()?>videoentrevista/review.php?arq="+fileName+"'>Ver resposta</a>";

            gravarArquivoBanco(<?echo $idVaga?>,fileName,idQuestion,speechText);
        });
    }

    var speechText = '';
    function goSpeech() {
        window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;                            
        if (window.SpeechRecognition != null) {
            var recognizer = new window.SpeechRecognition();
            recognizer.continuous = true
            recognizer.onresult = function(event){
                for (var i = event.resultIndex; i < event.results.length; i++) {
                    speechText += event.results[i][0].transcript;
                }
            }     
            recognizer.start();   
        }
    }

    var record = document.getElementById('record');
    var stop = document.getElementById('stop');
    var audio = document.querySelector('audio');
    var recordVideo = document.getElementById('record-video');
    var preview = document.getElementById('preview');
    var previewAudio = document.getElementById('previewAudio');
    var container = document.getElementById('container');
    var isFirefox = !!navigator.mozGetUserMedia;
    var recordAudio, recordVideo;
    preview.poster = '<?echo base_url()?>img/logovideocapa.png';
    
    record.onclick = function() {
        container.innerHTML = '';
        record.disabled = true;
        !window.stream && navigator.getUserMedia({
            audio: true,
            video: true
        }, function(stream) {
            window.stream = stream;
            onstream();
        }, function(error) {
            alert(JSON.stringify(error, null, '\t'));
        });
        goSpeech();
        window.stream && onstream();

        function onstream() {
            preview.src = window.URL.createObjectURL(stream);
            preview.play();
            preview.muted = true;

            recordAudio = RecordRTC(stream, {
                // bufferSize: 16384,
                onAudioProcessStarted: function() {
                    if (!isFirefox) {
                        recordVideo.startRecording();
                    }
                }
            });

            recordVideo = RecordRTC(stream, {
                type: 'video'
            });

            recordAudio.startRecording();

            stop.disabled = false;
            preview.poster = '';
            document.getElementById('titulo').innerHTML = 'Seu tempo máximo é de 2 minutos';
        }
    };

    var fileName;
    stop.onclick = function() {
        document.getElementById('titulo').innerHTML = '';
        record.disabled = false;
        stop.disabled = true;
        preview.src = '';
        preview.poster = '<?echo base_url()?>img/loadervideo.gif';
        now = new Date;
            
        fileName = Math.round(Math.random() * 99999999) + 99999999;
        fileName += '' + now.getDate() + "" + (now.getMonth()+1) + "" + now.getFullYear();

        if (!isFirefox) {
            recordAudio.stopRecording(function() {
                recordVideo.stopRecording(function() {
                    PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
                });
            });
        }else{
            recordAudio.stopRecording(function(url) {
                preview.src = url;
                PostBlobFirefox(recordAudio.getBlob(), 'video', fileName);
            });
        }
    };

    function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        };
        request.upload.onprogress = function(event) {
            document.getElementById('titulo').innerHTML = 'Salvando ' + Math.round(event.loaded / event.total * 100) + "%";
        };
        request.open('POST', url);
        request.send(data);
    }

    function xhrPersist(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        };
        request.upload.onprogress = function(event) {
            
        };
        request.open('POST', url);
        request.send(data);
    }
    </script>

<?
$this->load->view('priv/videoentrevista/_inc/inferior');
?>
