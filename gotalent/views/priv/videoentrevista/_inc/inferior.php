

<!-- Mainly scripts -->
<script src="<? echo base_url() ?>js/jquery-2.1.1.js"></script>
<script src="<? echo base_url() ?>js/bootstrap.min.js"></script>
<script src="<? echo base_url() ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- jQuery UI -->
<script src="<? echo base_url() ?>js/plugins/jquery-ui/jquery-ui.min.js"></script>



<!-- Custom and plugin javascript -->
<script src="<? echo base_url() ?>js/inspinia.js"></script>

<!-- CKEDITOR -->
<script type="text/javascript" src="<?= base_url() ?>system/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= base_url() ?>system/ckeditor/config.js"></script>

<!-- Data Tables -->
<script src="<? echo base_url() ?>js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<? echo base_url() ?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<? echo base_url() ?>js/plugins/dataTables/dataTables.responsive.js"></script>



<script src="<? echo base_url() ?>js/plugins/iCheck/icheck.min.js"></script>

<!-- Input Mask-->
<script src="<? echo base_url() ?>js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="<? echo base_url() ?>js/plugins/datapicker/bootstrap-datepicker.js"></script>	

<script type='text/javascript' src="<?= base_url() ?>js/jquery.autocomplete.js"></script>
<script src="<? echo base_url() ?>js/jquery.maskMoney.js"></script>
<script src="<? echo base_url() ?>js/jquery.maskedinput.min.js"></script>
<script src="<? echo base_url() ?>js/funcoes.js"></script>


<div class="footer">
    <div class="pull-right">
        Suporte/Dúvidas/Sugestões: <strong>contato@gotalent.com.br</strong> 
    </div>
    <div>
        Copyright © 2015 - Go Talent Portal On-line LTDA/21.642.706/0001­27 - Todos os direitos reservados
    </div>

</div>
</div>
</div>
</body>
</html>