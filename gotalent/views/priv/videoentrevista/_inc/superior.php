<?
if ($this->Usuario_model->logged() == FALSE && $this->session->userdata("tipo") == 'Profissional')  {
redirect('/login/', 'refresh');
}




//Transforma títulos em URL amigáveis
function url($str) {
$str = strtolower(utf8_decode($str)); $i=1;
$str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
$str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
while($i>0) $str = str_replace('--','-',$str,$i);
if (substr($str, -1) == '-') $str = substr($str, 0, -1);
return $str;
}

$description = "Na Go Talent você tem acesso a vagas de TI e ainda pode agendar sua entrevista de emprego direto.";
$keywords = "vagas de TI";
$title = "Vagas de TI: Encontre o emprego que você deseja";

?>


<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title><?=$title?></title>
        <meta name="author" content="Go Talent"/>
        <meta name="keywords" content="<?=$keywords?>"/>
        <meta name="description" content="<?=$description?>">
        <meta name="copyright" content="Copyright (c) 2015 GoTalent" />
        <meta http-equiv="content-language" content="pt-br">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link href="<?echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?echo base_url()?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?echo base_url()?>css/animate.css" rel="stylesheet">
        <link href="<?echo base_url()?>css/style.css" rel="stylesheet">
        <link href="<?echo base_url()?>css/plugins/iCheck/custom.css" rel="stylesheet">

        <link href="<?= base_url() ?>css/jquery.autocomplete.css" rel="stylesheet" />

        <!-- Morris -->
        <link href="<?echo base_url()?>css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

        <!-- Gritter -->
        <link href="<?echo base_url()?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

        <link href="<?echo base_url()?>css/plugins/datapicker/datepicker3.css" rel="stylesheet">

        <link href="<?echo base_url()?>css/videoentrevista.css" rel="stylesheet">


        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53411814-1', 'auto');
            ga('send', 'pageview');

        </script> 

    </head>

    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                    <a href="<?echo base_url()?>"> <img alt="image"  width="90%" src="<?echo base_url()?>img/logo.png" /></a>
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"> Olá, <? echo $this->session->userdata("nome")  ?>!</strong>
                                        </span>  </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="<? echo base_url()?>profissionalController/editarProfissionalAction">Meu currículo</a></li>
                                    <li><a href="<? echo base_url()?>profissionalController/editarUsuarioAction">Meus dados de acesso</a></li>

                                    <li><a href="<?= base_url() ?>login/login/logoff">SAIR</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                GO
                            </div>
                        </li>

                        <li>
                            <a href="<?= base_url() ?>area-restrita"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> </a>
                        </li>

                        <li>
                            <a href="<?= base_url() ?>profissionalController/editarUsuarioAction"><i class="fa fa-user"></i> <span class="nav-label">Meus dados de acesso</span> </a>
                        </li>

                        <li>
                            <a href="<?= base_url() ?>profissionalController/editarProfissionalAction"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Meu currículo</span> </a>
                        </li>					

                        <li>
                            <a href="<?= base_url() ?>profissionalController/meusProcessosSeletivos"><i class="fa fa-desktop"></i> <span class="nav-label">Meus processos</span> </a>
                        </li>
                         
                        <li>
                            <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Clube Go Talent</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?= base_url() ?>profissionalController/qualificacao">Qualificação </a></li>
                                <li><a href="<?= base_url() ?>profissionalController/eventos">Eventos 2015 </a></li>
                            </ul>
                        </li>  
                       

                        <? if($profissional[0]->tipoPlano == 'FREE'){?>
                        <li>
                            <a href="<?= base_url() ?>principal/plano"><i class="fa fa-cogs"></i> <span class="nav-label">Seja PREMIUM</span> </a>
                        </li> 
                        <?}?>

                    </ul>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                        </div>
                        <ul class="nav navbar-top-links navbar-right">

                      

                            <li>
                                <a href="<?= base_url() ?>login/login/logoff">
                                    <i class="fa fa-sign-out"></i> SAIR
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>	