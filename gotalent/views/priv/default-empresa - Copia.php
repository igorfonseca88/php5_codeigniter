<? $this->load->view('priv/empresa/_inc/superior');?>

    <div class="wrapper wrapper-content">  
    
        <!--Aviso pagamento de plano pendente-->
    	<? if (count($extrato_pendente) > 0) { ?>
    	<div class="row">
            <div class="col-lg-12">				
                <div class="alert alert-warning alert-dismissable">
                    Aguardando confirmacao de pagamento do plano escolhido. Quando o mesmo for confirmado, sera possivel publicar novas vagas.
                </div>
            </div>
        </div>  
        <? } ?>
        
        <!--Dashboard-->    
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">                                
                        <h5>Saldo de vagas <small> <a href="<?= base_url()?>empresaController/extrato">(ver extrato completo)</a> </small> </h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><? echo $totais[0]->saldo == ""? 0 :  $totais[0]->saldo ?></h1>
                        <div class="stat-percent font-bold text-success"><? echo $totais[0]->destaque == "" ? 0 : $totais[0]->destaque ?> <i class="fa fa-star"></i></div>
                        <small>Vagas</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Brasil</span>
                        <h5>Profissionais</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><? echo $totais[0]->total?></h1>                        
                        <small>Registrados</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right"><? echo $empresa[0]->estado?></span>
                        <h5>Profissionais</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><? echo $totais[0]->estado?></h1>
                        <small>Registrados</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right"><? echo $empresa[0]->cidade?></span>
                        <h5>Profissionais</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><? echo $totais[0]->cidade?></h1>
                        <small>Registrados</small>
                    </div>
                </div>
            </div>
        </div>
        
		<? if ($totais[0]->saldo <= 0 && count($extrato_pendente) == 0) { ?>
        <div class="row">
            <div class="col-lg-12 avisoPlano">
            	Adquira um pacote para continuar a publicar suas vagas em nosso portal <br /> e encontrar os melhores profissionais de TI!
            </div>
        </div>		
		
        <div class="wp-section pricing-plans">
            <div class="row">
            <? foreach($planos_empresa as $plano ){ ?>
                <div class="col-md-4">
                    <form class="plano <?= $plano->plano?>" method="post" action="<?=base_url()?>planoController/planoEmpresa">
                    	<input type="hidden" name="idplano" id="idplano" value="<?= $plano->id?>" />
                        <h2><?= $plano->plano?></h2>
                        <h3><span id="valorAnuncio">R$ <?= $plano->valor?></span></h3>
                        <ul>
                        	<li><?= $plano->quantidade?> anúncio(s)</li>
                            <? if($plano->tipoPlano == 'basico') { ?>
                                    <li><label><input type="checkbox" name="destaque" value="SIM"/> Destaque para a vaga</label> </li>
                               <? } else { ?>
                                    <li>Desconto de <?= $plano->desconto?>%</li>
                                    <li><?= $plano->destaque?> destaques para vagas</li>
                               <? } ?>
                               <li><?= $plano->diasVigencia?> dias de vigência</li>  
                        </ul>
                        <input type="submit" class="btn btn-base btn-icon btn-check" value="COMPRAR" />
                    </form>
                </div>
             <? } ?>
            </div>
        </div>
		<? } ?>
    </div>
    
<? $this->load->view('priv/empresa/_inc/inferior'); ?>
