<?php
$description = "Na Go Talent você tem acesso a vagas de TI e ainda pode agendar sua entrevista de emprego direto.";
$keywords = "vagas de TI";
$title = "Vagas de TI: Encontre o emprego que você deseja";
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">

    <title><?= $title ?></title>
    <link href="<? echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<? echo base_url() ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<? echo base_url() ?>css/animate.css" rel="stylesheet">
    <link href="<? echo base_url() ?>css/style.css" rel="stylesheet">

    <meta name="description" content="<?= $description ?>"/>
    <meta name="keywords" content="<?= $keywords ?>"/>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53411814-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name"><a href="<? echo base_url() ?>"><img src="<? echo base_url() ?>img/logo.png"/></a>
            </h1>

        </div>
        <?= ($this->session->flashdata('erroUsuarioNaoLogado') != "") ? "<div class='alert alert-danger'>" . $this->session->flashdata('erroUsuarioNaoLogado') . "</div>" : "" ?>
        <?= $error != "" ? '<div class="alert alert-danger">' . $error . '</div>' : "" ?>
        <p>Informe seus dados de Usuário</p>

        <form class="m-t" role="form" id="formLogin" name="formLogin" action="<?= base_url() ?>login/login/autenticar"
              method="post">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="usuarioEmail" id="usuarioEmail"
                       required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Senha" name="senha" id="senha" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
           
                <a class="link-esqueci-senha" href="<? echo base_url() ?>esqueci-minha-senha">
                    <small>Esqueceu sua senha ?</small>
                </a>
                <a class="link-cadastrar" href="<? echo base_url() ?>cadastro-profissional">
                    <small>Quero me Cadastrar !</small>
                </a><br>

        </form>
        <br style="clear: both">
        <p class="m-t">
            <small> Copyright © 2015 - Go Talent Portal On-line LTDA/21.642.706/0001­27 - Todos os direitos reservados
            </small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<? echo base_url() ?>js/jquery-2.1.1.js"></script>
<script src="<? echo base_url() ?>js/bootstrap.min.js"></script>

</body>

</html>
