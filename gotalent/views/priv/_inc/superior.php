<?
if ($this->Usuario_model->logged() == FALSE) {
    redirect('/login/', 'refresh');
}




//Transforma títulos em URL amigáveis
function url($str) {
 $str = strtolower(utf8_decode($str)); $i=1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
    while($i>0) $str = str_replace('--','-',$str,$i);
    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
    return $str;
}
?>



<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta name="robots" content="noindex,nofollow" />
		<title>GoTalent - Empregos e Vagas de TI</title>
		<meta name="author" content="GoTalent"/>
		<meta name="keywords" content="Profissionais de TI, Empregos TI, Vagas TI, Empresas de TI, Empresas de TI em SP, Emprego TI, Vagas em TI, Empregos de TI, Empregos em TI, Estágio TI, Vagas de emprego TI, Empregos na área de TI, Trabalhar com TI, Vagas na área de TI, Oportunidades em TI"/>
		<meta name="description" content="Para profissionais que procuram ">
		<meta name="copyright" content="Copyright (c) 2014 GoTalent" />
		<meta http-equiv="content-language" content="pt-br">
		<link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/sb-admin.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/jquery.autocomplete.css" rel="stylesheet" />
		<link href="<?=base_url()?>css/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link rel="shortcut icon" href="<?=base_url()?>img/favico.ico" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/maskedinput.js"></script> 
		<script type="text/javascript" src="<?=base_url()?>js/jquery.maskMoney.js"></script> 
	    <script type="text/javascript" src="<?=base_url()?>js/bootstrap.min.js"></script>
    	<script type="text/javascript" src="<?=base_url()?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>js/sb-admin.js"></script>
		<script type="text/javascript" src="<?=base_url()?>system/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?=base_url()?>system/ckeditor/config.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/plugins/dataTables/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
		<script type='text/javascript' src="<?=base_url()?>js/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/funcoes.js"></script>
		
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
		
		

 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53411814-1', 'auto');
  ga('send', 'pageview');

</script> 

<style>
.foto{position:relative;margin:15px;display:table}
.foto a{width:100%;line-height:13px;padding:5px;position:absolute;z-index:1;bottom:1px;left:1px;font-size:12px;color:black;text-align:center;text-decoration:none;background:url(<?= base_url()?>img/icons/bg-foto.png) repeat;-moz-opacity:0;opacity:0;filter:alpha(opacity=0);-webkit-transition:.2s ease-out;-moz-transition:.2s ease-out;-o-transition:.2s ease-out;transition:.2s ease-out;cursor:pointer}
.foto a input[type=file]{border:none;background:none;width:100%;height:100%;-moz-opacity:0;opacity:0;filter:alpha(opacity=0);position:absolute;z-index:1;top:0;left:0;cursor:pointer}
.foto label{color:#95a5a5;font-size:12px;line-height:14px;text-align:center;width:60px;float:right}
.foto img{float:left;border:1px solid #00a186}
.foto:hover a{-moz-opacity:1.00;opacity:1.0;filter:alpha(opacity=100);-webkit-transition:.2s ease-out;-moz-transition:.2s ease-out;-o-transition:.2s ease-out;transition:.2s ease-out}

@media print{
.logo_print{width:60%;}
.no_print{display:none;}
}
</style>
		
	</head>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?=base_url()?>principal/arearestrita">Bem vindo <?= $this->session->userdata("nome") ?>!</a>
			</div>
			
            <ul class="nav navbar-top-links navbar-right">				
				<!--Dropown-->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i> </a>
                    <ul class="dropdown-menu dropdown-user">                        
                        <li><a href="<?=base_url()?>login/login/logoff"><i class="fa fa-sign-out fa-fw"></i> Sair</a></li>
                    </ul>
                </li>
			</ul>
        </nav>

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
			
			<? if($this->session->userdata("tipo") == 'Administrador'){?>	
				<ul class="nav" id="side-menu">
					<li class="sidebar-search">
						<img src="<?= base_url() ?>img/logo.png" class="logo_print" width="100%" alt="GoTalent - Empregos e Vagas de TI" title="GoTalent - Empregos e Vagas de TI" />
					</li>
				</ul>
			<?} else {?>	
				<ul class="nav" id="side-menu">
					<li class="sidebar-search">
						<a href="<?=base_url()?>"><img src="<?= base_url() ?>img/logo.png" class="logo_print" width="100%" alt="GoTalent - Empregos e Vagas de TI" title="GoTalent - Empregos e Vagas de TI" /></a>
					</li>
				</ul>
			<?}?>
				<? if($this->session->userdata("tipo") == 'Profissional'){?>
				<div class="foto">
				<form id="upload" name="upload" action="<?= base_url() ?>profissionalController/uploadArquivo/<?echo $profissional[0]->id ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="tipoAnexo" name="tipoAnexo" value="Foto"/>
					<span class="file">
						<a>Escolha uma imagem para seu perfil <input type="file" onchange="document.upload.submit()" name="userfile" id="userfile"></a>								
					</span>
							
					<? if($profissional[0]->foto!= ""){?>
						<img src="<?= base_url() ?>upload/profissional/<?echo $profissional[0]->foto?>" id="img-text" width="100%" alt="<?= $this->session->userdata("nome") ?>" title="<?= $this->session->userdata("nome") ?>" />
						<?}else{?>
						<img width="100%" src="<?= base_url() ?>img/icons/nopicture.png" alt="" id="img-text" alt="<?= $this->session->userdata("nome") ?>" title="<?= $this->session->userdata("nome")?>"/>	
						<?}?>					
				</form>		
				</div>
				<?}?>

				<? if($this->session->userdata("tipo") == 'Empresa'){?>
				<div class="foto no_print">
				<form id="formUpload" name="formUpload" action="<?= base_url() ?>empresaController/uploadArquivo/<?echo $empresa[0]->id ?>" method="post" enctype="multipart/form-data">
					
					<span class="file">
						<a>Faça upload da logo de sua Empresa <input type="file" onchange="document.formUpload.submit()" name="userfile" id="userfile"></a>								
					</span>
							
					<? if($empresa[0]->logo!= ""){?>
						<img src="<?= base_url() ?>images/empresas/<?echo $empresa[0]->logo?>" id="img-text" width="100%" alt="<?= $this->session->userdata("nome") ?>" title="<?= $this->session->userdata("nome") ?>" />
						<?}else{?>
						<img width="100%" src="<?= base_url() ?>img/icons/nopicture.png" alt="" id="img-text" alt="<?= $this->session->userdata("nome") ?>" title="<?= $this->session->userdata("nome")?>"/>	
						<?}?>					
				</form>		
				</div>
				<?}?>	
						
                <ul class="nav no_print" id="side-menu">
					
					<? if($this->session->userdata("tipo") == 'Profissional'){?>
					
					<!--Menu PROFISSIONAL PREMIUM -->	
						<? if($this->session->userdata("tipoPlano") == 'PREMIUM'){?>
							<li> <a href="<?=base_url()?>principal/arearestrita"><i class="fa fa-flag"></i> Vagas </a> </li>
							<li> <a href="<?=base_url()?>profissionalController/editarProfissionalAction"><i class="fa fa-user fa-fw"></i> Meu Perfil </a> </li>
							<li> <a href="<?=base_url()?>profissionalController/meusProcessosSeletivos"><i class="fa fa-user fa-check"></i> Meus Processos Seletivos </a> </li>
						<?}?>
						
					<!--Menu PROFISSIONAL FREE -->
						<? if($this->session->userdata("tipoPlano") == 'FREE'){?>
						<li> <a href="<?=base_url()?>principal/arearestrita"><i class="fa fa-flag"></i> Vagas </a> </li>
						<li> <a href="<?=base_url()?>profissionalController/editarProfissionalAction"><i class="fa fa-user fa-fw"></i> Meu Perfil </a> </li>
						<?}?>

					
					<?} else if($this->session->userdata("tipo") == 'Administrador'){?>
						<li class="no_print"> <a href="<?=base_url()?>administradorController/listarProfissionais"><i class="fa fa-user fa-fw"></i> Profissionais </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>administradorController/listarVagas"><i class="fa fa-user fa-fw"></i> Vagas </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>administradorController/listarSkills"><i class="fa fa-user fa-fw"></i> Skills </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>administradorController/listarEmpresas"><i class="fa fa-user fa-fw"></i> Empresas </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>entrevistaController"><i class="fa fa-user fa-fw"></i> Entrevistas </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>administradorController/listarProfissionaisSkills"><i class="fa fa-user fa-fw"></i> Profissionais/Skills </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>administradorController/indicacao"><i class="fa fa-user fa-fw"></i> Indicação de Profissionais </a> </li>
					<?}
					else if($this->session->userdata("tipo") == 'Empresa'){?>
						<li class="no_print"> <a href="<?=base_url()?>empresaController/editarEmpresaAction"><i class="fa fa-dashboard fa-fw"></i> Meus Dados </a> </li>
						<li class="no_print"> <a href="<?=base_url()?>empresaController/listarVagas"><i class="fa fa-user fa-fw"></i> Vagas </a> </li>
					<?}?>
                </ul>
            </div>
        </nav>
	   
	   