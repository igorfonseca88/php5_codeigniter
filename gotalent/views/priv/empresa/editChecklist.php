<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">


        <h1>Edição do checklist</h1>
        <ol class="breadcrumb">
            <li> <a href="<? echo base_url() ?>area-restrita-empresa">Home</a> </li>
            <li> <a href="<? echo base_url() ?>checklistController/listarChecklists">Listagem de Checklists</a> </li>
            <li class="active"> <strong>Edição do checklist</strong> </li>
        </ol>
    </div>

</div>


<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados do Checklist </h5>
                </div>
                <div class="ibox-content">
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <form method="post" action="<?= base_url() ?>checklistController/editChecklist" class="form-horizontal" name="formEditChecklist">
                        <input type="hidden" name="idChecklist" id="idChecklist" value="<? echo $checklist[0]->id ?>"/>
                        
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Código</label>
                                <div class="col-sm-4">
                                    <p class="form-control-static">
                                        <?= $checklist[0]->id ?>
                                    </p>
                                </div>
                                
                                <label class="col-sm-2 control-label">Data de Criação</label>
                                <div class="col-sm-4">
                                    <p class="form-control-static">
                                        <?= implode("/", array_reverse(explode("-", $checklist[0]->dataCriacao))) ?>
                                    </p>
                                </div>
                                
                            </div>
                        


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição do checklist</label>
                            <div class="col-lg-10">
                                <textarea name="checklist" id="checklist" class="form-control"><?= $checklist[0]->checklist ?></textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Situação</label>
                            <div class="col-lg-6">
                                <select name="situacao" id="situacao" class="form-control">
                                    <option <?= $checklist[0]->situacao == "Ativo" ? "selected" : "" ?> value="Ativo">Ativo</option>
                                    <option <?= $checklist[0]->situacao == "Inativo" ? "selected" : "" ?> value="Inativo">Inativo</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>empresaController/listarVagas'"  />

                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />

                            </div>
                        </div>
                    </form>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Itens do checklist </h5>
                        <small class="text-navy"> ( Adicione até 5 questões com respostas objetivas (Sim ou Não), 5 questões para serem respondidas por vídeo e 2 com respostas discursivas )</small>
                        <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form id="formItemChecklist" class="form-horizontal" name="formItemChecklist" method="POST" action="<? echo base_url() ?>checklistController/addItemChecklist">
                                 <input type="hidden" name="idCk" id="idCk" value="<? echo $checklist[0]->id ?>"/>
                                <div class="row">
                               
                                    
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tipo da resposta</label>
                                <div class="col-lg-6">
                                    <select name="componente" id="componente"  onchange="escondeMostrarCampoResposta(this.value)" class="form-control">
                                            <option value="Objetiva">Objetiva</option>
                                            <option value="Discursiva">Discursiva</option>
                                            <option value="Video">Vídeo entrevista</option>
                                        </select>
                                </div>
                            </div>
                                    
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Questão</label>
                                <div class="col-lg-8">
                                    <textarea name="questao" id="questao" class="form-control"></textarea>
                                </div>
                            </div> 
                            
                            <div class="form-group" id="divResposta" style="diplay:block;">
                                <label class="col-sm-2 control-label">Resposta esperada</label>
                                <div class="col-lg-6">
                                    <select name="respostaEsperada" id="respostaEsperada" class="form-control">
                                            <option value="SIM">SIM</option>
                                            <option value="NAO">NÃO</option>
                                        </select>
                                </div>
                            </div>         


                                
                                
                                <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>checklistController/listarChecklists'"  />

                                <? if(count($questoes_objetivas) <=5 && count($questoes_discursivas)<=2){?>    
                                        <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
                                    <?}?> 

                            </div>
                        </div>    
                                    
                                    
                            </div>
                                
                            </form>
                            
                            <table class="table table-hover" >
                            <thead>
                            
                            <th>Questão</th>
                            <th>Tipo da resposta</th>
                            <th>Resposta esperada</th>
                            

                            <th align="center">Ações</th>
                            </thead>
                            <? foreach ($questoes as $questao) { ?>
                            <tr>
                                <td> <?= $questao->questao ?></td>
                                <td> <?= $questao->componente == "Video" ? "Vídeo Entrevista" : $questao->componente ?></td>
                              
                                <td> <?= $questao->respostaEsperada ?></td>
                                
                                <td>
                                    <a href="<?= base_url()?>checklistController/excluirItemChecklist/<?= $questao->id?>/<?= $checklist[0]->id?>">Excluir</a>
                                </td>
                            </tr>
                            <? } ?>
                        </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?
$this->load->view('priv/empresa/_inc/inferior');
?>
