<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Listagem de Checklists</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Checklists</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listagem de todos os Checklists</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>checklistController/novoChecklistAction" class="btn btn-primary btn-xs">Criar novo</a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="project-list">

                        <table class="table table-hover">
                            <tbody>
                                <? if (count($checklists) > 0) { ?>        
                                    <? foreach ($checklists as $row) { ?>
                                        <tr>
                                            <td class="project-status">
                                                <?= $row->id ?>
                                            </td>
                                            <td class="project-title">
                                                <?= implode("/", array_reverse(explode("-", $row->dataCriacao))) ?>

                                            </td>


                                            <td class="project-people">
                                                <?= $row->checklist ?>
                                            </td>
                                            <td class="project-actions">

                                                <a href="<?= base_url() ?>checklistController/editarChecklistAction/<?= $row->id ?>" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Editar </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                <? } else { ?>            
                                <p> Nenhum registro encontrado. </p>    
                            <? } ?>         

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$this->load->view('priv/empresa/_inc/inferior');
?>
