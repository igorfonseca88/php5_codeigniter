<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h1>Novo Checklist</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li>
                <a href="<? echo base_url() ?>checklistController/listarChecklists">Listagem de Checklists</a>
            </li>
            <li class="active">
                <strong>Novo checklist</strong>
            </li>
        </ol>
    </div>
</div>



<div class="wrapper wrapper-content animated fadeInUp">

    <div class="row">
        <div class="col-lg-12">

            <div class="ibox float-e-margins">	
                <div class="ibox-title">
                    <h5>Dados da checklist </h5>
                </div>

                <div class="ibox-content">		
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>


                    <form method="post" action="<?= base_url() ?>checklistController/addChecklist" class="form-horizontal">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição do checklist </label>
                            <div class="col-lg-4">	
                                <input type="text" name="checklist" id="checklist" class="form-control" value="<?= $_POST["checklist"] ?>" />
                            </div>

                            
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>checklistController/listarChecklists'"  />
                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
                            </div>
                        </div>

                    </form>


                </div>
            </div>	

        </div>	
    </div>
</div>



<?
$this->load->view('priv/empresa/_inc/inferior');
?>
