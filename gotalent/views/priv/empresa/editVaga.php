<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <!--BOTÃO PARA FECHAR A VAGA-->
        <? if ($vaga[0]->situacao == 'Ativo') { ?>
            <a data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-lg"
               style="float:right;margin-top:20px">ENCERRAR VAGA</a>
        <? } ?>

        <? if ($vaga[0]->situacao == 'Em Elaboração') { ?>
            <a class="btn btn-primary btn-lg" onclick="confirmaExistenciaSkill();" style="float:right;margin-top:20px">PUBLICAR</a>
        <? } ?>

        <h1>Edição de anúncio de vaga</h1>
        <ol class="breadcrumb">
            <li><a href="<? echo base_url() ?>area-restrita-empresa">Home</a></li>
            <li><a href="<? echo base_url() ?>empresaController/listarVagas">Vagas</a></li>
            <li class="active"><strong>Edição de anúncio de vaga</strong></li>
        </ol>
    </div>
    <?
    // SE A VAGA É DESTAQUE MOSTRA O ÍCONE PARA O USUÁRIO IDENTIFICAR
    if ($vaga[0]->destaque == 'SIM') {
        ?>
        <div class="col-sm-3">
            <div class="widget style1 navy-bg">
                <div class="row vertical-align">
                    <i class="fa fa-star fa-3x"></i>

                    <h2 class="font-bold">DESTAQUE</h2>
                </div>
            </div>
        </div>

    <? } ?>
</div>
<? if ($vaga[0]->destaque == 'NAO' && ($vaga[0]->situacao == 'Ativo' || $vaga[0]->situacao == 'Aguardando Aprovação')) { ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Serviços adicionais</h5>
                </div>
                <div class="ibox-content">
                    <?
                    // HABILITA OPÇÃO DE DESTAQUE DA VAGA
                    if ($vaga[0]->destaque == 'NAO') {
                        ?>
                        <a href="<?= base_url() ?>planoController/destacarVaga/<?= $vaga[0]->id ?>"
                           class="btn btn-warning btn-lg">
                            <h2 class="font-bold" style="margin:0">DESTACAR </h2>
                            <small>(Por apenas R$ 59,00)</small>
                        </a>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
<? } ?>
<!-- FECHA WIDGETS-->

<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados da vaga
                    </h5>
                    <?
                    if ($dateInterval <= 15 && $vaga[0]->situacao == 'Ativo') {
                        ?>
                        <a class="btn btn-lg btn-success btn-sm" style="float:right"
                           onclick="enableInputDataVigencia()">
                            <p class="font-bold" style="margin:-5px; ">ESTENDER VIGÊNCIA DA VAGA</p>
                        </a>
                        <?
                    }
                    ?>
                </div>
                <div class="ibox-content">
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <form method="post" action="<?= base_url() ?>empresaController/editVaga" class="form-horizontal"
                          name="formEditVaga">
                        <input type="hidden" name="idVaga" id="idVaga" value="<? echo $vaga[0]->id ?>"/>
                        <? if ($vaga[0]->situacao == "Fechado") { ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Data do Fechamento:</label>

                                <div class="col-sm-4">

                                    <input class="" type="date" name="bday" value="<?= $vaga[0]->dataFechamento ?>"
                                           disabled>
                                </div>
                                <label class="col-sm-2 control-label">Motivo:</label>

                                <div class="col-sm-4">
                                    <p class="form-control-static">
                                        <?= $vaga[0]->motivoFechamento ?>
                                    </p>
                                </div>
                            </div>
                        <? } ?>
                        <? if ($vaga[0]->situacao == "Aguardando Aprovação" || $vaga[0]->situacao == "Ativo" || $vaga[0]->situacao == "Fechado") { ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Envio aprovação:</label>

                                <div class="col-sm-2">
                                    <input class="" type="date" name="bday" value="<?= $vaga[0]->dataEnvioAprovacao ?>"
                                           readonly>

                                </div>
                                <label class="col-sm-2 control-label">Data da publicação:</label>

                                <div class="col-sm-2">
                                    <input class="" type="date" name="bday" value="<?= $vaga[0]->dataPublicacao ?>"
                                           readonly>

                                </div>
                                <label class="col-sm-2 control-label">Data da vigência:</label>

                                <div class="col-sm-2">

                                    <input id="dataVigencia" type="date" name="dataVigencia"
                                           value="<?= $vaga[0]->dataVigencia ?>" readonly
                                           min="<?= $minDiasCalendario ?>" max="<?= $maxDiasCalendario ?>">

                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Título da Vaga: </label>

                                <div class="col-sm-2">
                                    <input type="hidden" name="vaga" id="vaga" value="<?= $vaga[0]->vaga ?>"/>

                                    <p class="form-control-static">
                                        <?= $vaga[0]->vaga ?>
                                    </p>
                                </div>
                                <label class="col-sm-3 control-label">Quantidade de Profissionais:</label>

                                <div class="col-sm-2">
                                    <input type="hidden" name="quantidade" id="quantidade"
                                           value="<?= $vaga[0]->quantidade ?>"/>

                                    <p class="form-control-static">
                                        <?= $vaga[0]->quantidade == 11 ? "Acima de 10 vagas" : $vaga[0]->quantidade ?>
                                    </p>
                                </div>
                                <label class="col-sm-1 control-label">Situação:</label>

                                <div class="col-sm-2">
                                    <p class="form-control-static">
                                        <?= $vaga[0]->situacao ?>
                                    </p>
                                </div>
                            </div>

                        <? } else { ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Título da Vaga:<span
                                        style="color:red">*</span></label>

                                <div class="col-sm-3">
                                    <input type="text" name="vaga"
                                           id="vaga" <?= $vaga[0]->situacao == "Ativo" ? "readonly='true'" : "" ?>
                                           class="form-control" value="<?= $vaga[0]->vaga ?>"/>
                                </div>
                                <label class="col-sm-3 control-label">Quantidade de Profissionais:</label>

                                <div class="col-sm-2">
                                    <select
                                        name="quantidade" <?= $vaga[0]->situacao == "Ativo" ? "readonly='true'" : "" ?>
                                        class="form-control" style="width:300px;">
                                        <option <?= $vaga[0]->quantidade == "1" ? "selected" : "" ?> value="1"> 1 vaga
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "2" ? "selected" : "" ?> value="2"> 2
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "3" ? "selected" : "" ?> value="3"> 3
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "4" ? "selected" : "" ?> value="4"> 4
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "5" ? "selected" : "" ?> value="5"> 5
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "6" ? "selected" : "" ?> value="6"> 6
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "7" ? "selected" : "" ?> value="7"> 7
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "8" ? "selected" : "" ?> value="8"> 8
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "9" ? "selected" : "" ?> value="9"> 9
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "10" ? "selected" : "" ?> value="10"> 10
                                            vagas
                                        </option>
                                        <option <?= $vaga[0]->quantidade == "11" ? "selected" : "" ?> value="11"> Acima
                                            de 10 vagas
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <? if ($vaga[0]->situacao == "Desaprovado") { ?>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="width: 180px;">Data de Desaprovação:</label>

                                    <div class="col-sm-4">
                                        <p class="form-control-static"> <?= implode("/", array_reverse(explode("-", $vaga[0]->dataDesaprovacao))) ?> </p>
                                    </div>

                                    <label class="col-sm-2 control-label">Motivo:</label>

                                    <div class="col-sm-3">
                                        <p class="form-control-static"
                                           style="text-align: justify"> <?= $vaga[0]->motivoDesaprovacao ?></p>
                                    </div>

                                </div>
                            <? }; ?>
                        <? } ?>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Acessibilidades:</label>

                                <div class="col-lg-4">
                                    <? if ($vaga[0]->receberCurriculoEmail == "SIM") {
                                        $curriculoEmail = "checked";
                                    } ?>
                                    <input type="checkbox" name="receberCurriculoEmail" id="receberCurriculoEmail"
                                           class="form-control checkbox i-checks" <?= $curriculoEmail ?>/><i></i>
                                    Receber currículos por
                                    e-mail.
                                </div>

                                <div class="col-lg-4">
                                    <? if ($vaga[0]->receberCurriculoFora == "SIM") {
                                        $curriculoFora = "checked";
                                    } ?>
                                    <input type="checkbox" name="receberCurriculoFora"
                                           id="receberCurriculoFora" <?= $curriculoFora ?>
                                           class="form-control checkbox i-checks"/><i></i> Aceitar somente curriculos da
                                    mesma localidade
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Local de Trabalho:<span
                                        style="color:red">*</span></label>

                                <div class="col-lg-6">

                                    <?
                                    $chkLocalVaga="";
                                    if($vaga[0]->empresaMatriz == "NAO"){
                                        $chkLocalVaga = "";
                                    }else{
                                        $chkLocalVaga = "checked";
                                    }
                                    ?>
                                    <input type="checkbox" name="localVaga"  <?=$chkLocalVaga?> id="localVaga"
                                           class="form-control checkbox i-checks"/><i></i> O local do
                                    emprego é o mesmo endereço da empresa sede/matriz.
                                </div>

                            </div>
                            <div id="divsLocalVaga">
                                <div class="form-group ">
                                    <label class="col-lg-2 control-label">CEP:</label>

                                    <div class="col-lg-2">
                                        <input type="text" name="cep" id="cep" class="cep form-control"
                                               value="<?= $vaga[0]->cep ?>"/>
                                    </div>
                                    <label class="col-lg-2 control-label" id="labelEndereco">Endereço:</label>

                                    <div class="col-lg-5">
                                        <input type="text" name="endereco" id="endereco" class="form-control"
                                               value="<?= $vaga[0]->endereco ?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-2 control-label" id="labelNumero">Número:</label>

                                    <div class="col-lg-2">
                                        <input type="number" name="numero" id="numero" class="form-control"
                                               value="<?= $vaga[0]->numero ?>"/>
                                    </div>

                                    <label class="col-sm-2 control-label" id="labelBairro">Bairro:</label>

                                    <div class="col-lg-4">
                                        <input type="text" name="bairro" id="bairro" class="form-control"
                                               value="<?= $vaga[0]->bairro ?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-2 control-label">Estado:</label>

                                    <div class="col-sm-2">
                                        <select name="estado" id="estado" class="form-control" style="width:200px;">
                                            <option value="">Selecione</option>
                                            <? foreach ($estados as $estado) { ?>
                                                <option <?= $vaga[0]->estado == $estado->uf ? "selected" : "" ?>
                                                    value="<?= $estado->uf ?>"><?= $estado->nome ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Cidade:</label>

                                    <div class="col-sm-4">
                                        <select name="cidade" id="cidade" class="form-control">
                                            <option value="">Selecione</option>
                                            <? foreach ($cidades as $cidade) { ?>
                                                <option <?= $vaga[0]->cidade == $cidade->nome ? "selected" : "" ?>
                                                    value="<?= $cidade->nome ?>"><?= $cidade->nome ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <!--Área TI-->
                            <div class="form-group">
                            <span id="spanComboSubArea">
                                <label class="col-sm-2 control-label">Área da TI:</label>
                                <div class="col-lg-2">
                                    <select name="comboSubArea" id="comboSubArea" class="form-control" style="width:200px;">
                                        <option value="">Selecione</option>
                                        <? foreach ($subareas as $sub) { ?>
                                            <option <?= $vaga[0]->idSubArea == $sub->id ? "selected" : "" ?>
                                                value="<?= $sub->id ?>"><?= $sub->subarea ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </span>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <!--Descrição-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Descrição:<span
                                        style="color:red">*</span></label>

                                <div class="col-sm-10">
                                <textarea name="descricao" id="descricao" class="form-control ckeditor"
                                          rows="6"><?= $vaga[0]->descricao ?>
                                </textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tipo de contratação:<span
                                        style="color:red">*</span></label>

                                <div class="col-sm-4">
                                    <div class="radio i-checks">
                                        <label>
                                            <input type="radio" <?= $vaga[0]->tipo == "CLT" ? "checked" : "" ?>
                                                   name="tipo" value="CLT"/>
                                            <i></i> CLT </label>
                                    </div>
                                    <div class="radio i-checks">
                                        <label>
                                            <input type="radio" <?= $vaga[0]->tipo == "PJ" ? "checked" : "" ?>
                                                   name="tipo" value="PJ"/>
                                            <i></i> PJ </label>
                                    </div>
                                    <div class="radio i-checks">
                                        <label>
                                            <input
                                                type="radio" <?= $vaga[0]->tipo == "Freelancer" ? "checked" : "" ?>
                                                name="tipo" value="Freelancer"/>
                                            <i></i> Freelancer </label>
                                    </div>
                                    <div class="radio i-checks">
                                        <label>
                                            <input type="radio" <?= $vaga[0]->tipo == "Estágio" ? "checked" : "" ?>
                                                   name="tipo" value="Estágio"/>
                                            <i></i> Estágio </label>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label">Benefícios:</label>

                                <div class="col-lg-4">
                                    <?
                                    foreach ($beneficios as $beneficio) {
                                        $checked = "";
                                        foreach ($beneficios_vaga as $bv) {
                                            if ($bv->idBeneficio == $beneficio->id) {
                                                $checked = "checked";
                                                continue;
                                            }
                                        }
                                        ?>
                                        <div class="checkbox i-checks">
                                            <label>
                                                <input type="checkbox" <? echo $checked ?>
                                                       value="<? echo $beneficio->id ?>" name="beneficios[]"/>
                                                <i></i> <? echo $beneficio->beneficio ?> </label>
                                        </div>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <!--Salário-->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Salário:</label>

                                <div class="col-lg-3">
                                    <input type="text" name="salario" id="salario" class="form-control"
                                           value="<?= $vaga[0]->salario; ?>"/>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <input type="button" value="Voltar" class="btn btn-white"
                                           onclick="location.href = '<?= base_url() ?>empresaController/listarVagas'"/>
                                    <!--Este If nunca ocorrer no modo simplificado de cadastrar uma vaga-->
                                    <? if ($vaga[0]->situacao != 'Fechado' && $vaga[0]->situacao != 'Aguardando Aprovação' && $vaga[0]->situacao != 'Ativo' && $vaga[0]->situacao != 'Desaprovado') { ?>
                                        <input type="submit" class="btn btn-primary" name="btSalvar"
                                               value="Salvar em Rascunho"/>
                                    <? } ?>
                                    <!--Este If nunca ocorrer no modo simplificado de cadastrar uma vaga-->
                                    <? if ($vaga[0]->situacao == 'Em Elaboração') {
                                        ?>
                                        <input type="button" class="btn btn-primary" name="btPublicar"
                                               onclick="confirmaExistenciaSkill();" style="float:right;"
                                               value="Publicar"/>
                                    <? } ?>
                                    <!--Botão para editar dados da vaga -->
                                    <? if ($vaga[0]->situacao != 'Em Elaboração' && $vaga[0]->situacao != 'Fechado' && $vaga[0]->situacao != 'Finalizada' && $vaga[0]->situacao != 'Desaprovado') { ?>
                                        <input type="submit" class="btn btn-primary" name="btSalvar"
                                               onclick="confirmaExistenciaSkill();" value="Salvar Alterações">
                                    <? } else {
                                        if ($vaga[0]->situacao == 'Desaprovado') { ?>
                                            <input type="submit" class="btn btn-primary" name="btSalvar"
                                                   onclick="confirmaExistenciaSkill();" value="Republicar">
                                        <? }
                                    } ?>
                                </div>
                            </div>

                    </form>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Competências dos candidatos </h5>
                        <small class="text-navy"> ( Adicione competências a vaga, pois com elas iremos calcular a
                            porcentagem de compatibilidade dos profissionais )
                        </small>
                        <div class="ibox-tools"><a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a></div>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <input type="button" data-toggle="modal" data-backdrop="static"
                                       data-target="#myModalSkill" class="btn btn-primary" name="btNovaSkill"
                                       value="Nova Competência"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <ul style="list-style: none;padding-left: 0;margin: 0;">
                                    <? foreach ($vaga_skills as $ps) { ?>
                                        <li style="border: 1px solid #59b2e5;background: #FFF;padding: 5px;float: left;
                                            margin-right: 5px;margin-bottom: 5px;color: #59b2e5;"><? echo $ps->skill ?>
                                            <a href="<? echo base_url() ?>empresaController/excluirSkillVaga/<? echo $ps->id ?>/<? echo $ps->idVaga ?>"><img
                                                    style="margin-left:10px"
                                                    src="<? echo base_url() ?>img/excluir.png"/></a></li>
                                    <? } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Fechar</span></button>
                <h4 class="modal-title" id="myModalLabel">Informe um motivo para fechar a vaga</h4>
            </div>
            <form id="formFeedback" action="<?= base_url() ?>empresaController/fecharVagaAction/<?= $vaga[0]->id ?>"
                  name="formFeedback" method="POST">
                <div class="modal-body">
                    <textarea class="form-control" name="motivoFechamento" id="motivoFechamento"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <input type="submit" class="btn btn-success" name="btSalvarEnviar" value="Salvar"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="myModalSkill" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Fechar</span></button>

                <h4 class="modal-title">Competências disponíveis</h4>
            </div>

            <form id="formSkillAuto" name="formSkillAuto" method="POST"
                  action="<? echo base_url() ?>empresaController/addSkillVaga">
                <input type="hidden" name="idVagah" id="idVagah" value="<?= $vaga[0]->id ?>"/>

                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-skill">
                        <thead>
                        <th></th>
                        <th>Skill</th>
                        </thead>
                        <? foreach ($skills as $sk) { ?>
                            <tr>
                                <td><input type="checkbox" id="lista[]" value="<?= $sk->id ?>" name="lista[]"/></td>
                                <td> <?= $sk->skill ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" name="btAdicionar" value="Adicionar"/>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </form>


        </div>
    </div>
</div>
<?
$this->load->view('priv/empresa/_inc/inferior');
?>

<script>
    function confirmaExistenciaSkill() {
        var check = 0;
        check =
        <?echo count($vaga_skills);?>

        if (check == 0) {
            var r = confirm("Nenhuma competência foi adicionada para essa vaga, se não encontrou na lista de competências do sistema entre contato conosco para adicionarmos. Deseja continuar mesmo assim ?");
            if (r == true) {
                location.href = '<?= base_url() ?>empresaController/enviarVagaParaAprovacaoAction/<?= $vaga[0]->id ?>';
            }
        } else {
            location.href = '<?= base_url() ?>empresaController/enviarVagaParaAprovacaoAction/<?= $vaga[0]->id ?>';
        }
    }
</script>