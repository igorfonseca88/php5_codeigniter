<?
$this->load->view('priv/empresa/_inc/superior');
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h1>Anúncio de nova vaga</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li>
                <a href="<? echo base_url() ?>empresaController/listarVagas">Vagas</a>
            </li>
            <li class="active">
                <strong>Anúncio de nova vaga</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados da vaga </h5>
                </div>
                <div class="ibox-content">
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
                    <form method="post" action="<?= base_url() ?>empresaController/addVaga" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título da Vaga <span
                                    style="color:red">*</span></label>

                            <div class="col-lg-4">
                                <input type="text" name="vaga" id="vaga" class="form-control"
                                       value="<?= $_POST["vaga"] ?>"/>
                            </div>
                            <label class="col-sm-2 control-label">Quantidade de Profissionais:</label>

                            <div class="col-lg-4">
                                <select name="quantidade" class="form-control">
                                    <option value="1"> 1</option>
                                    <option value="2"> 2</option>
                                    <option value="3"> 3</option>
                                    <option value="4"> 4</option>
                                    <option value="5"> 5</option>
                                    <option value="6"> 6</option>
                                    <option value="7"> 7</option>
                                    <option value="8"> 8</option>
                                    <option value="9"> 9</option>
                                    <option value="10"> 10</option>
                                    <option value="11"> Acima de 10</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Acessibilidades:</label>

                            <div class="col-lg-4">
                                <input type="checkbox" name="receberCurriculoEmail" id="receberCurriculoEmail" class="form-control checkbox i-checks"                                       /><i></i> Receber currículos por e-mail.
                            </div>

                            <div class="col-lg-4">
                                <input type="checkbox" name="receberCurriculoFora" id="receberCurriculoFora" class="form-control checkbox i-checks" /><i></i> Aceitar somente curriculos da mesma localidade </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Local de Trabalho:<span
                                    style="color:red">*</span></label>

                            <div class="col-lg-6">
                                <input type="checkbox" name="localVaga"  id="localVaga" checked class="form-control checkbox i-checks"/><i></i> O local do                                 emprego é o mesmo endereço da empresa sede/matriz.
                            </div>
                        </div>
                        <div id="divsLocalVaga">
                            <div class="form-group ">
                                <label class="col-lg-2 control-label">CEP:</label>

                                <div class="col-lg-2">
                                    <input type="text" name="cep" id="cep" class="cep form-control"
                                           value="<?= $empresa[0]->cep ?>"/>
                                </div>
                                <label class="col-lg-2 control-label" id="labelEndereco">Endereço:</label>

                                <div class="col-lg-5">
                                    <input type="text" name="endereco" id="endereco" class="form-control"
                                           value="<?= $empresa[0]->endereco ?>"/>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" id="labelNumero">Número:</label>

                                <div class="col-lg-2">
                                    <input type="number" name="numero" id="numero" class="form-control"
                                           value="<?= $empresa[0]->numero ?>"/>
                                </div>

                                <label class="col-sm-2 control-label" id="labelBairro">Bairro:</label>

                                <div class="col-lg-4">
                                    <input type="text" name="bairro" id="bairro" class="form-control"
                                           value="<?= $empresa[0]->bairro ?>"/>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label">Estado:</label>
                                <div class="col-sm-4">
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="">Selecione</option>
                                        <? foreach ($estados as $estado) { ?>
                                            <option <?= $empresa[0]->estado == $estado->id ? "selected" : "" ?> value="<?= $estado->uf ?>"><?= $estado->nome ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label">Cidade:</label>
                                <div class="col-sm-4">
                                    <select name="cidade" id="cidade" class="form-control">
                                        <option value="">Selecione</option>
                                        <? foreach ($cidades as $cidade) { ?>
                                            <option <?= $empresa[0]->cidade == $cidade->id ? "selected" : "" ?> value="<?= $cidade->nome ?>"><?= $cidade->nome ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <span id="spanComboSubArea">
                                <label class="col-sm-2 control-label">Área da TI:</label>
                                <div class="col-lg-4">
                                    <select name="comboSubArea" id="comboSubArea" class="form-control">
                                        <option value="">Selecione</option>
                                        <? foreach ($subareas as $sub) { ?>
                                            <option <?= $vaga[0]->idSubArea == $sub->id ? "selected" : "" ?>
                                                value="<?= $sub->id ?>"><?= $sub->subarea ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </span>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição:<span style="color:red">*</span></label>

                            <div class="col-lg-10">
                                <textarea class="form-control ckeditor" name="descricao" id="descricao"
                                          rows="6"><?= $_POST["descricao"] ?></textarea>
                            </div>
                        </div>

                       <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tipo de contratação:<span
                                    style="color:red">*</span></label>

                            <div class="col-lg-4">
                                <div class="radio i-checks"><label> <input
                                            type="radio" <?= $_POST["tipo"] == "CLT" ? "checked" : "" ?> value="CLT"
                                            name="tipo"> <i></i> CLT </label></div>
                                <div class="radio i-checks"><label> <input
                                            type="radio" <?= $_POST["tipo"] == "PJ" ? "checked" : "" ?> value="PJ"
                                            name="tipo"> <i></i> PJ </label></div>
                                <div class="radio i-checks"><label> <input
                                            type="radio" <?= $_POST["tipo"] == "Freelancer" ? "checked" : "" ?>
                                            value="Freelancer" name="tipo"> <i></i> Freelancer </label></div>
                                <div class="radio i-checks"><label> <input
                                            type="radio" <?= $_POST["tipo"] == "Estágio" ? "checked" : "" ?>
                                            value="Estágio" name="tipo"> <i></i> Estágio </label></div>
                            </div>

                            <label class="col-sm-2 control-label">Benefícios:</label>
                            <div class="col-lg-4">
                                <? foreach ($beneficios as $beneficio) { ?>

                                    <div class="checkbox i-checks">
                                        <label>
                                            <input type="checkbox" name="beneficios[]"                                                                                value="<? echo $beneficio->id ?>"/> <i></i> <? echo $beneficio->beneficio ?>
                                        </label>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Salário:</label>

                            <div class="col-lg-3">
                                <input type="text" name="salario" id="salario" class="form-control"
                                       value="<?= $_POST["salario"] ?>"/>
                            </div>
                            <div class=" checkbox i-checks col-lg-4">
                                <input type="checkbox" id="salarioVaga" name="salarioVaga"/> Salário a Combinar

                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white"
                                       onclick="location.href = '<?= base_url() ?>empresaController/listarVagas'"/>
                                <input type="submit" class="btn btn-primary" name="btSalvar"
                                       value="Publicar"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$this->load->view('priv/empresa/_inc/inferior');
?>
