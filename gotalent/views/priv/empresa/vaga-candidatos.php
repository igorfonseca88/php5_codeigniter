<?php $this->load->view('priv/empresa/_inc/superior'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Detalhes da Vaga</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>empresaController/listarVagas">Vagas</a>
            </li>
            <li class="active">
                <strong>Detalhes da Vaga</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-b-md">
                                <a href="<?= base_url() ?>empresaController/editarVagaAction/<?= $vaga[0]->id ?>" class="btn btn-white btn-xs pull-right">Editar vaga</a>
                                <h2><? echo $vaga[0]->vaga ?> - <? echo $vaga[0]->total ?> Candidato(s) à vaga</h2>

                            </div>
                            <dl class="dl-horizontal">
                                <small style="display:block;margin-bottom:10px">Navegue nos botões abaixo:</small>
                                <button type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/vagaCandidatos/<? echo $vaga[0]->id ?>'" class="btn btn-success">Em análise (<? echo $vaga[0]->em_analise ?>)</button>
                                <button type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/vagaCandidatos/<? echo $vaga[0]->id ?>/Classificado'" class="btn btn-info m-l-sm">Classificados (<? echo $vaga[0]->classificados ?>)</button> 
                                <button type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/vagaCandidatos/<? echo $vaga[0]->id ?>/Entrevista'" class="btn btn-primary m-l-sm"> Entrevistas (<? echo $vaga[0]->entrevista ?>) <img height="20px" src="<?=base_url()?>img/beta.png"></button>
                                <button type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/vagaCandidatos/<? echo $vaga[0]->id ?>/Contratado'" class="btn btn-default m-l-sm"> Contratados (<? echo $vaga[0]->contratado ?>)</button>
                                <button type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/vagaCandidatos/<? echo $vaga[0]->id ?>/Nao_Classificado'" class="btn btn-danger m-l-sm"> Não Classificados (<? echo $vaga[0]->nao_classificados ?>)</button>
                            </dl>
                        </div>
                    </div>
                    <? if($empresa[0]->moduloVideoEntrevista == "NAO" && $qtd_video[0]->total >= $p_qtd_video[0]->valor && $sit_tela == 'Classificados'){ ?>
                        <div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><? echo $msgPlano ?></div>
                    <? } ?>
                    <div class="row m-t-sm">
                        <div class="col-lg-12">								
                            <h3>Candidatos <? echo $sit_tela ?></h3>
                            <?
                            /* HABILITA O BOTÃO APENAS PARA OS CLASSIFICADOS */
                            if ($sit_tela == 'Classificados') {
                                ?>
                                <? if ($empresa[0]->moduloVideoEntrevista == "SIM") { ?>
                                    <input type="button" data-toggle="modal"  data-target="#myModalPerguntasVideo" value="Selecionar para entrevista"/>  
                                <? } else {
                                    if($empresa[0]->moduloVideoEntrevista == "NAO" && $qtd_video[0]->total >= $p_qtd_video[0]->valor && $sit_tela == 'Classificados'){ ?>
                                        <input type="button" data-toggle="modal"  data-target="" onclick="alert('Seu plano atual permite que tenha até <? echo $p_qtd_video[0]->valor ?> faça UPGRADE para o plano Go Talent e continue realizando entrevistas de forma ilimitada.')" value="Selecionar para entrevista"/>
                                    <? } else { ?>
                                        <input type="button" data-toggle="modal"  data-target="#myModalPerguntasVideo" value="Selecionar para entrevista"/>  
                                    <? } ?>
                                <? } ?>
                            <? } ?>
                            <? foreach ($profissionais_vaga as $profissional) { ?>
                                <div class="contact-box">											
                                    <div class="col-sm-3" style="position:relative">	

                                        <?
                                        /* HABILITA O CHECKBOX APENAS PARA OS CLASSIFICADOS */
                                        if ($sit_tela == 'Classificados') {
                                            ?>
                                            <div class="checkbox i-checks">
                                                <input type="checkbox" name="checkSel[]" id="checkSel" value="<?= $profissional->id ?>"/>
                                            </div>    
                                        <? } ?>

                                        <? if ($profissional->isIndicacao == "SIM") { ?>    
                                            <img  alt="Go Talent - Recomenda" title="<?= $profissional->motivoIndicacao ?>" width="68px" height="68px" class="img-circle m-t-xs img-responsive imgRecomendacao" src="<? echo base_url() ?>img/recomendado.png"> 
                                        <? } ?>
                                        <? if ($profissional->foto != "") { ?>
                                            <img alt="<? echo $profissional->nome ?>" width="100%" class="m-t-xs img-responsive" src="<? echo base_url() ?>upload/profissional/<? echo $profissional->foto ?>">
                                        <? } else { ?>
                                            <img alt="<? echo $profissional->nome ?>" width="100%" class="m-t-xs img-responsive" src="<? echo base_url() ?>img/icons/nopicture.png"> 
                                        <? } ?>

                                        <div class="m-t-xs font-bold"><strong><span style="text-transform:capitalize"><? echo $profissional->nome ?></span></strong></div>													
                                    </div>

                                    <div class="col-sm-5">	
                                        <p><b>Compatibilidade: <? echo number_format($profissional->porcentagem, 0) . "%"; ?></b></p>                                       

                                        <p><i class="fa fa-at"></i> 
                                            <? echo $profissional->email ?></p>
                                        <p><i class="fa fa-phone"></i> 
                                            <? echo $profissional->telefone ?></p>
                                        <p><i class="fa fa-money"></i> 
                                            <? echo "R$ " . number_format($profissional->pretensao, 2, ',', ''); ?></p>
                                        <p><i class="fa fa-map-marker"></i> 
                                            <? echo $profissional->cidade . "-" . $profissional->estado ?></p>



                                        <a title="Currículo" 
                                           onclick="alteraAtributo('<?= base_url() ?>empresaController/visualizarDadosProfissionalAction/<? echo $profissional->id ?>')" data-toggle="modal" data-target="#myModal5" style="float:left"><img src="<? echo base_url() ?>img/icons/curriculo-icone.png" width="25px"></img></a>

                                        <a title="Mensagem" 
                                           onclick="alteraAtributoMessage('<?= base_url() ?>empresaController/enviarMensagemAction/<? echo $profissional->id ?>')" data-toggle="modal" data-target="#myModalMensagem" style="float:left;margin:0 6px 0 3px;"><i class="fa fa-envelope" style="font-size:26px"></i></a>

                                        <a href="<? echo base_url() ?>pdfController/pdf/<? echo $profissional->id ?>" title="Exportar Currículo" 
                                           onclick="" style="float:left;margin-top:3px;color:red"><i class="fa fa-file-pdf-o" style="font-size:23px"></i></a>

                                        <? if ($profissional->video != "") { ?>
                                            <a title="Vídeo Apresentação" data-toggle="modal" data-target="#myModalVideo"
                                               onclick="alteraAtributoVideo('<?= $profissional->video ?>')" style="float:left;margin:2px 0 0 5px"><img src="<? echo base_url() ?>img/play.jpg" height="24px"/></a>
                                           <? } ?>

                                        <? if ($profissional->checklist > 0) { ?>
                                            <a title="Checklist" onclick="alteraAtributoChecklist('<?= base_url() ?>checklistController/visualizarChecklistAction/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>')" data-toggle="modal" data-target="#myModalChecklist"

                                               style="float:left;margin:2px 0 0 5px">
                                                <img src="<? echo base_url() ?>img/checklist.png" height="24px"/></a>
                                        <? } ?>
                                    </div>

                                    <div class="col-sm-4" style="text-align:right">
                                        <?
                                        /* HABILITA OS BOTÕES CLASSIFICAR E NÃO CLASSIFICAR */
                                        if ($profissional->situacao == "Em análise") {
                                            ?>
                                            <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;"><button class="btn btn-info btn-circle btn-lg" title="Classificar" type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/classificarCanditado/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>'" style="float:left;margin-right:10px">
                                                    <i class="fa fa-check"></i>
                                                </button>CLASSIFICAR</span>
                                            <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;"><button class="btn btn-danger btn-circle btn-lg" title="Não Classificar" data-toggle="modal" data-target="#myModalNaoClassificar" onclick="alteraAtributoMotivo('<?= base_url() ?>empresaController/naoClassificarCanditado/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>')" type="button" style="float:left;margin-right:10px"><i class="fa fa-times"></i>
                                                </button> NÃO CLASSIFICAR</span>
                                            <?
                                        }
                                        /* NÃO MOSTRA BOTÃO E MOSTRA O FEEDBACK E VÍDEO SE TIVER  */ else if ($profissional->situacao == "Não Classificado") {
                                            ?>
                                            <span style="font-size: 10px"> <?= $profissional->feedback ?></span>

                                            <? if ($profissional->checklist_video > 0) { ?>  

                                            
                                            <div>
                                                <span class="rating"><a style="width:<?=$profissional->avaliacaoVideo*20?>%"></a></span>
                                            </div>
                                            
                                                <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;">
                                                    <button class="btn btn-primary btn-circle btn-lg" title="Assitir Entrevista" type="button" data-toggle="modal" data-target="#myModalChecklist"
                                                            onclick="alteraAtributoChecklist('<?= base_url() ?>checklistController/visualizarChecklistAction/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>/Video')" style="float:left;margin-right:10px">
                                                        <i class="fa fa-video-camera"></i>
                                                    </button>ASSISTIR 
                                                </span>
                                            <? } ?>

                                            <?
                                        }
                                        /* HABILITA OS BOTÕES CONTRATADO E NÃO CLASSIFICAR */ else if ($profissional->situacao == "Classificado") {
                                            ?>

                                            <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;"><button class="btn btn-default btn-circle btn-lg" title="Marcar como contratado" type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/contratar/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>'" style="float:left;margin-right:10px">
                                                    <i class="fa fa-legal"></i>
                                                </button>CONTRATADO
                                            </span>
                                            <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;">
                                                <button class="btn btn-danger btn-circle btn-lg" title="Não Classificar" data-toggle="modal" data-target="#myModalNaoClassificar" onclick="alteraAtributoMotivo('<?= base_url() ?>empresaController/naoClassificarCanditado/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>')" type="button" style="float:left;margin-right:10px"><i class="fa fa-times"></i>
                                                </button> NÃO CLASSIFICAR
                                            </span>
                                            <?
                                        }
                                        /* HABILITA OS BOTÕES ASSISTIR ENTREVISTA, CONTRATADO E NÃO CLASSIFICAR */ 
                                        else if ($profissional->situacao == "Entrevista" || $profissional->situacao == "Contratado") {
                                            ?>
                                            <? if ($profissional->checklist_video > 0) { ?>  

                                            
                                            <div>
                                                <span class="rating"><a style="width:<?=$profissional->avaliacaoVideo*20?>%"></a></span>
                                            </div>
                                            
                                                <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;">
                                                    <button class="btn btn-primary btn-circle btn-lg" title="Assitir Entrevista" type="button" data-toggle="modal" data-target="#myModalChecklist"
                                                            onclick="alteraAtributoChecklist('<?= base_url() ?>checklistController/visualizarChecklistAction/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>/Video')" style="float:left;margin-right:10px">
                                                        <i class="fa fa-video-camera"></i>
                                                    </button>ASSISTIR 
                                                </span>
                                            <? } ?>

                                            <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;"><button class="btn btn-default btn-circle btn-lg" title="Classificar" type="button" onclick="window.location.href = '<?= base_url() ?>empresaController/contratar/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>'" style="float:left;margin-right:10px">
                                                    <i class="fa fa-legal"></i>
                                                </button>CONTRATADO
                                            </span>
                                            <span style="display:block;width: 100%;float: left;line-height: 50px;margin-bottom: 15px;font-weight: 600;font-size: 15px;text-align: left;"><button class="btn btn-danger btn-circle btn-lg" title="Não Classificar" data-toggle="modal" data-target="#myModalNaoClassificar" onclick="alteraAtributoMotivo('<?= base_url() ?>empresaController/naoClassificarCanditado/<? echo $profissional->id ?>/<? echo $vaga[0]->id ?>')" type="button" style="float:left;margin-right:10px"><i class="fa fa-times"></i>
                                                </button> NÃO CLASSIFICAR
                                            </span>
                                        <? } ?>    
                                    </div>
                                    <div class="clearfix"></div>                                            
                                </div>
                            <? } ?>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="wrapper wrapper-content project-manager">
            <h4><? echo $vaga[0]->vaga ?></h4>
            <p class="small font-bold">
                <span><i class="fa fa-circle text-warning"></i> Publicada em: <? echo implode("/", array_reverse(explode("-", $vaga[0]->dataPublicacao))) ?></span>
            </p>
            <p class="small font-bold">
                <span><i class="fa fa-circle text-warning"></i> Vigência: <? echo implode("/", array_reverse(explode("-", $vaga[0]->dataVigencia))) ?></span>
            </p>
            <p class="small font-bold">
                <span><i class="fa fa-circle text-warning"></i> Destaque: <? echo $vaga[0]->destaque ?></span>
            </p>
            <br /><br />
            <h4>Competências</h4>
            <ul class="tag-list" style="padding: 0">
                <? foreach ($vaga_skills as $skill) { ?>
                    <li><a><i class="fa fa-tag"></i> <? echo $skill->skill ?></a></li>
                <? } ?>
            </ul>               
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <img width="40px" src="<? echo base_url() ?>img/icons/curriculo-icone.png"></img>
                <h4 class="modal-title">Currículo</h4>                  
            </div>
            <div class="modal-body" style="width:100%; height:600px">
                <iframe width="100%" height="100%" id="iframeCandidato" src=""></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>                 
            </div>
        </div>
    </div>
</div>	

<div class="modal inmodal fade" id="myModalMensagem" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Mensagem</h4>
                <small class="font-bold">Informe os dados e envie a mensagem para o candidato.</small>
            </div>
            <form id="formMessage" name="formMessage" action="" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="idVagaRetorno" value="<? echo $vaga[0]->id ?>"/>
                    <div class="form-group"><label>Assunto</label> <input type="text" name="assunto" id="assunto" placeholder="Informe o assunto" class="form-control"></div>
                    <div class="form-group"><label>Mensagem</label> 
                        <textarea class="form-control" name="mensagem" id="mensagem" placeholder="Informe a mensagem"></textarea>                    
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Enviar mensagem</button>
                    </div>
            </form>
        </div>
    </div>
</div>		
</div>	

<div class="modal inmodal fade" id="myModalNaoClassificar" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Motivo</h4>
                <small class="font-bold">Informe o motivo para enviarmos um feedback ao candidato.</small>                  
            </div>
            <form id="formMotivo" name="formMotivo" onSubmit=" return vmotivo()" action="" method="POST">
                <div class="modal-body">
                    <div class="form-group"><label>Motivo</label> 
                        <select name="feedback" id="feedback" onchange="habitaCampoMotivoDetalhado()" class="form-control">
                            <option value="">Selecione um motivo </option>
                            <option value="Vaga preenchida.">Vaga preenchida.</option>
                            <option value="Processo de seleção encerrado.">Processo de seleção encerrado.</option>
                            <option value="Baixa compatibilidade com os requisitos da vaga.">Baixa compatibilidade com os requisitos da vaga.</option>
                            <option value="Baixa compatibilidade com as competências exigidas da vaga.">Baixa compatibilidade com as competências exigidas da vaga.</option>
                            <option value="Pretensão salarial fora da faixa esperada.">Pretensão salarial fora da faixa esperada.</option>
                            <option value="Distância. Buscamos profissionais da região/cidade.">Distância. Buscamos profissionais da região/cidade.</option>
                            <option value="Não possui experiência na área. Estamos contratando alguém experiente.">Não possui experiência na área. Estamos contratando alguém experiente.</option>
                            <option value="Não respondeu a vídeo entrevista em tempo hábil.">Não respondeu a vídeo entrevista em tempo hábil.</option>
                            <option value="Função não compatível com o que é oferecido na vaga.">Função não compatível com o que é oferecido na vaga.</option>
                            <option value="Outro">Outro</option>
                        </select><br>
                        <textarea class="form-control" name="feedbackDetalhado" id="feedbackDetalhado" style="display: none;" placeholder="Informe o motivo"></textarea>                    
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
            </form>
        </div>
    </div>
</div>
</div>

<div class="modal inmodal fade" id="myModalVideo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-file-video-o modal-icon"></i>
                <h4 class="modal-title">Vídeo Apresentação</h4>                                        
            </div>

            <div class="modal-body">
                <iframe width="425" id="frameVideo" height="349" src="" frameborder="0"></iframe>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>                    
            </div>

        </div>
    </div>
</div>	

<div class="modal inmodal fade" id="myModalChecklist" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:1200px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <img width="40px" src="<? echo base_url() ?>img/checklist.png"></img>
                <h4 class="modal-title">Respostas do Checklist</h4>                  
            </div>
            <div class="modal-body" style="width:100%; height:400px">
                <iframe width="100%" height="100%" id="iframeChecklist" src=""></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>                 
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="myModalPerguntasVideo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>

                <h4 class="modal-title">Selecione um checklist</h4>
                <p>O profissional irá responder através de vídeos</p>
            </div>

            <form id="formPerguntas" name="formPerguntas" method="POST" action="<? echo base_url() ?>empresaController/entrevistar/<?= $vaga[0]->id ?>">

                <div class="modal-body">
                    <select id="idTiposPerguntas" name="idTiposPerguntas" class="form-control">
                        <option value="">Selecione</option>
                        <? foreach ($checklists as $chec) { ?>    
                            <option value="<?= $chec->id ?>"><?= $chec->checklist ?></option>
                        <? } ?>    
                    </select>


                    <p id="idPerguntas"></p>
                    <input type="hidden" name="hprofissionais" id="hprofissionais"/>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="recuperaChecks()" class="btn btn-primary" name="btSalvar" value="Salvar" />
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>                    
                </div>
            </form>    


        </div>
    </div>
</div>


<?php $this->load->view('priv/empresa/_inc/inferior'); ?>

		
