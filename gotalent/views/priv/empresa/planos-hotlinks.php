<? $this->load->view('priv/empresa/_inc/superior');?>
	
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-6">
            <h1>Plano Empresarial</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?echo base_url()?>area-restrita-empresa">Home</a>
                </li>
                <li class="active">
                    <strong>Plano Empresarial</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wp-section pricing-plans">
            <div class="row">
                    

                        <script type="text/javascript">
                            function importHotmart(){ 
                                var imported = document.createElement('script'); 
                                imported.src = 'https://static.hotmart.com/checkout/widget.min.js'; 
                                document.head.appendChild(imported); 
                            } 
                            importHotmart(); 
                         </script>  
                    <div class="col-md-4">
                        <div class="plano Bronze">
                            <h2>Básico</h2>
                            <h3><span id="valorAnuncio">R$ 79,90</span></h3>
                            <ul>                     
                                <li>Anúncio ilimitado de vagas</li>  
                                <li>Gestão de candidatos</li>
                                <li>Criação de checklists</li>
                            </ul>
                            <a onclick="return false;" href="https://www.hotmart.net.br/checkout.html?order=G3107674G&checkoutMode=2" class="hotmart-fb"><img src='https://static.hotmart.com/img/btn-buy-green.png'></a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="plano Prata">
                            <h2>Ideal</h2>
                            <h3><span id="valorAnuncio">R$ 119,90</span></h3>
                            <ul>
                                <li>Anúncio ilimitado de vagas</li>  
                                <li>Gestão de candidatos</li>
                                <li>Criação de checklists</li>
                                <li>Acesso ilimitado ao banco de currículos</li>
                            </ul>
                            <a onclick="return false;" href="https://www.hotmart.net.br/checkout.html?order=G3107674G&checkoutMode=2" class="hotmart-fb"><img src='https://static.hotmart.com/img/btn-buy-green.png'></a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="plano Ouro">
                            <h2>Go Talent</h2>
                            <h3><span id="valorAnuncio">R$ 199,90</span></h3>
                            <ul>
                                <li>Anúncio ilimitado de vagas</li> 
                                <li>Gestão de candidatos</li>
                                <li>Criação de checklists</li>
                                <li>Acesso ilimitado ao banco de currículos</li>
                                <li>Acesso a ferramenta 'Go Bot' para vídeo-entrevistas</li>
                            </ul>
                            <a onclick="return false;" href="https://www.hotmart.net.br/checkout.html?order=G3107674G&checkoutMode=2" class="hotmart-fb"><img src='https://static.hotmart.com/img/btn-buy-green.png'></a>
                        </div>
                    </div>
            </div>
        </div>
	
<?  $this->load->view('priv/empresa/_inc/inferior'); ?>
