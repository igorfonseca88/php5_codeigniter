<?
if ($this->Usuario_model->logged() == FALSE && $this->session->userdata("tipo") == 'Empresa') {
    redirect('/login/', 'refresh');
}

//Transforma títulos em URL amigáveis
function url($str) {
    $str = strtolower(utf8_decode($str));
    $i = 1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
    while ($i > 0)
        $str = str_replace('--', '-', $str, $i);
    if (substr($str, -1) == '-')
        $str = substr($str, 0, -1);
    return $str;
}

$title = "Confira como é fácil criar uma vaga de TI";
$description = "Aqui a sua empresa encontra os melhores profissionais de TI, basta cadastrar suas oportunidades para ter acesso a pessoas qualificadas que buscam um emprego.";
$keywords = "vagas de emprego em ti";
?>


<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title><?=$title?></title>
        <meta name="author" content="Go Talent"/>
        <meta name="keywords" content="<?=$keywords?>"/>
        <meta name="description" content="<?=$description?>">
        <meta name="copyright" content="Copyright (c) 2015 GoTalent" />
        <meta http-equiv="content-language" content="pt-br">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link href="<? echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<? echo base_url() ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/animate.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/style.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/plugins/iCheck/custom.css" rel="stylesheet">

        <link href="<?= base_url() ?>css/jquery.autocomplete.css" rel="stylesheet" />

        <!-- Morris -->
        <link href="<? echo base_url() ?>css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

        <!-- Gritter -->
        <link href="<? echo base_url() ?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">



    </head>

    <body style="background-color: #f3f3f4">
        <div id="wrapper">


            
