<?
if ($this->Usuario_model->logged() == FALSE && $this->session->userdata("tipo") == 'Empresa') {
    redirect('/login/', 'refresh');
}

//Transforma títulos em URL amigáveis
function url($str) {
    $str = strtolower(utf8_decode($str));
    $i = 1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
    while ($i > 0)
        $str = str_replace('--', '-', $str, $i);
    if (substr($str, -1) == '-')
        $str = substr($str, 0, -1);
    return $str;
}

$title = "Confira como é fácil criar uma vaga de TI";
$description = "Aqui a sua empresa encontra os melhores profissionais de TI, basta cadastrar suas oportunidades para ter acesso a pessoas qualificadas que buscam um emprego.";
$keywords = "vagas de emprego em ti";
?>


<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title><?=$title?></title>
        <meta name="author" content="Go Talent"/>
        <meta name="keywords" content="<?=$keywords?>"/>
        <meta name="description" content="<?=$description?>">
        <meta name="copyright" content="Copyright (c) 2015 GoTalent" />
        <meta http-equiv="content-language" content="pt-br">
   
        <link href="<? echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<? echo base_url() ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/animate.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/style.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/plugins/iCheck/custom.css" rel="stylesheet">

        <link href="<?= base_url() ?>css/jquery.autocomplete.css" rel="stylesheet" />

        <!-- Morris -->
        <link href="<? echo base_url() ?>css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

        <!-- Gritter -->
        <link href="<? echo base_url() ?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<? echo base_url() ?>css/jquery.cleditor.css" />
        
        <link href="<? echo base_url() ?>css/plugins/datapicker/datepicker3.css" rel="stylesheet">
        
        
        
        <script>
           (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53411814-1', 'auto');
            ga('send', 'pageview');

        </script> 


    </head>

    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> 
                                <span>
                                    <a href="<? echo base_url() ?>"> 
                                        <img alt="image"  width="90%" src="<? echo base_url() ?>img/logo.png" />
                                    </a>
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> 
                                            <strong class="font-bold"> Olá, <? echo $this->session->userdata("nome") ?>!</strong><br>
                                            <strong class="font-bold"> Empresa: <? echo $empresa[0]->razaosocial ?></strong><br>
                                        </span>  </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="<? echo base_url() ?>empresaController/editarEmpresaAction">Empresa</a></li>
                                    <li><a href="<? echo base_url() ?>empresaController/editarUsuarioAction">Alterar Senha</a></li>

                                    <li><a href="<?= base_url() ?>login/login/logoff">SAIR</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                GO
                            </div>
                        </li>

                        <li>
                            <a href="<?= base_url() ?>empresaController"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> </a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Cadastros</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                
                                <li><a href="<?= base_url() ?>empresaController/editarEmpresaAction">Dados da Empresa </a></li>
                                <li><a href="<?= base_url() ?>empresaController/listarUsuarios">Usuários </a></li>

                            </ul>
                        </li>
                        
                        <li>
                            <a href="<?= base_url() ?>checklistController/listarChecklists"><i class="fa fa-file-text-o"></i> <span class="nav-label">Checklists</span> </a>
                        </li>
                    
                        <li>
                            <a href="<?= base_url() ?>empresaController/listarVagas"><i class="fa fa-bullhorn"></i> <span class="nav-label">Vagas</span> </a>
                        </li>
                        
                        <li>
                            <a href="<?= base_url() ?>bcController/banco"><i class="fa fa-database"></i> <span class="nav-label">Banco de currículos</span> </a>
                        </li>
                        
                        <li>
                            <a href="<?= base_url() ?>empresaController/faq"><i class="fa fa-question-circle"></i> <span class="nav-label">FAQ</span> </a>
                        </li>
                      <!--
                         <li>
                            <a href="<?//= base_url() ?>empresaController/plano"><i class="fa fa-cogs"></i> <span class="nav-label">Quero mais VAGAS</span> </a>
                        </li>
                      -->
                    </ul>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                        </div>
                        <ul class="nav navbar-top-links navbar-right">

                          

                            <li>
                                <a href="<?= base_url() ?>login/login/logoff">
                                    <i class="fa fa-sign-out"></i> SAIR
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>	