<script src="<?echo base_url()?>js/jquery-2.1.1.js"></script>
<script src="<?echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?echo base_url()?>js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?echo base_url()?>js/inspinia.js"></script>

<!-- CKEDITOR -->
<script type="text/javascript" src="<?=base_url()?>system/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>system/ckeditor/config.js"></script>

<!-- Data Tables -->
<script src="<? echo base_url() ?>js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<? echo base_url() ?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<? echo base_url() ?>js/plugins/dataTables/dataTables.responsive.js"></script>

<!-- jQuery UI -->
<script src="<?echo base_url()?>js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?echo base_url()?>js/plugins/iCheck/icheck.min.js"></script>
<script type='text/javascript' src="<?=base_url()?>js/jquery.autocomplete.js"></script>
<script src="<?echo base_url()?>js/jquery.maskMoney.js"></script>
<script src="<?echo base_url()?>js/jquery.maskedinput.min.js"></script>
<script src="<?echo base_url()?>js/funcoes.js"></script>


        
    </div>
</body>
</html>