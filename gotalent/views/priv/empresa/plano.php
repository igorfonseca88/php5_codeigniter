<? $this->load->view('priv/empresa/_inc/superior');?>
	
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-6">
            <h1>Plano Empresarial</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?echo base_url()?>area-restrita-empresa">Home</a>
                </li>
                <li class="active">
                    <strong>Plano Empresarial</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wp-section pricing-plans">
            <div class="row">
                <? foreach ($planos_empresa as $plano) { ?>
                <?if($plano->id != 4) {?>
                    <div class="col-md-4">
                        <form class="plano Ouro" method="post" action="<?= base_url() ?>planoController/planoEmpresa">
                            <input type="hidden" name="idplano" id="idplano" value="<?= $plano->id ?>" />
                            <h2><?= $plano->plano ?></h2>
                            <h3><span id="valorAnuncio">R$ <?= $plano->valor ?></span></h3>
                            <ul>
                                <?if($plano->id == 1) {?>
                                <li>Gerencia seus processos com eficiência e baixo custo. Com acesso ao painel de vagas e triagem de currículos.</li>  
                                <?}?>
                                <?if($plano->id == 2) {?>
                                <li>O melhor custo beneficio para sua empresa. Com o exclusivo banco de currículos de profissionais de TI.</li>  
                                <?}?>
                                <?if($plano->id == 3) {?>
                                <li>Tenha uma experiência completa utilizando a recrutadora virtual Go Bot.</li>  
                                <?}?>
                               
                            </ul>
                            <?if($plano->id == 1) {?>
                                <a mp-mode="dftl" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=0362519e70c74186b1992540325d6dc0" name="MP-payButton" class="orange-l-rn-ar">Assinar Plano</a>
                                <script type="text/javascript">
                                (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                                </script>
                            <?}?>
                            <?if($plano->id == 2) {?>
                                <a mp-mode="dftl" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=f7f443dcab704390b36b65144826c765" name="MP-payButton" class="orange-l-rn-ar">Assinar Plano</a>
                                <script type="text/javascript">
                                (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                                </script>
                            <?}?>
                            <?if($plano->id == 3) {?>
                                <a mp-mode="dftl" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=5aabcd807799427f926a1406babf4bc0" name="MP-payButton" class="orange-l-rn-ar">Assinar Plano</a>
                                <script type="text/javascript">
                                (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                                </script>
                            <?}?>
                        </form>
                    </div>
                <?}?>
                <? } ?>
            </div>
        </div>
	
<?  $this->load->view('priv/empresa/_inc/inferior'); ?>
