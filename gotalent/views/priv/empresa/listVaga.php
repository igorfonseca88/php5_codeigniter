<?
$this->load->view('priv/empresa/_inc/superior');
$variavelGlobal = 0;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Listagem de Vagas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Vagas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listagem de todas as vagas</h5>

                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>empresaController/novaVagaAction" class="btn btn-primary btn-xs">Criar
                            nova Vaga</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>
                    <div class="project-list">
                        <table class="table table-hover">
                            <tbody>
                            <? if (count($vagas) > 0) { ?>
                                <? foreach ($vagas as $row) { ?>
                                    <tr>
                                        <td class="project-status">
                                            <? if ($row->situacao == 'Em Elaboração') { ?>
                                                <span class="label label-danger"><?= $row->situacao ?></span>
                                            <? } else if ($row->situacao == 'Ativo') { ?>
                                                <span class="label label-primary"><?= $row->situacao ?></span>
                                            <? } else if ($row->situacao == 'Ativo') { ?>
                                                <span class="label label-primary"><?= $row->situacao ?></span>
                                            <? } else { ?>
                                                <span class="label label-default"><?= $row->situacao ?></span>
                                            <? } ?>
                                        </td>
                                        <td class="project-title">
                                            <a href="<?= base_url() ?>empresaController/editarVagaAction/<?= $row->id ?>"><?= $row->vaga ?></a>
                                            <br/>
                                            <small>
                                                Criada <?= implode("/", array_reverse(explode("-", $row->dataCadastro))) ?></small>
                                            <br/>
                                            <small>
                                                Publicada <?= implode("/", array_reverse(explode("-", $row->dataPublicacao))) ?></small>
                                            <br/>
                                            <? if ($row->situacao != 'Desaprovado') { ?>
                                                <small>Vigente
                                                    até: <?= implode("/", array_reverse(explode("-", $row->dataVigencia))) ?></small>
                                                <br/>
                                                <small> <?= $row->cidade . "/" . $row->estado ?></small>
                                            <? } else if ($row->situacao == 'Desaprovado') { ?>
                                                <small>
                                                    Desaprovado: <?= implode("/", array_reverse(explode("-", $row->dataDesaprovacao))) ?></small>
                                                <br/>
                                                <small> <?= $row->cidade . "/" . $row->estado ?></small>
                                            <? } ?>
                                        </td>

                                        <td class="project-indication" width="150px">
                                            <a title="Indicação de Profissionais a Vaga" data-toggle="modal"
                                               data-backdrop="static" <? if ($row->situacao == "Ativo") {
                                                echo "data-target=#{$row->id}";
                                            } ?>>
                                                <div
                                                    class="widget_new style1 navy-bg" <? if ($row->situacao != "Ativo") {
                                                    echo 'style="background:#ccc;"';
                                                } ?>>
                                                    <div class="row vertical-align">
                                                        <div class="box_image_indicacao col-xs-3">
                                                            <img src="../_imagens/icon_teste.png"
                                                                 class="button_mais_indicados"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="project-completion" width="150px">
                                            <a title="Quantidade de views">
                                                <div class="widget style1 navy-bg">
                                                    <div class="row vertical-align">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-eye fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <h2 class="font-bold"
                                                                style="margin-left:8px;"><?= $row->visualizacao ?></h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="project-people" width="150px">
                                            <a title="Candidatos à vaga"
                                               href="<? echo base_url() ?>empresaController/vagaCandidatos/<? echo $row->id ?>">
                                                <div class="widget style1 navy-bg">
                                                    <div class="row vertical-align">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-user fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <h2 class="font-bold"><?= $row->total ?></h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="project-people" width="150px">
                                            <a title="Candidatos pendentes de análise"
                                               href="<? echo base_url() ?>empresaController/vagaCandidatos/<? echo $row->id ?>">
                                                <div class="widget style1 red-bg">
                                                    <div class="row vertical-align">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-bell-o fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <h2 class="font-bold"><?= $row->analise ?></h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="project-actions">
                                            <a href="<?= base_url() ?>empresaController/editarVagaAction/<?= $row->id ?>"
                                               class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Editar </a>
                                            <a href="<? echo base_url() ?>empresaController/vagaCandidatos/<? echo $row->id ?>"
                                               class="btn btn-white btn-sm"> Candidatos </a>
                                        </td>
                                    </tr>
                                <? } ?>
                            <? } else { ?>
                                <p> Nenhum registro encontrado. </p>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? $i = 0;
foreach ($vagas as $row) { ?>
    <div class="modal inmodal fade" <? echo "id={$row->id}" ?> tabindex="-1" role="dialog" aria-hidden="true"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span
                            class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Indicação de Profissionais</h4>
                </div>
                <div class="modal-body" style="overflow:auto; height:400px">
                    <table class="table table-hover">
                        <?
                        for ($j = 0; $j < 10; $j++) {
                            if ($profissionais[$i][$j]->id != null) {
                                ?>
                                <tr>
                                    <td width="100px">
                                        <? if ($profissionais[$i][$j]->foto != "") { ?>
                                            <img class="img-circle" alt="<? echo $profissionais[$i][$j]->nome ?>"
                                                 width="100px"
                                                 src="<? echo base_url() ?>upload/profissional/<? echo $profissionais[$i][$j]->foto ?>">
                                        <? } else { ?>
                                            <img class="img-circle" alt="<? echo $profissionais[$i][$j]->nome ?>"
                                                 width="100px"
                                                 src="<? echo base_url() ?>img/icons/nopicture.png">
                                        <? } ?>
                                    </td>
                                    <td>
                                        <b style="text-transform:uppercase"><?= $profissionais[$i][$j]->nome ?></b><br>
                                        <small><?= $profissionais[$i][$j]->telefone ?>
                                            - <?= $profissionais[$i][$j]->email ?></small>
                                        <br>
                                        <small><?= $profissionais[$i][$j]->cidade ?>
                                            /<?= $profissionais[$i][$j]->estado ?></small>
                                        <br>
                                        <small><?= $profissionais[$i][$j]->ultCargo ?></small>
                                        <br>
                                        <small>Pretensão salarial de
                                            R$ <?= number_format($profissionais[$i][$j]->pretensao, 2, ',', '') ?></small>
                                        <br>
                                    </td>
                                </tr>
                            <? } else {
                                continue;
                            }
                        } ?>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <? $i++;
} ?>
<?
$this->load->view('priv/empresa/_inc/inferior');
?>
