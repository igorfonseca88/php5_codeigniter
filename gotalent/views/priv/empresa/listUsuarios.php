<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Listagem de Usuários</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Usuários</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listagem de todos os Usuários</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>empresaController/novoUsuarioAction" class="btn btn-primary btn-xs">Criar novo Usuário</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <div class="project-list">

                        <table class="table table-hover">
                            <tbody>
                                <? if (count($usuarios) > 0) { ?>        
                                    <? foreach ($usuarios as $row) { ?>
                                        <tr>
                                            <td class="project-status">
                                            <? if ($row->situacao == 'ATIVO') { ?>
                                                <span class="label label-primary"><?= $row->situacao ?></span>
                                           
                                            <? } else { ?>
                                                <span class="label label-default"><?= $row->situacao ?></span>
                                            <? } ?>
                                            </td>
                                            <td class="project-title">
                                                <?= implode("/", array_reverse(explode("-", $row->dataCriacao))) ?>

                                            </td>


                                            <td class="project-people">
                                                <?= $row->nome ?>
                                            </td>
                                            
                                             <td class="project-people">
                                                <?= $row->login ?>
                                            </td>
                                            <td class="project-actions">

                                                <a href="<?= base_url() ?>empresaController/editarUsuarioAction/<?= $row->idUsuario ?>" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Editar </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                <? } else { ?>            
                                <p> Nenhum registro encontrado. </p>    
                            <? } ?>         

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$this->load->view('priv/empresa/_inc/inferior');
?>
