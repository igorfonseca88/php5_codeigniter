<?
$this->load->view('priv/empresa/_inc/superior');
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h1>Usuário</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li>
                <a href="<? echo base_url() ?>empresaController/listarUsuarios">Listagem de Usuários</a>
            </li>
            <li class="active">
                <strong>Editar usuário</strong>
            </li>
        </ol>
    </div>
</div>	
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">		
            <div class="ibox float-e-margins">	
                <div class="ibox-title">
                    <h5>Dados de Acesso</h5>
                </div>				
                <div class="ibox-content">	
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>

                    <form method="post" action="<?= base_url() ?>empresaController/editUsuario" class="form-horizontal">
                        <input type="hidden" name="id" id="id" value="<?= $usuario[0]->idUsuario ?>"/>	
                                       
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Data Cadastro </label>
                            <div class="col-lg-4">	
                                <p class="form-control-static"><?= implode("/", array_reverse(explode("-", $usuario[0]->dataCriacao))) ?> </p>
                            </div>
                           
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail (utilizado para o login) </label>
                            <div class="col-lg-4">	
                                <p class="form-control-static"><?= $usuario[0]->login ?> </p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome <span style="color:red">*</span></label>
                            <div class="col-lg-4">	
                                <input type="text" name="nome" id="nome" class="form-control" value="<?= $usuario[0]->nome ?>" />
                            </div>
                           
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Senha <span style="color:red">*</span></label>
                            <div class="col-lg-4">	
                                <input type="password" name="senha" id="senha" class="form-control" value="<?= $usuario[0]->senha ?>" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Situação <span style="color:red">*</span></label>
                            <div class="col-lg-4">	
                                <select class="form-control" name="situacao" id="situacao">
                                    
                                    <option value="ATIVO" <?=$usuario[0]->situacao == "ATIVO" ? "selected" : ""?>> ATIVO</option>
                                    <option value="INATIVO" <?=$usuario[0]->situacao == "INATIVO" ? "selected" : ""?>> INATIVO</option>
                                    
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>empresaController/listarUsuarios'"  />
                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




<?
$this->load->view('priv/empresa/_inc/inferior');
?>
