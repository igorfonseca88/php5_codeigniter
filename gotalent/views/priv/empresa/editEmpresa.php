<?
$this->load->view('priv/empresa/_inc/superior');
?>

<script>
    function confirmaExcluir(id) {
        var r = confirm("Confirma a exclusão do item?");
        if (r == true) {
            location.href = "<?= base_url() ?>empresaController/deletarGaleria/" + id;
        }
    }
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h1>Empresa</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url() ?>/empresaController">Home</a>
            </li>
            <li class="active">
                <strong>Dados da Empresa</strong>
            </li>
        </ol>
    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
            <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
            <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
            <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>


            <form method="post" action="<?= base_url() ?>empresaController/editEmpresa" class="form-horizontal">
                <input type="hidden" name="id" id="id" value="<?= $empresa[0]->id ?>"/>


                <div class="ibox float-e-margins">	
                    <div class="ibox-title">
                        <h5>Dados da Empresa</h5>
                    </div>

                    <div class="ibox-content">	


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Data do Cadastro </label>
                            <div class="col-sm-4">
                                <p class="form-control-static"><?= implode("/", array_reverse(explode("-", $empresa[0]->dataCadastro))) ?> </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">CNPJ </label>
                            <div class="col-sm-4">
                                <input type="text" name="cnpj" id="cnpj" class="form-control cnpj" value="<?= $empresa[0]->cnpj ?>" />
                            </div>


                            <label class="col-sm-2 control-label">Razão Social</label>
                            <div class="col-sm-4">
                                <input type="text" name="razaosocial" id="razaosocial" class="form-control" value="<?= $empresa[0]->razaosocial ?>" />
                            </div>

                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Contato</label>
                            <div class="col-sm-4">	
                                <input type="text" name="contato" id="contato" class="form-control"  value="<?= $empresa[0]->contato ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">E-mail <small class="text-navy">(Enviamos as notificações para este e-mail)</small></label>
                            <div class="col-sm-4">
                                <input type="text" name="email" id="email" class="form-control" value="<?= $empresa[0]->email ?>" />
                            </div>

                            <label class="col-sm-2 control-label">Telefone</label>
                            <div class="col-sm-4">
                                <input type="text" name="telefone" id="telefone" class="telefone form-control"  value="<?= $empresa[0]->telefone ?>" />
                            </div>

                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">

                            <label class="col-sm-2 control-label">CEP</label>
                            <div class="col-sm-4">
                                <input type="text" name="cep" id="cep" class="cep form-control" value="<?= $empresa[0]->cep ?>" />
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Endereço</label>
                            <div class="col-sm-6">
                                <input type="text" name="endereco" id="endereco" class="form-control"  value="<?= $empresa[0]->endereco ?>" />
                            </div>

                            <label class="col-sm-2 control-label">Número</label>
                            <div class="col-sm-2">
                                <input type="text" name="numero" id="numero" class="form-control"  value="<?= $empresa[0]->numero ?>" />
                            </div>	
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Bairro</label>
                            <div class="col-sm-4">
                                <input type="text" name="bairro" id="bairro" class="form-control"  value="<?= $empresa[0]->bairro ?>" />
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">Estado</label>
                            <div class="col-sm-4">
                                <select name="estado" id="estado" class="form-control">
                                    <option value="">Selecione</option>    
                                    <? foreach ($estados as $estado) { ?>
                                        <option <?= $empresa[0]->estado == $estado->id ? "selected" : "" ?> value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                    <? } ?>    
                                </select>
                            </div>

                            <label class="col-sm-2 control-label">Cidade</label>
                            <div class="col-sm-4">	
                                <select name="cidade" id="cidade" class="form-control">
                                    <option value="">Selecione</option>    
                                    <? foreach ($cidades as $cidade) { ?>
                                        <option <?= $empresa[0]->cidade == $cidade->id ? "selected" : "" ?> value="<?= $cidade->id ?>"><?= $cidade->nome ?></option>
                                    <? } ?>    
                                </select>
                            </div>
                        </div>

                    </div>
                </div>  

                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h5> Dados públicos no portal  </h5>

                    </div>

                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">URL de sua empresa: </label>
                            <div class="col-sm-4">	
                                <p class="form-control-static">http://www.gotalent.com.br/empresa/<?= $empresa[0]->apelido ?></p>
                            </div>
                            <!--<div class="col-sm-4">	
                                <input type="text" name="url" id="url" class="form-control" onblur="validarUrl();"  value="<? //= $empresa[0]->apelido   ?>" />
                             </div>
                             <div class="col-sm-2">
                                 <p id="respValidacao"></p>
                             </div> -->
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Site</label>
                            <div class="col-sm-4">	
                                <input type="text" name="site" id="site" class="form-control"  value="<?= $empresa[0]->site ?>" />
                            </div>

                            <label class="col-sm-2 control-label">Facebook</label>
                            <div class="col-sm-4">	
                                <input type="text" name="facebook" id="facebook" class="form-control"  value="<?= $empresa[0]->facebook ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Twitter</label>
                            <div class="col-sm-4">	
                                <input type="text" name="twitter" id="twitter" class="form-control"  value="<?= $empresa[0]->twitter ?>" />
                            </div>

                            <label class="col-sm-2 control-label">Instagram</label>
                            <div class="col-sm-4">	
                                <input type="text" name="instagram" id="instagram" class="form-control"  value="<?= $empresa[0]->instagram ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Google Plus</label>
                            <div class="col-sm-4">	
                                <input type="text" name="googleplus" id="googleplus" class="form-control"  value="<?= $empresa[0]->googleplus ?>" />
                            </div>

                            <label class="col-sm-2 control-label">Youtube</label>
                            <div class="col-sm-4">	
                                <input type="text" name="youtube" id="youtube" class="form-control"  value="<?= $empresa[0]->youtube ?>" />
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Número de colaboradores</label>
                            <div class="col-sm-4">	
                                <input type="text" name="numeroColaboradores" id="numeroColaboradores" class="form-control"  value="<?= $empresa[0]->numeroColaboradores ?>" />
                            </div>
                            <label class="col-sm-2 control-label">Fundação</label>
                            <div class="col-sm-4">	
                                <input type="text" name="fundacao" id="fundacao" class="form-control"  value="<?= $empresa[0]->fundacao ?>" />
                            </div>

                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slogan</label>
                            <div class="col-sm-10">		
                                <textarea name="slogan" id="slogan" class="form-control"><?= $empresa[0]->slogan ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sobre a Empresa</label>
                            <div class="col-sm-10">		
                                <textarea name="sobre" id="sobre" class="form-control"><?= $empresa[0]->sobre ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pontos positivos</label>
                            <div class="col-sm-10">		
                                <textarea name="pontosPositivos" id="pontosPositivos" class="form-control"><?= $empresa[0]->pontosPositivos ?></textarea>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Localização - Google Maps</label>
                            <div class="col-sm-10">		
                                <textarea name="mapa" id="mapa" class="form-control"><?= $empresa[0]->mapa ?></textarea>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>empresaController'"  />
                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
                            </div>		
                        </div>
                    </div>
                </div>
            </form>
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5> Logomarca da empresa  </h5>

                </div>

                <div class="ibox-content">

                    <div class="row">

                        <form id="upload" name="upload" action="<?= base_url() ?>empresaController/uploadArquivo/<? echo $empresa[0]->id ?>" method="post" enctype="multipart/form-data">

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <? if ($empresa[0]->logo != "") { ?>	
                                        <img class="" src="<?= base_url() ?>upload/empresas/<? echo $empresa[0]->logo ?>" id="foto-curriculo" title="<? echo $empresa[0]->logo ?>" width="100%" />
                                    <? } ?>	
                                </div>
                            </div>

                            <div class="col-lg-5">
                                <input type="file"  class="btn btn-default" onchange="document.upload.submit()" name="userfile" id="userfile" value="ESCOLHER FOTO" />
                                <span><small class="text-navy">Tamanho máximo de 2MB</small></span>
                            </div>
                        </form>	

                    </div>	
                </div>
            </div>

            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5> Galeria de fotos (será exibido em sua página)  </h5>

                </div>

                <div class="ibox-content">

                    <div class="row">
                        <p><small>
                                <? if ($galeria) { ?> * Clique na imagem para excluir / <? } ?> * Upar imagens de igual proporção recomendado: 200x150 (px)</small></p>
                        <div class="galeria">
                            <? foreach ($galeria as $g) { ?> <img height="100px" src="<?= base_url() ?>upload/galeria/<?= $g->imagem ?>" onclick="confirmaExcluir('<?= $g->id ?>')" alt="Excluir" title="Excluir" /> <? } ?>
                        </div>
                        <br />
                        <? if (count($galeria) < 5) { ?>
                            <form action="<?= base_url() ?>empresaController/addGaleria" method="post" enctype="multipart/form-data">
                                <div class="form-group"> <input type="file" class="form-control" name="userfile" id="userfile" style="width:200px;float:left;margin-right:10px" /> <input class="btn btn-success" type="submit" name="enviar" value="Salvar foto" /> </div>
                            </form>
                        <? } else { ?>
                            <b style="color:red">* Limite de 5 fotos atingido.</b>
                        <? } ?>
                    </div>	
                </div>
            </div>

        </div>
    </div>
</div>



<?
$this->load->view('priv/empresa/_inc/inferior');
?>
