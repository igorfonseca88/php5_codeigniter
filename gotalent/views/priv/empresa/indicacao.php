<?
$this->load->view('priv/empresa/_inc/superior');
?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-6">
            <h1>Banco de Currículos</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
                </li>
                <li class="active">
                    <strong>Banco de Currículos</strong>
                </li>
            </ol>
        </div>
    </div>


    <div id="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div id="alerta">
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#opcoes">
                                    Opções de Filtro
                                </button>
                            </div>
                        </div>

                        <div id="opcoes" class="collapse">
                            <form method="post" id="formIndicacao" class="form-horizontal"
                                  action="<?= base_url() ?>bcController/buscarProfissionais">
                                <div class="row">


                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Digite um Cargo (Buscaremos nos cargos que o profissional ocupa ou já
                                                ocupou)</label><br>
                                            <input type="text" name="bc_cargo" id="bc_cargo" class="form-control"
                                                   value="<?= $_SESSION["bc_cargo"] ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Digite uma Competência (Você pode selecionar uma competência, ou
                                                digitar
                                                o que deseja buscar. EX: Linguagem PHP Avançado ou PHP)</label><br>
                                            <input type="text" name="bc_skill" id="bc_skill" class="form-control"
                                                   value="<?= $_SESSION["bc_skill"] ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Pesquisar por palavra-chave (Você pode digitar alguma palavra que
                                                estará
                                                presente nas informações do currículo do profissional)</label><br>
                                            <input type="text" name="bc_palavra_chave" id="bc_palavra_chave"
                                                   class="form-control"
                                                   value="<?= $_SESSION["bc_palavra_chave"] ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Estado</label><br>
                                            <select name="estado" id="estado" class="form-control">
                                                <option value="">TODOS</option>
                                                <? foreach ($estados as $estado) { ?>
                                                    <option <?= $_SESSION["filtroEstado"] == $estado->id ? "selected" : "" ?>
                                                        value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                                <? } ?>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Cidade</label><br>
                                            <select name="cidade" id="cidade" class="form-control">
                                                <option value="">Selecione um Estado</option>
                                                <? foreach ($cidades as $cidade) { ?>
                                                    <option <?= $_SESSION["filtroCidade"] == $cidade->id ? "selected" : "" ?>
                                                        value="<?= $cidade->id ?>"><?= $cidade->nome ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Pretensão</label><br>
                                            <select id="tipoPretensao" name="tipoPretensao" class="form-control"
                                                    style="width:50%;float:left">
                                                <option <?= $_SESSION["tipoPretensao"] == "<=" ? "selected" : "" ?>
                                                    value="<=">
                                                    Menor igual
                                                </option>
                                                <option <?= $_SESSION["tipoPretensao"] == ">=" ? "selected" : "" ?>
                                                    value=">=">
                                                    Maior igual
                                                </option>
                                            </select>
                                            <input type="text" name="pretensao" id="pretensao"
                                                   class="form-control money"
                                                   style="width:50%;float:left"
                                                   value="<? echo $_SESSION["pretensao"] ?>"/>
                                        </div>
                                    </div>


                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Sexo</label><br>
                                            <select id="sexo" name="sexo" class="form-control">
                                                <option value="">TODOS</option>
                                                <option <?= $_SESSION["sexo"] == "Feminino" ? "selected" : "" ?>
                                                    value="Feminino">Feminino
                                                </option>
                                                <option <?= $_SESSION["sexo"] == "Masculino" ? "selected" : "" ?>
                                                    value="Masculino">Masculino
                                                </option>
                                            </select>

                                        </div>
                                    </div>


                                    <!-- <div class="col-lg-4">
                                     <div class="form-group">
                                         <label>Nível</label><br>
                                         <select name="searchNivelBanco" id="searchNivelBanco" multiple="multiple" name="example-basic" size="5" class="form-control">
                                             <option value="Todos">Todos os níveis</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Estagiario" ? "selected" : ""  ?> value="Estagiario">Estagiário</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Trainee" ? "selected" : ""  ?> value="Trainee">Trainee</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Junior" ? "selected" : ""  ?> value="Junior">Júnior</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Pleno" ? "selected" : ""  ?> value="Pleno">Pleno</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Senior" ? "selected" : ""  ?> value="Senior">Sênior</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Supervisor" ? "selected" : ""  ?> value="Supervisor">Supervisor</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Coordenador" ? "selected" : ""  ?> value="Coordenador">Coordenador</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Gerente" ? "selected" : ""  ?> value="Gerente">Gerente</option>
                                             <option <? //= $_SESSION["searchNivel"] == "Diretor" ? "selected" : ""  ?> value="Diretor">Diretor</option>
                                         </select> 
     
                                     </div>
                                 </div> -->


                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Situação do profissional</label><br>
                                            <select id="sitProfissional" name="sitProfissional" class="form-control">
                                                <option value="">TODOS</option>
                                                <option <?= $_SESSION["sitProfissional"] == "Empregado" ? "selected" : "" ?>
                                                    value="Empregado">Empregado
                                                </option>
                                                <option <?= $_SESSION["sitProfissional"] == "Desempregado" ? "selected" : "" ?>
                                                    value="Desempregado">Desempregado
                                                </option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Faixa Etária (Idade entre)</label><br/>

                                            <input type="number" name="idadeFinal" id="idadeFinal"
                                                   class="form-control "
                                                   style="width:50%;float:left;" placeholder="De:"
                                                   value="<? echo $_SESSION["idadeFinal"] ?>"/>
                                            <input type="number" name="idadeInicial" id="idadeInicial"
                                                   class="form-control"
                                                   style="width:50%" placeholder="Até:"
                                                   value="<? echo $_SESSION["idadeInicial"] ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-success" name="btPesquisar"
                                                   value="Pesquisar"/>
                                            <a href="<? echo base_url() ?>bcController/limparFiltros"
                                               class="btn btn-white"
                                               name="btLimpar">Limpar Filtros</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <? if ($empresa[0]->moduloBancoCurriculo == "NAO") { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×
                                </button><? echo $msgPlano ?></div>
                        <? } ?>

                        <table class="table table-hover">
                            <? foreach ($profissionais as $row) { ?>
                                <tr>

                                    <td width="100px">
                                        <? if ($row->foto != "") { ?>
                                            <img class="img-circle" alt="<? echo $row->nome ?>" width="100px"
                                                 src="<? echo base_url() ?>upload/profissional/<? echo $row->foto ?>">
                                        <? } else { ?>
                                            <img class="img-circle" alt="<? echo $row->nome ?>" width="100px"
                                                 src="<? echo base_url() ?>img/icons/nopicture.png">
                                        <? } ?>
                                    </td>
                                    <td>
                                        <b style="text-transform:uppercase"><?= $row->nome ?></b><br>
                                        <small><?= $row->telefone ?> - <?= $row->email ?></small>
                                        <br>
                                        <small><?= $row->cidade ?>/<?= $row->estado ?></small>
                                        <br>
                                        <small><?= $row->ultCargo ?></small>
                                        <br>
                                        <small>Pretensão salarial de
                                            R$ <?= number_format($row->pretensao, 2, ',', '') ?></small>
                                        <br>
                                    </td>
                                    <td width="320px">
                                        <div class="btn btn-warning"
                                             onclick="alteraAtributo('<?= base_url() ?>empresaController/visualizarDadosProfissionalAction/<? echo $row->id ?>')"
                                             data-toggle="modal" data-target="#myModal6">
                                            Ver currículo
                                        </div>
                                        <div class="btn btn-primary"
                                             onclick="setHidden(<? echo $row->id ?>, 'hidprofissional')"
                                             data-toggle="modal" data-target="#myModalConvite">
                                            Convidar
                                        </div>
                                        <a class="btn btn-primary"
                                           href="<?= base_url() ?>pdfController/pdf/<? echo $row->id ?>"
                                           title="Imprimir Currículo"><i class="fa fa-print"></i> Imprimir</a>
                                    </td>
                                </tr>
                            <? } ?>
                            <tr>
                                <td><?= $paginacao; ?></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="myModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                </div>
                <div class="modal-body" style="width:100%; height:600px">
                    <iframe width="100%" height="100%" id="iframeCandidato" src=""></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="myModalConvite" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" onclick="limparResultado();" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>

                    <h4 class="modal-title">Selecione uma vaga</h4>

                    <p>E convide o profissional para participar do processo</p>
                </div>

                <form id="formMessage" name="formMessage" method="POST" action="">

                    <div class="modal-body">
                        <div id="resulAlert"></div>
                        <select id="idvagaconvite" name="idvagaconvite" class="form-control">
                            <option value="">Selecione</option>
                            <? foreach ($vagas as $vaga) { ?>
                                <option value="<?= $vaga->id ?>"><?= $vaga->vaga ?></option>
                            <? } ?>
                        </select>
                        <br>
                        <label>Mensagem</label>
                        <textarea id="mensagemConvite" name="mensagemConvite" class="form-control">Seu currículo foi avaliado pela empresa <?= $empresa[0]->razaosocial ?>
                            e queremos convidá-lo a participar do processo de seleção da vaga.</textarea>

                        <input type="hidden" id="hidprofissional" name="hidprofissional" value=""/>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-primary" onclick="convidarProfissional();" name="btSalvar"
                               value="Convidar"/>
                        <button type="button" onclick="limparResultado();" class="btn btn-white" data-dismiss="modal">
                            Fechar
                        </button>
                    </div>
                </form>


            </div>
        </div>
    </div>


<? $this->load->view('priv/empresa/_inc/inferior'); ?>