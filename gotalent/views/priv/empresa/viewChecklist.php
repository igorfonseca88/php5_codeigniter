<?
$this->load->view('priv/empresa/_inc/superiorModal');
?>



<div class="wrapper-content">
    <div class="row">
        <div class="col-lg-12">



            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5><? echo $checklist[0]->checklist ?> </h5> 
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                    </div>
                </div>

                <div class="ibox-content">
                <?if($habilitaAvaliacao){?>  
                <div class="row">   
                  
                 <div class="form-group">
                                
                    <label class="col-sm-2 control-label">Avaliação da entrevista:</label>
                    <div class="col-sm-4">
                        <input type="hidden" name="idprofvaga" id="idprofvaga" value="<?=$profissional_vaga[0]->id?>"/>
                        <select name="avaliacaoVideo" id="avaliacaoVideo" class="form-control">
                            <option value="">Selecione</option>
                            <option <?=$profissional_vaga[0]->avaliacaoVideo == "5" ? "selected" : ""?> value="5">5 - Ótimo</option>
                            <option <?=$profissional_vaga[0]->avaliacaoVideo == "4" ? "selected" : ""?> value="4">4 - Bom</option>
                            <option <?=$profissional_vaga[0]->avaliacaoVideo == "3" ? "selected" : ""?> value="3">3 - Regular</option>
                            <option <?=$profissional_vaga[0]->avaliacaoVideo == "2" ? "selected" : ""?> value="2">2 - Ruim</option>
                            <option <?=$profissional_vaga[0]->avaliacaoVideo == "1" ? "selected" : ""?> value="1">1 - Péssimo</option>
                        </select>
                        
                    </div>
                    <div class="col-sm-2">
                        <input type="button" class="btn btn-primary" name="btAvaliacao" onclick="gravaAvaliacaoVideo();" value="OK"/>
                    </div>
                    <div class="col-sm-2">
                        <p id="resulAlert"></p>
                    </div>
                </div>    
                </div>   
                    
                    <div class="hr-line-dashed"></div>   
                 <?}?>     
                    <form class="form-horizontal">
                        <? foreach ($respostas as $questao) { ?>     

                            <div class="form-group">
                                <label class="col-sm-10 control-label"><span style="float:left"><?= $questao->questao ?></span></label>
                                <?if($questao->respostaVideo != ""){?>
                                <a target='_blank' class="btn btn-primary" href='<?echo base_url()?>videoentrevista/review.php?arq="<?=$questao->respostaVideo?>"'>Ver resposta</a>
                                <?}else{?>
                                <label class="col-sm-12 control-label"><span style="float:left">Resposta: <?= $questao->respostaObjetiva ?><?= $questao->respostaDiscursiva ?></span></label>
                                <?}?>
                            </div>
                        <? } ?>   

                </div>

            </div>	

        </div>		

    </div>			
</div>



<?
$this->load->view('priv/empresa/_inc/inferiorModal');
?>
