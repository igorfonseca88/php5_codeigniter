<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h1>Cadastro de novo usuário</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li>
                <a href="<? echo base_url() ?>empresaController/listarUsuarios">Listagem de Usuários</a>
            </li>
            <li class="active">
                <strong>Novo usuário</strong>
            </li>
        </ol>
    </div>
</div>



<div class="wrapper wrapper-content animated fadeInUp">

    <div class="row">
        <div class="col-lg-12">

            <div class="ibox float-e-margins">	
                <div class="ibox-title">
                    <h5>Dados do Usuário</h5>
                </div>

                <div class="ibox-content">		
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>


                    <form method="post" action="<?= base_url() ?>empresaController/addUsuario" class="form-horizontal">


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome <span style="color:red">*</span></label>
                            <div class="col-lg-4">	
                                <input type="text" name="nome" id="nome" class="form-control" value="<?= $_POST["nome"] ?>" />
                            </div>
                           
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail (utilizado para o login) <span style="color:red">*</span></label>
                            <div class="col-lg-4">	
                                <input type="text" name="login" id="login" class="form-control" value="<?= $_POST["login"] ?>" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Senha <span style="color:red">*</span></label>
                            <div class="col-lg-4">	
                                <input type="password" name="senha" id="senha" class="form-control" value="<?= $_POST["senha"] ?>" />
                            </div>
                        </div>

                      


                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>empresaController/listarUsuarios'"  />
                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
                            </div>
                        </div>

                    </form>


                </div>
            </div>	

        </div>	
    </div>
</div>



<?
$this->load->view('priv/empresa/_inc/inferior');
?>
