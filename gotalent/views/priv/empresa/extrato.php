<?
$this->load->view('priv/empresa/_inc/superior');
?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-6">
		<h1>Extrato</h1>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>area-restrita-empresa">Home</a>
			</li>
			<li class="active">
				<strong>Extrato</strong>
			</li>
		</ol>
	</div>
</div>


<div id="wrapper wrapper-content animated fadeInRight">
	
	<div class="row">
			<div class="col-lg-12">

			<div class="ibox">
				<div class="ibox-title">
					<h5>Extrato</h5>
				   
				</div>
				
			 <div class="ibox-content">
			
			
			<table class="table table-hover">
				<thead>
					<th></th>
					<th>Data</th>
					<th>Vagas</th>
					<th>Vagas em destaque</th>
					<th>Vigência</th>
					<th>Tipo</th>
					
					
					
					
					
				</thead>
				<? foreach ($extratos as $row) { ?>
				 <tr>
					<td> 
					
					</td>
					<td> <?= implode("/",array_reverse(explode("-",$row->dataCriacao))) ?></td>
					<td> <?= $row->saldoAnuncio ?></td>
					<td> <?= $row->saldoDestaque ?></td>
					<td> <?= implode("/",array_reverse(explode("-",$row->vigencia))) ?></td>
					<td> <span class="label label-<? echo $row->tipo == 'Crédito' ? 'primary' : 'danger'?>"><?= $row->tipo ?></span></td>
					
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>
</div>
</div>

<?
$this->load->view('priv/empresa/_inc/inferior');
?>