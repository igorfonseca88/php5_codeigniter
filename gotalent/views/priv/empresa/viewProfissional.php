<?
$this->load->view('priv/empresa/_inc/superiorModal');
?>



<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-12">
		
		<div class="ibox">	
				<div class="ibox-title">
                      <h5>Currículo do Candidato: <? echo $profissional[0]->nome?></h5>
                </div>
				
		<div class="ibox-content">	
	
			<div class="row">
				<div class="form-group">
					<div class="col-sm-2">
						<? if($profissional[0]->foto!= ""){?>	
						<img class="img-circle m-t-xs img-responsive" src="<?= base_url() ?>upload/profissional/<?echo $profissional[0]->foto?>" id="foto-curriculo" alt="<? echo $profissional->nome?>" title="<? echo $profissional->nome?>"  />
						<?}else{?>
						<img class="img-circle m-t-xs img-responsive" src="<?= base_url() ?>img/icons/nopicture.png" alt="<? echo $profissional->nome?>" title="<? echo $profissional->nome?>" id="foto-curriculo"/>	
						<?}?>
					</div>
					<div class="col-sm-10">
							<?=$profissional[0]->resumo?>
					</div>
				</div>	
			</div>
		</div>	

		<div class="ibox float-e-margins">
		
		 <div class="ibox-title">
                            <h5>Dados Pessoais </h5> 
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                            </div>
                        </div>
		
		<div class="ibox-content">
			
			 
			 <form class="form-horizontal">
			    <div class="form-group">
					<label class="col-sm-2 control-label">Nome Completo</label>
					<div class="col-sm-2">	
						<p class="form-control-static"><?=$profissional[0]->nome?></p>
					</div>
					<label class="col-sm-2 control-label">E-mail</label>
					<div class="col-sm-2">	
						<p class="form-control-static"><?=$profissional[0]->email?></p>
					</div>
					<label class="col-sm-2 control-label">Telefone</label>
					<div class="col-sm-2">
						<p class="form-control-static"><?= $profissional[0]->telefone?> </p>
					</div>
					
					
				</div>
					
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Nascimento</label>
					<div class="col-sm-2">	
						<p class="form-control-static"><?=implode("/",array_reverse(explode("-",$profissional[0]->nascimento)))?></p>
					</div>
					
					<label class="col-sm-2 control-label">Sexo</label>
					<div class="col-sm-2">
						<p class="form-control-static"><?= $profissional[0]->sexo?> </p>
					
					</div>
				</div>	 
					
				<div class="form-group">	
					<label class="col-sm-2 control-label">Estado</label>
					<div class="col-sm-2">
							<p class="form-control-static"><?= $profissional[0]->uf?> </p>
					</div>
					
					<label class="col-sm-2 control-label">Cidade</label>
					<div class="col-sm-2">
							<p class="form-control-static"><?=$profissional[0]->cid?> </p>
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Facebook</label>
					<div class="col-sm-4">
						<p class="form-control-static"><?=$profissional[0]->facebook?></p>
					</div>
					
					<label class="col-sm-2 control-label">Linkedin</label>
					<div class="col-sm-4">
						<p class="form-control-static"><?=$profissional[0]->linkedin?></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Repositório (GIT, Portfólio)</label>
					<div class="col-sm-4">
						<p class="form-control-static"><?=$profissional[0]->repositorio?></p>
					</div>
					
					<label class="col-sm-2 control-label">Pretensão salarial</label>
					<div class="col-sm-4">
						<p class="form-control-static"><?=number_format($profissional[0]->pretensao, 2, ',', '');?></p>
					</div>
				</div>	
		
					
			
		</form>
		</div>
	</div>			
					
	<div class="ibox float-e-margins">
		
		 <div class="ibox-title">
                            <h5>Competências  </h5> 
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                            </div>
                        </div>
		
		<div class="ibox-content">
				
				<div class="row">
					<div class="col-lg-12">
					<ul style="list-style: none;padding-left: 0;margin: 0;">
					<?foreach($profissional_skills as $ps){ ?>
						<li style="border: 1px solid #59b2e5;background: #FFF;padding: 5px;float: left;
						margin-right: 5px;margin-bottom: 5px;color: #59b2e5;"><? echo $ps->skill?></li>
						<?}?>	
					</ul>
					</div>
				</div>
				
			 </div>
	</div>		 
			
		<div class="ibox float-e-margins">
		
		 <div class="ibox-title">
                            <h5>Experiências profissionais  </h5> 
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                            </div>
                        </div>
		
		<div class="ibox-content">
            
				<table class="table table-hover" >
				<thead>
					<th>Empresa</th>
					<th>Cargo</th>
					<th>Data de início</th>
					<th>Data fim</th>
					<th>Emprego atual</th>
					<th>Resumo das atividades</th>
					
					
				</thead>
				<? foreach ($experiencias as $experiencia) { ?>
				 <tr>
					<td> <?= $experiencia->empresa ?></td>
					<td> <?= $experiencia->cargo ?></td>
					<td> <?=implode("/",array_reverse(explode("-",$experiencia->dataInicio)))?> </td>
					<td> <?=implode("/",array_reverse(explode("-",$experiencia->dataFim))) == "00/00/0000" ? " - " : implode("/",array_reverse(explode("-",$experiencia->dataFim))) ?></td>
					<td> <?= $experiencia->empregoAtual == 1 ? "Sim" : "Não" ?></td>
					<td> <?= $experiencia->descricaoAtividade ?></td>
					
				 </tr>
			  <? } ?>
		   </table>
		</div>
	</div>	

	<div class="ibox float-e-margins">
		
		 <div class="ibox-title">
                            <h5>Formação educacional  </h5> 
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                            </div>
                        </div>
		
		<div class="ibox-content">		
			<table class="table table-hover" >
				<thead>
					<th>Instituição</th>
					<th>Formação</th>
					<th>Data de início</th>
					<th>Data fim</th>
					<th>Curso</th>
					
					
					
				</thead>
				<? foreach ($formacoes as $formacao) { ?>
				 <tr>
					<td> <?= $formacao->instituicao ?></td>
					<td> <?= $formacao->formacao ?></td>
					<td> <?=implode("/",array_reverse(explode("-",$formacao->dataInicio)))?> </td>
					<td> <?=implode("/",array_reverse(explode("-",$formacao->dataFim))) == "00/00/0000" ? " - " : implode("/",array_reverse(explode("-",$formacao->dataFim))) ?></td>
					
					<td> <?= $formacao->curso ?></td>
					
				 </tr>
			  <? } ?>
		   </table>				
		   </div>
					
				
		</div>
		
    </div>	
		
</div>		
		
</div>			
</div>



<?
$this->load->view('priv/empresa/_inc/inferiorModal');
?>
