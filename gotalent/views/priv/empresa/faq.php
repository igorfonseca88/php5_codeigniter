<?
$this->load->view('priv/empresa/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h1>FAQ</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-empresa">Home</a>
            </li>
            <li class="active">
                <strong>FAQ</strong>
            </li>
        </ol>
    </div>
</div>


<div class="wrapper-content">

<div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content animated fadeInRight">

                       

                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq1" class="faq-question">Como criar e publicar uma nova vaga ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 07.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Vídeo</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq1" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/siTetq7kKks?rel=0" frameborder="0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq4" class="faq-question">Como encontrar profissionais por Competência no banco de currículos ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Dicas</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq4" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                                A Competência é um atributo que o profissional adiciona em seu currículo ao se cadastrar em nosso portal. Portanto quando estiver buscando um profissional com nível de eficiência Avançado em PHP por exemplo, basta indicar no campo Competência a palavra PHP, selecionar a opção sugerida Linguagem PHP Avançado, selecionar os demais filtros caso necessário e clicar em Pesquisar. O sistema irá buscar no banco todos os profissionais com este perfil. <br>
                                                
                                                <br> Observamos também que é possível filtrar sem a necessidade de selecionar uma Competência sugerida pelo sistema, basta informar a competência e pesquisar.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq5" class="faq-question">Como encontrar profissionais por Cargo no banco de currículos ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Dicas</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq5" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                               O Cargo é um atributo que o profissional adiciona em seu currículo ao cadastrar suas experiências profissionais. Portanto quando estiver buscando um profissional que ocupa ou ocupou o cargo de Analista de Sistemas por exemplo, basta indicar no campo Cargo a palavra "Analista", selecionar a opção sugerida Analista de Sistemas, selecionar os demais filtros caso necessário e clicar em Pesquisar. O sistema irá buscar no banco todos os profissionais que ocupam ou já ocuparam este cargo. <br>
                                                
                                                <br> Observamos também que é possível filtrar sem a necessidade de selecionar um Cargo sugerido pelo sistema, basta informar o cargo e pesquisar.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq6" class="faq-question">Como criar checklists com perguntas a serem respondidas no ato da candidatura do profissional ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Vídeo</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq6" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                              <iframe width="560" height="315" src="https://www.youtube.com/embed/l5KkGbtc62g?rel=0" frameborder="0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq7" class="faq-question">Como criar e assistir vídeo entrevistas?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Vídeo</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq7" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                              <iframe width="560" height="315" src="https://www.youtube.com/embed/rzVsJJxfva8?rel=0" frameborder="0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
           

        </div>		

    </div>			
</div>



<?
$this->load->view('priv/empresa/_inc/inferior');
?>
