<? $this->load->view('priv/empresa/_inc/superior'); ?>

<div class="wrapper wrapper-content">  

    <? if ($this->session->flashdata('sucesso') != "") { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><? echo $this->session->flashdata('sucesso') ?> </div>
            </div>
        </div>
    <? } ?>


    <!--Dashboard-->    
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">                                
                    <h5>Seu Plano: <?= $totais[0]->plano?></h5>
                </div>
                <div class="ibox-content">
                    <?if($totais[0]->dataFinalPlano != null){?>
                    <h3 class="no-margins">Vigente até <?= implode("/", array_reverse(explode("-", $totais[0]->dataFinalPlano))) ?></h3> <br>                       
                    <small><?=$totais[0]->demonstracao == 1 ? "Período de demonstração" : "<br>"?></small>    
                    <?}else{?>
                    <h3 class="no-margins">Renove seu plano</h3> <br>                       
                    <small> E tenha acesso a todas funcionalidades!</small>    
                    <?}?>
                    
                </div>
            </div>
        </div>
     <!--   <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Brasil</span>
                    <h5>Profissionais</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><? //echo $totais[0]->total * 9 ?></h1>                        
                    <small>Registrados</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right"><? //echo $empresa[0]->uf ?></span>
                    <h5>Profissionais</h5>
                </div>
                <div class="ibox-content">
                    <? //if ($empresa[0]->estado == '12') { ?>
                        <h1 class="no-margins"><? //echo $totais[0]->estado * 5 ?></h1>
                    <? //} else { ?>
                        <h1 class="no-margins"><? //echo $totais[0]->estado * 33 ?></h1>
                    <? //} ?>	
                    <small>Registrados</small>
                </div>
            </div>
        </div> -->
        <!-- <div class="col-lg-3">
             <div class="ibox float-e-margins">
                 <div class="ibox-title">
                     <span class="label label-danger pull-right"><? //echo $empresa[0]->cidade  ?></span>
                     <h5>Profissionais</h5>
                 </div>
                 <div class="ibox-content">
                     <h1 class="no-margins"><? //echo $totais[0]->cidade  ?></h1>
                     <small>Registrados</small>
                 </div>
             </div>
         </div> -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1>Passo a Passo</h1>
        </div>
        <div class="col-lg-12">
            <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq1" class="faq-question">Como criar e publicar uma nova vaga ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 07.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Vídeo</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq1" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/siTetq7kKks?rel=0" frameborder="0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq4" class="faq-question">Como encontrar profissionais por Competência no banco de currículos ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Dicas</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq4" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                                A Competência é um atributo que o profissional adiciona em seu currículo ao se cadastrar em nosso portal. Portanto quando estiver buscando um profissional com nível de eficiência Avançado em PHP por exemplo, basta indicar no campo Competência a palavra PHP, selecionar a opção sugerida Linguagem PHP Avançado, selecionar os demais filtros caso necessário e clicar em Pesquisar. O sistema irá buscar no banco todos os profissionais com este perfil. <br>
                                                
                                                <br> Observamos também que é possível filtrar sem a necessidade de selecionar uma Competência sugerida pelo sistema, basta informar a competência e pesquisar.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq5" class="faq-question">Como encontrar profissionais por Cargo no banco de currículos ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Dicas</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq5" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                               O Cargo é um atributo que o profissional adiciona em seu currículo ao cadastrar suas experiências profissionais. Portanto quando estiver buscando um profissional que ocupa ou ocupou o cargo de Analista de Sistemas por exemplo, basta indicar no campo Cargo a palavra "Analista", selecionar a opção sugerida Analista de Sistemas, selecionar os demais filtros caso necessário e clicar em Pesquisar. O sistema irá buscar no banco todos os profissionais que ocupam ou já ocuparam este cargo. <br>
                                                
                                                <br> Observamos também que é possível filtrar sem a necessidade de selecionar um Cargo sugerido pelo sistema, basta informar o cargo e pesquisar.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq6" class="faq-question">Como criar checklists com perguntas a serem respondidas no ato da candidatura do profissional ?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Vídeo</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq6" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                              <iframe width="560" height="315" src="https://www.youtube.com/embed/l5KkGbtc62g?rel=0" frameborder="0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="faq-item">
                            <div class="row">
                                <div class="col-md-7">
                                    <a data-toggle="collapse" href="#faq7" class="faq-question">Como criar, assistir e avaliar vídeo entrevistas?</a>
                                    <small><i class="fa fa-clock-o"></i> Criado em 08.04.2015</small>
                                </div>
                                <div class="col-md-3">
                                    <span class="small font-bold">Categoria</span>
                                    <div class="tag-list">
                                        <span class="tag-item">Ferramenta</span>
                                        <span class="tag-item">Vídeo</span>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="faq7" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                              <iframe width="560" height="315" src="https://www.youtube.com/embed/rzVsJJxfva8?rel=0" frameborder="0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
    
    <? //if ($totais[0]->saldo <= 0) {?>
    <!--<div class="row">
        <div class="col-lg-12" style="text-align: center">				
            <a href="<?//=  base_url()?>empresaController/plano"><img width="100%" src="<?//= base_url()?>img/banner2.png"/></a>
        </div> 
    </div> -->                   
    <? //} ?>
    
    

</div>

<? $this->load->view('priv/empresa/_inc/inferior'); ?>
