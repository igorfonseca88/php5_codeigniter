<?
$this->load->view('priv/administrador/_inc/superior');
?>



<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Profissionais/Skills
				
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestritaadmin">Principal</a> &raquo; Profissionais Skills </div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<th>Nome</th>
					<th>E-mail</th>
					<th>Telefone</th>
					<th>Cidade</th>
					<th>Estado</th>
					<th>Skill</th>
					<th>Pretensão</th>
					
					
					
				</thead>
				<? foreach ($profissional_skills as $row) { ?>
				 <tr>
					<td> <?= $row->nome ?></td>
					<td> <?= $row->email ?></td>
					<td> <?= $row->telefone ?></td>
					<td> <?= $row->cidade ?></td>
					<td> <?= $row->estado ?></td>
					<td> <?= $row->skill ?></td>
					<td> <?= $row->pretensao ?></td>
					
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>

<?
$this->load->view('priv/administrador/_inc/inferior');
?>