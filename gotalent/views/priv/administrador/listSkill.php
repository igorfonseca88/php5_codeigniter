<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-sm-4">
	<h2>Listagem de Skills</h2>
	<ol class="breadcrumb">
		<li>
			<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
		</li>
		<li class="active">
			<strong>Listagem de Skills</strong>
		</li>
	</ol>
</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
<div class="col-lg-12">

	<div class="ibox">
		<div class="ibox-title">
			<h5>Skills</h5>
			<div class="ibox-tools">
				<a href="<?= base_url() ?>administradorController/novaSkillAction" class="btn btn-primary btn-xs">Nova Skill</a>
			</div>
		</div>
		<div class="ibox-content">
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<table class="table table-striped table-bordered table-hover" id="dataTables-example" >
				<thead>
					<th>Categoria</th>
					<th>Skill</th>
					
					
					
				</thead>
				<? foreach ($skills as $row) { ?>
				 <tr>
					<td> <?= $row->categoria ?></td>
					
					<td> <?= $row->skill ?></td>
					
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>
</div>
</div>

<?
$this->load->view('priv/administrador/_inc/inferior');
?>