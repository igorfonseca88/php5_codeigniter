<?
$this->load->view('priv/administrador/_inc/superior');
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Listagem de Vagas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita-admin">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Vagas</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Vagas</h5>
                    <div class="ibox-tools">
                        <a href="<?= base_url() ?>profissionalController/imprimirProfissionaisAction" class="btn btn-primary btn-xs">Imprimir</a>
                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>

                        <th>Vaga</th>
                        <th>Data do cadastro</th>
                        <th>Empresa</th>
                        <th>Situação</th>
                        <th>Total Candidatos</th>

                        <th width="120" align="center">Ações</th>
                        </thead>
                        <? foreach ($vagas as $row) { ?>
                            <tr>
                                <td> <?= $row->vaga ?></td>
                                <td> <?= implode("/", array_reverse(explode("-", $row->dataCadastro))) ?></td>
                                <td> <?= $row->razaosocial ?></td>
                                <td> <?= $row->situacao ?></td>
                                <td> <?= $row->total ?></td>
                                <td align="center">
                                    <a href="<?= base_url() ?>administradorController/editarVagaAction/<?= $row->id ?>">Editar</a> 
                                </td>
                            </tr>
                        <? } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?
$this->load->view('priv/administrador/_inc/inferior');
?>