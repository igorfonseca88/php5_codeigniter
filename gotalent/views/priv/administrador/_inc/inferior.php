<!-- Mainly scripts -->
<script src="<?echo base_url()?>js/jquery-2.1.1.js"></script>
<script src="<?echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?echo base_url()?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?echo base_url()?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Data Tables -->
<script src="<?echo base_url()?>js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?echo base_url()?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<?echo base_url()?>js/plugins/dataTables/dataTables.responsive.js"></script>


<!-- Custom and plugin javascript -->
<script src="<?echo base_url()?>js/inspinia.js"></script>
<script src="<?echo base_url()?>js/plugins/pace/pace.min.js"></script>

<!-- CKEDITOR -->
<script type="text/javascript" src="<?=base_url()?>system/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=base_url()?>system/ckeditor/config.js"></script>


<!-- jQuery UI -->
<script src="<?echo base_url()?>js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?echo base_url()?>js/plugins/iCheck/icheck.min.js"></script>
<script type='text/javascript' src="<?=base_url()?>js/jquery.autocomplete.js"></script>
<script src="<?echo base_url()?>js/jquery.maskMoney.js"></script>
<script src="<?echo base_url()?>js/jquery.maskedinput.min.js"></script>
<script src="<?echo base_url()?>js/funcoes.js"></script>


    
<div class="modal inmodal fade" id="modalIndicacao" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
               
                <h4 class="modal-title">Indicação</h4>
                <small class="font-bold">Informe as considerações da indicação do profissional. Será mostrado para a empresa.</small>                  
            </div>
            <form id="formIndicacaoAction" name="formIndicacaoAction" action="" method="POST">
                <div class="modal-body">
                    <div class="form-group"><label>Motivo</label> 
                        <textarea class="form-control" name="indicacao" id="indicacao" placeholder="Informe o motivo"></textarea>                    
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
            </form>
        </div>
    </div>
</div>
</div> 

<div class="footer">
            <div class="pull-right">
                Suporte/Dúvidas/Sugestões: <strong>contato@gotalent.com.br</strong> 
            </div>
            <div>
                <strong>Copyright</strong> Go Talent &copy; 2014-2015
            </div>
            
        </div>
        </div>
    </div>
</body>
</html>