<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Envio de E-mail</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>principal/arearestritaadmin">Home</a>
            </li>
            <li class="active">
                <strong>Novo envio de e-mail</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Dados do E-mail</h5>
                </div>

                <div class="ibox-content">	


                    <form method="post" action="<?= base_url() ?>administradorController/enviarEmail" class="form-horizontal">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Lista de e-mails (separado por ponto e vírgula) </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="lista" id="lista"/>
                            </div>
                            <div class="col-sm-4">
                                <input type="button"  value="Buscar Profissionais"/>
                            </div>
                        </div>
                        <div class="form-group">				
                            <label class="col-sm-2 control-label">Assunto </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="assunto" id="assunto"/>
                            </div>
                        </div>
                        <div class="form-group">				
                            <label class="col-sm-2 control-label">Texto </label>
                            <div class="col-sm-10">
                                <textarea class="form-control ckeditor"  name="texto" id="texto"></textarea>
                            </div>
                        </div>		


                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>administradorController/listarSkills'"  />

                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
                            </div>		
                        </div>

                    </form>
                </div>
            </div>	
        </div>
    </div>	
</div>



<?
$this->load->view('priv/administrador/_inc/inferior');
?>
