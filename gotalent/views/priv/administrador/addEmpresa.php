<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-sm-8">
	<h2>Novo cadastro de Empresa</h2>
	<ol class="breadcrumb">
		<li>
			<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
		</li>
		<li>
			<a href="<?echo base_url()?>administradorController/listarEmpresas">Listagem de Empresas</a>
		</li>
		<li class="active">
			<strong>Novo cadastro de Empresa </strong>
		</li>
	</ol>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
		<div class="col-lg-12">
		
		<div class="ibox float-e-margins">	
				<div class="ibox-title">
                      <h5>Dados da Empresa</h5>
                </div>
				
			<div class="ibox-content">	
			<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
			<?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>
			
			
			<form method="post" action="<?= base_url() ?>administradorController/editEmpresa" class="form-horizontal">
				
						<div class="form-group">
							<label class="col-sm-2 control-label">Data do Cadastro </label>
							<div class="col-sm-4">
								<p class="form-control-static"><?=implode("/",array_reverse(explode("-",date('Y-m-d'))))?> </p>
							</div>
						
							
							<label class="col-sm-2 control-label">Razão Social</label>
							<div class="col-sm-4">
								<input type="text" name="razaosocial" id="razaosocial" class="form-control" value="" />
							</div>
							
						</div>
					 
						<div class="hr-line-dashed"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">E-mail</label>
							<div class="col-sm-4">
								<input type="text" name="email" id="email" class="form-control" value="" />
							</div>
							
							<label class="col-sm-2 control-label">Telefone</label>
							<div class="col-sm-4">
								<input type="text" name="telefone" id="telefone" class="telefone form-control"  value="" />
							</div>
							
						</div>
					
					
					<div class="hr-line-dashed"></div>
					<div class="form-group">
					
						<label class="col-sm-2 control-label">CEP</label>
						<div class="col-sm-4">
							<input type="text" name="cep" id="cep" class="cep form-control" value="" />
						</div>
					</div>
					
					<div class="form-group">
					
						<label class="col-sm-2 control-label">Endereço</label>
						<div class="col-sm-6">
							<input type="text" name="endereco" id="endereco" class="form-control"  value="" />
						</div>
						
						<label class="col-sm-2 control-label">Número</label>
						<div class="col-sm-2">
							<input type="text" name="numero" id="numero" class="form-control"  value="" />
						</div>	
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-2 control-label">Bairro</label>
						<div class="col-sm-4">
							<input type="text" name="bairro" id="bairro" class="form-control"  value="" />
						</div>
					</div>
					
					<div class="form-group">
					
						<label class="col-sm-2 control-label">Estado</label>
						<div class="col-sm-4">
							<select name="estado" id="estado" class="form-control">
								<option <?= $row->estado == "AC" ? "selected" : "" ?> value="AC">Acre</option>
								<option <?= $row->estado == "AL" ? "selected" : "" ?> value="AL">Alagoas</option>
								<option <?= $row->estado == "AM" ? "selected" : "" ?> value="AM">Amazonas</option>
								<option <?= $row->estado == "AP" ? "selected" : "" ?> value="AP">Amapá</option>
								<option <?= $row->estado == "BA" ? "selected" : "" ?> value="BA">Bahia</option>
								<option <?= $row->estado == "CE" ? "selected" : "" ?> value="CE">Ceará</option>
								<option <?= $row->estado == "DF" ? "selected" : "" ?> value="DF">Distrito Federal</option>
								<option <?= $row->estado == "ES" ? "selected" : "" ?> value="ES">Espirito Santo</option>
								<option <?= $row->estado == "GO" ? "selected" : "" ?> value="GO">Goiás</option>
								<option <?= $row->estado == "MA" ? "selected" : "" ?> value="MA">Maranhão</option>
								<option <?= $row->estado == "MG" ? "selected" : "" ?> value="MG">Minas Gerais</option>
								<option <?= $row->estado == "MS" ? "selected" : "" ?> value="MS">Mato Grosso do Sul</option>
								<option <?= $row->estado == "MT" ? "selected" : "" ?> value="MT">Mato Grosso</option>
								<option <?= $row->estado == "PA" ? "selected" : "" ?> value="PA">Pará</option>
								<option <?= $row->estado == "PB" ? "selected" : "" ?> value="PB">Paraíba</option>
								<option <?= $row->estado == "PE" ? "selected" : "" ?> value="PE">Pernambuco</option>
								<option <?= $row->estado == "PI" ? "selected" : "" ?> value="PI">Piauí</option>
								<option <?= $row->estado == "PR" ? "selected" : "" ?> value="PR">Paraná</option>
								<option <?= $row->estado == "RJ" ? "selected" : "" ?> value="RJ">Rio de Janeiro</option>
								<option <?= $row->estado == "RN" ? "selected" : "" ?> value="RN">Rio Grande do Norte</option>
								<option <?= $row->estado == "RO" ? "selected" : "" ?> value="RO">Rondônia</option>
								<option <?= $row->estado == "RR" ? "selected" : "" ?> value="RR">Roraima</option>
								<option <?= $row->estado == "RS" ? "selected" : "" ?> value="RS">Rio Grande do Sul</option>
								<option <?= $row->estado == "SC" ? "selected" : "" ?> value="SC">Santa Catarina</option>
								<option <?= $row->estado == "SE" ? "selected" : "" ?> value="SE">Sergipe</option>
								<option <?= $row->estado == "SP" ? "selected" : "" ?> value="SP">São Paulo</option>
								<option <?= $row->estado == "TO" ? "selected" : "" ?> value="TO">Tocantins</option>
							</select>
						</div>
					
						<label class="col-sm-2 control-label">Cidade</label>
						<div class="col-sm-4">	
							<input type="text" name="cidade" id="cidade" class="form-control" value="" />
						</div>
					</div>
					
					<div class="hr-line-dashed"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">Facebook</label>
						<div class="col-sm-4">	
							<input type="text" name="facebook" id="facebook" class="form-control"  value="" />
						</div>
						
						<label class="col-sm-2 control-label">Contato</label>
						<div class="col-sm-4">	
							<input type="text" name="contato" id="contato" class="form-control"  value="" />
						</div>
					</div>
					
					<div class="hr-line-dashed"></div>
					
					<div class="form-group">
							<label class="col-sm-2 control-label">Sobre a Empresa</label>
						<div class="col-sm-10">		
							<textarea name="sobre" id="sobre" class="form-control"></textarea>
						</div>
					</div>
					
					<div class="hr-line-dashed"></div>
					<div class="form-group">
					<div class="col-sm-4 col-sm-offset-2">
							<input type="button" value="Voltar" class="btn btn-white" onclick="location.href='<?= base_url() ?>administradorController/listarEmpresas'"  />
							
							<input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
					</div>		
					</div>
				
				
			</form>
			
		
		</div>
</div>
</div>
</div>
</div>



<?
$this->load->view('priv/administrador/_inc/inferior');
?>


