<?
$this->load->view('priv/administrador/_inc/superior');
?>



<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h1>Currículo </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>arearestritaadmin">Home</a>
            </li>

            <li>
                <a href="<? echo base_url() ?>administradorController/listarProfissionais">Listagem de Profissionais</a>
            </li>

            <li class="active">
                <strong>Currículo</strong>
            </li>
        </ol>
    </div>
</div>  

<div class="wrapper wrapper-content">

    <div class="row">
        <div class="col-lg-12">

            <div class="ibox">	
                <div class="ibox-title">
                    <div class="col-lg-3">
                        <h5>Dados pessoais </h5>
                    </div> 

                </div>

                <div class="ibox-content">	
                    <div id="alerta">
                    </div>	
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>

                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>




                    <? foreach ($profissional as $row) { ?>

                        <div class="row">

                            <form id="upload" name="upload" action="<?= base_url() ?>profissionalController/uploadArquivo/<? echo $profissional[0]->id ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="tipoAnexo" name="tipoAnexo" value="Foto"/>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <? if ($row->foto != "") { ?>	
                                            <img class="" src="<?= base_url() ?>upload/profissional/<? echo $row->foto ?>" id="foto-curriculo" title="<? echo $row->nome ?>" width="100%" />
                                        <? } else { ?>
                                            <img class="" src="<?= base_url() ?>img/icons/nopicture.png" id="foto-curriculo" title="<? echo $row->nome ?>" width="100%" />
                                        <? } ?>	
                                    </div>
                                </div>


                                <div class="col-lg-2">

                                    <a href="<? echo base_url() ?>pdfController/gerarPDF/<? echo $row->id ?>" title="Exportar Currículo" 
                                       onclick=""><i class="fa fa-file-pdf-o"></i></a>

                                    <? if ($row->video != "") { ?>
                                        <a title="Vídeo Entrevista" data-toggle="modal" data-target="#myModalVideo"
                                           onclick=""><i class="fa fa-file-video-o"></i></a>
                                       <? } ?>
                                </div>
                            </form>	
                        </div>	



                        <form method="post" id="formDadosPessoais" action="<?= base_url() ?>profissionalController/editProfissional" >
                            <input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Nome Completo <span style="color:red">*</span></label><br>
                                        <input type="text" name="nome" id="nome" class="form-control" value="<?= $row->nome ?>" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>E-mail <span style="color:red">*</span></label><br>
                                        <input type="text" name="email" readonly="true" id="email" class="form-control" value="<?= $row->email ?>" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group" id="data_1">
                                        <label>Nascimento</label><br>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nascimento" id="nascimento" value="<?= implode("/", array_reverse(explode("-", $row->nascimento))) ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Sexo <span style="color:red">*</span></label><br>
                                        <select name="sexo" id="sexo" class="form-control">
                                            <option value="">Selecione</option>
                                            <option <?= $row->sexo == "Feminino" ? "selected" : "" ?> value="Feminino">Feminino</option>
                                            <option <?= $row->sexo == "Masculino" ? "selected" : "" ?> value="Masculino">Masculino</option>
                                        </select>
                                    </div>
                                </div>	 
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Telefone <span style="color:red">*</span></label><br>
                                        <input type="text" name="telefone" id="telefone" class="telefone form-control"  value="<?= $row->telefone ?>" />
                                    </div>
                                </div>



                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Estado <span style="color:red">*</span></label><br>
                                        <select name="estado" id="estado" class="form-control">
                                            <option value="">Selecione</option>
                                            <option value="">Selecione</option>    
                                            <? foreach ($estados as $estado) { ?>
                                                <option <?= $row->estado == $estado->id ? "selected" : "" ?> value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                            <? } ?> 

                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade <span style="color:red">*</span></label><br>
                                        <select name="cidade" id="cidade" class="form-control">
                                            <option value="">Selecione</option>    
                                            <? foreach ($cidades as $cidade) { ?>
                                                <option <?= $row->cidade == $cidade->id ? "selected" : "" ?> value="<?= $cidade->id ?>"><?= $cidade->nome ?></option>
                                            <? } ?> 
                                        </select>  
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Facebook</label><br>
                                        <input type="text" name="facebook" id="facebook" class="form-control"  value="<?= $row->facebook ?>" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Linkedin</label><br>
                                        <input type="text" name="linkedin" id="linkedin" class="form-control"  value="<?= $row->linkedin ?>" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Repositório (GIT, Portfólio)</label><br>
                                        <input type="text" name="repositorio" id="repositorio" class="form-control"  value="<?= $row->repositorio ?>" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Pretensão salarial <span style="color:red">*</span></label><br>
                                        <input type="text" name="pretensao" id="pretensao" class="form-control money"  value="<?= number_format($row->pretensao, 2, ',', ''); ?>" />
                                    </div>
                                </div> 

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Resumo<span style="color:red">*</span><small class="text-navy">(Preencha um breve resumo sobre você e suas experiências. O mesmo será demonstrado para as empresas)</small></label><br>
                                        <textarea class="form-control" name="resumo" id="resumo" rows="4" class="form-control"><?= $row->resumo ?></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12 no_print">
                                    <div class="form-group">
                                        <input type="button" value="Voltar" class="btn btn-default" onclick="location.href = '<?= base_url() ?>administradorController/listarProfissionais'"  />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Formação educacional  </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>

                        <div class="ibox-content">	


                            <table class="table table-hover">
                                <thead>
                                <th>Instituição</th>
                                <th>Formação</th>
                                <th>Data de início</th>
                                <th>Data fim</th>
                                <th>Curso</th>



                                </thead>
                                <? foreach ($formacoes as $formacao) { ?>
                                    <tr>
                                        <td> <?= $formacao->instituicao ?></td>
                                        <td> <?= $formacao->formacao ?></td>
                                        <td> <?= implode("/", array_reverse(explode("-", $formacao->dataInicio))) ?> </td>
                                        <td> <?= implode("/", array_reverse(explode("-", $formacao->dataFim))) == "00/00/0000" ? " - " : implode("/", array_reverse(explode("-", $formacao->dataFim))) ?></td>

                                        <td> <?= $formacao->curso ?></td>

                                    </tr>
                                <? } ?>
                            </table>
                        </div>	
                    </div>

                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Experiências profissionais  </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>

                        <div class="ibox-content">	


                            <table class="table table-hover" >
                                <thead>
                                <th>Empresa</th>
                                <th>Cargo</th>
                                <th>Data de início</th>
                                <th>Data fim</th>
                                <th>Emprego atual</th>
                                <th>Resumo das atividades</th>


                                </thead>
                                <? foreach ($experiencias as $experiencia) { ?>
                                    <tr>
                                        <td> <?= $experiencia->empresa ?></td>
                                        <td> <?= $experiencia->cargo ?></td>
                                        <td> <?= implode("/", array_reverse(explode("-", $experiencia->dataInicio))) ?> </td>
                                        <td> <?= implode("/", array_reverse(explode("-", $experiencia->dataFim))) == "00/00/0000" ? " - " : implode("/", array_reverse(explode("-", $experiencia->dataFim))) ?></td>
                                        <td> <?= $experiencia->empregoAtual == 1 ? "Sim" : "Não" ?></td>
                                        <td> <?= $experiencia->descricaoAtividade ?></td>

                                    </tr>
                                <? } ?>
                            </table>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>SKILLS  </h5> <small class="text-navy"> (A skill será utilizada para calcular a porcentagem de compabilidade com as vagas)</small>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>

                        <div class="ibox-content">		

                            <div class="row no_print">
                                <form id="formSkillAuto" name="formSillAuto" method="POST" action="<? echo base_url() ?>profissionalController/addSkillProfissional">


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="hidden" name="idProfissionalh" id="idProfissionalh" value = "<? echo $row->id ?>"/>
                                            <!--<input type="text" name="skill_auto" class="form-control" placeHolder="Ex: PHP, MYSQL, JAVA, Pró-atividade, Boa Comunicação, Trello" id="skill_auto" value=""/> -->
                                            <select class="form-control" style="height:200px" name="lista[]" multiple="">
                                                <? foreach ($skills as $sk) { ?>	
                                                    <option value="<? echo $sk->id ?>"><? echo $sk->skill ?></option>
                                                <? } ?>  
                                            </select>
                                        </div>
                                    </div>

                                </form>	
                            </div>	

                            <div class="row">
                                <div class="col-lg-12">
                                    <ul style="list-style: none;padding-left: 0;margin: 0;">
                                        <? foreach ($profissional_skills as $ps) { ?>
                                            <li style="border: 1px solid #59b2e5;background: #FFF;padding: 5px;float: left;
                                                margin-right: 5px;margin-bottom: 5px;color: #59b2e5;"><? echo $ps->skill ?></li>
                                            <? } ?>	
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>	


                <!--<div class="row no_print">		
                        <h2>Currículo</h2><br />
                        
                                        
                                        
                                        <form id="upload" action="<? //= base_url()   ?>profissionalController/uploadArquivo/<? //= $row->id   ?>" method="post" enctype="multipart/form-data">
                                                <div class="row">	
                                                
                                                <div class="col-lg-4">
                                                        <div class="form-group">
                                                                <label>Tipo:</label><br>
                                                                <select name="tipoAnexo" id="tipoAnexo" class="form-control">
                                                                        <option  value="Curriculo">Currículo</option>
                                                                        
                                                                </select>
                                                        </div>
                                                </div>
                                                
                                                <div class="col-lg-6">
                                                        <div class="form-group">
                                                                <label>Arquivo: </label> 
                                                                <input type="file" class="input" class="form-control" name="userfile" id="userfile" />
                                                                
                                                        </div>
                                                 </div>		

                                                 <div class="col-lg-2">
                                                        <div class="form-group">
                                                                
                                                                <input  type="submit" class="btn btn-success" name="enviar" value="Enviar" />
                                                        </div>
                                                 </div>	
                                                 
                                         </div>	 
                                        <div class="row">	
                <? //if ($row->curriculo != null) { ?>
                                                        <div class="col-lg-2">
                                                        <div class="form-group">
                                                                        <img src="<? //= base_url()   ?>upload/profissional/img_curriculo.png" height="150px"/><br>
                                                                        <a href='<? //= base_url()   ?>upload/profissional/<? //= $row->curriculo;   ?>' target="_blank">Download</a>
                                                                        
                                                        </div>
                                                        </div>	
                                                                
                <? // }
                ?>
                                                
                                                
                                        </div>
                                                 
                                        </form>
                        </div> -->



            <? } ?>

        </div>
    </div>	

</div>
<div class="modal inmodal fade" id="myModalVideo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-file-video-o modal-icon"></i>
                <h4 class="modal-title">Vídeo Entrevista</h4>


            </div>

            <div class="modal-body">


                <iframe width="425" height="349" src="https://www.youtube.com/embed/wFj5tCBAxNA" frameborder="0"></iframe>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>

            </div>

        </div>
    </div>
</div>		

<?
$this->load->view('priv/administrador/_inc/inferior');
?>
