<? $this->load->view('priv/administrador/_inc/superior'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Editar Integrante</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li>
				<a href="<?echo base_url()?>equipeController/">Equipe</a>
			</li>
			<li class="active">
				<strong>Editar Integrante</strong>
			</li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Dados</h5>
			</div>
			
		<div class="ibox-content">	
		
			
			<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
			<?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>

			<? foreach ($equipe as $row) { ?>
				<form method="post" action="<?= BASE_URL(); ?>equipeController/edit">			
					<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
					<div class="form-group">
						<label>Nome</label><br />
						<input type="text" name="nome" id="nome" value="<?= $row->nome ?>" class="form-control" />
					</div>
					<div class="form-group">
						<label>Cargo</label><br />
						<input type="text" name="cargo" id="cargo" value="<?= $row->cargo ?>" class="form-control" />
					</div>
					<div class="form-group">
						<label>Texto</label><br />
						<textarea name="texto" id="texto" class="form-control ckeditor"><?= $row->texto ?></textarea>
					</div>
					<div class="form-group">
						<label>Facebook</label><br />
						<input type="text" name="facebook" id="facebook" value="<?= $row->facebook ?>" class="form-control"  />
					</div>
					<div class="form-group">
						<label>Twitter</label><br />
						<input type="text" name="twitter" id="twitter" value="<?= $row->twitter ?>" class="form-control" />
					</div>
					<div class="form-group">
						<label>Instagram</label><br />
						<input type="text" name="instagram" id="instagram" value="<?= $row->instagram ?>" class="form-control" />
					</div>
					<div class="form-group">
						<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>equipeController'" />
						<input type="submit" class="btn btn-success" name="btSalvarEquipe" value="Salvar" />
					</div>
				</form>
				<br /><br />
				
				<h1 class="page-header">Imagem</h1>
				<p>* Imagem de 200x200 px</p><br />
				<? if ($row->imagem) { ?>
					<div class="galeria"><a onclick="confirmaExcluirGaleria('<?= $row->id ?>')"><img src="<?=base_url()?>upload/equipe/<?=$row->imagem?>" height="200px;float:left" alt="Clique para excluir" /></a></div>
				<? } ?>
				<form action="<?=base_url()?>equipeController/addGaleria/<?= $row->id ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input type="file" class="form-control" name="userfile" id="userfile" style="float:left;width:200px;margin-right:10px" />
						<input class="btn btn-success" type="submit" name="enviar" value="Salvar" />
					</div>
				</form>				
			<? } ?>
		</div>
	</div>
</div>
</div>
</div>

<? $this->load->view('priv/administrador/_inc/inferior'); ?>
