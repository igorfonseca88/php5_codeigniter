<?
$this->load->view('priv/administrador/_inc/superior');
?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Textos</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li class="active">
				<strong>Textos</strong>
			</li>
		</ol>
	</div>
</div>
		
<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Textos</h5>
				
			</div>
						
		<div class="ibox-content">	
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			<table id="dataTables-example" class="table table-striped table-bordered table-hover">
				<thead>
					<th>Título</th>
					<th width="120">Ações</th>
				</thead>
				<? foreach ($textos as $row) { ?>
				<tr>
					<td><?= $row->titulo ?></td>
					<td align="center"><a href="<?= base_url() ?>textoController/editAction/<?= $row->id ?>">Editar</a></td>
				</tr>
				<? } ?>
			</table>
		</div>
		</div>
	</div>
</div>
</div>	

<? $this->load->view('priv/administrador/_inc/inferior'); ?>
