<? $this->load->view('priv/administrador/_inc/superior'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Editar Texto</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li>
				<a href="<?echo base_url()?>textoController/">Textos</a>
			</li>
			<li class="active">
				<strong>Editar Texto</strong>
			</li>
		</ol>
	</div>
</div>

<script>
	function confirmaExcluirGaleria(id) {
		var r=confirm("Deseja excluir este item?")
		if (r==true) { location.href = "<?= base_url() ?>textoController/deleteGaleria/" + id; }
	}
</script>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Dados</h5>
			</div>
			
		<div class="ibox-content">	
		
			
			<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
			<?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>

			<? foreach ($texto as $row) { ?>
				<form method="post" action="<?= BASE_URL(); ?>textoController/edit">			
					<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
					<div class="form-group">
						<label>Título</label><br />
						<input type="text" name="titulo" id="titulo" value="<?= $row->titulo ?>" class="form-control" />
					</div>
					<div class="form-group">
						<label>Descrição</label><br />
						<textarea name="descricao" id="descricao" class="form-control"><?= $row->descricao ?></textarea>
					</div>
					<div class="form-group">
						<label>Texto</label><br />
						<textarea name="texto" id="texto" class="form-control ckeditor"><?= $row->texto ?></textarea>
					</div>
					<div class="form-group">
						<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>textoController'" />
						<input type="submit" class="btn btn-success" name="btSalvarTexto" value="Salvar" />
					</div>
				</form>
				<br /><br />
				
				<h1 class="page-header">Imagem</h1>
				<? if ($row->imagem) { ?>
					<div class="galeria"><a onclick="confirmaExcluirGaleria('<?= $row->id ?>')"><img src="<?=base_url()?>upload/texto/<?=$row->imagem?>" style="height:200px;float:left" alt="Clique para excluir" /></a></div>
				<? } ?>
				<form action="<?=base_url()?>textoController/addGaleria/<?= $row->id ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input type="file" class="form-control" name="userfile" id="userfile" style="float:left;width:200px;margin-right:10px" />
						<input class="btn btn-success" type="submit" name="enviar" value="Salvar" />
					</div>
				</form>				
			<? } ?>
		</div>
	</div>
</div>
</div>
</div>

<? $this->load->view('priv/administrador/_inc/inferior'); ?>
