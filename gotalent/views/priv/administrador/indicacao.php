<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Banco de profissionais</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>principal/arearestritaadmin">Home</a>
            </li>
            <li class="active">
                <strong>Banco de profissionais </strong>
            </li>
        </ol>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Banco de indicações</h5>

                </div>
                <div class="ibox-content">		
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>

                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <form method="post" action="<?= base_url() ?>administradorController/buscarProfissionaisIndicados">

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Vaga</label><br>

                                    <select name="vaga" id="vaga" class="form-control">
                                        <option>Selecione</option>
                                        <? foreach ($vagas as $vaga) { ?>
                                            <option <?= $_POST["vaga"] == $vaga->id ? "selected" : "" ?> value="<? echo $vaga->id ?>"><? echo $vaga->vaga . " - " . $vaga->razaosocial ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Estado</label><br>
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="">TODOS</option>
                                        <? foreach ($estados as $estado) { ?>
                                            <option <?= $_POST["estado"] == $estado->id ? "selected" : "" ?> value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                        <? } ?>

                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Pretensão</label><br>
                                    <select id="tipoPretensao" name="tipoPretensao" class="form-control" style="width:50%;float:left">
                                        <option value="<=">Menor igual</option>
                                        <option value=">=">Maior igual</option>
                                    </select>
                                    <input type="text" name="pretensao" id="pretensao" class="form-control money" style="width:50%;float:left" value="<? echo $_POST["pretensao"] ?>" />
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">

                                    <input type="submit" class="btn btn-success" name="btPesquisar" value="Pesquisar" />
                                </div>
                            </div>
                        </div>
                    </form>		

                    <table class="table table-striped table-bordered table-hover" id="dataTable-custom">
                        <thead>
                        <th>Porcentagem</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Cidade</th>
                        <th>Estado</th>
                        <th>Pretensão</th>
                        <th>Plano</th>
                        <th>Ações</th>




                        </thead>
                        <? foreach ($profissionais as $row) { ?>
                            <tr>
                                <td> <?= number_format($row->porcentagem, 2, ',', ' ') . "%" ?></td>
                                <td> <?= $row->nome ?></td>
                                <td> <?= $row->email ?></td>
                                <td> <?= $row->telefone ?></td>
                                <td> <?= $row->cidade ?></td>
                                <td> <?= $row->estado ?></td>

                                <td> <?= $row->pretensao ?></td>
                                <td> <?= $row->tipoPlano ?></td>
                                <td><a href="<?= base_url() ?>administradorController/editarProfissionalRecrutadorAction/<?= $row->id ?>">Editar</a>|
                                    <a data-toggle="modal" data-target="#modalIndicacao" 
                                       onclick="alteraAtributoIndicacao('<?= base_url() ?>administradorController/indicarProfissionalAction/<? echo $row->id ?>/<? echo $_POST["vaga"] ?>')">Indicar Profissional</a>
                                </td>
                                <td><a href="<?= base_url() ?>pdfController/pdf/<? echo $row->id ?>" title="Exportar Currículo" 
                                       onclick="" style="float:left;margin-top:3px;color:red"><i class="fa fa-file-pdf-o" style="font-size:23px"></i></a></td>

                            </tr>
                        <? } ?>
                    </table>
                </div>
            </div>
        </div
    </div>


    <?
    $this->load->view('priv/administrador/_inc/inferior');
    ?>