<?
$this->load->view('priv/_inc/superior');
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Entrevistas
				
			</h1>
		<!--
								<form id="formPesquisa" name="formPesquisa" action="<? //echo base_url()?>buscar" method="post">
									<div class="col-md-5"> <input type="text" class="form-control" name="searchVaga" value="<? //echo $_POST['searchVaga']?>" placeholder="Digite um cargo ou palavra-chave  ..." /> </div>
									<div class="col-md-5"> 
									<select name="estado" id="estado" class="form-control">
										<option value="">Estado</option>
										<option <?//= $row->estado == "AC" ? "selected" : "" ?> value="AC">Acre</option>
										<option <?//= $row->estado == "AL" ? "selected" : "" ?> value="AL">Alagoas</option>
										<option <?//= $row->estado == "AM" ? "selected" : "" ?> value="AM">Amazonas</option>
										<option <?//= $row->estado == "AP" ? "selected" : "" ?> value="AP">Amapá</option>
										<option <?//= $row->estado == "BA" ? "selected" : "" ?> value="BA">Bahia</option>
										<option <?//= $row->estado == "CE" ? "selected" : "" ?> value="CE">Ceará</option>
										<option <?//= $row->estado == "DF" ? "selected" : "" ?> value="DF">Distrito Federal</option>
										<option <?//= $row->estado == "ES" ? "selected" : "" ?> value="ES">Espirito Santo</option>
										<option <?//= $row->estado == "GO" ? "selected" : "" ?> value="GO">Goiás</option>
										<option <?//= $row->estado == "MA" ? "selected" : "" ?> value="MA">Maranhão</option>
										<option <?//= $row->estado == "MG" ? "selected" : "" ?> value="MG">Minas Gerais</option>
										<option <?//= $row->estado == "MS" ? "selected" : "" ?> value="MS">Mato Grosso do Sul</option>
										<option <?//= $row->estado == "MT" ? "selected" : "" ?> value="MT">Mato Grosso</option>
										<option <?//= $row->estado == "PA" ? "selected" : "" ?> value="PA">Pará</option>
										<option <?//= $row->estado == "PB" ? "selected" : "" ?> value="PB">Paraíba</option>
										<option <?//= $row->estado == "PE" ? "selected" : "" ?> value="PE">Pernambuco</option>
										<option <?//= $row->estado == "PI" ? "selected" : "" ?> value="PI">Piauí</option>
										<option <?//= $row->estado == "PR" ? "selected" : "" ?> value="PR">Paraná</option>
										<option <?//= $row->estado == "RJ" ? "selected" : "" ?> value="RJ">Rio de Janeiro</option>
										<option <?//= $row->estado == "RN" ? "selected" : "" ?> value="RN">Rio Grande do Norte</option>
										<option <?//= $row->estado == "RO" ? "selected" : "" ?> value="RO">Rondônia</option>
										<option <?//= $row->estado == "RR" ? "selected" : "" ?> value="RR">Roraima</option>
										<option <?//= $row->estado == "RS" ? "selected" : "" ?> value="RS">Rio Grande do Sul</option>
										<option <?//= $row->estado == "SC" ? "selected" : "" ?> value="SC">Santa Catarina</option>
										<option <?//= $row->estado == "SE" ? "selected" : "" ?> value="SE">Sergipe</option>
										<option <?//= $row->estado == "SP" ? "selected" : "" ?> value="SP">São Paulo</option>
										<option <?//= $row->estado == "TO" ? "selected" : "" ?> value="TO">Tocantins</option>
									</select>
									</div>
									<div class="col-md-2"> <input type="submit" class="btn btn-success" value="Go !" /> </div>
								</form>
								-->
						</div>
					
                    
                </div>
	
  <br>
   <div class="row">
   <div class="col-lg-12"> 
    <section class="slice bg-3 animate-hover-slide">
        <div class="w-section inverse work">
            
                
                
                <div class="row">      
				 <div class="col-lg-12">	
                   
    
                      
						
						
						<? foreach($profissionais as $profissional){?>
						<div class="col-lg-3 col-md-3 col-sm-6">
                            
                                <div class="figure">
									 <a href="#" 
									 data-toggle="modal" 
									 data-target="#videoModal" 
									 data-theVideo="http://www.youtube.com/embed/<? echo $profissional->video?>">
									 <img src="http://img.youtube.com/vi/<? echo $profissional->video?>/mqdefault.jpg" width="230px" alt="" /></a>  
                                    
                                    <p>Nome: <? echo $profissional->nome?></p>
									<p><small>Cidade: <? echo $profissional->cidade?>/<? echo $profissional->estado?></small></p>
                                    <p><a href="<?= base_url() ?>entrevistaController/visualizarDadosProfissionalAction/<?= $profissional->id*1000 ?>" class="btn btn-xs btn-one">
										<i class="fa fa-link"></i> Visualizar currículo
										</a>
									</p>
									<p><small></small></p>
                                </div>
                            
                        </div>
                       
                        <?}?>
                                    
                </div>
			 </div>	
          
        </div>
    </section>
 </div>
</div>



</div>

<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div>
          <iframe width="100%" height="350" src=""></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  autoPlayYouTubeModal();
});

function autoPlayYouTubeModal(){
  var trigger = $("body").find('[data-toggle="modal"]');
  trigger.click(function() {
    var theModal = $(this).data( "target" ),
    videoSRC = $(this).attr( "data-theVideo" ), 
    videoSRCauto = videoSRC+"?autoplay=1" ;
    $(theModal+' iframe').attr('src', videoSRCauto);
    $(theModal+' button.close').click(function () {
        $(theModal+' iframe').attr('src', videoSRC);
    });   
  });
}
</script>  
    <?
$this->load->view('priv/_inc/inferior');
?>