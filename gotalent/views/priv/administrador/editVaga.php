<?
$this->load->view('priv/administrador/_inc/superior');

?>


<div class="wrapper wrapper-content animated fadeInUp">

    <div class="row">
        <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados da vaga </h5>


                    <? if ($vaga[0]->situacao == 'Aguardando Aprovação') { ?>

                        <input type="button" class="btn btn-warning" name="btPublicar" value="DESAPROVAR VAGA"
                               style="display: inline;float: right;margin-top: -8px;margin-left:10px;font-weight: bold"
                               data-toggle="modal" data-target="#myModal"/>
                        <input type="button" class="btn btn-primary" name="btPublicar"
                               onclick="location.href = '<?= base_url() ?>administradorController/publicarVagaAction/<?= $vaga[0]->id ?>/<?= $vaga[0]->idEmpresa ?>'"
                               value="APROVAR VAGA"
                               style="display: inline;float: right;margin-top: -8px; margin-left: 10px;font-weight: bold"/>
                    <? } ?>
                </div>

                <div class="ibox-content" style="clear: both">


                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>

                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <form method="post" class="form-horizontal"
                          action="<?= base_url() ?>administradorController/editVaga" name="formEditVaga">
                        <input type="hidden" name="idVaga" id="idVaga" value="<? echo $vaga[0]->id ?>"/>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Data do Fechamento</label>

                            <div class="col-sm-4">
                                <p class="form-control-static"> <?= implode("/", array_reverse(explode("-", $vaga[0]->dataFechamento))) ?> </p>
                            </div>
                            <label class="col-sm-2 control-label">Motivo</label>

                            <div class="col-sm-4">
                                <p class="form-control-static"> <?= $vaga[0]->motivoFechamento ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Envio aprovação</label>

                            <div class="col-sm-2">
                                <p class="form-control-static"><?= implode("/", array_reverse(explode("-", $vaga[0]->dataEnvioAprovacao))) ?> </p>
                            </div>
                            <label class="col-sm-2 control-label">Data da publicação</label>

                            <div class="col-sm-2">
                                <p class="form-control-static"><?= implode("/", array_reverse(explode("-", $vaga[0]->dataPublicacao))) ?> </p>
                            </div>

                            <label class="col-sm-2 control-label">Data da vigência</label>

                            <div class="col-sm-2">
                                <p class="form-control-static"><?= implode("/", array_reverse(explode("-", $vaga[0]->dataVigencia))) ?></p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título da Vaga</label>

                            <div class="col-sm-2">
                                <input type="hidden" name="vaga" id="vaga" value="<?= $vaga[0]->vaga ?>"/>

                                <p class="form-control-static"><?= $vaga[0]->vaga ?> </p>
                            </div>
                            <label class="col-sm-2 control-label">Quantidade:</label>

                            <div class="col-sm-2">
                                <input type="hidden" name="quantidade" id="quantidade"
                                       value="<?= $vaga[0]->quantidade ?>"/>

                                <p class="form-control-static"><?= $vaga[0]->quantidade == 11 ? "Acima de 10 vagas" : $vaga[0]->quantidade ?> </p>
                            </div>
                            <label class="col-sm-2 control-label">Situação:</label>

                            <div class="col-sm-2">
                                <p class="form-control-static"><?= $vaga[0]->situacao ?> </p>
                            </div>
                        </div>
                        <? if ($vaga[0]->situacao == "Desaprovado") { ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Data de Desaprovação</label>

                                <div class="col-sm-4">
                                    <p class="form-control-static"> <?= implode("/", array_reverse(explode("-", $vaga[0]->dataDesaprovacao))) ?> </p>
                                </div>
                                <label class="col-sm-2 control-label">Motivo</label>

                                <div class="col-sm-4">
                                    <p class="form-control-static"> <?= $vaga[0]->motivoDesaprovacao ?></p>
                                </div>
                            </div>
                        <? }; ?>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Acessibilidades </label>

                            <div class="col-lg-4">
                                <?if($vaga[0]->receberCurriculoEmail == "SIM"){
                                    $curriculoEmail = "checked";
                                }?>
                                <input type="checkbox" name="receberCurriculoEmail" id="receberCurriculoEmail"
                                       class="form-control checkbox i-checks" <?=$curriculoEmail?>/><i></i> Receber currículos por
                                e-mail.
                            </div>

                            <div class="col-lg-4">
                                <?if($vaga[0]->receberCurriculoFora == "SIM"){
                                    $curriculoFora = "checked";
                                }?>
                                <input type="checkbox" name="receberCurriculoFora" id="receberCurriculoFora" <?=$curriculoFora?>
                                       class="form-control checkbox i-checks"/><i></i> Aceitar somente curriculos da
                                mesma localidade
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <span id="spanComboSubArea">
                                <label class="col-sm-2 control-label">Área da TI:</label>
                                <div class="col-lg-4">
                                    <select name="comboSubArea" id="comboSubArea" class="form-control">
                                        <option value="">Selecione</option>
                                        <? foreach ($subareas as $sub) { ?>
                                            <option <?= $vaga[0]->idSubArea == $sub->id ? "selected" : "" ?> value="<?= $sub->id ?>"<?= $sub->area . " > " . $sub->subarea ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </span>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição</label>

                            <div class="col-sm-10">
                                <textarea class="ckeditor" name="descricao" id="descricao"
                                          class="form-control"><?= $vaga[0]->descricao ?></textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tipo de contratação</label>

                            <div class="col-sm-4">
                                <div class="radio i-checks"><label> <input
                                            type="radio" <?= $vaga[0]->tipo == "CLT" ? "checked" : "" ?> name="tipo"
                                            value="CLT"/> <i></i> CLT </label></div>

                                <div class="radio i-checks"><label> <input
                                            type="radio" <?= $vaga[0]->tipo == "PJ" ? "checked" : "" ?> name="tipo"
                                            value="PJ"/> <i></i> PJ </label></div>

                                <div class="radio i-checks"><label><input
                                            type="radio" <?= $vaga[0]->tipo == "Freelancer" ? "checked" : "" ?>
                                            name="tipo" value="Freelancer"/> <i></i> Freelancer </label></div>
                                <div class="radio i-checks"><label><input
                                            type="radio" <?= $vaga[0]->tipo == "Estágio" ? "checked" : "" ?>
                                            name="tipo" value="Estágio"/> <i></i> Estágio </label></div>
                            </div>
                            <!--Tipo de Profissional-->
                           <label class="col-sm-2 control-label">Benefícios</label>

                            <div class="col-lg-4">
                                <?
                                foreach ($beneficios as $beneficio) {
                                    $checked = "";
                                    foreach ($beneficios_vaga as $bv) {
                                        if ($bv->idBeneficio == $beneficio->id) {
                                            $checked = "checked";
                                            continue;
                                        }
                                    }
                                    ?>
                                    <div class="checkbox i-checks">
                                        <label>
                                            <input type="checkbox" <? echo $checked ?>
                                                   value="<? echo $beneficio->id ?>" name="beneficios[]"/>
                                            <i></i> <? echo $beneficio->beneficio ?> </label>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Salário</label>

                            <div class="col-lg-3">
                                <input type="text" name="salario" id="salario" class="form-control"
                                       value="<?= $vaga[0]->salario; ?>"/>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">CEP</label>

                            <div class="col-lg-2">
                                <input type="text" name="cep" id="cep" class="cep form-control"
                                       value="<?= $vaga[0]->cep ?>"/>
                            </div>

                            <label class="col-sm-2 control-label">Endereço</label>

                            <div class="col-lg-6">
                                <input type="text" name="endereco" id="endereco" class="form-control"
                                       value="<?= $vaga[0]->endereco ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Número</label>

                            <div class="col-lg-2">
                                <input type="text" name="numero" id="numero" class="form-control"
                                       value="<?= $vaga[0]->numero ?>"/>
                            </div>

                            <label class="col-sm-2 control-label">Bairro</label>

                            <div class="col-lg-4">
                                <input type="text" name="bairro" id="bairro" class="form-control"
                                       value="<?= $vaga[0]->bairro ?>"/>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Estado:</label>

                            <div class="col-sm-2">
                                <select name="estado" id="estado" class="form-control" style="width:200px;">
                                    <option value="">Selecione</option>
                                    <? foreach ($estados as $estado) { ?>
                                        <option <?= $vaga[0]->estado == $estado->uf ? "selected" : "" ?>
                                            value="<?= $estado->uf ?>"><?= $estado->nome?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Cidade:</label>

                            <div class="col-sm-4">
                                <select name="cidade" id="cidade" class="form-control">
                                    <option value="">Selecione</option>
                                    <? foreach ($cidades as $cidade) { ?>
                                        <option <?= $vaga[0]->cidade == $cidade->nome ? "selected" : "" ?>
                                            value="<?=$cidade->nome?>" > <?= $cidade->nome ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" hidden>
                            <label class="col-sm-2 control-label">Informações adicionais</label>

                            <div class="col-lg-10">
                                <textarea name="informacoesAdicionais" id="informacoesAdicionais"
                                          class="form-control"><?= $vaga[0]->informacoesAdicionais ?></textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="submit" class="btn btn-primary" name="btSalvar" value="SALVAR"
                                       style="font-weight: bold; margin-right: 10px;"/>
                                <input type="button" value="VOLTAR" class="btn btn-white" style="font-weight: bold"
                                       onclick="location.href = '<?= base_url() ?>administradorController/listarVagas'"/>

                            </div>
                            <? if ($vaga[0]->situacao == 'Aguardando Aprovação') { ?>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <input type="button" class="btn btn-warning" name="btPublicar"
                                           value="DESAPROVAR VAGA"
                                           style="display: inline;float: right;margin-top: -8px;margin-left:10px;font-weight: bold"
                                           data-toggle="modal" data-target="#myModal"/>
                                    <input type="button" class="btn btn-primary" name="btPublicar"
                                           onclick="location.href = '<?= base_url() ?>administradorController/publicarVagaAction/<?= $vaga[0]->id ?>/<?= $vaga[0]->idEmpresa ?>'"
                                           value="APROVAR VAGA"
                                           style="display: inline;float: right;margin-top: -8px; margin-left: 10px;font-weight: bold"/>
                                </div>
                                <? } ?>
                            </div>
                    </form>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Competências dos candidatos </h5>
                        <small class="text-navy"> ( Adicione competências a vaga, pois com elas iremos calcular a
                            porcentagem de compatibilidade dos profissionais )
                        </small>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form id="formSkillAuto" name="formSkillAuto" method="POST"
                                  action="<? echo base_url() ?>administradorController/addSkillVaga">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="hidden" name="idVagah" id="idVagah"
                                               value="<? echo $vaga[0]->id ?>"/>
                                        <select class="form-control" style="height:200px" name="lista[]" multiple="">
                                            <? foreach ($skills as $sk) { ?>
                                                <option value="<? echo $sk->id ?>"><? echo $sk->skill ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" name="btAdicionar"
                                               value="Adicionar Competência"/>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <ul style="list-style: none;padding-left: 0;margin: 0;">
                                    <? foreach ($vaga_skills as $ps) { ?>
                                        <li style="border: 1px solid #59b2e5;background: #FFF;padding: 5px;float: left;
                                            margin-right: 5px;margin-bottom: 5px;color: #59b2e5;"><? echo $ps->skill ?>
                                            <a href="<? echo base_url() ?>administradorController/excluirSkillVaga/<? echo $ps->id ?>/<? echo $ps->idVaga ?>"><img
                                                    style="margin-left:10px"
                                                    src="<? echo base_url() ?>img/excluir.png"/></a></li>
                                    <? } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Profissionais </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <th>Nome</th>
                                <th>Foto</th>
                                <th>E-mail</th>
                                <th>Pretensão</th>
                                <th>Plano</th>
                                <th>Fim plano</th>
                                <th>Compatibilidade</th>
                                <th>Indicado</th>
                                <th>Ações</th>
                            </thead>
                            <? foreach ($profissionais_vaga as $row) { ?>
                                <tr>

                                    <td> <?= $row->nome ?></td>
                                    <td>
                                        <? if ($row->foto != "") { ?>
                                            <img alt="<? echo $row->nome ?>" width="100px" height="117px" class=""
                                                 src="<? echo base_url() ?>upload/profissional/<? echo $row->foto ?>">
                                        <? } else { ?>
                                            <img alt="<? echo $row->nome ?>" width="100px" height="117px"
                                                 src="<? echo base_url() ?>img/icons/nopicture.png">
                                        <? } ?>
                                    </td>
                                    <td> <?= $row->email ?></td>


                                    <td> <?= $row->pretensao ?></td>
                                    <td> <?= $row->idPlano == '1' ? "FREE" : "PREMIUM" ?></td>

                                    <td> <?= implode("/", array_reverse(explode("-", $row->dataFinalPlano))) ?></td>
                                    <?
                                    $compat = explode(",", implode(",", $compatibilidade_vaga[$row->id]));
                                    //echo $compat[1] == "" ? "0%" : number_format($compat[1], 2, ',', ' ')."%";
                                    ?>
                                    <td><? echo number_format($compat[1], 0, ',', ' ') == "" ? "0" : number_format($compat[1], 0, ',', ' ') ?>
                                        %
                                    </td>
                                    <td> <?= $row->isIndicacao == "" ? "NÃO" : $row->isIndicacao ?></td>
                                    <td>
                                        <a href="<?= base_url() ?>administradorController/editarProfissionalRecrutadorAction/<?= $row->id ?>">Editar</a>
                                        |

                                        <a href='<? echo base_url() ?>pdfController/pdf/<? echo $row->id ?>'
                                           target="_blank">Exportar currículo</a> |
                                        <a data-toggle="modal" data-target="#modalIndicacao"
                                           onclick="alteraAtributoIndicacao('<?= base_url() ?>administradorController/indicarProfissionalAction/<? echo $row->id ?>/<? echo $vaga[0]->id ?>')">Indicar
                                            Profissional</a>
                                    </td>
                                </tr>
                            <? } ?>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Fechar</span></button>
                <h4 class="modal-title" id="myModalLabel">Informe o motivo para desaprovar esta vaga</h4>
            </div>
            <form id="formFeedback" action="<?= base_url() ?>administradorController/desaprovarVagaAction"
                  name="formFeedback" method="POST">

                <div class="modal-body">
                    <input type="text" hidden name="idVaga" value="<?= $vaga[0]->id ?>" style="pointer-events:none;">
                    <input type="text" hidden name="idEmpresa" value="<?= $vaga[0]->idEmpresa ?>"
                           style="pointer-events:none;">
                    <label>Motivo:</label>
                    <textarea class="form-control" name="motivoFechamento" id="motivoFechamento"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <input type="submit" class="btn btn-success" name="btSalvarEnviar" value="Salvar"/>
                </div>
            </form>
        </div>
    </div>
</div>

<?
$this->load->view('priv/administrador/_inc/inferior');
?>
