<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-sm-4">
	<h2>Nova Skill</h2>
	<ol class="breadcrumb">
		<li>
			<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
		</li>
		<li>
			<a href="<?echo base_url()?>administradorController/listarSkills">Listagem de Skills</a>
		</li>
		<li class="active">
			<strong>Nova Skill</strong>
		</li>
	</ol>
</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Dados da Categoria</h5>
			</div>
			
		<div class="ibox-content">	

		
			<form method="post" action="<?= base_url() ?>administradorController/addSkill" class="form-horizontal">
				
				<div class="form-group">
							<label class="col-sm-2 control-label">Skill </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="skill" id="skill"/>
							</div>
							
							<label class="col-sm-2 control-label">Categoria </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="categoria" id="categoria"/>
							</div>
				</div>
				
				<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
						<input type="button" value="Voltar" class="btn btn-white" onclick="location.href='<?= base_url() ?>administradorController/listarSkills'"  />
						
						<input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
				</div>		
				</div>
				
			</form>
		</div>
	</div>	
	</div>
</div>	
</div>



<?
$this->load->view('priv/administrador/_inc/inferior');
?>
