<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h1>Empresa</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">
                <strong>Dados da Empresa</strong>
            </li>
        </ol>
    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">

            <div class="ibox float-e-margins">	
                <div class="ibox-title">
                    <h5>Dados da Empresa</h5>
                </div>

                <div class="ibox-content">	
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>



                    <? foreach ($empresa as $row) { ?>



                        <form method="post" action="<?= base_url() ?>administradorController/editEmpresa" class="form-horizontal">
                            <input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Data do Cadastro </label>
                                <div class="col-sm-4">
                                    <p class="form-control-static"><?= implode("/", array_reverse(explode("-", $row->dataCadastro))) ?> </p>
                                </div>


                                <label class="col-sm-2 control-label">Razão Social</label>
                                <div class="col-sm-4">
                                    <input type="text" name="razaosocial" id="razaosocial" class="form-control" value="<?= $row->razaosocial ?>" />
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">E-mail</label>
                                <div class="col-sm-4">
                                    <input type="text" name="email" id="email" class="form-control" value="<?= $row->email ?>" />
                                </div>

                                <label class="col-sm-2 control-label">Telefone</label>
                                <div class="col-sm-4">
                                    <input type="text" name="telefone" id="telefone" class="telefone form-control"  value="<?= $row->telefone ?>" />
                                </div>

                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group">

                                <label class="col-sm-2 control-label">CEP</label>
                                <div class="col-sm-4">
                                    <input type="text" name="cep" id="cep" class="cep form-control" value="<?= $row->cep ?>" />
                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-sm-2 control-label">Endereço</label>
                                <div class="col-sm-6">
                                    <input type="text" name="endereco" id="endereco" class="form-control"  value="<?= $row->endereco ?>" />
                                </div>

                                <label class="col-sm-2 control-label">Número</label>
                                <div class="col-sm-2">
                                    <input type="text" name="numero" id="numero" class="form-control"  value="<?= $row->numero ?>" />
                                </div>	
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Bairro</label>
                                <div class="col-sm-4">
                                    <input type="text" name="bairro" id="bairro" class="form-control"  value="<?= $row->bairro ?>" />
                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-sm-2 control-label">Estado</label>
                                <div class="col-sm-4">
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="">Selecione</option>    
                                        <? foreach ($estados as $estado) { ?>
                                            <option <?= $row->estado == $estado->id ? "selected" : "" ?> value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                        <? } ?> 
                                    </select>
                                </div>

                                <label class="col-sm-2 control-label">Cidade</label>
                                <div class="col-sm-4">	
                                    <select name="cidade" id="cidade" class="form-control">
                                    <option value="">Selecione</option>    
                                    <? foreach ($cidades as $cidade) { ?>
                                        <option <?= $row->cidade == $cidade->id ? "selected" : "" ?> value="<?= $cidade->id ?>"><?= $cidade->nome ?></option>
                                    <? } ?> 
                                    </select>    
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Facebook</label>
                                <div class="col-sm-4">	
                                    <input type="text" name="facebook" id="facebook" class="form-control"  value="<?= $row->facebook ?>" />
                                </div>

                                <label class="col-sm-2 control-label">Contato</label>
                                <div class="col-sm-4">	
                                    <input type="text" name="contato" id="contato" class="form-control"  value="<?= $row->contato ?>" />
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Sobre a Empresa</label>
                                <div class="col-sm-10">		
                                    <textarea name="sobre" id="sobre" class="form-control"><?= $row->sobre ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <input type="button" value="Voltar" class="btn btn-white" onclick="location.href = '<?= base_url() ?>administradorController/listarEmpresas'"  />


                                </div>		
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">E-mail</label>
                                <div class="col-sm-4">
                                    <input type="text" name="login" id="login" class="form-control" value="<?= $usuario[0]->login ?>" />
                                </div>

                                <label class="col-sm-2 control-label">Senha</label>
                                <div class="col-sm-4">
                                    <input type="text" name="senha" id="senha" class="form-control"  value="<?= $usuario[0]->senha ?>" />
                                </div>

                            </div>

                        </form>

                    <? } ?>


                </div>
            </div>
        </div>
    </div>
</div>



<?
$this->load->view('priv/administrador/_inc/inferior');
?>
