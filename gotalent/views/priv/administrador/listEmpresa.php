<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-sm-4">
	<h2>Listagem de Empresas</h2>
	<ol class="breadcrumb">
		<li>
			<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
		</li>
		<li class="active">
			<strong>Listagem de Empresas </strong>
		</li>
	</ol>
</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
<div class="col-lg-12">

	<div class="ibox">
		<div class="ibox-title">
			<h5>Skills</h5>
			<div class="ibox-tools">
				<a href="<?= base_url() ?>administradorController/novaEmpresaAction" class="btn btn-primary btn-xs">Cadastrar</a>
			</div>
		</div>
	
		<div class="ibox-content">	
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<th>Razão Social</th>
					<th>E-mail</th>
					<th>Telefone</th>
					<th>Endereço</th>
					<th>Contato</th>
					<th>Senha</th>
					<th>Data do cadastro</th>
                                        <th>Apelido</th>
					
					<th width="120" align="center">Ações</th>
				</thead>
				<? foreach ($empresas as $row) { ?>
				 <tr>
					<td> <?= $row->razaosocial ?></td>
					<td> <?= $row->email ?></td>
					<td> <?= $row->telefone ?></td>
					<td> <?= $row->endereco ?></td>
					<td> <?= $row->contato ?></td>
					<td> <?= $row->senha ?></td>
					<td> <?= implode("/",array_reverse(explode("-",$row->dataCadastro)))?></td>
                                        <td> <?= $row->apelido ?></td>
					<td align="center">
						<a href="<?= base_url() ?>administradorController/editarEmpresaAction/<?= $row->id ?>">Editar</a> 
					</td>
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>
</div>
</div>

<?
$this->load->view('priv/administrador/_inc/inferior');
?>