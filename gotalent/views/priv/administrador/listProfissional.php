<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-sm-4">
	<h2>Listagem de Profissionais</h2>
	<ol class="breadcrumb">
		<li>
			<a href="<?echo base_url()?>area-restrita-admin">Home</a>
		</li>
		<li class="active">
			<strong>Listagem de Profissionais</strong>
		</li>
	</ol>
</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
<div class="col-lg-12">

	<div class="ibox">
		<div class="ibox-title">
			<h5>Profissionais</h5>
			<div class="ibox-tools">
				<a href="<?= base_url() ?>administradorController/imprimirProfissionaisAction" class="btn btn-primary btn-xs">Imprimir</a>
			</div>
		</div>
		<div class="ibox-content">


		
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<table class="table table-striped table-bordered table-hover" id="dataTables-example" >
				<thead>
					
					<th>Nome</th>
					<th>E-mail</th>
					<th>Login</th>
					<th>Senha</th>
					
					<th>Pretensão</th>
					<th>Plano</th>
					<th>Fim plano</th>
					
					
					<th>Ações</th>
				</thead>
				<? foreach ($profissionais as $row) { ?>
				 <tr>
					
					<td> <?= $row->nome ?></td>
					<td> <?= $row->email ?></td>
					<td> <?= $row->login ?></td>
					<td> <?= $row->senha ?></td>
					
					<td> <?= $row->pretensao ?></td>
					<td> <?= $row->tipoPlano ?></td>
					
					<td> <?= implode("/",array_reverse(explode("-",$row->dataFinalPlano)))?></td>
					<td>
						<a href="<?= base_url() ?>administradorController/editarProfissionalRecrutadorAction/<?= $row->id ?>">Editar</a> |
						<a href='<? echo base_url() ?>pdfController/pdf/<? echo $row->id ?>' target="_blank">Exportar currículo</a>
                                                
					</td>
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>
</div>
</div>

<?
$this->load->view('priv/administrador/_inc/inferior');
?>