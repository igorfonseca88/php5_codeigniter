<?
$this->load->view('priv/administrador/_inc/superior');
?>

<script>
	function confirmaExcluirGaleria(id) {
		var r=confirm("Deseja excluir este item?")
		if (r==true) { location.href = "<?= base_url() ?>postagensController/deleteGaleria/" + id; }
	}
</script>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Editar Post</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li>
				<a href="<?echo base_url()?>postagensController/">Postagens</a>
			</li>
			<li class="active">
				<strong>Editar Post</strong>
			</li>
		</ol>
	</div>
</div>



<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Editar dados do Post</h5>
			</div>
			
		<div class="ibox-content">	
		
			
			<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
			<?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>
			
			<?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
			<?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

			
				<form method="post" action="<?= BASE_URL(); ?>postagensController/edit">			
					<input type="hidden" name="id" id="id" value="<?= $postagem[0]->id ?>"/>
					<div class="form-group">
						<label>Data</label><br />
						<input type="text" name="data" id="data" class="form-control" value="<?=implode("/",array_reverse(explode("-", $postagem[0]->data)))?>" />
					</div>
					<div class="form-group">
						<label>Título</label><br />
						<input type="text" name="titulo" id="titulo" class="form-control" value="<?=$postagem[0]->titulo?>"/>
					</div>
					<div class="form-group">
					<label>URL(ex:um-profissional-de-ti)</label><br />
					<input type="text" name="urlPost" id="urlPost" class="form-control" value="<?=$postagem[0]->urlPost?>"/>
				</div>
					<div class="form-group">
						<label>Resumo</label><br />
						<textarea name="descricao" id="descricao" class="form-control"><?=$postagem[0]->descricao?></textarea>
					</div>
					<div class="form-group">
						<label>Tags <small>[ termos separados por vírgula ]</small></label><br />
						<textarea name="tags" id="tags" class="form-control"><?=$postagem[0]->tags?></textarea>
					</div>
					<div class="form-group">
						<label>Texto</label><br />
						<textarea name="texto" id="texto" class="form-control ckeditor"><?=$postagem[0]->texto?></textarea><br>
					</div>
					<div class="form-group">
						<label>Categoria</label><br />
						<select name="idcategoria" id="idcategoria" class="form-control">
							<option value=""></option>
							<? foreach ($categorias as $cat) { ?>
							<option <?=$postagem[0]->idcategoria == $cat->id ? "selected" : "" ?> value="<?=$cat->id?>"><?=$cat->titulo?></option>
							<? } ?>
						</select>
					</div>
					<div class="form-group">
						<label>Status</label><br />
						<select name="status" id="status" class="form-control">
							<option <?=$postagem[0]->status == "ATIVO" ? "selected" : "" ?> value="ATIVO">ATIVO</option>
							<option <?=$postagem[0]->status == "INATIVO" ? "selected" : "" ?> value="INATIVO">INATIVO</option>
						</select>
					</div>
					<div class="form-group">
						<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>postagensController'" />
						<input type="submit" class="btn btn-primary" name="btSalvarArtigo" value="Salvar" />
					</div>
				</form>
				<br /><br />
				
				<h1 class="page-header">Imagens</h1>
                <p>Proporçao: 500x350 px</p>
				
				<div class="galeria">
				<? foreach($galeria as $gal) { ?>
					<span>
                                            <img width="500px" src="<?=base_url()?>upload/blog/<?=$gal->imagem?>" /><br />
						<? if ($gal->principal == "NAO") { ?>
						<a href="<?= base_url() ?>postagensController/imagemPrincipal/<?=$gal->id?>">Tornar principal</a>
						<? } else { ?>
						<b>Imagem principal</b>
						<? } ?>
						/
						<a onclick="confirmaExcluirGaleria('<?= $gal->id ?>')">Excluir</a>						
					</span>
				<? } ?>
				</div>
				
				<form action="<?=base_url()?>postagensController/addGaleria/<?= $postagem[0]->id ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input type="file" class="form-control" name="userfile" id="userfile" style="float:left;width:200px;margin-right:10px" />
						<input class="btn btn-success" type="submit" name="enviar" value="Salvar" />
					</div>
				</form>				
			
		</div>
	</div>
</div>

<? $this->load->view('priv/administrador/_inc/inferior'); ?>
