<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Novo Post</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li>
				<a href="<?echo base_url()?>postagensController/">Postagens</a>
			</li>
			<li class="active">
				<strong>Novo Post</strong>
			</li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

			<div class="ibox">
				<div class="ibox-title">
					<h5>Dados do Post</h5>
				</div>
						
			<div class="ibox-content">	
			
			<form method="post" action="add">
				<div class="form-group">
					<label>Data</label><br />
					<input type="text" name="data" id="data" class="form-control" value="<?=date("d/m/Y")?>"/>
				</div>
				<div class="form-group">
					<label>Título</label><br />
					<input type="text" name="titulo" id="titulo" class="form-control"/>
				</div>
				<div class="form-group">
					<label>URL(ex:um-profissional-de-ti)</label><br />
					<input type="text" name="urlPost" id="urlPost" class="form-control"/>
				</div>
				<div class="form-group">
					<label>Resumo</label><br />
					<textarea name="descricao" id="descricao" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label>Tags <small>[ termos separados por vírgula ]</small></label><br />
					<textarea name="tags" id="tags" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label>Texto</label><br />
					<textarea name="texto" id="texto" class="form-control ckeditor"></textarea><br>
				</div>
				<div class="form-group">
					<label>Categoria</label><br />
					<select name="idcategoria" id="idcategoria" class="form-control">
						<option value=""></option>
						<? foreach ($categorias as $cat) { ?>
						<option value="<?=$cat->id?>"><?=$cat->titulo?></option>
						<? } ?>
					</select>
				</div>
				<div class="form-group">
					<label>Status</label><br />
					<select name="status" id="status" class="form-control">
						<option value="ATIVO">ATIVO</option>
						<option value="INATIVO">INATIVO</option>
					</select>
				</div>
				<div class="form-group">
					<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>postagensController'" />
					<input type="submit" value="Salvar" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
	</div>
</div>
</div>	

<? $this->load->view('priv/administrador/_inc/inferior'); ?>
