<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Editar Categoria</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li>
				<a href="<?echo base_url()?>categoriaController/">Categorias</a>
			</li>
			<li class="active">
				<strong>Editar Categoria</strong>
			</li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
<div class="row" >
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Editar dados da Categoria</h5>
			</div>
			
		<div class="ibox-content">	
		
			
			<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
			<?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>
			<br />
			<form method="post" class="form-horizontal" action="<?= base_url() ?>categoriaController/editCategoria">
				<input type="hidden" name="id" id="id" value="<?= $categoria[0]->id ?>"/>
                
                <div class="form-group">
							<label class="col-sm-2 control-label">Título </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="titulo" id="titulo" value="<?=$categoria[0]->titulo?>"/>
							</div>
				</div>
				
				<div class="form-group">
							<label class="col-sm-2 control-label">URL (ex: ambiente-de-trabalho) </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="urlCat" id="urlCat" value="<?=$categoria[0]->urlCat?>" />
							</div>
						</div>
				
				<div class="form-group">
							<label class="col-sm-2 control-label">Descrição</label>
							<div class="col-sm-10">
								<textarea name="descricao" id="descricao" class="form-control"><?=$categoria[0]->descricao?></textarea>
							</div>
				</div>
              
               
			
               <div class="form-group">
					<label class="col-sm-2 control-label">Status</label>
					<div class="col-sm-4">
					<select name="status" id="status" class="form-control">
                        <option value="ATIVO" <?  if ($categoria[0]->status == "ATIVO") {?> selected="selected" <? } ?> > ATIVO</option>
                        <option value="INATIVO" <?  if ($categoria[0]->status == "INATIVO") {?> selected="selected" <? } ?>>INATIVO</option>
                    </select>
					</div>
			   </div>	
				<div class="hr-line-dashed"></div>
				<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
						<input type="button" value="Voltar" class="btn btn-white" onclick="location.href='<?= base_url() ?>categoriaController'"  />
						
						<input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
				</div>		
				</div>
			</form>
		
		</div>
	  </div>
	</div>
</div>
</div>
<?
$this->load->view('priv/administrador/_inc/inferior');
?>
