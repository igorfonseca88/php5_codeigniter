<?
$this->load->view('priv/administrador/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>Nova Categoria</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
			</li>
			<li>
				<a href="<?echo base_url()?>categoriaController/">Categorias</a>
			</li>
			<li class="active">
				<strong>Nova Categoria</strong>
			</li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
            <div class="row" >
                <div class="col-lg-12">

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Dados da Categoria</h5>
						</div>
						
					<div class="ibox-content">	
					<form method="post" action="addCategoria" class="form-horizontal">
					
						<div class="form-group">
							<label class="col-sm-2 control-label">Título </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="titulo" id="titulo" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">URL (ex: ambiente-de-trabalho) </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="urlCat" id="urlCat" />
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Descrição</label>
							<div class="col-sm-10">
								<textarea name="descricao" id="descricao" class="form-control"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Status</label>
							<div class="col-sm-4">
							<select name="status" id="status" class="form-control">
								<option value="ATIVO">ATIVO</option>
								<option value="INATIVO">INATIVO</option>
							</select>
							</div>
						</div>	
						<div class="hr-line-dashed"></div>
						<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
								<input type="button" value="Voltar" class="btn btn-white" onclick="location.href='<?= base_url() ?>categoriaController'"  />
								
								<input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" />
						</div>		
						</div>
					</form>
				</div>
			</div>
		</div>	
	</div>	
</div>	
<?
$this->load->view('priv/administrador/_inc/inferior');
?>
