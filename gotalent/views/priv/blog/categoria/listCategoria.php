<?
$this->load->view('priv/administrador/_inc/superior');
?>
<script>
	function confirmaExcluir(id) {
		var r=confirm("Deseja excluir este item?")
		if (r==true) { location.href = "<?= base_url() ?>categoriaController/deleteCategoria/" + id; }
	}
</script>

<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-sm-4">
				<h2>Categorias</h2>
				<ol class="breadcrumb">
					<li>
						<a href="<?echo base_url()?>principal/arearestritaadmin">Home</a>
					</li>
					<li class="active">
						<strong>Categorias</strong>
					</li>
				</ol>
			</div>
		</div>

<div class="wrapper wrapper-content animated fadeInUp" >
            <div class="row" >
                <div class="col-lg-12">

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Listagem de Categorias</h5>
							<div class="ibox-tools">
                                <a href="<?= base_url() ?>categoriaController/novaCategoriaAction" class="btn btn-primary btn-xs">Nova Categoria</a>
                            </div>
						</div>
						
					<div class="ibox-content">	
					
					<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
					<?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>
					
				<table class="table table-hover">
				<thead>
					<th>Categoria</th>
					<th>Descrição</th>
					<th>Situação</th>
					<th></th>
					
					
				</thead>
				<? foreach ($categoria as $row) { ?>
				 <tr>
					<td> <?= $row->titulo ?></td>
					<td> <?= $row->descricao ?></td>
					<td> <?= $row->status ?></td>
					<td align="center"><a href="<?= base_url() ?>categoriaController/editarCategoriaAction/<?= $row->id ?>">Editar</a> | <a onclick="confirmaExcluir('<?= $row->id ?>')">Excluir</a></td>
				 </tr>
			  <? } ?>
		   </table>
			</div>
			
			</div>
		</div>	
			
		</div>
</div>

	
<?
$this->load->view('priv/administrador/_inc/inferior');
?>
