<? $this->load->view('priv/profissional/_inc/superior'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h1>Currículo </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita">Home</a>
            </li>			
            <li class="active">
                <strong>Meu currículo</strong>
            </li>
        </ol>
    </div>
</div>  

<div class="wrapper wrapper-content">	
    <div class="row">
        <div class="col-lg-12">

            <div class="ibox">	
                <div class="ibox-title">
                    <div class="col-lg-3">
                        <h5>Dados pessoais </h5>
                    </div> 
                    <div class="col-lg-9"> 
                        <div class="progress">
                            <div style="width: <? echo $porcentagemPreenchida ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar" class="progress-bar progress-bar-success">
                                <span class="">Currículo <? echo $porcentagemPreenchida ?>% completo</span>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="ibox-content">	
                    <div id="alerta">
                    </div>	
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>

                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <? if ($porcentagemPreenchida < 40) { ?>
                        <div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> Preencha os campos obrigatórios do painel Dados Pessoais e faça upload de sua foto. Posteriormente, preencha suas formações, experiências e Skills. </div>
                    <? } ?>





                    <? foreach ($profissional as $row) { ?>		   
                        <div class="row">				
                            <form id="upload" name="upload" action="<?= base_url() ?>profissionalController/uploadArquivo/<? echo $profissional[0]->id ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="tipoAnexo" name="tipoAnexo" value="Foto"/>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <? if ($row->foto != "" && count(explode(':',$row->foto)) <= 1 ) { ?>
                                            <img class="img-circle" src="<?= base_url() ?>upload/profissional/<? echo $row->foto ?>" id="foto-curriculo" title="<? echo $row->nome ?>" width="100%"/>
                                        <? }else if($row->foto != "" && count(explode(':',$row->foto)) > 1){ ?>
                                            <img class="img-circle" src="<?=$row->foto?>" id="foto-curriculo" title="<? echo $row->nome ?>" width="100%"/>
                                        <? } else { ?>
                                            <img class="img-circle" src="<?= base_url() ?>img/icons/nopicture.png" id="foto-curriculo" title="<? echo $row->nome ?>" width="100%" />
                                        <? } ?>
                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <input type="file"  class="btn btn-default" onchange="document.upload.submit()" name="userfile" id="userfile" value="ESCOLHER FOTO" />
                                    <span><small class="text-navy">Selecione e envie sua foto!</small></span>
                                </div>

                                <div class="col-lg-4">
                                    <p align="right">
                                        <a href="<?= base_url() ?>pdfController/gerarCurriculo" title="Exportar Currículo"  onclick=""><i style="font-size:50px;color:red" class="fa fa-file-pdf-o"></i></a>
                                        <? if ($row->video != "") { ?>
                                            <a title="Vídeo Apresentação" data-toggle="modal" data-target="#myModalVideo" onclick="alteraAtributoVideo('<?= $row->video ?>')"><i style="font-size:50px" class="fa fa-file-video-o"></i></a>
                                        <? } ?>
                                    </p>
                                </div>
                            </form>	
                        </div>	



                        <form method="post" id="formDadosPessoais" action="<?= base_url() ?>profissionalController/editProfissional" >


                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Nome Completo <span style="color:red">*</span></label><br>
                                        <input type="text" name="nome" id="nome" class="form-control" value="<?= $row->nome ?>" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>E-mail <span style="color:red">*</span></label><br>
                                        <input type="text" name="email" readonly="true" id="email" class="form-control" value="<?= $row->email ?>" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group" id="data_1">
                                        <label>Nascimento</label><br>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="nascimento" id="nascimento" value="<?= ($row->nascimento == "" || $row->nascimento == "0000-00-00") ? "" : implode("/", array_reverse(explode("-", $row->nascimento))) ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Sexo <span style="color:red">*</span></label><br>
                                        <select name="sexo" id="sexo" class="form-control">
                                            <option value="">Selecione</option>
                                            <option <?= $row->sexo == "Feminino" ? "selected" : "" ?> value="Feminino">Feminino</option>
                                            <option <?= $row->sexo == "Masculino" ? "selected" : "" ?> value="Masculino">Masculino</option>
                                        </select>
                                    </div>
                                </div>	 
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Telefone <span style="color:red">*</span></label><br>
                                        <input type="text" name="telefone" id="telefone" class="telefone form-control"  value="<?= $row->telefone ?>" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Estado <span style="color:red">*</span></label><br>
                                        <select name="estado" id="estado" class="form-control">
                                            <option value="">Selecione</option>
                                            <? foreach ($estados as $estado) { ?>
                                                <option <?= $row->estado == $estado->id ? "selected" : "" ?> value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade <span style="color:red">*</span></label><br>
                                        <select name="cidade" id="cidade" class="form-control">
                                            <option value="">Selecione</option>    
                                            <? foreach ($cidades as $cidade) { ?>
                                                <option <?= $row->cidade == $cidade->id ? "selected" : "" ?> value="<?= $cidade->id ?>"><?= $cidade->nome ?></option>
                                            <? } ?>    
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Pretensão salarial <span style="color:red">*</span></label><br>
                                        <input type="text" name="pretensao" id="pretensao" class="form-control money"  value="<?= number_format($row->pretensao, 2, ',', ''); ?>" />
                                    </div>
                                </div> 

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Facebook</label><br>
                                        <input type="text" name="facebook" id="facebook" class="form-control"  value="<?= $row->facebook ?>" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Linkedin</label><br>
                                        <input type="text" name="linkedin" id="linkedin" class="form-control"  value="<?= $row->linkedin ?>" />
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Repositório (GIT, Portfólio)</label><br>
                                        <input type="text" name="repositorio" id="repositorio" class="form-control"  value="<?= $row->repositorio ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Resumo<span style="color:red">*</span><small class="text-navy">(Preencha um breve resumo sobre você e suas experiências. O mesmo será demonstrado para as empresas)</small></label><br>
                                        <textarea class="form-control" name="resumo" id="resumo" rows="4" class="form-control"><?= $row->resumo ?></textarea>
                                    </div>
                                </div>
                                
                                 <div class="col-sm-12">
                                    <div class="form-group"> 
                                        <label>Busco vagas de nível:</label><br>
                                        <div class="checkbox i-checks">
                                        <label>
                                            <input type="checkbox" name="nivelCarreira[]" 
                                                   <?= strripos($row->nivelCarreira, "Estagiario") === false ? "" : "checked" ?> value="Estagiario"/>
                                            <i></i> Estagiário</label>
                                            
                                            <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Trainee") === false ? "" : "checked" ?> value="Trainee"/>
                                                <i></i> Trainee </label>
                                            
                                            <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Junior") === false ? "" : "checked" ?> value="Junior"/>
                                                <i></i> Júnior </label>
                                            
                                            <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Pleno") === false ? "" : "checked" ?> value="Pleno"/>
                                                <i></i> Pleno </label>
                                             <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Senior") === false ? "" : "checked" ?> value="Senior"/>
                                                <i></i> Sênior</label>
                                            
                                            <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Supervisor") === false ? "" : "checked" ?> value="Supervisor"/>
                                                <i></i> Supervisor</label>
                                            
                                            <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Coordenador") === false ? "" : "checked" ?> value="Coordenador"/>
                                                <i></i> Coordenador</label>
                                            
                                             <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Gerente") === false ? "" : "checked" ?> value="Gerente"/>
                                                <i></i> Gerente</label>
                                            
                                            <label>
                                                <input type="checkbox" name="nivelCarreira[]" <?= strripos($row->nivelCarreira, "Diretor") === false ? "" : "checked" ?> value="Diretor"/>
                                                <i></i> Diretor</label>
                                        </div>
                                   
                                    </div>
                                </div>    

                                <div class="col-lg-12 no_print">
                                    <div class="form-group">
                                        <input type="button" value="Voltar" class="btn btn-default" onclick="location.href = '<?= base_url() ?>area-restrita'"  />
                                        <input type="button" class="btn btn-primary" onclick="validaDados()" name="btSalvar" id="btSalvar" value="Salvar Dados Pessoais" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <? //if($porcentagemPreenchida >= 40){?>	
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>Formação educacional  </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>

                        <div class="ibox-content" id="divFormacoes">
                            <?php if ($editFormacao == "") { ?>
                                <div class="row">
                                    <div class="col-lg-12">

                                        <input type="button" class="btn btn-success btn-xs no_print" name="btNovo" onclick="$('#divFormacaoEducacional').css('display', 'block')"  style="float:right;margin-left:23px" value="Adicionar Nova Formação" />

                                    </div>
                                </div>


                                <form id="formFormacao" action="<?= base_url() ?>profissionalController/formacao/<?= $row->id ?>" method="post" 
                                      onsubmit="return validarDataFormacao()" >

                                    <div class="row" id="divFormacaoEducacional" style="display:none">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Instituição</label><br>
                                                <input type="text" name="instituicao" id="instituicao" class="form-control profissional_instituicao" value="" />
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nível</label><br>
                                                <select class="form-control" name="formacao" id="formacao">
                                                    <option value="">Selecione o nível de formação</option>
                                                    <option value="Ensino Médio" >Ensino Médio</option>
                                                    <option value="Cursos" >Cursos</option>
                                                    <option value="Técnico" >Técnico</option>
                                                    <option value="Graduação" >Graduação</option>
                                                    <option value="Especialização" >Especialização</option>
                                                    <option value="MBA" >MBA</option>
                                                    <option value="Mestrado" >Mestrado</option>
                                                    <option value="Doutorado" >Doutorado</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Curso</label><br>
                                                <input type="text" name="curso" id="curso" class="form-control profissional_curso" value="" />
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Data de Início</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataInicioFormacao" id="dataInicioFormacao" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Data Fim</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataFimFormacao" id="dataFimFormacao" value="">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input type="button" value="Cancelar" class="btn btn-default" onclick="$('#divFormacaoEducacional').css('display', 'none')" />
                                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar Formação" />
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            <? } else { ?>  
                                <form id="formFormacao" action="<?= base_url() ?>profissionalController/editFormacao" method="post" 
                                      onsubmit="return validarDataFormacao()" >

                                    <div class="row" id="divFormacaoEducacional" style="display:block;">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Instituição</label><br>
                                                <input type="hidden" name="idForm" value="<?= $formacao[0]->id ?>" id="idExp"/>
                                                <input type="text" name="instituicao" id="instituicao" class="form-control profissional_instituicao" value="<? echo $formacao[0]->instituicao ?>" />
                                            </div>
                                        </div>



                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nível</label><br>
                                                <select class="form-control" name="formacao" id="formacao">
                                                    <option value="">Selecione o nível de formação</option>
                                                    <option value="Ensino Médio" <? echo $formacao[0]->formacao == "Ensino Médio" ? "selected" : "" ?>>Ensino Médio</option>
                                                    <option value="Cursos" <? echo $formacao[0]->formacao == "Cursos" ? "selected" : "" ?>>Cursos</option>
                                                    <option value="Técnico" <? echo $formacao[0]->formacao == "Técnico" ? "selected" : "" ?>>Técnico</option>
                                                    <option value="Graduação" <? echo $formacao[0]->formacao == "Graduação" ? "selected" : "" ?>>Graduação</option>
                                                    <option value="Especialização" <? echo $formacao[0]->formacao == "Especialização" ? "selected" : "" ?>>Especialização</option>
                                                    <option value="MBA" <? echo $formacao[0]->formacao == "MBA" ? "selected" : "" ?>>MBA</option>
                                                    <option value="Mestrado" <? echo $formacao[0]->formacao == "Mestrado" ? "selected" : "" ?>>Mestrado</option>
                                                    <option value="Doutorado" <? echo $formacao[0]->formacao == "Doutorado" ? "selected" : "" ?>>Doutorado</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Curso</label><br>
                                                <input type="text" name="curso" id="curso" class="form-control profissional_curso" value="<? echo $formacao[0]->curso ?>" />
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Data de Início</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataInicioFormacao" id="dataInicioFormacao" value="<?= implode("/", array_reverse(explode("-", $formacao[0]->dataInicio))) ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Data Fim</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataFimFormacao" id="dataFimFormacao" value="<? echo implode("/", array_reverse(explode("-", $formacao[0]->dataFim))) ?>">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input type="button" value="Cancelar" class="btn btn-default" onclick="location.href = '<?= base_url() ?>profissionalController/editarProfissionalAction'" />
                                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar Formação" />
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            <? } ?>
                            <table class="table table-hover">
                                <thead>
                                <th>Instituição</th>
                                <th>Formação</th>
                                <th>Data de início</th>
                                <th>Data fim</th>
                                <th>Curso</th>


                                <th align="center" class="no_print">Ações</th>
                                </thead>
                                <? foreach ($formacoes as $formacao) { ?>
                                    <tr>
                                        <td> <?= $formacao->instituicao ?></td>
                                        <td> <?= $formacao->formacao ?></td>
                                        <td> <?= implode("/", array_reverse(explode("-", $formacao->dataInicio))) ?> </td>
                                        <td> <?= implode("/", array_reverse(explode("-", $formacao->dataFim))) == "00/00/0000" ? " - " : implode("/", array_reverse(explode("-", $formacao->dataFim))) ?></td>

                                        <td> <?= $formacao->curso ?></td>
                                        <td align="center" class="no_print">
                                            <a  href="<?= base_url() ?>profissionalController/editarFormacao/<?= $formacao->id ?>/#divFormacoes">Editar</a>|
                                            <a href="<?= base_url() ?>profissionalController/excluirFormacao/<?= $formacao->id ?>">Excluir</a>
                                        </td>
                                    </tr>
                                <? } ?>
                            </table>
                        </div>	
                    </div>

                    <div class="ibox float-e-margins" id="divExperiencias">

                        <div class="ibox-title">
                            <h5>Experiências profissionais  </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>

                        <div class="ibox-content">	



                            <?php if ($editExperiencia == "") { ?>
                                <div class="row" >
                                    <div class="col-lg-12">

                                        <input type="button" class="btn btn-success btn-xs no_print" name="btNovo" onclick="$('#divExperienciaProfissional').css('display', 'block')"  style="float:right;margin-left:23px" value="Adicionar Nova Experiência" />

                                    </div>
                                </div>
                                <form id="formExperiencia" action="<?= base_url() ?>profissionalController/experiencia/<?= $row->id ?>" method="post" 
                                      onsubmit="return validarData()" >

                                    <div class="row" id="divExperienciaProfissional" style="display:none;">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Empresa</label><br>
                                                <input type="text" name="empresa" id="empresa" class="form-control profissional_empresa" value="" />
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Cargo</label><br>
                                                <input type="text" name="cargo" id="cargo" class="form-control profissional_cargo" value="" />
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Data de Início</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataInicio" id="dataInicio" value="">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-lg-2" id="campoDataFim" style="display:block">
                                            <div class="form-group">
                                                <label>Data Fim</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataFim" id="dataFim" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Emprego atual</label><br>
                                                <input type="checkbox" name="empregoAtual" onclick="escondeMostrarCampo()" id="emprego" value="1" />
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Resumo das atividades</label><br>
                                                <textarea name="descricaoAtividades" id="descricaoAtividades" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input type="button" value="Cancelar" class="btn btn-default" onclick="$('#divExperienciaProfissional').css('display', 'none')" />
                                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar Experiências" />
                                            </div>
                                        </div>
                                    </div>		
                                </form>
                            <?php } else { ?>
                                <form id="formExperiencia" action="<?= base_url() ?>profissionalController/editExperiencia" method="post" 
                                      onsubmit="return validarData()" >

                                    <div class="row" id="divExperienciaProfissional" style="display:block;">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <input type="hidden" name="idExp" value="<?= $experiencia[0]->id ?>" id="idExp"/>
                                                <label>Empresa</label><br>
                                                <input type="text" name="empresa" id="empresa" class="form-control profissional_empresa" value="<?= $experiencia[0]->empresa ?>" />
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Cargo</label><br>
                                                <input type="text" name="cargo" id="cargo" class="form-control profissional_cargo" value="<?= $experiencia[0]->cargo ?>" />
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Data de Início</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataInicio" id="dataInicio" value="<?= implode("/", array_reverse(explode("-", $experiencia[0]->dataInicio))) ?>">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-lg-2" id="campoDataFim" style="display:block">
                                            <div class="form-group">
                                                <label>Data Fim</label><br>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="dataFim" id="dataFim" value="<?= implode("/", array_reverse(explode("-", $experiencia[0]->dataFim))) ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Emprego atual</label><br>
                                                <? if ($experiencia[0]->empregoAtual == "1") { ?>
                                                    <script>document.getElementById("campoDataFim").style.display = "none";</script>
                                                <? } ?>
                                                <input type="checkbox" name="empregoAtual" <?= $experiencia[0]->empregoAtual == "1" ? "checked" : "" ?> onclick="escondeMostrarCampo()" id="emprego" value="1" />
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Resumo das atividades</label><br>
                                                <textarea name="descricaoAtividades" id="descricaoAtividades" class="form-control"><?= $experiencia[0]->descricaoAtividade ?></textarea>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input type="button" value="Cancelar" class="btn btn-default" onclick="location.href = '<?= base_url() ?>profissionalController/editarProfissionalAction'" />
                                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar Experiências" />
                                            </div>
                                        </div>
                                    </div>		
                                </form>    
                            <?php } ?>
                            <table class="table table-hover" >
                                <thead>
                                <th>Empresa</th>
                                <th>Cargo</th>
                                <th>Data de início</th>
                                <th>Data fim</th>
                                <th>Emprego atual</th>
                                <th>Resumo das atividades</th>

                                <th class="no_print" align="center">Ações</th>
                                </thead>
                                <? foreach ($experiencias as $experiencia) { ?>
                                    <tr>
                                        <td> <?= $experiencia->empresa ?></td>
                                        <td> <?= $experiencia->cargo ?></td>
                                        <td> <?= implode("/", array_reverse(explode("-", $experiencia->dataInicio))) ?> </td>
                                        <td> <?= implode("/", array_reverse(explode("-", $experiencia->dataFim))) == "00/00/0000" ? " - " : implode("/", array_reverse(explode("-", $experiencia->dataFim))) ?></td>
                                        <td> <?= $experiencia->empregoAtual == 1 ? "Sim" : "Não" ?></td>
                                        <td> <?= $experiencia->descricaoAtividade ?></td>
                                        <td class="no_print">
                                            <a  href="<?= base_url() ?>profissionalController/editarExperiencia/<?= $experiencia->id ?>/#divExperiencias">Editar</a>|
                                            <a href="<?= base_url() ?>profissionalController/excluirExperiencia/<?= $experiencia->id ?>">Excluir</a>
                                        </td>
                                    </tr>
                                <? } ?>
                            </table>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>SKILLS  </h5> <small class="text-navy"> ( A skill será utilizada para calcular a porcentagem de compabilidade com as vagas)</small>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>

                        <div class="ibox-content">		

                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="button" data-toggle="modal" data-backdrop="static" data-target="#myModalSkill" class="btn btn-success btn-xs no_print" style="float:right;margin-left:23px"  name="btNovaSkill" value="Adicionar Nova Skill" />

                                </div>       
                            </div>	

                            <div class="row">
                                <div class="col-lg-12">
                                    <ul style="list-style: none;padding-left: 0;margin: 0;">
                                        <? foreach ($profissional_skills as $ps) { ?>
                                            <li style="border: 1px solid #59b2e5;background: #FFF;padding: 5px;float: left;
                                                margin-right: 5px;margin-bottom: 5px;color: #59b2e5;"><? echo $ps->skill ?><a class="no_print" href="<? echo base_url() ?>profissionalController/excluirSkillProfissional/<? echo $ps->id ?>/<? echo $ps->idProfissional ?>"><img style="margin-left:10px" src="<? echo base_url() ?>img/excluir.png" /></a></li>
                                            <? } ?>	
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <? //}?>

                </div>	



            <? } ?>

        </div>
    </div>	

</div>
<div class="modal inmodal fade" id="myModalVideo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-file-video-o modal-icon"></i>
                <h4 class="modal-title">Vídeo Apresentação</h4>                                        
            </div>

            <div class="modal-body">
                <iframe width="425" id="frameVideo" height="349" src="" frameborder="0"></iframe>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>                    
            </div>

        </div>
    </div>
</div>	

<div class="modal inmodal fade" id="myModalSkill" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>

                <h4 class="modal-title">Skills disponíveis</h4>                                        
            </div>

            <form id="formSkillAuto" name="formSkillAuto" method="POST" action="<? echo base_url() ?>profissionalController/addSkillProfissional">

                <div class="modal-body">

                    <table class="table table-striped table-bordered table-hover" id="dataTables-skill" >
                        <thead>
                        <th></th>
                        <th>Skill</th>
                        </thead>

                        <? foreach ($skills as $sk) { ?>
                            <tr>
                                <td> <input type="checkbox" id="lista" value="<?= $sk->id ?>" name="lista[]"/></td>
                                <td> <?= $sk->skill ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" name="btAdicionar" value="Adicionar" />
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>                    
                </div>
            </form>    


        </div>
    </div>
</div>	

<?php
$this->load->view('priv/profissional/_inc/inferior');
?>
