<? $this->load->view('priv/profissional/_inc/superior'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h1>Seja um profissional PREMIUM</h1>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita">Home</a>
            </li>
            <li class="active">
                <strong>Planos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row profissional wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12 avisoPlano alignLeft">
            <b>Vantagens de ser um profissional PREMIUM:</b> <br> <img src="<?= base_url() ?>_imagens/beneficios/2.png" alt=""/> Candidatura imediata às vagas publicadas. Aumentando as chances em até 70% na contratação;
            <br> <img src="<?= base_url() ?>_imagens/beneficios/4.png" alt=""> Receba notificações de empresas que estão vendo seu currículo!
            <br><img src="<?= base_url() ?>_imagens/beneficios/unlock.png" alt=""/> Acesso a conteúdos e descontos exclusivos do Clube GO; 
            <br> <img src="<?= base_url() ?>_imagens/beneficios/4.png" alt=""> Análise de currículo!
        </div>
    </div>


    <? foreach ($planos_profissional as $plano) { ?>                
        <div class="col-md-4">
            <div class="plano <?= $plano->plano ?>">
                <h2>Assinatura mensal</h2>
                <h3>R$ <?= $plano->valorPlano ?></h3>
                <? if ($plano->tipoPlano == 'FREE') { ?>  <a href="<?= base_url() ?>planoController/planoFree/<?= $plano->id ?>" class="btn btn-base btn-icon btn-check">COMPRAR</a> <? } else { ?>  <a data-toggle="modal" data-backdrop="static" data-target="#myModalPagamento" class="btn btn-base btn-icon btn-check">COMPRAR</a> <? } ?>     
            </div>       
        </div>	
    <? } ?>
</div>

<div class="modal inmodal fade" id="myModalPagamento" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" onclick="limparResultado()" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>

                <h4 class="modal-title">Dados da Compra</h4>  

            </div>
            <div class="modal-body">
                <div class="alert alert-danger" style="display:none;" id="divErro">
                    <ul id="resulAlert">
                    </ul>
                </div>

                <form class="form-horizontal" method="post">
                   
                    <div class="row">
                        <legend>1) Informações Pessoais</legend>

                        <div class="col-lg-12">
                            <input type="hidden" id="idP" name="idP" value="<?= $profissional[0]->id ?>"/>

                            <div class="form-group">
                                <div class="col-lg-4">   
                                    <label for="fullname">Nome</label><br>
                                    <input type="hidden" name="fullname" id="fullname" value="<?= $profissional[0]->nome ?>"/>
                                    <p class="form-control-static">
                                        <?= $profissional[0]->nome ?>
                                    </p>
                                </div>
                                <div class="col-lg-4">    
                                    <label for="email">E-mail</label><br>                                    
                                    <input type="hidden" name="email" id="email" value="<?= $profissional[0]->email ?>"/>
                                    <p class="form-control-static">
                                        <?= $profissional[0]->email ?>
                                    </p>
                                </div>
                                <div class="col-lg-4"> 
                                    <label for="cpf">CPF</label><br>
                                    <input id="cpf" maxlength="11" name="cpf" class="form-control" placeholder="Somente números" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-4"> 
                                    <label for="phone">DDD</label><br>
                                    <input class="form-control" id="ddd" maxlength="2" name="ddd" placeholder="00" type="text">
                                </div>
                                <div class="col-lg-4"> 
                                    <label for="phone">Telefone</label><br>
                                    <input class="form-control" id="phone" maxlength="9" name="phone" placeholder="00000000" type="text">
                                </div>
                                <div class="col-lg-4"> 
                                    <label for="birthdate_day">Nascimento</label><br>
                                    <input class="form-control" id="birthdate_date" maxlength="10" value="<?= implode("/", array_reverse(explode("-", $profissional[0]->nascimento)))   ?>" name="birthdate_date" placeholder="DD/MM/YYYY" type="text">
                                </div>

                            </div>


                            <div class="form-group">
                                <div class="col-lg-5"> 
                                    <label for="rua">Rua</label> <br>
                                    <input id="rua" maxlength="45" class="form-control" name="rua" placeholder="Rua, Avenida, Praça..." type="text">
                                </div>

                                <div class="col-lg-2"> 
                                    <label for="numero">Número</label><br>
                                    <input class="form-control" id="numero" maxlength="20" name="numero" placeholder="Número" type="text">

                                </div>

                                <div class="col-lg-5"> 
                                    <label for="complemento">Complemento</label><br>
                                    <input class="form-control" id="complemento" maxlength="45" name="complemento" placeholder="Complemento" type="text">
                                </div>

                            </div>


                            <div class="form-group">
                                <div class="col-lg-5"> 
                                    <label for="bairro">Bairro</label><br>
                                    <input class="form-control" id="bairro" maxlength="45" name="bairro" placeholder="Bairro" type="text">
                                </div>

                                <div class="col-lg-3"> 
                                    <label  for="cep">CEP</label><br>
                                    <input class="form-control" id="cep" maxlength="9" name="cep" placeholder="00000-000" type="text">
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-lg-5"> 
                                    <label for="estado">Estado</label><br>

                                    <select name="cbestado" class="form-control" id="cbestado">
                                        <option value="">Selecione o Estado (UF)</option>
                                        <option <?= $profissional[0]->descEstado == "Acre" ? "selected" : "" ?>  value="Acre">Acre</option>
                                        <option <?= $profissional[0]->descEstado == "Alagoas" ? "selected" : "" ?> value="Alagoas">Alagoas</option>
                                        <option <?= $profissional[0]->descEstado == "Amapá" ? "selected" : "" ?> value="Amapá">Amapá</option>
                                        <option <?= $profissional[0]->descEstado == "Amazonas" ? "selected" : "" ?> value="Amazonas">Amazonas</option>
                                        <option <?= $profissional[0]->descEstado == "Bahia" ? "selected" : "" ?> value="Bahia">Bahia</option>
                                        <option <?= $profissional[0]->descEstado == "Ceará" ? "selected" : "" ?> value="Ceará">Ceará</option>
                                        <option <?= $profissional[0]->descEstado == "Distrito Federal" ? "selected" : "" ?> value="Distrito Federal">Distrito Federal</option>
                                        <option <?= $profissional[0]->descEstado == "Espírito Santo" ? "selected" : "" ?> value="Espírito Santo">Espírito Santo</option>
                                        <option <?= $profissional[0]->descEstado == "Goiás" ? "selected" : "" ?> value="Goiás">Goiás</option>
                                        <option <?= $profissional[0]->descEstado == "Maranhão" ? "selected" : "" ?> value="Maranhão">Maranhão</option>
                                        <option <?= $profissional[0]->descEstado == "Mato Grosso" ? "selected" : "" ?> value="Mato Grosso">Mato Grosso</option>
                                        <option <?= $profissional[0]->descEstado == "Mato Grosso do Sul" ? "selected" : "" ?> value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                        <option <?= $profissional[0]->descEstado == "Minas Gerais" ? "selected" : "" ?> value="Minas Gerais">Minas Gerais</option>
                                        <option <?= $profissional[0]->descEstado == "Pará" ? "selected" : "" ?> value="Pará">Pará</option>
                                        <option <?= $profissional[0]->descEstado == "Paraíba" ? "selected" : "" ?> value="Paraíba">Paraíba</option>
                                        <option <?= $profissional[0]->descEstado == "Paraná" ? "selected" : "" ?> value="Paraná">Paraná</option>
                                        <option <?= $profissional[0]->descEstado == "Pernambuco" ? "selected" : "" ?> value="Pernambuco">Pernambuco</option>
                                        <option <?= $profissional[0]->descEstado == "Piauí" ? "selected" : "" ?> value="Piauí">Piauí</option>
                                        <option <?= $profissional[0]->descEstado == "Rio de Janeiro" ? "selected" : "" ?> value="Rio de Janeiro">Rio de Janeiro</option>
                                        <option <?= $profissional[0]->descEstado == "Rio Grande do Sul" ? "selected" : "" ?> value="Rio Grande do Sul">Rio Grande do Sul</option>
                                        <option <?= $profissional[0]->descEstado == "Rio Grande do Norte" ? "selected" : "" ?> value="Rio Grande do Norte">Rio Grande do Norte</option>
                                        <option <?= $profissional[0]->descEstado == "Rondônia" ? "selected" : "" ?> value="Rondônia">Rondônia</option>
                                        <option <?= $profissional[0]->descEstado == "Roraima" ? "selected" : "" ?> value="Roraima">Roraima</option>
                                        <option <?= $profissional[0]->descEstado == "Santa Catarina" ? "selected" : "" ?> value="Santa Catarina">Santa Catarina</option>
                                        <option <?= $profissional[0]->descEstado == "São Paulo" ? "selected" : "" ?> value="São Paulo" >São Paulo</option>
                                        <option <?= $profissional[0]->descEstado == "Sergipe" ? "selected" : "" ?> value="Sergipe">Sergipe</option>
                                        <option <?= $profissional[0]->descEstado == "Tocantins" ? "selected" : "" ?> value="Tocantins">Tocantins</option>
                                    </select>
                                </div>

                                <div class="col-lg-5"> 
                                    <label for="cidade">Cidade</label><br>
                                    <input  class="form-control" id="ipcidade" maxlength="32" name="ipcidade" value="<?= $profissional[0]->cid ?>" placeholder="Cidade" type="text">
                                </div>
                            </div>

                        </div>


                        <legend>2) Informações de Cobrança</legend>
                        <div class="col-lg-12">
                           

                            <div class="form-group">
                                <div class="col-lg-5"> 
                                    <label for="holder_name">Nome do Portador</label><br>
                                    <input id="holder_name" class="form-control" maxlength="45" name="holder_name" placeholder="Nome escrito no cartão" type="text">
                                </div>

                                <div class="col-lg-5"> 
                                    <label for="credit_card">Número Cartão de Credito</label><br>
                                    <input id="credit_card" class="form-control" maxlength="16" name="credit_card" placeholder="XXXXXXXXXXXX1234" type="text">
                                </div>

                            </div>



                           <div class="form-group">
                               <div class="col-lg-5"> 
                                   <label for="credit_card">Data de Expiração</label><br>
                                 <select  name="expiration_month" id="expiration_month" class="input-mini">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12" selected="selected">12</option>
                                    </select> / <select name="expiration_year" id="expiration_year" class="input-small">
                                        <option value="13">2013</option>
                                        <option value="14">2014</option>
                                        <option value="15">2015</option>
                                        <option value="16">2016</option>
                                        <option value="17">2017</option>
                                        <option value="18">2018</option>
                                        <option value="19">2019</option>
                                        <option value="20" selected="selected">2020</option>
                                    </select>
                                </div>
                            </div>
                             Pagamento através do MOIP <img src="<?= base_url() ?>img/moip.png" alt="Pagamento Seguro MOIP"/>
                             <div class="form-actions">
                            <a href="#" class="btn btn-large btn-primary" id="assinar">Assinar</a>
                        </div>
                        </div>
                        
                </form>
            </div>

        </div>
        <div class="modal-footer">

            <button type="button" onclick="limparResultado()" class="btn btn-white" data-dismiss="modal">Fechar</button>                    
        </div>



    </div>
</div>
</div>	

<? $this->load->view('priv/profissional/_inc/inferior'); ?>
