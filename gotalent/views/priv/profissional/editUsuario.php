<? $this->load->view('priv/profissional/_inc/superior'); ?>
    
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h1>Usuário</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<? echo base_url() ?>area-restrita">Home</a>
                </li>
                <li class="active">
                    <strong>Dados de Acesso</strong>
                </li>
            </ol>
        </div>
    </div>
	
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
            <div class="col-lg-12">
            
                <div class="ibox float-e-margins">	
                    <div class="ibox-title">
                    	<h5>Dados de Acesso</h5>
                    </div>
				
                <div class="ibox-content">	
					<?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error . ' </div>' : "" ?>
                    
                    <form method="post" action="<?= base_url() ?>profissionalController/editUsuario" class="form-horizontal">
                       
                        <div class="form-group">
                            <div class="col-sm-3">
                            	<label class="control-label">Login</label>
                                <p class="form-control-static"><?=$usuario[0]->login?></p>
                            </div>                                
                            <div class="col-sm-4">
                           		<label class="control-label">Nova Senha</label>
                                <input type="text" name="senha" id="senha" class="form-control"  value="" />
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar" style="float:right;margin:30px 0 0 10px" />
                                <input type="button" value="Voltar" class="btn btn-white" onclick="location.href='<?= base_url() ?>empresaController'" style="float:right;margin-top:30px"  />
                            </div>		
                        </div>
					</form>			
				</div>
			</div>
		</div>
	</div>
</div>

<? $this->load->view('priv/empresa/_inc/inferior'); ?>
