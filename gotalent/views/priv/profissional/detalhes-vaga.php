<? $this->load->view('priv/profissional/_inc/superior');?>



<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-6">
		<h2>Detalhes da vaga:  <? echo $vaga[0]->vaga." (".$vaga[0]->quantidade.")" ?></h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?echo base_url()?>area-restrita">Home</a>
			</li>
			<li class="active">
				<strong>Detalhes da vaga</strong>
			</li>
		</ol>
	</div>
</div>
		
<div class="wrapper wrapper-content animated fadeInUp">
	
	<div class="row">
		<div class="col-lg-12">
			
		  <div class="ibox float-e-margins">	
			<div class="ibox-title">
				  <h5>Dados da vaga </h5>
			</div>
			
			<div class="ibox-content">	
	
	
		
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success"> ' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
			<?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger"> ' . $this->session->flashdata('error') . ' </div>' : "" ?>
	
		   
		    <form class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label">Vaga</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?echo $vaga[0]->vaga." (".$vaga[0]->quantidade.")"?></p>
					</div>
				</div>
			    <div class="form-group">
					<label class="col-sm-2 control-label">Descrição dos requisitos da vaga</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?=$vaga[0]->descricao?></p>
					</div>
				</div>
				<?if($vaga[0]->diferenciais!= ""){?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Diferenciais</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?echo $vaga[0]->diferenciais?></p>
					</div>
				</div>
				<?}?>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Profissional</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?echo str_replace('Senior', ' Sênior', str_replace('Junior', ' Júnior', $vaga[0]->tipoProfissional))?></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Tipo de contratação</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?echo $vaga[0]->tipo;?></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Faixa salarial</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?echo $vaga[0]->salario?></p>
					</div>
				</div>
				<?if(count($beneficios_vaga)>0){?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Benefícios</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><? 
							foreach($beneficios_vaga as $beneficio){
								echo $beneficio->beneficio.", ";
							}
						?> </p>
					</div>
				</div>
				<?}?>
				<?if($vaga[0]->informacoesAdicionais!= ""){?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Informações adicionais</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><?echo $vaga[0]->informacoesAdicionais?></p>
					</div>
				</div>
				<?}?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Skills</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><? 
							foreach($vaga_skills as $skill){
								echo $skill->skill.", ";
							}
						?> </p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Empresa</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><? if($profissional[0]->tipoPlano == 'PREMIUM'){?>
							<? echo $vaga[0]->razaosocial?>
						<?}?>
						<? if($profissional[0]->tipoPlano == 'FREE'){?>
							Exclusivo para assinantes
						<?}?></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Sobre a Empresa</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><? if($profissional[0]->tipoPlano == 'PREMIUM'){?>
							<? echo $vaga[0]->sobre?>
						<?}?>
						<? if($profissional[0]->tipoPlano == 'FREE'){?>
							Exclusivo para assinantes
						<?}?></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Site</label>
					<div class="col-sm-8">	
						<p class="form-control-static"><? if($profissional[0]->tipoPlano == 'PREMIUM'){?>
							<? echo $vaga[0]->site?>
						<?}?>
						<? if($profissional[0]->tipoPlano == 'FREE'){?>
							Exclusivo para assinantes
						<?}?></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Local de trabalho</label>
					<div class="col-sm-8">	
						<p class="form-control-static"<? echo $vaga[0]->cidade."/".$vaga[0]->estado?></p>
					</div>
				</div>
				
			<div class="hr-line-dashed"></div>
				<div class="form-group">
				
				<? if($profissional[0]->tipoPlano == 'PREMIUM'){?>
					
					<? $candidatou = "N";
					foreach($candidaturas as $cand){
						if($cand->idVaga == $vaga[0]->id ){
							$candidatou = "S";
							break;
						}
					}
					if($candidatou == 'N'){
					?>
						<div class="col-sm-4 col-sm-offset-2">
						<input type="button" onclick="window.location.href='<? echo base_url()?>principal/candidatar/<? echo $vaga[0]->id?>'" class="btn btn-success" name="btSalvar" value="Candidatar" />
						</div>
					<?} else {?>
					<div class="col-sm-4 col-sm-offset-2">
						<p style="margin-top:30px; color:green">Já me candidatei!</p>
					</div>	
					<?}?>												
					
				<?}?>
				<? if($profissional[0]->tipoPlano == 'FREE'){?>
				<div>
					<input type="button" class="btn btn-success" style="margin-top:20px" onclick="location.href='<? echo base_url()?>principal/plano/'" name="btSalvar" value="Quero ser Premium" />
				</div>	
				<?}?>
			</div>	
			</form>		
			
					
					
						
			
			
					
				</div>
			
		</div>
							
	
</div>
</div>


<? $this->load->view('priv/profissional/_inc/inferior'); ?>
