<?
$this->load->view('priv/profissional/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Clube Go Talent</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita">Home</a>
            </li>
            <li class="active">
                <strong>Clube Go Talent</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Fique por dentro dos maiores eventos que acontecerão em 2015! </h5>
                </div>
                <div class="ibox-content">	

                    <table class="table table-hover">
                        

<thead>
            <tr>
                <th width="88px">Data</th>
                <th width="140px">Logo</th>
                <th width="148px">Nome do Evento</th>
                <th width="140px">Segmento</th>
                <th width="62px">UF</th>
                <th width="125px">Cidade</th>
                <th width="96px">Promotor</th>
                <th width="133px">Pavilhao</th>
            </tr>
        </thead>

                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        20/07/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/10a-eletrolar-show/"><img width="122" height="82" src="http://meuguru.com.br/wp-content/uploads/eletrolar-show-122x82.jpg" class="attachment-tutorial-thumb wp-post-image" alt="eletrolar-show" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        10ª Eletrolar Show   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Eletroeletrônica Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        Azul Play Feiras e Eventos    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Transamerica Expo Center        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        21/07/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/the-developers-conference-sao-paulo/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/The-Developers-Conference.jpg" class="attachment-tutorial-thumb wp-post-image" alt="TDC 2015" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        The Developer&#8217;s Conference São Paulo   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        Globalcode    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
                </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        23/07/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/campus-party-recife/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/campus-party-recife-2051.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Campus Party Recife 2015" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Campus Party Recife   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        PE    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Recife    </td>
    
    <td width="96" class="promotorFeiraTable">
        Futura Networks do Brasil Consultoria    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Centro de Convenções de Pernambuco        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        11/08/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/business-summit/"><img width="122" height="82" src="http://meuguru.com.br/wp-content/uploads/bitsbrasil-122x82.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Dedicado à inovação e tecnologias, empresas incubadas e aceleradoras tecnológicas" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Business IT Summit   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        RS    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Porto Alegre    </td>
    
    <td width="96" class="promotorFeiraTable">
        Hannover Fairs Sulamérica    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Centro de Eventos FIERGS        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        18/08/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/congresso-fecomercio-de-crimes-eletronicos/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/VI-Congresso-Fecomercio-de-Crimes-Eletrônicos.jpg" class="attachment-tutorial-thumb wp-post-image" alt="VI Congresso Fecomercio de Crimes Eletrônicos" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        VI Congresso Fecomercio de Crimes Eletrônicos   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        FECOMERCIO SP    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Fecomercio SP        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        24/08/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/qcon-rio-de-janeiro/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/QCon-Rio-de-Janeiro.jpg" class="attachment-tutorial-thumb wp-post-image" alt="QCon Rio de Janeiro 2015" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        QCon Rio de Janeiro   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        RJ    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Rio de Janeiro    </td>
    
    <td width="96" class="promotorFeiraTable">
        InfoQ    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Centro de Convenções SulAmérica        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        25/08/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/7a-feira-e-congresso-de-redes-e-telecom/"><img width="122" height="82" src="http://meuguru.com.br/wp-content/uploads/netcom-122x82.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Traz as mais recentes novidades em produtos e serviços" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        7ª Feira e Congresso de Redes e Telecom   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        Aranda Eventos    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Expo Center Norte        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        26/08/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/mind-the-sec/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Mind-the-Sec.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Mind the Sec" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Mind the Sec   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        FLIPSIDE    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Grand Hyatt São Paulo        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        09/09/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/infofair-brasil/"><img width="122" height="82" src="http://meuguru.com.br/wp-content/uploads/infofair-122x82.jpg" class="attachment-tutorial-thumb wp-post-image" alt="infofair" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Infofair Brasil   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SC    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Blumenau    </td>
    
    <td width="96" class="promotorFeiraTable">
        Via Ápia Eventos    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Parque de Exposições Vila Germânica        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        15/09/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/rio-info/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Rio-Info.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Rio Info 2015" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Rio Info   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        RJ    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Rio de Janeiro    </td>
    
    <td width="96" class="promotorFeiraTable">
        TI Rio - Sindicato das Empresas de Informática do Rio de Janeiro    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Royal Tulip Rio de Janeiro        </td>
    </tr>

          <tr valign="middle">
      
    <td width="88" class="dataTable">
        16/09/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/8a-mobile-payment-summit/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Mobile-Payment-Summit.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Mobile Payment Summit" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        8ª Mobile Payment Summit   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        Corpbusiness    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Hotel Transamérica São Paulo        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        22/09/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/8o-forum-mobile/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Forum-Mobile.jpg" class="attachment-tutorial-thumb wp-post-image" alt="É o evento que decifra as novidades do setor de mobilidade e suas aplicações." /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        8º Forum Mobile+   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        Converge Comunicações    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        World Trade Center São Paulo        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        09/10/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/brasil-game-show/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Brasil-Game-Show1.jpg" class="attachment-tutorial-thumb wp-post-image" alt="conta com a presença das maiores empresas de games do mundo." /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Brasil Game Show   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        BGS    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Expo Center Norte        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        15/10/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/15o-congresso-latino-americano-de-satelites/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Congresso-Latino-Americano-Satélites.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Visa reunir os decisores do mercado satelital." /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        15º Congresso Latino Americano de Satélites   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        RJ    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Rio de Janeiro    </td>
    
    <td width="96" class="promotorFeiraTable">
        Converge Comunicações    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Royal Tulip Rio de Janeiro        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        26/10/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/futurecom/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/Futurecom3.jpg" class="attachment-tutorial-thumb wp-post-image" alt="Futurecom 2015" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        Futurecom   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        Provisuale    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Transamerica Expo Center        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        17/11/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/3o-forum-expo/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/IT-Forum-Expo.jpg" class="attachment-tutorial-thumb wp-post-image" alt="IT Forum Expo 2015" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        3º IT Forum Expo   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        São Paulo    </td>
    
    <td width="96" class="promotorFeiraTable">
        UBM    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Transamerica Expo Center        </td>
    </tr>
  
 
                             
    
      <tr valign="middle">
      
    <td width="88" class="dataTable">
        02/12/2015    </td>
    
    <td width="145" class="imgTable">
    
        <span class="boxImgFeira"><a href="http://meuguru.com.br/feiras/php-conference-brasil/"><img width="119" height="82" src="http://meuguru.com.br/wp-content/uploads/PHP-Conference-Brasil.jpg" class="attachment-tutorial-thumb wp-post-image" alt="10º PHP Conference Brasil" /></a></span>
 

    </td>
    
    <td width="148" class="nomeFeiraTable">
        10º PHP Conference Brasil   </td>
   
    <td width="140" class="nomeSegmentoTable">
 Informática, Telecomunicações e TI    </td>
    
    <td width="62" class="estadoFeiraTable">
        SP    </td>
    
    <td width="125" class="cidadeFeiraTable">
        Osasco    </td>
    
    <td width="96" class="promotorFeiraTable">
        Tempo Real Eventos    </td>
    
    <td width="133" style="border-right: 1px dotted; border-bottom: 1px dotted; text-align: center; max-width:133px; vertical-align:middle;">
        Centro Universitário FIEO – UNIFIEO        </td>
    </tr>
  
 

                    </table>
                </div>

            </div>
        </div>	

    </div>
</div>



<?
$this->load->view('priv/profissional/_inc/inferior');
?>
