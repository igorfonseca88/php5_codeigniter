<?
$this->load->view('priv/profissional/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">


        <h1>Checklist - Resposta</h1>
        <ol class="breadcrumb">
            <li> <a href="<? echo base_url() ?>area-restrita">Home</a> </li>
            <li class="active"> <strong>Checklist - Resposta</strong> </li>
        </ol>
    </div>

</div>


<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Dados do Checklist </h5>
                </div>
                <div class="ibox-content">
                    <?= $sucesso != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $sucesso . ' </div>' : "" ?>
                    <?= $error != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ' . $error . ' </div>' : "" ?>
                    <?= $this->session->flashdata('sucesso') != "" ? '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('sucesso') . ' </div>' : "" ?>
                    <?= $this->session->flashdata('error') != "" ? '<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $this->session->flashdata('error') . ' </div>' : "" ?>

                    <form method="post"  class="form-horizontal" name="formChecklist">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Código</label>
                            <div class="col-sm-4">
                                <p class="form-control-static">
                                    <?= $checklist[0]->id ?>
                                </p>
                            </div>

                            <label class="col-sm-2 control-label">Vaga</label>
                            <div class="col-sm-4">
                                <p class="form-control-static">
                                    <?= $vaga[0]->vaga ?>
                                </p>
                            </div>

                        </div>



                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição do checklist</label>
                            <div class="col-lg-10">
                                <p class="form-control-static">
                                    <?= $checklist[0]->checklist ?>
                                </p>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Questões do checklist </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form id="formItemChecklist"  name="formItemChecklist" method="POST" action="<? echo base_url() ?>profissionalController/salvarChecklistAction">
                                
                                <div class="row">
                                    <? $i=1; foreach ($questoes_objetivas as $questao) {  ?>     

                                        <div class="form-group">
                                            <label class="col-sm-10 control-label"><?= $questao->questao ?></label>
                                            <input type="hidden" name="hcomponente<?= $i;?>" id="hcomponente<?= $i;?>" value="<?= $questao->id ?>"/>
                                            <div class="col-lg-2">
                                                <select name="componente<?= $i;?>" id="componente<?= $i;?>"  class="form-control">
                                                    
                                                    <option value="SIM">Sim</option>
                                                    <option value="NAO">Não</option>
                                                </select>
                                            </div>
                                        </div>
                                    <? $i++;} ?>    
                                </div>
                                <input type="hidden" name="ck" id="ck" value="<? echo $checklist[0]->id ?>"/>
                                <input type="hidden" name="v" id="v" value="<? echo $vaga[0]->id ?>"/>
                                <div class="row">
                                    <? $i=1;foreach ($questoes_discursivas as $questao) { ?>     

                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="hidden" name="hresposta<?= $i;?>" id="hresposta<?= $i;?>" value="<?= $questao->id ?>"/>
                                                <div class="form-group">
                                                    <label><?= $questao->questao ?></label><br>
                                                     <textarea name="resposta<?= $i;?>" id="resposta<?= $i;?>" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    <? $i++;} ?> 


                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            
                                            <input type="submit" class="btn btn-primary" name="btSalvar" value="Salvar e Candidatar" />
                                        </div>
                                    </div>
                                </div>

                            </form>

                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?
$this->load->view('priv/profissional/_inc/inferior');
?>
