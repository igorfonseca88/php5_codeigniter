<?
$this->load->view('priv/profissional/_inc/superior');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Meus processos seletivos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<? echo base_url() ?>area-restrita">Home</a>
            </li>
            <li class="active">
                <strong>Meus processos seletivos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInUp" >
    <div class="row" >
        <div class="col-lg-12">

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listagem de todos os processos</h5>
                </div>
                <div class="ibox-content">	

                    <table class="table table-hover">
                        <thead>
                        <th>Vaga</th>
                        <th>Empresa</th>
                        <th>Data da candidatura</th>
                        <th>Situação</th>  
                        <th>Vigência</th>
                        <th>Mensagem</th>


                        </thead>
                        <? foreach ($vagas as $row) { ?>
                            <tr>
                                <td> <?= $row->vaga ?></td>
                                <td> <?= $row->razaoSocial ?></td>
                                <td> <?= implode("/", array_reverse(explode("-", $row->data))) ?></td>
                                <td> <?= $row->situacao ?></td>
                                <td> <?= implode("/", array_reverse(explode("-", $row->dataVigencia))) ?></td>
                                <?
                                $msg = "";
                                switch ($row->situacao) {
                                    case 'Classificado':
                                        $msg = $row->msgClassificar;
                                        break;
                                    case 'Não Classificado':
                                        $msg = $row->msgNaoClassificar;
                                        break;
                                    case 'Entrevista':
                                        $msg = "<a class='link_video' href='".base_url()."videoEntrevistaController/novaVideoEntrevistaAction/$row->idVaga/$row->idProfissional'>Fazer entrevista</a>";
                                        break;
                                     case 'Contratado':
                                        $msg = "Parabéns! Vida longa e próspera!";
                                        break;
                                    default:
                                        break;
                                }
                                ?>    
                                <td> <?= $msg ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </div>

            </div>
        </div>	

    </div>
</div>



<?
$this->load->view('priv/profissional/_inc/inferior');
?>
