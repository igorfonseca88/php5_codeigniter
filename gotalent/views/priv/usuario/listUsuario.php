<?
$this->load->view('priv/_inc/superior');
?>
<script>
	function confirmaExcluir(id) {
		var r=confirm("Deseja excluir este usuário?")
		if (r==true) { location.href = "<?= base_url() ?>usuarioController/deleteUsuario/" + id; }
	}
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Usuários
				<input type="button" class="btn btn-success" name="btNovo" onclick="location.href='<?= base_url() ?>usuarioController/novoUsuarioAction'"  style="float:right" value="Cadastrar" />
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Usuários </div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			
			<table class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
				  <th>Nome</th>
				  <th>Tipo</th>
				  <th width="120">Situação</th>
				  <th width="120">Ações</th>
			  </thead>
			  <? foreach ($usuario as $row) { ?>
				 <tr>
					<td> <?= $row->nome ?></td>
					<td> <?= $row->tipo ?></td>
					<td> <?= $row->situacao ?></td>
					<td align="center">
						<a href="<?= base_url() ?>usuarioController/editarUsuarioAction/<?= $row->idUsuario ?>">Editar</a> |
						<a onclick="confirmaExcluir('<?= $row->id ?>')">Excluir</a>
					</td>
				 </tr>
			  <? } ?>
		   </table>
	    </div>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>