<?
$this->load->view('priv/_inc/superior');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Usuários</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>usuarioController/">Usuários</a> &raquo; Cadastrar</div>
			</div>
			<form method="post" action="addUsuario">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome</label><br />
							<input type="text" name="nome" id="nome" class="form-control" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Login</label><br />
							<input type="text" name="login" id="login" class="form-control" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Senha</label><br />
							<input type="text" name="senha" id="senha" class="form-control" />
						</div>
					</div>
					<div class="col-lg-6">					
						<div class="form-group">
							<label>Tipo</label><br />
							<select name="tipo" id="tipo"class="form-control">
								<option value="Operador">Operador</option>
								<option value="Administrador">Administrador</option>
							</select>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Situação</label> <br />
							<select name="situacao" id="situacao"class="form-control">
								<option value="ATIVO">ATIVO</option>
								<option value="INATIVO">INATIVO</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>usuarioController/'" />
							<input type="submit" class="btn btn-success" name="btSalvarUsuario" value="Salvar" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>
