

<!--Banner-->
<section>
    <div id="banner" class="pageempresa">
        <div class="container clearfix">
            <div class="grid_9">
                <h1>TRABALHE CONOSCO</h1>
                <p>Confira nossas vagas abertas</p>
            </div>
            <div class="grid_3">
                <a href="<?= base_url() ?>trabalhe-na/<?= $empresa[0]->apelido ?>"><img src="<?= base_url() ?>upload/empresas/<? echo $empresa[0]->logo ?>" alt="<? echo $empresa[0]->razaoSocial ?>" /><a/>
            </div>
        </div>
    </div>
</section>



<!--Vaga-->
<section>
    <div id="vaga">
        <div class="container clearfix">
            <div class="grid_12">
                <h1><?= $vaga[0]->vaga ?> (<?= str_replace('Estagiario', 'Estagiário', str_replace('Senior', ' Sênior', str_replace('Junior', ' Júnior', $vaga[0]->tipoProfissional))) ?>)</h1>
                <p><?= $vaga[0]->cidade ?>/<?= $vaga[0]->estado ?></p>
                <p style="float: left">

                    <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.gotalent.com.br%2Fjob%2F<?= $empresa[0]->apelido ?>%2F<?= $empresa[0]->id ?>-<?= $vaga[0]->id ?>-<?= url(str_replace(" ", "-", $vaga[0]->vaga)) ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; float:right;" allowTransparency="true"></iframe>

                    <a href="https://twitter.com/share"  class="twitter-share-button" data-url="<? echo base_url() ?>job/<?= $empresa[0]->apelido ?>/<?= $empresa[0]->id ?>-<?= $vaga[0]->id ?>-<?= url(str_replace(" ", "-", $vaga[0]->vaga)) ?>" data-related="PortalGotalent" data-via="PortalGotalent">Tweet</a>
                    <script>!function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, 'script', 'twitter-wjs');</script>

                </p>
            </div>
            <div class="grid_8">
                <div class="info">
                    <? if ($this->session->userdata("tipo") == 'Profissional') { ?>
                        <h5> Compatibilidade <span><?= number_format($compatibilidade_vaga, 0, ',', ' ') == "" ? "0" : number_format($compatibilidade_vaga, 0, ',', ' ') ?>%</span> </h5>        
                    <? } ?>		
                    <h5> Faixa Salarial <span><?= $vaga[0]->salario ?></span> </h5>
                    <h5> Contratação <span><?= $vaga[0]->tipo ?></span> </h5>
                    <h5> Vagas <span><?= $vaga[0]->quantidade ?></span> </h5>
                </div>

                <? if ($vaga[0]->descricao) { ?>
                    <h3>Descrição</h3>
                    <p><?= $vaga[0]->descricao ?></p>
                <? } ?>

                <? if ($vaga[0]->diferenciais) { ?>
                    <h3>Diferenciais</h3>
                    <ul>
                        <?
                        $diferenciais = explode(";", $vaga[0]->diferenciais);
                        for ($i = 0; $i < count($diferenciais); $i++) {
                            echo "<li>" . $diferenciais[$i] . "</li>";
                        }
                        ?>
                    </ul>
                <? } ?>

                <? if ($vaga_skills) { ?>
                    <h3>Conhecimentos Desejados</h3>
                    <p><?
                        foreach ($vaga_skills as $skill) {
                            echo $skill->skill . ", ";
                        }
                        ?> </p>
                <? } ?>

                <? if ($beneficios_vaga) { ?>
                    <h3>Benefícios</h3>
                    <p><?
                        foreach ($beneficios_vaga as $beneficio) {
                            echo $beneficio->beneficio . ", ";
                        }
                        ?> </p>
                <? } ?>

                <? if (trim($vaga[0]->informacoesAdicionais) != "") { ?>
                    <h3>Outras Informações</h3>
                    <p><?= $vaga[0]->informacoesAdicionais ?></p>
                <? } ?>

                <?
                if ($this->session->userdata("tipo") == 'Profissional' && $vaga[0]->recebeCurriculoFora == "SIM") {
                    ?>
                    <h3 style="color: orange">Inscreva-se</h3>
                    <p><?= $vaga[0]->textoAplicacao ?></p>
                    <?
                } else if ($this->session->userdata("tipo") == 'Profissional') {
                    $candidatou = "N";
                    foreach ($candidaturas as $cand) {
                        if ($cand->idVaga == $vaga[0]->id) {
                            $candidatou = "S";
                            break;
                        }
                    }
                    if ($candidatou == 'N') {
                        ?>	
                        <a class="btn" href="<?= base_url() ?>principal/candidatar/<?= $vaga[0]->id ?>">Quero me Candidatar!</a>
                    <? } else { ?>
                        <h6><img src="<?= base_url() ?>_imagens/icon-check2.png" alt="" /> Já me candidatei!</h6>
                    <? } ?>
                <? } else {
                    ?>
                    <div id="divBotao" style="display: block;">    
                        <a class="btn" onclick="$('#divCadastro').css('display', 'block');
                                $('#divBotao').css('display', 'none')">CADASTRE-SE!</a>
                    </div>
                    

<? } ?>
            </div>
            
            <div class="grid_4 anunciante">
                <h4><?= $vaga[0]->razaosocial ?></h4>
                <? if ($vaga[0]->logo != "") { ?>

                    <? if ($vaga[0]->apelido != "") { ?>
                        <a href="<?= base_url() ?>trabalhe-na/<?= $empresa[0]->apelido ?>"> <img src="<?= base_url() ?>upload/empresas/<?= $vaga[0]->logo ?>" alt="<?= $vaga[0]->razaosocial ?>" /> </a>
                    <? } else { ?> 
                        <img src="<?= base_url() ?>upload/empresas/<?= $vaga[0]->logo ?>" alt="<?= $vaga[0]->razaosocial ?>" />
                    <? } ?>  

<? } ?>

                <p><?= $vaga[0]->sobre ?></p>
                <p>Site: <?= $vaga[0]->site ?></p>

                <br />
                <? if (count($vagas_empresa) > 0) { ?>
                    <h4> Mais vagas desta empresa</h4>               
                    <? foreach ($vagas_empresa as $vaga_empresa) { ?>
                        <p> <a href="<?= base_url() ?><?=$empresa[0]->apelido?>/job/<?=$empresa[0]->id?>-<?= $vaga_empresa->id ?>-<?= url(str_replace(" ", "-", $vaga_empresa->vaga)) ?>"> <?= $vaga_empresa->vaga ?> (<?= $vaga_empresa->cidade ?>/<?= $vaga_empresa->estado ?>) </a> </p>
                    <? } ?>
<? } ?>


            </div>
            <div class="grid_12">
            <p id="sucesso" style="font-size: 24px; color: green; "> </p>
            </div>
            
            <div id="divCadastro" style="display: none">            
                        <div class="container clearfix">
                            <div class="grid_8" style="float: left;">
                            
                                    <h3>Formulário de cadastro</h3>
                                    <form class="form" style="width: 100%" action="<?= base_url() ?>profissional" name="formCadProfissional" id="formCadProfissional" method="post">
                                        <p id="erro"> </p>
                                        
                                        <label><b>Nome Completo</b>
                                            <input type="text" id="nome" name="nome" value="<?= $_POST["nome"] ?>"></label>
                                        <label><b>Telefone</b>
                                            <input type="phone" id="phone" name="telefone" value="<?= $_POST["telefone"] ?>"></label>
                                        <label><b>E-mail</b>  <br><small><span style="color:red">*</span> <b>Informe um e-mail válido para receber o e-mail de ativação do seu cadastro</b></small> 
                                            <input type="mail" id="emailLogin" name="emailLogin" value="<?= $_POST["emailLogin"] ?>"></label>
                                        <label><b>Senha</b>
                                            <input type="password" id="senha" name="senha"></label>
                                        <button type="button" class="btn" onclick="cadProfissionalParceiro()">Cadastrar</button>   
                                        <span>Ao se cadastrar, você automaticamente concorda<br>com os <a target="_blank" href="<?= base_url() ?>termo-de-uso">Termos de Uso </a> da Go Talent.</span>
                                    </form>
    
                            </div>                 
                        </div>
                    </div>

            
        </div>
    </div>
</section>


