<!--Banner-->
<section>
    <div id="banner" class="vagas">
        <div class="container clearfix">
            <div class="grid_12">
                <h1><b>CONFIRA AS VAGAS</b> DIPONÍVEIS PARA O SEU TALENTO</h1>
            </div>
        </div>
    </div>
</section>

<!--Busca-->
<section>
    <div id="busca">
        <div class="container clearfix">
            <div class="grid_12">
                <h2>Encontre aqui a sua oportunidade de emprego!</h2>

                <form id="formPesquisa" name="formPesquisa" action="<?= base_url() ?>buscar" method="post">
                    <input type="text" id="searchVaga" name="searchVaga" placeholder="Pesquise por vaga ..."
                           value="<?= $_SESSION['searchVaga'] ?>" class="placeholder input"/>
                    <input type="text" id="searchVaga" name="searchCidade" placeholder="Pesquise por cidade ..."
                           value="<?= $_SESSION['searchCidade'] ?>" class="placeholder input"/>
                    <select name="searchNivel" class="input">
                        <option value="Todos">Todos os níveis</option>
                        <option <?= $_SESSION["searchNivel"] == "Estagiario" ? "selected" : "" ?> value="Estagiario">
                            Estagiário
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Trainee" ? "selected" : "" ?> value="Trainee">Trainee
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Junior" ? "selected" : "" ?> value="Junior">Júnior
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Pleno" ? "selected" : "" ?> value="Pleno">Pleno
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Senior" ? "selected" : "" ?> value="Senior">Sênior
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Supervisor" ? "selected" : "" ?> value="Supervisor">
                            Supervisor
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Coordenador" ? "selected" : "" ?> value="Coordenador">
                            Coordenador
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Gerente" ? "selected" : "" ?> value="Gerente">Gerente
                        </option>
                        <option <?= $_SESSION["searchNivel"] == "Diretor" ? "selected" : "" ?> value="Diretor">Diretor
                        </option>
                    </select> </label>
                    <input type="submit" value="GO!" class="btn"/>
                </form>
            </div>
        </div>
    </div>
</section>

<!--Vaga-->
<section>
    <div id="vaga">
        <div class="container clearfix">
            <div class="grid_12">
                <h1><?= $vaga[0]->vaga ?>
                    (<?= str_replace('Estagiario', 'Estagiário', str_replace('Senior', ' Sênior', str_replace('Junior', ' Júnior', $vaga[0]->tipoProfissional))) ?>
                    )</h1>

                <p><?= $vaga[0]->cidade ?>/<?= $vaga[0]->estado ?></p>

                <p style="float: left">

                    <iframe
                        src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.gotalent.com.br%2Fvaga%2F<?= $vaga[0]->id ?>-<?= url(str_replace(" ", "-", $vaga[0]->vaga)) ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21"
                        scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; float:right;"
                        allowTransparency="true"></iframe>

                    <a href="https://twitter.com/share" class="twitter-share-button"
                       data-url="<? echo base_url() ?>vaga/<?= $vaga[0]->id ?>-<?= url(str_replace(" ", "-", $vaga[0]->vaga)) ?>"
                       data-related="PortalGotalent" data-via="PortalGotalent">Tweet</a>
                    <script>!function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, 'script', 'twitter-wjs');</script>

                </p>
            </div>
            <div class="grid_8">
                <div class="info">
                    <? if ($this->session->userdata("tipo") == 'Profissional') { ?>
                        <!--h5> Compatibilidade
                            <span><?= number_format($compatibilidade_vaga, 0, ',', ' ') == "" ? "0" : number_format($compatibilidade_vaga, 0, ',', ' ') ?>
                                %</span></h5-->
                    <? } ?>
                    <h5> Faixa Salarial <span><?= $vaga[0]->salario ?></span></h5>
                    <h5> Contratação <span><?= $vaga[0]->tipo ?></span></h5>
                    <h5> Vagas <span><?= $vaga[0]->quantidade ?></span></h5>
                </div>

                <? if ($vaga[0]->descricao) { ?>
                    <h3>Descrição</h3>
                    <p><?= $vaga[0]->descricao ?></p>
                <? } ?>

                <? if ($vaga[0]->diferenciais) { ?>
                    <h3>Diferenciais</h3>
                    <ul>
                        <?
                        $diferenciais = explode(";", $vaga[0]->diferenciais);
                        for ($i = 0; $i < count($diferenciais); $i++) {
                            echo "<li>" . $diferenciais[$i] . "</li>";
                        }
                        ?>
                    </ul>
                <? } ?>

                <? if ($vaga_skills) { ?>
                    <h3>Conhecimentos Desejados</h3>
                    <p><?
                        foreach ($vaga_skills as $skill) {
                            echo $skill->skill . ", ";
                        }
                        ?> </p>
                <? } ?>

                <? if ($beneficios_vaga) { ?>
                    <h3>Benefícios</h3>
                    <p><?
                        foreach ($beneficios_vaga as $beneficio) {
                            echo $beneficio->beneficio . ", ";
                        }
                        ?> </p>
                <? } ?>

                <? if (trim($vaga[0]->informacoesAdicionais) != "") { ?>
                    <h3>Outras Informações</h3>
                    <p><?= $vaga[0]->informacoesAdicionais ?></p>
                <? } ?>

                <?
                $dataCandidaturaFree = date('Y-m-d', strtotime($vaga[0]->dataPublicacao . ' + 2 days'));

                $dataAtual = date('Y-m-d');

                if ($this->session->userdata("tipo") == 'Profissional' && ($profissional[0]->tipoPlano == 'PREMIUM' || $dataAtual > $dataCandidaturaFree || $vaga[0]->prioridade == "SIM")) {
                    $candidatou = "N";
                    foreach ($candidaturas as $cand) {
                        if ($cand->idVaga == $vaga[0]->id) {
                            $candidatou = "S";
                            break;
                        }
                    }
                    if ($candidatou == 'N') {
                        //$compatibilidade = intval(ceil(floatval($compatibilidade_vaga)));
                        //if ($compatibilidade > 0) {
                            if ($this->session->userdata("tipo") == 'Profissional' && $vaga[0]->recebeCurriculoFora == "NAO") {
                                ?>
                                <a class="btn" href="<?= base_url() ?>principal/candidatar/<?= $vaga[0]->id ?>">Quero me
                                    Candidatar!</a>
                                <?
                            } else {
                                if ($this->session->userdata("tipo") == 'Profissional' && $vaga[0]->receberCurriculoFora == "SIM" && $profissional[0]->cid == $vaga[0]->cidade) {
                                    ?>
                                    <a class="btn" href="<?= base_url() ?>principal/candidatar/<?= $vaga[0]->id ?>">Quero
                                        me Candidatar!</a>
                                    <?
                                } else {
                                    ?>
                                    <div id="info_profissional">
                                        <p>Desculpe, somente profissionais da mesma cidade da vaga podem se candidatar a
                                            esta vaga.z <?=$profissional[0]->cid?> - <?=$vaga[0]->cidade?></p>
                                    </div>
                                    <?
                                }
                            }
                        /*} else {
                            ?>
                            <div id="info_profissional">
                                <p>Desculpe, mas você não possui compatibilidade suficiente para poder se candidatar a
                                    esta vaga. <a
                                        href="<? echo base_url() ?>profissionalController/editarProfissionalAction">Atualize suas Skills.</a></p>
                            </div>
                            <?
                        }*/
                    } else {
                        ?>
                        <h6>
                            <img src="<?= base_url() ?>_imagens/icon-check2.png" alt=""/> Já me candidatei!
                        </h6>
                        <?
                    }
                } else {
                    if ($this->session->userdata("tipo") == 'Profissional' && $profissional[0]->tipoPlano == 'FREE') {
                        ?>
                            <a class="btn" href="<?= base_url() ?>seja-premium">Exclusivo para Premium!</a>
                        <?
                    } else {
                        ?>
                            <a class="btn" href="<?= base_url() ?>cadastro-profissional">CADASTRE-SE!</a>
                            <a class="btnOff" href="<?= base_url() ?>login">SOU CADASTRADO</a>
                        <?
                    }
                }
                ?>
            </div>
            <div class="grid_4 anunciante">
                <h4><?= $vaga[0]->razaosocial ?></h4>
                <? if ($vaga[0]->logo != "") { ?>

                    <? if ($vaga[0]->apelido != "") { ?>
                        <a href="<? base_url() ?>/empresa/<?= $vaga[0]->apelido ?>"> <img
                                src="<?= base_url() ?>upload/empresas/<?= $vaga[0]->logo ?>"
                                alt="<?= $vaga[0]->razaosocial ?>"/> </a>
                    <? } else { ?>
                        <img src="<?= base_url() ?>upload/empresas/<?= $vaga[0]->logo ?>"
                             alt="<?= $vaga[0]->razaosocial ?>"/>
                    <? } ?>

                <? } ?>
                <? if ($vaga[0]->apelido != "") { ?>
                    <p><a href="<? base_url() ?>/empresa/<?= $vaga[0]->apelido ?>">Visualizar perfil da empresa</a></p>
                <? } ?>
                <p><?= $vaga[0]->sobre ?></p>

                <p>Site: <?= $vaga[0]->site ?></p>

                <br/>
                <? if (count($vagas_empresa) > 0) { ?>
                    <h4> Mais vagas desta empresa</h4>
                    <? foreach ($vagas_empresa as $vaga_empresa) { ?>
                        <p>
                            <a href="<?= base_url() ?>vaga/<?= $vaga_empresa->id ?>-<?= url(str_replace(" ", "-", $vaga_empresa->vaga)) ?>"> <?= $vaga_empresa->vaga ?>
                                (<?= $vaga_empresa->cidade ?>/<?= $vaga_empresa->estado ?>) </a></p>
                    <? } ?>
                <? } ?>


            </div>
        </div>
    </div>
</section>