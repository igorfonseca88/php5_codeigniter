<?php
$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
$symbian = strpos($_SERVER['HTTP_USER_AGENT'], "Symbian");
?>

<?
if (($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) && ($acessoAPP == true)) {

} else {
    ?>
    <!--Banner-->
    <section>
        <div id="banner" class="profissionais">
            <div class="container clearfix">
                <div class="grid_12">
                    <h1><b>CADASTRO DE PROFISSIONAL</b> Crie o seu perfil e candidate-se a vagas, é Grátis.</h1>
                </div>
            </div>
        </div>
    </section>
    <?
}
?>
<!--Cadastro-->
<section>
    <div id="cadastroProfissional">

        <div class="container clearfix">
            <?= ($this->session->flashdata('erroUsuarioNaoLogado') != "") ? "<div class='alert alert-danger'>" . $this->session->flashdata('erroUsuarioNaoLogado') . "</div>" : "" ?>
            <?= $error != "" ? '<div class="alert alert-danger">' . $error . '</div>' : "" ?>
            <div class="grid_12">
                <center>
                    <fb:login-button class="fb-login-button"
                                     data-scope="email,user_birthday,user_location,user_website,user_work_history,user_about_me, user_education_history"
                                     onlogin="checkLoginState()" data-size="large" data-max-rows="1">Entrar com Facebook
                    </fb:login-button>

                </center>
                <br>
                <p class="line-thru"><b>OU</b></p>
            </div>


            <div class="grid_6">
                <form class="form" role="form" id="formLogin" name="formLogin"
                      action="<?= base_url() ?>login/login/autenticarFacebook"
                      method="post">
                    <div style="display: inline ">
                        <p><b>ENTRAR COM SEU EMAIL</b></b></p>
                    </div>
                    <p id="erro"> <?= $this->session->flashdata('error') != "" ? $this->session->flashdata('error') : "" ?> </p>
                    <label><b>E-mail</b> <br>
                        <input type="text" name="usuarioEmail" id="usuarioEmail"
                               required placeholder="EMAIL"
                               value="<?= $this->session->flashdata('email') ? $this->session->flashdata('email') : $_POST["emailLogin"] ?>"></label>

                    <label><b>Senha</b><br>
                        <input type="password" class="form-control" placeholder="Senha" name="senha" id="senha"
                               required=""></label>

                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                    <a class="link-esqueci-senha" style="margin-right: 20px;"
                       href="<? echo base_url() ?>esqueci-minha-senha">
                        <small>Esqueceu sua senha ?</small>
                    </a>
                    <a class="link-cadastrar" href="<? echo base_url() ?>cadastro-profissional">
                        <small>Quero me Cadastrar !</small>
                    </a><br>
                </form>
            </div>
            <div class="grid_6">
                <? if ($this->session->flashdata('sucesso')) { ?>
                    <h3><?= $this->session->flashdata('sucesso') ?></h3>
                    <script>ga('send', 'pageview', '/cadastrousuario');</script>

                <? } else { ?>

                    <form class="form" action="<?= base_url() ?>profissionalFacebook" name="formCadProfissional"
                          id="formCadProfissional" method="post">

                        <p><b>CADASTRAR-SE COM SEU EMAIL</b></b></p>

                        <p id="erro"> <?= $this->session->flashdata('error') != "" ? $this->session->flashdata('error') : "" ?> </p>
                        <label><b>Nome Completo</b>
                            <input type="text" id="nome" name="nome"
                                   value="<?= $this->session->flashdata('nome') ? $this->session->flashdata('nome') : $_POST["nome"] ?>"></label>
                        <label><b>E-mail</b> <br>
                            <small><span style="color:red">*</span> <b>Informe um e-mail válido para receber o
                                    e-mail de
                                    ativação do seu cadastro</b></small>
                            <input type="mail" id="emailLogin" name="emailLogin"
                                   value="<?= $this->session->flashdata('email') ? $this->session->flashdata('email') : $_POST["emailLogin"] ?>"></label>

                        <label><b>Senha</b>
                            <input type="password" id="senha_" name="senha_"></label>

                        <button type="button" class="btn" onclick="validaFormProfissionalFacebook()">Cadastrar</button>
                        <span>Ao se cadastrar, você automaticamente concorda<br>com os <a
                                href="<?= base_url() ?>termo-de-uso">Termos de Uso </a> da Go Talent.</span>
                    </form>
                <? } ?>
            </div>
        </div>
    </div>
</section>
<script>

</script>