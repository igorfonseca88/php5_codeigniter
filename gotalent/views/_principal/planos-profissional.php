<!--Banner-->
<section>
    <div id="banner" class="profissional">
        <div class="container clearfix">
            <div class="grid_12">					
                <h1>AINDA NÃO TEM UM CADASTRO ?</h1>
                <h2>Crie seu currículo e candidate-se às vagas de empregos de TI.<br><b>É de graça!</b></h2>
            </div>
        </div>
    </div>
</section>

<!--Planos-->
<section>
    <div id="planos-profissional">
        <div class="container clearfix">
            <div class="grid_12">
                <a href="<?= base_url() ?>cadastro-profissional" class="btn">Cadastrar Agora</a>
            </div>
          
        </div>
    </div>
</section>

