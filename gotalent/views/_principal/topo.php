<?php

$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
$symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");

//Transforma títulos em URL amigáveis
function url($str) {
    $str = strtolower(utf8_decode($str));
    $i = 1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
    while ($i > 0)
        $str = str_replace('--', '-', $str, $i);
    if (substr($str, -1) == '-')
        $str = substr($str, 0, -1);
    return $str;
}

//Busca URL para setar os title e description
$url = explode('?', $_SERVER['REQUEST_URI']);
$urlAtual = $url[0];
$urlParam = explode('/', $urlAtual);
$urlPagina = $urlParam[1];

//Escolhe as páginas
switch ($urlPagina) {
    case "" :
        $description = "Na Go Talent você tem acesso a vagas de TI e ainda pode agendar sua entrevista de emprego direto.";
        $keywords = "vagas de TI";
        $title = "Vagas de TI: Encontre o emprego que você deseja";
        break;
    case "vagas" :
        if ($urlParam[2] != "") {

            if ($urlParam[2] == "AM") {
                $title = "Emprego para TI: Veja as vagas anunciadas para o Amazonas";
                $description = "Faça o cadastro no Go Talent se você procura vagas ou deseja oferecer um emprego para TI, venha conferir o mercado de TI no Amazonas e outras regiões do país.";
                $keywords = "Empregos TI";
            } else if ($urlParam[2] == "BA") {
                $title = "Vagas TI Bahia: Confira as vagas para você que deseja um emprego";
                $description = "Vagas TI Bahia: Confira as vagas anunciadas na sua região. Faça o cadastro que entramos em contato com você caso haja um emprego para o seu perfil.";
                $keywords = "Vagas TI Bahia";
            } else if ($urlParam[2] == "DF") {
                $title = "Veja as vagas para você que procura emprego na área de TI no DF";
                $description = "Confira as vagas ofertadas para você que busca um emprego na área de TI no Distrito Federal e região.";
                $keywords = "emprego na área de ti";
            } else if ($urlParam[2] == "GO") {
                $title = "Vagas de emprego TI: Confira as oportunidades para sua região";
                $description = "Vagas de emprego TI: Confira as vagas oferecidas para você que mora em Goiás ou busca empregos nessa região.";
                $keywords = "vagas de emprego ti";
            } else if ($urlParam[2] == "MG") {
                $title = "Vaga de Emprego TI: Confiras as vagas de TI para Minas Gerais";
                $description = "Vaga de Emprego TI: Veja as vagas para TI que são ofertadas para o estado de Minas Gerais e região.";
                $keywords = "vaga de emprego ti";
            } else if ($urlParam[2] == "MS") {
                $title = "Confira as vagas de TI em Campo Grande MS";
                $description = "Faça o cadastro na Go Talent e seja informado quando houver vagas de TI em Campo Grande MS.";
                $keywords = "vagas de ti em campo grande ms";
            } else if ($urlParam[2] == "PE") {
                $title = "Vagas TI Pernambuco: Veja as vagas oferecidas pela Go Talent";
                $description = "Candidata-se a vagas oferecidas para a área de TI. Aqui no Go Talent você acesso a vagas e entrevistas de emprego para o estado de Pernambuco e todo o país.";
                $keywords = "vagas TI Pernambuco";
            } else if ($urlParam[2] == "PR") {
                $title = "Encontra na Go Talent a vaga de Ti que você procura";
                $description = "Buscando uma vaga de TI? Confira vagas de emprego para o estado do Paraná e todo o país, faça o cadastro no nosso site e fique por dentro das melhores oportunidades.";
                $keywords = "vaga de TI";
            } else if ($urlParam[2] == "RJ") {
                $title = "Vagas TI: Confira as vagas de emprego para TI";
                $description = "Veja as vagas de empregos oferecidas para todo o Brasil, aqui você efetua o cadastro e é avisado quando tem uma oportunidade de trabalho para o seu perfil.";
                $keywords = "Vagas TI";
            } else if ($urlParam[2] == "RS") {
                $title = "Vagas TI Rio Grande do Sul: Confira as melhores oportunidades";
                $description = "Aqui você tem acesso as melhores oportunidades de vagas de empregos para a sua região.";
                $keywords = "Vagas TI Rio Grande do Sul";
            } else if ($urlParam[2] == "SC") {
                $title = "Vagas TI Santa Catarina: Cadastre-se para as vagas de TI";
                $description = "Vagas TI Santa Catarina: Confiras as melhores vagas de TI para o estado de Santa Catarina.";
                $keywords = "Vagas TI Santa Catarina";
            } else if ($urlParam[2] == "SP") {
                $title = "Encontre as melhores vagas de TI em SP";
                $description = "Confira as melhores vagas de TI em SP, basta fazer seu cadastro para ter acesso a entrevistas de emprego.";
                $keywords = "vagas de ti em SP";
            } else {
                $title = "TI Vagas: Confira a lista de vagas para seu estado";
                $description = "TI Vagas: Se candidate a vagas de emprego para TI em todo o Brasil.";
                $keywords = "TI Vagas";
            }
        } else {
            $title = "Empregos TI: Um site desenvolvido para quem busca uma vaga";
            $description = "Empregos TI: Aqui o profissional da área de TI encontra as melhores vagas, um banco de dados com as melhores vagas em todo o país. Confira!";
            $keywords = "Empregos TI";
        }


        $menuVagas = "class='select'";
        break;
    case "buscar" :
        $title = "Empregos TI: Um site desenvolvido para quem busca uma vaga";
        $description = "Empregos TI: Aqui o profissional da área de TI encontra as melhores vagas, um banco de dados com as melhores vagas em todo o país. Confira!";
        $keywords = "Empregos TI";

        $menuVagas = "class='select'";
        break;
    case "vaga" :
        $title = $vaga[0]->vaga . " | Vagas em TI";
        $description = strip_tags(substr($vaga[0]->descricao, 0, 230));
        $keywords = "TI Vagas";
        $menuVagas = "class='select'";
        break;
    case "profissionais" :
        $title = "Cadastre-se grátis para ter acesso as vagas de emprego em TI";
        $description = "Cadastre-se grátis para que você tenha acesso a vagas de emprego em TI, aqui você já saí com a entrevista agendada.";
        $keywords = "vagas de emprego em ti";
        $menuProfissionais = "class='select'";
        break;
    case "empresas" :
        $title = "Confira como é fácil criar uma vaga de TI";
        $description = "Aqui a sua empresa encontra os melhores profissionais de TI, basta efetuar o cadastro para ter acesso a pessoas qualificadas que buscam um emprego.";
        $keywords = "vagas de emprego em ti";
        $menuEmpresas = "class='select'";
        break;
    case "blog" :
        if ($urlParam[2] != "") {

            if ($urlParam[2] == "ambiente-de-trabalho") {
                $title = "Veja como é trabalhar com TI";
                $keywords = "Trabalhar com TI";
                $description = "Dicas e novidades para desenvolver uma boa eficiência no ambiente de trabalho. Veja como é trabalhar com TI e confira as melhores vagas para o setor.";
            } else if ($urlParam[2] == "carreira") {
                $title = "Saiba como está o mercado para a carreira de TI";
                $keywords = "Carreira de TI";
                $description = "Veja quais são os requisitos para quem deseja investir na área de TI, confira as melhores vagas de trabalho e dicas para desenvolver melhor o seu trabalho.";
            } else if ($urlParam[2] == "recrutamento-e-selecao") {
                $title = "Sabia como conseguir um bom emprego no setor de TI";
                $keywords = "Como conseguir um bom emprego";
                $description = "Veja as novidades sobre uma das áreas que mais cresce no país, aqui você encontra dicas de como conseguir um com emprego na área de TI.";
            } else if ($urlParam[2] == "tecnologia") {
                $title = "Veja as novidades da tecnologia e tendências para o setor de TI";
                $keywords = "Novidades da tecnologia";
                $description = "Fique por dentro do que está acontecendo através do Go Talent. Aqui você encontra as novidades da tecnologia que serão tendências para o novo mercado.";
            } else {
                $title = "Veja dicas, curiosidades, vagas e sites sobre TI";
                $description = "No Blog Go Talent, você encontra dicas para desenvolver seu aprendizado, confere curiosidades, vagas e sites sobre TI.";
                $keywords = "sites sobre TI";
            }
        }if ($urlParam[3] != "") { 
			$title = $post[0]->titulo . " | Blog";
			$description = $post[0]->descricao;
			$keywords = $post[0]->tags;
			$menuBlog = "class='select'";
			$imagem = base_url() . "upload/blog/" . $post[0]->imgprincipal;
			$ogUrl = base_url()."blog/".$post[0]->urlCat."/".$post[0]->urlPost;
		
		}else {
            $title = "Veja dicas, curiosidades, vagas e sites sobre TI";
            $description = "No Blog Go Talent, você encontra dicas para desenvolver seu aprendizado, confere curiosidades, vagas e sites sobre TI.";
            $keywords = "sites sobre TI";
        }

        $menuBlog = "class='select'";
		
        break;
    
    case "quem-somos" :
        $title = "A Go Talent desenvolve métodos inteligentes para anunciar vagas para TI";
        $description = "Somos um site que fortalece o mercado de vagas para TI, através de métodos inteligentes para o profissional achar o melhor emprego.";
        $keywords = "vagas para TI";
        $menuQuemSomos = "class='select'";
        break;
    case "contato" :
        $title = "Entre em contato para anunciar ou buscar vagas na área de TI";
        $description = "Preencha o formulário no site da Go Talent para encontrar um profissional ou as melhores vagas na área de TI, fale conosco para esclarecer quaisquer dúvidas.";
        $keywords = "vagas na área de ti";
        $menuContato = "class='select'";
        break;
    case "texto" :
        $title = $texto[0]->titulo;
        $description = $texto[0]->descricao;
        $keywords = $texto[0]->tags;
        break;
    case "cadastro-profissional" :
        $title = "Faça o cadastro na Go Talent para conferir vagas de empregos de TI";
        $description = "Profissional, ao se cadastrar na Go Talent você tem acesso a ofertas de empregos de TI.";
        $keywords = "empregos de ti";
        $menuProfissionais = "class='select'";
        break;
    case "cadastro-empresa" :
        $title = "Veja os profissionais de TI que se encaixam melhor na sua empresa";
        $description = "Aqui a sua empresa encontra os melhores profissionais de TI, basta efetuar o cadastro para ter acesso a pessoas qualificadas que buscam um emprego.";
        $keywords = "profissionais de ti";
        break;
    case "termo-uso" :
        $title = "Veja o Termo de Uso ao se cadastrar nas vagas para TI ofertadas pela Go Talent";
        $description = "Leia o Termo de Uso e entenda como funciona o processo para se cadastrar a uma vaga para TI ou anunciar um vaga.";
        $keywords = "vaga para ti";
        break;

    case "empresa" :
        $title = $empresa[0]->razaosocial . " proporcionando ótimas oportunidades | Go Talent";
        $description = "Na Go Talent você tem acesso a vagas de TI e ainda pode agendar sua entrevista de emprego direto.";
        $keywords = "vagas de emprego";
        $menuEmpresas = "class='select'";
        break;
    case "seja-premium" :
        $title = "Seja um profissional premium agora mesmo | Go Talent";
        $description = "Na Go Talent prosissional premium tem mais vantagens";
        $keywords = "vaga para ti";
        break;
    default :
        $description = "Na Go Talent você tem acesso a vagas em sua área e ainda pode agendar sua entrevista de emprego direto.";
        $keywords = "vagas de emprego";
        $title = "Vagas: Encontre o emprego que você deseja";
        break;
}


if ($imagem != "") {
    $content_imagem = $imagem;
} else {
    $content_imagem = base_url() . "_imagens/logo-redessociais.png";
}
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!--Site desenvolvido por Fernanda Pieretti - www.freelancehtmlcss.com-->
        <title><?= $title ?></title>
        <meta name="description" content="<?= $description ?>" />
        <meta name="keywords" content="<?= $keywords ?>" />
        <meta property="og:title" content="<?= $title ?>" />
        <meta property="og:description" content="<?= $description ?>" />
        <meta property="og:image" content="<?= $content_imagem ?>" />	
        <meta property="og:type" content="website" />
		<meta property="fb:app_id" content="966242223397117" />
		<meta property="og:url" content= "<?=$ogUrl?>"/>
		

        <meta name="viewport" content="width=device-width" />
        <link rel="shortcut icon" href="<? echo base_url() ?>_imagens/favicon.ico" type="image/x-icon" />

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,700' rel='stylesheet' type='text/css' />

        <link rel="stylesheet" href="<?= base_url() ?>_estilos/base.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/grid.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/style.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/fancybox.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/flexslider.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/demo.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/animate.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/jquery.autocomplete.css" />
        <script rel="text/javascript" src="<?= base_url() ?>_js/facebook.js" ></script>
        <script type="text/javascript" src="<?= base_url() ?>_js/modernizr-2.6.2.min.js"></script>

        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-53411814-1', 'auto');
            ga('send', 'pageview');
        </script> 

    </head>
    <body id="home">

        <!--Header-->
        <header>
            <div id="header">
                <?
                    if (($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) && ($acessoAPP == true)){

                    }else{
                ?>
                    <div class="container clearfix">
                        <div class="grid_4 logo">
                            <a href="<? echo base_url() ?>"><img src="<? echo base_url() ?>_imagens/logo.png" alt="Go Talent" /></a>
                            <p id="menuDropDown">Menu</p>
                        </div>
                        <div class="grid_8 menu">
                            <ul>
                                
                                <li><a href="<? echo base_url() ?>vagas">Vagas</a></li>
                                <li><a href="<? echo base_url() ?>empresas">Recrutadores</a></li>
                                <li><a href="<? echo base_url() ?>cadastro-profissional">Profissionais</a></li>
                                <li><a href="<? echo base_url() ?>blog">Blog</a></li>

                                <? if ($this->session->userdata("tipo") == "") { ?>
                                    <li><a class="log" href="<? echo base_url() ?>login/login">Login</a></li>
                                    <? } else if ($this->session->userdata("tipo") == "Profissional") {
                                        ?>
                                    <li><a class="log" href="<? echo base_url() ?>area-restrita">Área Restrita</a></li>
                                <? } else if ($this->session->userdata("tipo") == "Empresa") {
                                    ?>
                                    <li><a class="log" href="<? echo base_url() ?>area-restrita-empresa">Área Restrita</a></li>
                                <? } else if ($this->session->userdata("tipo") == "Administrador" || $this->session->userdata("tipo") == "Conteudo") {
                                    ?>
                                    <li><a class="log" href="<? echo base_url() ?>principal/arearestritaadmin">Área Restrita</a></li>
                                <? } ?>
                            </ul>				
                        </div>
                    </div>
                <?
                    }
                ?>
            </div>
        </header>


