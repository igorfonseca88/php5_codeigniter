
	
	<!--Banner-->
	<section>
		<div id="banner" class="empresa">
			<div class="container clearfix">
				<div class="grid_12">
					<h1><b>ENCONTRE OS PROFISSIONAIS</b> COM TALENTO CERTO PARA SUA EMPRESA!</h1>
				</div>
			</div>
		</div>
	</section>
		
	<!--Itens-->
	<section>
		<div id="itens">
			<div class="container clearfix">
				<div class="grid_4">
					<img src="<?echo base_url()?>_imagens/itens/1.png" alt="Cadastre" />
					<h2>CADASTRE<b> SUA EMPRESA</b> </h2>
				</div>
				<div class="grid_4">
					<img src="<?echo base_url()?>_imagens/itens/2.png" alt="Anuncie" />
					<h2>ANUNCIE  <b>SUAS VAGAS</b></h2>
				</div>
				<div class="grid_4">
					<img src="<?echo base_url()?>_imagens/itens/3.png" alt="Recrute" />
					<h2>RECRUTE <b>OS MELHORES</b></h2>
				</div>
			</div>
		</div>
	</section>
	
	<!--Experiencia-->
	<section>
		<div id="experiencia">
			<div class="container clearfix">
				<div class="grid_12">
					<h2>Rápido e inovador - A melhor experiência em recrutamento e seleção!</h2>
				</div>
				<div class="grid_4">
					<ul class="abas">
						<li class="select" name="aba1">
							<span>1</span>
							<div>
								<h3>Triagem</h3>
								<p>Selecione e classifique os candidatos.</p>
							</div>
						</li>
						<li name="aba2">
							<span>2</span>
							<div>
								<h3>Banco de Currículos</h3>
								<p>Busque profissionais por cargo, competências ou salários conforme sua necessidade.</p>
							</div>
						</li>
						<li name="aba3">
							<span>3</span>
							<div>
								<h3>Vídeo entrevista</h3>
								<p>Crie questões e recebe as respostas em vídeo.</p>
							</div>
						</li>
						<li name="aba4">
							<span>4</span>
							<div>
								<h3>Go Bot</h3>
								<p>Um robô de recrutamento te dará o auxilio na seleção.</p>
							</div>
						</li>
						<li name="aba5">
							<span>5</span>
							<div>
								<h3>Feedbacks</h3>
								<p>Envie mensagens diretamente da ferramenta para os candidatos.</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="grid_8">
					<div class="abas">
						<div class="aba select" id="aba1">
							<img src="<?echo base_url()?>_imagens/experiencia/1.png" alt="Triagem" />
						</div>
						<div class="aba" id="aba2">
							<img src="<?echo base_url()?>_imagens/experiencia/3.png" alt="Banco de currículos" />
						</div>
						<div class="aba" id="aba3">
							<img src="<?echo base_url()?>_imagens/experiencia/2.jpg" alt="Vídeo entrevista" />
						</div>
						<div class="aba" id="aba4">
							<img src="<?echo base_url()?>_imagens/experiencia/2.jpg" alt="Go Bot" />
						</div>
						<div class="aba" id="aba5">
							<img src="<?echo base_url()?>_imagens/experiencia/5.png" alt="Feedbacks" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
		
	<!--Depoimentos 
	<section>
		<div id="depoimentos">
			<div class="container clearfix">
				<div class="grid_2 prefix_2">
					<div class="img">
                                            <img src="<?//echo base_url()?>_imagens/site/icon16.png" alt="" />
					</div>
				</div>
				<div class="grid_7">
					<p>Ut malesuada dui nec arcu finibus, nec placerat mi dictum. Vivamus id convallis tellus. Duis faucibus justo a diam congue, id interdum purus finibus. Curabitur molestie vel urna eu aliquam. Nulla maximus commodo ante, eu luctus ex. Sed blandit eget felis id tempus. Etiam pretium mattis lacus, vitae vulputate urna dapibus ac. </p>
					<b>Gislaine</b>
					<p>AZ Tecnologia em Gestão</p>
				</div>
			</div>
		</div>
	</section>-->

	<!--Planos-->
	<section>
		<div id="planos">
			<div class="container clearfix">
				<div class="grid_10 prefix_1">
					<h2>CONFIRA NOSSOS <b>PLANOS E SOLUÇÕES</b> DE RECRUTAMENTO E SELEÇÃO NA ÁREA DE TI</h2>
				</div>                                                       
				<div class="grid_4">
                                    <div class="plano plano1">
                                        <h3>PLANO <b>BÁSICO</b></h3>
                                        <h4>Gerencie seus processos com eficiência e baixo custo.</h4>
                                        <p>Anunciar vagas nas <b>redes sociais</b></p>
                                        <p>Anunciar vagas no painel de <b>oportunidades</b> </p>
                                        <p><b>Triagem</b> de profissionais por perfil</p>
                                        <p><b>Gerenciar</b> seus processos seletivos</p>
                                        <p><b>Comunicação efetiva</b> entre candidato e recrutador</p>
                                        <p>Página com <b>perfil da empresa</b></p>
                                        <p></p>
                                        <p></p>
                                        <h5>FREE</h5>
                                        <?if ($this->session->userdata("idEmpresa") != "" ) {?>
                                        <!--<p class="pbtn">
                                            <a mp-mode="dftl" class="btn" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=0362519e70c74186b1992540325d6dc0" name="MP-payButton" class="orange-l-rn-ar">Assinar Plano</a>
                                            <script type="text/javascript">
                                            (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                                            </script>
                                        </p>-->
                                        <?}else{?>
                                            <p class="pbtn"><a href="<?echo base_url()?>cadastro-empresa/plano-1" class="btn">
                                                Adquirir o Básico</a>
                                        </p>
                                        <?}?>
                                    </div>
				</div>
				<div class="grid_4">
					<div class="plano plano2">
						<h3>PLANO <b>IDEAL</b></h3>
						<h4>O melhor custo beneficio para sua empresa.</h4>
						<p>Anunciar vagas nas <b>redes sociais</b></p>
						<p>Anunciar vagas no painel de <b>oportunidades</b> </p>
						<p><b>Triagem</b> de profissionais por perfil</p>
						<p><b>Gerenciar</b> seus processos seletivos</p>
						<p><b>Comunicação efetiva</b> entre candidato e recrutador</p>
						<p>Página com <b>perfil da empresa</b></p>
						<p>Buscar profissionais no <b>banco de currículos</b></p>
                                                <p></p>
						<h5>R$ 599,90</h5>
                                                <? if ($this->session->userdata("idEmpresa") != "") { ?>
                                                    <p class="pbtn">
                                                        <a mp-mode="dftl" class="btn" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=f7f443dcab704390b36b65144826c765" name="MP-payButton" class="orange-l-rn-ar">Assinar Plano</a>
                                                        <script type="text/javascript">
                                                        (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                                                        </script>
                                                    </p>
                                                <? } else { ?>
                                                    <p class="pbtn"><a href="<? echo base_url() ?>cadastro-empresa/plano-2" class="btn">
                                                            Experimentar Ideal <span>gratuitamente por 15 dias</span></a>
                                                    </p>
                                                <? } ?>
                                        
					</div>
				</div>
				<div class="grid_4">
					<div class="plano plano3">
						<h3>PLANO <b>GO TALENT</b></h3>
						<h4>Tenha uma experiência completa utilizando a recrutadora virtual Go Bot.</h4>
						<p>Anunciar vagas nas <b>redes sociais</b></p>
						<p>Anunciar vagas no painel de <b>oportunidades</b> </p>
						<p><b>Triagem</b> de profissionais por perfil</p>
						<p><b>Gerenciar</b> seus processos seletivos</p>
						<p><b>Comunicação efetiva</b> entre candidato e recrutador</p>
						<p>Página com <b>perfil da empresa</b></p>
						<p>Buscar profissionais no <b>banco de currículos</b></p>
						<p><b>Recrutador virtual</b> através da ferramenta Go Bot</p>
						<h5>R$ 999,90</h5>
                                                <? if ($this->session->userdata("idEmpresa") != "") { ?>
                                                    <p class="pbtn">
                                                        <a mp-mode="dftl" class="btn" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=5aabcd807799427f926a1406babf4bc0" name="MP-payButton" class="orange-l-rn-ar">Assinar Plano</a>
                                                        <script type="text/javascript">
                                                        (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                                                        </script>
                                                    </p>
                                                <? } else { ?>
                                                    <p class="pbtn"><a href="<? echo base_url() ?>cadastro-empresa/plano-3" class="btn">
                                                            Experimentar Go Talent <span>gratuitamente por 15 dias</span></a>
                                                    </p>
                                                <? } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
		
	<!--Consultores-->
	<section>
		<div id="consultores">
			<div class="container clearfix">
				<div class="grid_10 prefix_1">
					<h2>NÃO ENCONTROU O QUE PROCURAVA? CUSTOMIZE SEU PLANO DE ACORDO COM A SUA NECESSIDADE</h2>
				</div>
				<div class="grid_4 prefix_1">
					<p>Deixe seus dados que nossos consultores entrarão em contato.</p>
				</div>
				<div class="grid_6">
					<form action="">
                                            <input type="text" placeholder="nome" name="nomeFormConsultor" id="nomeFormConsultor" value="nome" class="placeholder input" />
                                            <input type="text" placeholder="email" name="emailFormConsultor" id="emailFormConsultor" value="email" class="placeholder input" />
                                            <input type="text" placeholder="telefone" name="telefoneFormConsultor" id="telefoneFormConsultor" value="telefone" class="placeholder telefone input" />
                                            <input type="button" onclick="faleComConsultor();" value="FALAR COM UM CONSULTOR" class="btn" />
                                            <div id="respFormConsultor"></div>
                                        </form>
				</div>
			</div>
		</div>
	</section>

	<!--Empresas-->
	<section>
		<div id="empresas">
			<div class="container clearfix">
				<div class="grid_12">
					<h2><b>EMPRESAS</b> QUE OFERECEM GRANDES <br> <b>OPORTUNIDADES</b> DE TRABALHO</h2>
					<div class="sliderEmpresas">
						<div id="sliderEmpresas" class="flexslider loading">
							<ul class="slides">
							<? foreach ($empresas as $empresa) { ?>
                        <li><a href="<?= $empresa->apelido == "" ? "#" : base_url()."empresa/".$empresa->apelido?>"><img src="<? echo base_url() ?>upload/empresas/<?= $empresa->logo ?>" title="<?= $empresa->razaosocial ?>"  /></a></li>
                        <? } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
