

<!--Banner-->
<section>
    <div id="banner-cadastro">
        <div class="container clearfix">
            <div class="grid_12">
                <h1>PROFISSIONAL</h1>
                <p>Reative seu perfil e tenha acesso a todas as oportunidades!</p>
            </div>
        </div>
    </div>
</section>

<!--Cadastro-->
<section>
    <div id="cadastroProfissional">            
        <div class="container clearfix">
            <div class="grid_8 prefix_2">
                <? if ($this->session->flashdata('sucesso')) { ?>
                    <h3><?= $this->session->flashdata('sucesso') ?></h3>
                <? } else { ?>
                    <form class="form" action="<?= base_url() ?>principal/reativarAssinatura" name="formReativacao" id="formReativacao" method="post">
                        <p id="erro"> <?= $this->session->flashdata('error') != "" ? $this->session->flashdata('error') : "" ?> </p>
                        <label>E-mail de acesso
                            <input type="text" id="email" name="email" value="<?= $_POST["email"] ?>"></label>


                        <button type="submit" class="btn">Quero Reativar</button>   
                    </form>
                <? } ?>
            </div>                 
        </div>
    </div>
</section>

<script>
    function validaEmail() {
        var msg = "";

        if (document.getElementById('email').value != "") {
            var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (!filter.test(document.getElementById("email").value)) {
                msg += 'Por favor, digite o email corretamente';
                document.getElementById("alerta").innerHTML = '<div class="alert alert-danger"> ' + msg + '</div>';

            }
            else {
                document.getElementById("formRecuperacao").submit();
            }
        }



    }
</script>	
