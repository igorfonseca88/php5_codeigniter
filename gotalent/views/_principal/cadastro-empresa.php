<!--Banner-->
	<section>
		<div id="banner" class="empresa">
			<div class="container clearfix">
				<div class="grid_12">
				<h1><b>CADASTRO DE EMPRESA</b> Cadastre-se e anuncie suas vagas à centenas de profissionais!</h1>
				</div>
			</div>
		</div>
	</section>

<!--Cadastro-->
<section>
    <div id="cadastroEmpresa">            
        <div class="container clearfix">
            <div class="grid_8 prefix_2">
                <? if ($this->session->flashdata('sucesso')) { ?>
                    <h3><?= $this->session->flashdata('sucesso') ?></h3>
                    <script>ga('send', 'pageview', '/cadastroempresa');</script>
                <? } else { ?>                     
                    <div>
                        <form class="form" method="post" action="<?= base_url() ?>empresa" name="formCadEmpresa" id="formCadEmpresa">
                            <p id="erro"> <?= $this->session->flashdata('error') != "" ? $this->session->flashdata('error') : "" ?> </p>
                            <p style="text-align: left">
                                <?if( $planos[0]->id == 1 ){?>
                                Você optou por utilizar o Plano Básico que é FREE. Para continuar, cadastre sua empresa.
                                <?} else {?>
                                Você optou por experimentar o <?=$planos[0]->plano?>, gratuitamente por 15 dias. Para continuar, cadastre sua empresa. 
                                
                                <?}?>
                            <br><br>
                            </p>
                            <input type="hidden" id="plan" name="plan" value="<?=$planos[0]->id ?>"/>
                            <label><b>CNPJ</b><input id="cnpjEmpresa" type="text" name="cnpjEmpresa" /></label>
                            <label><b>Empresa</b><input id="nomeEmpresa" type="text" name="nomeEmpresa" /></label>
                            <label><b>Nome do contato</b><input id="nomeContato" type="text" name="nomeContato" /></label>
                            <label><b>Telefone</b><input id="telefoneEmpresa" type="phone" name="telefoneEmpresa" /></label>
                            <label><b>E-mail</b> <br><small><span style="color:red">*</span> Informe um e-mail válido para receber o e-mail de ativação do seu cadastro</small><input id="emailEmpresa" type="mail" name="emailEmpresa" /></label>
                            <label><b>Senha</b><input id="senhaEmpresa" type="password" name="senhaEmpresa" /></label>
                            <label><button type="button" class="btn" onclick="validaFormEmpresa()">Cadastrar</button></label>
                            <span>Ao se cadastrar, você automaticamente concorda<br>com os <a href="<?= base_url() ?>termo-de-uso">Termos de Uso </a> da Go Talent.</span>
                        </form>
                    </div>
                <? } ?>

                </form>
            </div>                 
        </div>
    </div>
</section>