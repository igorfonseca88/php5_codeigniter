<!--Banner-->
<section>
    <div id="banner" class="pageempresa">
        <div class="container clearfix">

            <div class="grid_9">
                <h1><b><? echo $empresa[0]->razaosocial ?></b></h1>
                <p><? echo $empresa[0]->slogan ?></p>
                <p><a target="_blank" href="<? echo $empresa[0]->site ?>"><? echo $empresa[0]->site ?></a></p>
            </div>

            <div class="grid_3">
                <img src="<?= base_url() ?>upload/empresas/<? echo $empresa[0]->logo ?>" alt="<? echo $empresa[0]->razaoSocial ?>" />
            </div>
        </div>
    </div>
</section>

<!--Empresa-->
<section>
    <div id="empresa_view">
        <div class="container clearfix">
            <div class="grid_8">						
              
                <div class="linhas">
                
                    <h3>Vagas disponíveis</h3>
                    <? foreach ($vagas_empresa as $vaga) { ?>
                        <div class="vaga">
                               <img src="<? echo base_url() ?>_imagens/vagas/code.png" alt="Área" /> 
                               <div class="texto"> 
                                

                                <h3><?= $vaga->vaga ?> (<?= str_replace('Estagiario', 'Estagiário', str_replace('Senior', 'Sênior', str_replace('Junior', 'Júnior', $vaga->tipoProfissional))) ?>)</h3>
                                
                                <span class="local"> <?= $vaga->cidade . "-" . $vaga->estado ?></span>
                               <span class="salario"> <?= $vaga->salario ?> </span>
                               <span class="regime">Regime de contratação: <?= $vaga->tipo ?> </span>
                               </div>
                            <a href="<?= base_url() ?>job/<?= $empresa[0]->apelido ?>/<?= $empresa[0]->id ?>-<?= $vaga->id ?>-<?= url(str_replace(" ", "-", $vaga->vaga)) ?>" class="btn">VER VAGA</a>
                        </div>
                    <? } ?>
                
            

            </div>
            </div>

            <div class="grid_4">
                <? if ($empresa[0]->twitter != "") { ?>        
                    <a target="_blank" href="<?= $empresa[0]->twitter ?>"><img src="<?= base_url() ?>_imagens/icon-twitter.png" alt="Twitter" title="Twitter" /></a>
                <? } ?>
                <? if ($empresa[0]->facebook != "") { ?>            
                    <a target="_blank" href="<?= $empresa[0]->facebook ?>"><img src="<?= base_url() ?>_imagens/icon-facebook.png" alt="Facebook" title="Facebook" /></a>
                <? } ?>
                <? if ($empresa[0]->youtube != "") { ?>            
                    <a target="_blank" href="<?= $empresa[0]->youtube ?>"><img src="<?= base_url() ?>_imagens/icon-youtube.png" alt="Canal no Youtube" title="Canal no Youtube" /></a>
                <? } ?>
                <? if ($empresa[0]->googleplus != "") { ?>            
                    <a target="_blank" href="<?= $empresa[0]->googleplus ?>"><img src="<?= base_url() ?>_imagens/icon-plus.png" alt="Google Plus" title="Google Plus" /></a>
                <? } ?>
                <br><br>
                <p><? echo $empresa[0]->sobre ?></p>

                <br>
                <div class="form" style="background: #f4f4f4;margin: 0;padding: 20px;width: 100%;">
                    <p><strong>Receba e-mails de novas vagas da <?= $empresa[0]->razaosocial ?></strong></p>
                    <input type="text" class="" name="emailNews" id="emailNews"/>
                    <input type="hidden" name="hemp" id="hemp" value="<?= $empresa[0]->id ?>"/>
                    <input type="button" onclick="cadNews();" value="Seguir" class="btn"/></div>
                <p id="respNews"></p>
            </div>

        </div>
    </div>
</section>