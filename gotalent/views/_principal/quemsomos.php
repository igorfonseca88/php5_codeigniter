
	
	<!--Banner-->
	<section>
		<div id="bannerquemsomos" class="quemsomos">
			<div class="container clearfix">
				<div class="grid_12">
					<h1>DOS GEEKS DA TI PARA OS GEEKS DA TI.</h1>
				</div>
			</div>
		</div>
	</section>
	
	<!--Quem Somos-->
	<section>
		<div id="quemsomos">
			<div class="container clearfix">
				<div class="grid_12">
					<div class="texto">
						<h2>SOBRE A GO TALENT</h2>	 
						<p>Criada em 2014, somos uma empresa digital, comunidade virtual e ferramenta cuja proposta é ser uma facilitadora no processo de captação de novos profissionais de TI para as empresas.</p>
						<h3>NÓS FALAMOS A LÍNGUA DO PROFISSIONAL</h3>	 
						<p>Dos Geeks da TI para os Geeks da TI. Criada por profissionais que cansaram de ver boas empresas e bons profissionais não se entenderem.</p> 
						<p>Somos e lidamos com profissionais bastante distintos dos demais: informais, geeks e que buscam cada vez mais ter uma visão estratégica do negócio. </p>
						<p>O ambiente da Go Talent proporciona maior entrosamento entre o recrutador e o profissional.</p>
					</div>
				</div>
				<div class="grid_6">
				<?foreach($equipe as $row){?>
					<div class="colaborador">
						<div class="grid_2 alpha"><img src="<?= base_url()?>upload/equipe/<?= $row->imagem?>" alt="" /></div>
						<div class="grid_4 omega">
							<h2><?= $row->cargo?></h2>
							<h3><?= $row->nome?></h3>
							<p><?= $row->texto?></p>
						</div>
					</div>
				<?}?>	
				</div>
				<div class="grid_5 prefix_1">
					<div class="informacoes">
						<h2>MISSÃO</h2>
						<?= $missao[0]->texto?>
						<h2>VISÃO</h2>
						<?= $visao[0]->texto?>
						<h2>VALORES</h2>
						<?= $valores[0]->texto?>
					</div>
				</div>
			</div>
		</div>
	</section>
			