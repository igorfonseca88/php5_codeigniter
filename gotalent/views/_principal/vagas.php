<!--Banner-->
<section>
    <div id="banner" class="vagas">
        <div class="container clearfix">
            <div class="grid_12">
                <h1><b>CONFIRA AS VAGAS</b> DISPONÍVEIS PARA O SEU TALENTO</h1>
            </div>
        </div>
    </div>
</section>

<!--Busca-->
<section>
    <div id="busca">
        <div class="container clearfix">
            <div class="grid_12">
                <h2>Encontre aqui a sua oportunidade de emprego!</h2>
                <form id="formPesquisa" name="formPesquisa" action="<?= base_url() ?>buscar" method="post" >
                    <input type="text" id="searchVaga" name="searchVaga" placeholder="Pesquise por vaga ..." value="<?= $_SESSION['searchVaga'] ?>" class="placeholder input" />

                    <input type="text" id="searchVaga" name="searchCidade" placeholder="Pesquise por cidade ..." value="<?= $_SESSION['searchCidade'] ?>" class="placeholder input" />

                    <select name="searchNivel"class="input">
                        <option value="Todos">Todos os níveis</option>
                        <option <?= $_SESSION["searchNivel"] == "Estagiario" ? "selected" : "" ?> value="Estagiario">Estagiário</option>
                        <option <?= $_SESSION["searchNivel"] == "Trainee" ? "selected" : "" ?> value="Trainee">Trainee</option>
                        <option <?= $_SESSION["searchNivel"] == "Junior" ? "selected" : "" ?> value="Junior">Júnior</option>
                        <option <?= $_SESSION["searchNivel"] == "Pleno" ? "selected" : "" ?> value="Pleno">Pleno</option>
                        <option <?= $_SESSION["searchNivel"] == "Senior" ? "selected" : "" ?> value="Senior">Sênior</option>
                        <option <?= $_SESSION["searchNivel"] == "Supervisor" ? "selected" : "" ?> value="Supervisor">Supervisor</option>
                        <option <?= $_SESSION["searchNivel"] == "Coordenador" ? "selected" : "" ?> value="Coordenador">Coordenador</option>
                        <option <?= $_SESSION["searchNivel"] == "Gerente" ? "selected" : "" ?> value="Gerente">Gerente</option>
                        <option <?= $_SESSION["searchNivel"] == "Diretor" ? "selected" : "" ?> value="Diretor">Diretor</option>
                    </select> 


                    <input type="submit" value="GO!" class="btn" />
                </form>
            </div>
        </div>
    </div>
</section>

<script>
ga('send', 'pageview', '/busca?cidade=<?= $_SESSION['searchCidade'] ?>&estado=&vaga=<?= $_SESSION['searchVaga'] ?>&nivel=<?= $_SESSION['searchNivel'] ?>');
</script>
<!--Vagas-->
<section>
    <div id="vagas">
        <div class="container clearfix">
            <div class="grid_12">
                <h2>
                    BUSCA POR
                    <img src="<? echo base_url() ?>_imagens/site/icon19.png" onclick="setLinhas()" alt="Linhas" class="exibicao" />
                    <img src="<? echo base_url() ?>_imagens/site/icon20.png" onclick="setColunas()" alt="Colunas" class="exibicao" />
                    <a class="exibicao" href="#abrirModalMapa"><img src="<? echo base_url() ?>_imagens/site/icon39.png"/></a>
                </h2>
            </div>

            <div id="abrirModalMapa" class="modalDialogMapa">
                <div>
                    <a class="closeModalDialogMapa" title="Fechar" href="#closeModalDialogMapa">X</a>     
                    <iframe frameBorder=0 width=100% height=100% marginHeight=0 marginWidth=0 scrolling=no src="<? echo base_url() ?>mapaController/mapaWeb"></iframe>
                </div>
            </div>

            <div class="grid_3">
              

                <h3 style="margin: 0px 0 10px">Estado</h3>

                <ul>
                    <li><label><a style="text-decoration: none;color: #58595b;font-size: 14px;" href="<?= base_url() ?>vagas">Todos</a></label></li>	
                    <? foreach ($estados as $est) { ?>
                        <li><label><a style="text-decoration: none;color: #58595b;font-size: 14px;" href="<?= base_url() ?>vagas/<?= $est->estado ?>" <?= $estado == $est->estado ? "class='select'" : "" ?>> <?= $est->estado ?> <?= "(" . $est->contador . ")" ?></a></label></li>
                    <? } ?>
                </ul>
                <hr />

                <!--h3>Áreas</h3>
                <form id="formChecks" name="formChecks" action="<?= base_url() ?>buscar" method="post" >
                <? foreach ($areas as $area) { 
                   $checked=""; 
                   
                   if(count($_SESSION["searchAreaInteresseArray"])>0){
                       foreach($_SESSION["searchAreaInteresseArray"] as $ck){
                           
                         if($ck == $area->id){
                            $checked = "checked";
                            break;
                         }   
                       }?>
                       <label> <input type="checkbox" <?=$checked?> name="ckarea[]" id="ckarea" value="<?=$area->id?>" /> <?=$area->area?> </label>    
                      <?
                   }else{
                       $checked= $_COOKIE['areaInteresse']; ?>
                       <label> <input type="checkbox" <?=$checked == $area->id ? "checked" : ""?> name="ckarea[]" id="ckarea" value="<?=$area->id?>" /> <?=$area->area?> </label>
                   <?}?>
                    
                <?}?>
                               
                <input type="submit" class="btn" value="Pesquisar"/>
                </form-->
                
            </div>

            <div class="grid_9">
                <input type='hidden' id='current_page' />
                <input type='hidden' id='show_per_page' />
                <div  class="vagas colunas">
                    <div id='content'>         
                        <? foreach ($vagas as $vaga) { ?>	
                            <div class="vaga">
                             <a href="<?= base_url() ?>vaga/<?= $vaga->id ?>-<?= url(str_replace(" ", "-", $vaga->vaga)) ?>">   
                              <?if($vaga->imgSub == ""){?>
                                <img src="<? echo base_url() ?>_imagens/vagas/marketing.png" alt="Área" />
                              <?}else{?>
                                <img src="<? echo base_url() ?>_imagens/<?= $vaga->imgSub ?>" alt="Área" />
                              <?}?>  
                                <div class="texto">
                                    <h3><?= $vaga->vaga ?> (<?= str_replace('Estagiario', 'Estagiário', str_replace('Senior', 'Sênior', str_replace('Junior', 'Júnior', $vaga->tipoProfissional))) ?>)</h3>
                                    <span class="salario"><?= $vaga->cidade . "-" . $vaga->estado ?></span>
                                    <span class="local"><?= $vaga->salario ?></span>
                                    <span class="regime"><?= $vaga->tipo ?></span>
                                </div>
                                <span class="visualizacao"><b><?= $vaga->visualizacao ?> pessoa(s)</b>visualizaram a vaga</span>
                                <span class="btn">VER VAGA</span>

                                <?/* if ($this->session->userdata("tipo") == 'Profissional') { ?>
                                    <!--span class="compatibilidade">Compatibilidade: <?= number_format($vaga->porcentagem, 0, ',', ' ') ?>%</span-->
                                <?}*/?>
                             </a>      
                            </div>
                        <? } ?>   
                        <?if(count($vagas)>0){?>
                            <div id='page_navigation' class="paginacao"></div>        
                        <?}else{?>
                            <p>Nenhuma vaga localizada para sua pesquisa.</p>
                        <?}?>
                    </div>

                </div>

            </div>

        </div>

    </div>

</section>
<!--Cadastro-->
<section>
    <div id="cadastro" class="interno">
        <div class="container clearfix">
            <div class="grid_5 prefix_2">
                <h2><b>CADASTRE-SE GRÁTIS</b> E ENCONTRE SUA VAGA NO MERCADO DE TRABALHO DE TI</h2>
            </div>
            <div class="grid_3">
                <form>
                     <a href="<?echo base_url()?>cadastro-profissional" class="btn">CADASTRAR</a>
                </form>
            </div>
        </div>
    </div>
</section>

<!--<div id="maskAreaInteresse" style="display:none"></div>
	<div id="modalAreaInteresse" style="display:none">
            
		<form action="<?=  base_url()?>vagas"  method="post" onsubmit="setArea();" >
		    <select name="idAreaInteresse" id="idAreaInteresse">
                                   <? foreach ($areas as $area) { ?>
                                    <option value="<?echo $area->id?>"><?echo $area->area?></option>
                                   <?}?>
                    </select>
			
                        <input id="btnAreaInteresse" type="submit" value="ok" />
                        
		</form>		
	</div>-->



