<!--Banner-->
	<section>
		<div id="banner">
			<div id="slider" class="flexslider loading">
				<ul class="slides">
					<li style="background-image:url(_imagens/banner/4.jpg)">
						<div class="container clearfix">
							<div class="grid_6">
								<h1><b>FACILIDADE</b> EM ENCONTRAR AS MELHORES <b>VAGAS</b> AGORA NO SEU SMARTPHONE.</h1>
								<a target="_blank" href="https://play.google.com/store/apps/details?id=com.gotalent" class="btn">QUERO BAIXAR</a>
							</div>
						</div>
					</li>
					<li style="background-image:url(_imagens/banner/1.jpg)">
						<div class="container clearfix">
							<div class="grid_6">
								<h1>GRANDES <b>TALENTOS</b> ESPERANDO OPORTUNIDADES DE SUA <b>EMPRESA</b></h1>
								<a href="<? echo base_url()?>empresas" class="btn">VEJA MAIS</a>
							</div>
						</div>
					</li>
					<li style="background-image:url(_imagens/banner/2.jpg)">
						<div class="container clearfix">
							<div class="grid_6">
								<h1 class="orange">VAGAS DE TRABALHO SELECIONADAS PARA O <b>SEU PERFIL PROFISSIONAL</b></h1>
								<a href="<? echo base_url()?>vagas" class="btn">VER OPORTUNIDADES</a>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>
	
	<!--Cadastro-->
	<section>
		<div id="cadastro">
			<div class="container clearfix">
				<div class="grid_5 prefix_2">
					<h2><b>CADATRE-SE GRÁTIS</b> E ENCONTRE SUA VAGA NO MERCADO DE TRABALHO</h2>
				</div>
				<div class="grid_3">
                                    <form>
                                           <a href="<?echo base_url()?>cadastro-profissional" class="btn">CADASTRAR</a>
					</form>
				</div>
			</div>
		</div>
	</section>
	
	<!--Vídeo-->
	<section>
		<div id="video">
			<div class="container clearfix">
				<div class="grid_12">
					<h2>VOCÊ PROCURA A MELHOR <strong>VAGA NA SUA ÁREA</strong> <br> AS <strong>EMPRESAS</strong> PROCURAM O SEU <strong>TALENTO</strong>.</h2>
					<p>A Go Talent conecta você a grandes oportunidades.</p>
					<img src="_imagens/site/icon4.png" alt="" />					
					                                        
                                         <iframe width="100%" height="380" src="//www.youtube.com/embed/Noqe0q90XDA?wmode=opaque&amp;showinfo=0&amp;autoplay=0&amp;controls=0&amp;modestbranding=1&amp;rel=0" 
    frameborder="0" 
    allowfullscreen="">
    </iframe>  
				</div>
			</div>
		</div>
	</section>


	<!--Vagas por estado-->
	<section>
		<div id="estado">
			<div class="container clearfix">
				<div class="grid_3">
					<img src="_imagens/site/icon5.png" alt="" />
				</div>
				<div class="grid_6">
					<h2>VAGAS POR ESTADO</h2>
					<div id="sliderVagas" class="flexslider loading">
						<ul class="slides">
						<?foreach ($vagas_estados as $estados){?>
							<li> <a href="<?= base_url() ?>vagas/<?= $estados->estado ?>"> <b><?echo $estados->estado?></b> <?echo $estados->contador?> </a> </li>
						<?}?>	
							
						</ul>
					</div>
				</div>
				<div class="grid_3">
					<img src="_imagens/site/icon15.png" class="flRight" alt="" />
				</div>
			</div>
		</div>
	</section>

<!--Empresas-->
	<section>
		<div id="empresas">
			<div class="container clearfix">
				<div class="grid_12">
					<h2><b>EMPRESAS</b> QUE OFERECEM GRANDES <br> <b>OPORTUNIDADES</b> DE TRABALHO</h2>
					<div class="sliderEmpresas">
						<div id="sliderEmpresas" class="flexslider loading">
							<ul class="slides">
							<? foreach ($empresas as $empresa) { ?>
                        <li><a href="<?= $empresa->apelido == "" ? "#" : base_url()."empresa/".$empresa->apelido?>"><img src="<? echo base_url() ?>upload/empresas/<?= $empresa->logo ?>" title="<?= $empresa->razaosocial ?>"  /></a></li>
                        <? } ?>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<!--Consultores-->
	<section>
		<div id="consultores">
			<div class="container clearfix">
				<div class="grid_10 prefix_1">
					<h2>ENCONTRE PROFISSIONAIS QUALIFICADOS PARA SUAS VAGAS</h2>
				</div>
				<div class="grid_4 prefix_1">
					<p>Deixe seu contato que nossos consultores  vão auxiliar sua empresa a encontrar os melhores  profissionais do mercado de TI.</p>
				</div>
				<div class="grid_6">
					<form action="">
                                            <input type="text" placeholder="nome" name="nomeFormConsultor" id="nomeFormConsultor" value="nome" class="placeholder input" />
                                            <input type="text" placeholder="email" name="emailFormConsultor" id="emailFormConsultor" value="email" class="placeholder input" />
                                            <input type="text" placeholder="telefone" name="telefoneFormConsultor" id="telefoneFormConsultor" value="telefone" class="placeholder input" />
                                            <input type="button" onclick="faleComConsultor();" value="FALAR COM UM CONSULTOR" class="btn" />
                                            <div id="respFormConsultor"></div>
                                        </form>
				</div>
			</div>
		</div>
	</section>
	
	<!--Serviços-->
	<section>
		<div id="servicos">
			<div class="container clearfix">
				<div class="grid_5 prefix_1">
					<img src="_imagens/site/icon15.png" alt="Vagas" />
					<div class="box">
						<h2>PROFISSIONAL</h2>
						<p>Mais de 300 <br> oportunidades de trabalho</p>
						<a href="<? echo base_url()?>vagas" class="btn">VER OPORTUNIDADES</a>
					</div>
				</div>
				<div class="grid_5 prefix_1">
					<img src="_imagens/site/icon16.png" alt="Cadastros" />
					<div class="box">
						<h2>EMPRESA</h2>
						<p>Mais de 5000 <br> talentos cadastrados</p>
						<a href="<? echo base_url()?>empresas" class="btn">RECRUTAR</a>
					</div>
				</div>
			</div>
		</div>
	</section>
