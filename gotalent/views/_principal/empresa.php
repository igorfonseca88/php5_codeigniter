<!--Banner-->
<section>
    <div id="banner" class="pageempresa">
        <div class="container clearfix">
            
            <div class="grid_9">
                <h1><b><? echo $empresa[0]->razaosocial ?></b></h1>
                <p><? echo $empresa[0]->slogan ?></p>
                <p><a style="text-decoration: none;" target="_blank" href="<? echo $empresa[0]->site ?>"><? echo $empresa[0]->site ?></a></p>
            </div>
            
            <div class="grid_3">
                <img src="<?= base_url() ?>upload/empresas/<? echo $empresa[0]->logo ?>" alt="<? echo $empresa[0]->razaoSocial ?>" />
            </div>
        </div>
    </div>
</section>

<!--Empresa-->
<section>
    <div id="empresa">
        <div class="container clearfix">
            <div class="grid_8">						
                <h3>Sobre a Empresa</h3>
                <p><? echo $empresa[0]->sobre ?></p>

                <?if($empresa[0]->pontosPositivos != ""){?>
                <h3>Pontos Positivos</h3>
                <p><? echo $empresa[0]->pontosPositivos ?></p>
                <?}?>
                <!--Galeria-->	
                <div id="principal" class="flexslider">
                    <ul class="slides">
                        <? foreach ($galeria as $g) { ?>    
                            <li> <img src="<?= base_url() ?>upload/galeria/<?= $g->imagem ?>" alt="" title="" />	 </li>
                        <? } ?> 

                    </ul>
                </div>

                <!--Thumbs-->
                <div id="carousel" class="flexslider">
                    <ul class="slides">
                        <? foreach ($galeria as $g) { ?>    
                            <li> <img src="<?= base_url() ?>upload/galeria/<?= $g->imagem ?>" alt="" title="" />	 </li>
                        <? } ?>    
                    </ul>
                </div>

                <h3>Site</h3>
                <p><? echo $empresa[0]->site ?></p>
                
                <? if($empresa[0]->numeroColaboradores != ""){ ?>
                <h3>Total de colaboradores</h3>
                <p><? echo $empresa[0]->numeroColaboradores ?></p>
                <?}?>
                
                <? if($empresa[0]->fundacao != ""){ ?>
                <h3>Fundação</h3>
                <p><? echo $empresa[0]->fundacao ?></p>
                <?}?>

                <? if($empresa[0]->mapa != ""){ ?>
                <h3>Localização</h3>
                <? echo $empresa[0]->mapa ?>
                <?}?>

            </div>

            <div class="grid_4">
            <?if($empresa[0]->twitter != ""){?>        
                <a target="_blank" href="<?= $empresa[0]->twitter ?>"><img src="<?= base_url() ?>_imagens/icon-twitter.png" alt="Twitter" title="Twitter" /></a>
            <?}?>
            <?if($empresa[0]->facebook != ""){?>            
                <a target="_blank" href="<?= $empresa[0]->facebook ?>"><img src="<?= base_url() ?>_imagens/icon-facebook.png" alt="Facebook" title="Facebook" /></a>
            <?}?>
            <?if($empresa[0]->youtube != ""){?>            
                <a target="_blank" href="<?= $empresa[0]->youtube ?>"><img src="<?= base_url() ?>_imagens/icon-youtube.png" alt="Canal no Youtube" title="Canal no Youtube" /></a>
            <?}?>
            <?if($empresa[0]->googleplus != ""){?>            
                <a target="_blank" href="<?= $empresa[0]->googleplus ?>"><img src="<?= base_url() ?>_imagens/icon-plus.png" alt="Google Plus" title="Google Plus" /></a>
            <?}?>
                <br><br>
                <h4>Vagas disponíveis </h4>
                <? foreach ($vagas_empresa as $vaga_empresa) { ?>
                    <p> <a href="<?= base_url() ?>vaga/<?= $vaga_empresa->id ?>-<?= url(str_replace(" ", "-", $vaga_empresa->vaga)) ?>"> <?= $vaga_empresa->vaga ?> (<?= $vaga_empresa->cidade ?>/<?= $vaga_empresa->estado ?>) </a> </p>
                <? } ?>
            </div>

        </div>
    </div>
</section>