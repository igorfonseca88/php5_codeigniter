<?php

//Transforma títulos em URL amigáveis
function url($str) {
    $str = strtolower(utf8_decode($str));
    $i = 1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
    while ($i > 0)
        $str = str_replace('--', '-', $str, $i);
    if (substr($str, -1) == '-')
        $str = substr($str, 0, -1);
    return $str;
}

//Busca URL para setar os title e description
$url = explode('?', $_SERVER['REQUEST_URI']);
$urlAtual = $url[0];
$urlParam = explode('/', $urlAtual);
$urlPagina = $urlParam[1];

//Escolhe as páginas
switch ($urlPagina) {
    
    
    case "default" :
        $title = $empresa[0]->razaosocial." jobs | Go Talent";
        $description = "Na Go Talent você tem acesso a vagas de TI e ainda pode agendar sua entrevista de emprego direto.";
        $keywords = "vagas de emprego";
        
        break;
    default :
        $title = $empresa[0]->razaosocial." jobs | Go Talent";
        $description = "Na Go Talent você tem acesso a vagas de TI e ainda pode agendar sua entrevista de emprego direto.";
        $keywords = "vagas de emprego";
}


if ($imagem != "") {
    $content_imagem = $imagem;
} else {
    $content_imagem = base_url() . "_imagens/logo-redessociais.png";
}
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!--Site desenvolvido por Fernanda Pieretti - www.freelancehtmlcss.com-->
        <title><?= $title ?></title>
        <meta name="description" content="<?= $description ?>" />
        <meta name="keywords" content="<?= $keywords ?>" />
        <meta property="og:title" content="<?= $title ?>" />
        <meta property="og:description" content="<?= $description ?>" />
        <meta property="og:image" content="<?= $content_imagem ?>" />	
        <meta property="og:type" content="website" />

        <meta name="viewport" content="width=device-width" />
        <link rel="shortcut icon" href="<?echo base_url()?>_imagens/favicon.ico" type="image/x-icon" />

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,700' rel='stylesheet' type='text/css' />

        <link rel="stylesheet" href="<?= base_url() ?>_estilos/base.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/grid.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/style.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/fancybox.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/flexslider.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/demo.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/animate.css" />
        <link rel="stylesheet" href="<?= base_url() ?>_estilos/jquery.autocomplete.css" />

        <script type="text/javascript" src="<?= base_url() ?>_js/modernizr-2.6.2.min.js"></script>

        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-53411814-1', 'auto');
            ga('send', 'pageview');
        </script> 

    </head>
    <body>

        <!--Header-->
        <header>
            <div id="header">
                <div class="container clearfix">
                    
                    <div class="grid_12">
                        <nav>
                            <ul>
                              
                                
                                
                                <? if($this->session->userdata("tipo") == ""){?>
                                <li><a href="<? echo base_url()?>login/login">LOGIN</a></li>
                                <?}
                                else if($this->session->userdata("tipo") == "Profissional"){?>
                                <li><a href="<? echo base_url()?>area-restrita">Área Restrita</a></li>
                                <?}
                                else if($this->session->userdata("tipo") == "Empresa"){?>
                                <li><a href="<? echo base_url()?>area-restrita-empresa">Área Restrita</a></li>
                                <?}
                                else if($this->session->userdata("tipo") == "Administrador" || $this->session->userdata("tipo") == "Conteudo"){?>
                                <li><a href="<? echo base_url()?>principal/arearestritaadmin">Área Restrita</a></li>
                                <?}?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>  


