	<!--Banner-->
	<section>
		<div id="banner" class="empresa">
			<div class="container clearfix">
				<div class="grid_12">
					<h1>ENCONTRE OS PROFISSIONAIS MAIS TOPS PARA O SEU TIME</h1>
				</div>
			</div>
		</div>
	</section>
	
	<!--Planos-->
	<section>
		<div id="planos-empresa"> 
			<div class="container clearfix">
				<div class="grid_4 prefix_2">
					<p class="passo">Um passo a passo de como é fácil para cadastrar uma vaga.</p>
					<p class="texto">Vá direto ao ponto, <small>publique uma vaga sem <b>MIMIMI</b></p>
				</div>
				<div class="grid_6">
					 <div class="passos">
						<p>Preencha infos da empresa</p>
						<img src="<?=base_url()?>_imagens/empresa/icon-04.png" alt="" />
						<p>Preencha infos da vaga</p>
						<img src="<?=base_url()?>_imagens/empresa/icon-04.png" alt="" />
						<p>Adicione competências</p>
						<img src="<?=base_url()?>_imagens/empresa/icon-04.png" alt="" />
						<p>Publique</p>						
					 </div>
				</div>
				<div class="grid_6 oferecemos">
					<p><b>O QUE OFERECEMOS:</b></p>
					<p>Dos geeks da TI para os geeks da TI. Criado por profissionais cansados de serem mal interpretados.</p>
				</div>
				<div class="grid_6">
					<img src="<?=base_url()?>_imagens/empresa/icon-06.png" alt="" />
				</div>
				<div class="grid_6 oferecemos">
					<p>Uma ferramenta definitiva: redução de tempo e esforço com tarefas operacionais e gerenciamento do processo seletivo.</p>
				</div>
				<div class="grid_6">
					<img src="<?=base_url()?>_imagens/empresa/icon-07.png" alt="" />
				</div>
				<div class="grid_12 vantagens">
					<p>Vantagens</p>
					<p>DIVULGUE VAGAS NO SITE, NAS PRINCIPAIS MÍDIAS SOCIAIS E PARA OS PROFISSIONAIS DE TI MAIS TOPS QUE VOCÊ JÁ CONHECEU.</p>
				</div>
				<div class="grid_12">
					<a href="/cadastro-empresa">Cadastre-se</a>
				</div>
			</div>
		</div>
	</section>
	
	