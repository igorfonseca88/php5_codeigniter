<!--Banner-->
<section>
    <div id="bannercontato" class="contato">
        <div class="container clearfix">
            <div class="grid_12">
                <h1>ENTRE EM CONTATO CONOSCO</h1>
                <h2><b>(67)<big> 3043-2025</big></b></h2>
                <p>Você também pode puxar um papo por meio <br>de nossas redes sociais:</p>
                <a href="https://twitter.com/PortalGoTalent" target="_blank"><img src="_imagens/icon-twitter.png" alt="Twitter" /></a>
                <a href="https://www.facebook.com/PortalGoTalent" target="_blank"><img src="_imagens/icon-facebook.png" alt="Facebook" /></a>
                <a href="https://plus.google.com/117422981659620546894" target="_blank"><img src="<?= base_url()?>_imagens/icon-plus.png" alt="Google Plus" /></a> 
                <a href="https://www.youtube.com/channel/UClWEquVUou9rzVxHvJD3e0Q" target="_blank"><img src="_imagens/icon-youtube.png" alt="Youtube" /></a>
            </div>
        </div>
    </div>
</section>

<!--Contato-->
<section>
    <div id="contato">
        <div class="container clearfix">
            <div class="grid_8 prefix_2">
                <? if ($this->session->flashdata('sucesso')) { ?>
                    <h2><?= $this->session->flashdata('sucesso') ?></h2>
                <? } else { ?>
                    <h2>Se deseja mandar um e-mail para nós basta escrever para <b>contato@gotalent.com.br</b> ou preencher os campos abaixo:</h2>
                    <form action="<? echo base_url() ?>enviar-contato" method="POST" id="formContato" name="formContato" class="form">
                        <p id="erro"> <?= $this->session->flashdata('error') != "" ? $this->session->flashdata('error') : "" ?> </p>
                        <input type="text" name="nome" id="nome" placeholder="Nome" />
                        <input type="text" name="email" id="email" placeholder="E-mail" />
                        <input type="text" name="telefone" id="telefone" placeholder="Telefone" />
                        <textarea name="mensagem" id="mensagem" placeholder="Mensagem"></textarea>
                        <span id="spanCap">Quanto é <?echo $numeros[0]?> + <?echo $numeros[1]?> ?</span><input type="text" name="respCap" id="respCap" />
                        <input type="hidden" value="<?echo $numeros[0]+$numeros[1]?>" id="hCap" name="hCap"/>
                        <input type="button" value="Enviar" onclick="validaForm()" class="btn" />
                    </form>
                <? } ?>	
            </div>
        </div>
    </div>
</section>
