
	
	<!--Banner-->
	<section>
		<div id="banner-cadastroempresa">
			<div class="container clearfix">
				<div class="grid_12">
					<h1>CADASTRO DE EMPRESA</h1>
					<p>Cadastre-se e anuncie suas vagas à centenas de profissionais de TI!</p>
				</div>
			</div>
		</div>
	</section>
	
	<!--Cadastro-->
	<section>
		<div id="cadastroEmpresa">            
            <div class="container clearfix">
            	<div class="grid_8 prefix_2">
					<div class="passos">
						<div id="passo1" class="<?=$passo == null || $passo == 1 ? "select" : ""?>"><b>Passo 1</b> Dados da empresa</div>
						<div id="passo2" class="<?=$passo == 2 ? "select" : ""?>"><b>Passo 2</b> Planos</div>
						<div id="passo3" class="<?=$passo == 3 ? "select" : ""?>"><b>Passo 3</b> Pagamento</div>
						<div id="passo4" class="<?=$passo == 4 ? "select" : ""?>"><b>Passo 4</b> Confirmação</div>
					</div>
					
                        <? if ($passo == null || $passo == 1) { ?>                        
						<div class="passo1">
                        	<form class="form" method="post" action="<?=base_url()?>empresa" name="formCadEmpresa" id="formCadEmpresa">
								<p id="erro"> <?= $this->session->flashdata('error') != "" ?  $this->session->flashdata('error') : "" ?> </p>
                                <label>CNPJ<input id="cnpjEmpresa" type="text" name="cnpjEmpresa" /></label>
								<label>Empresa<input id="nomeEmpresa" type="text" name="nomeEmpresa" /></label>
                                <label>Nome do contato<input id="nomeContato" type="text" name="nomeContato" /></label>
                                <label>Telefone<input id="telefoneEmpresa" type="phone" name="telefoneEmpresa" /></label>
                                <label>E-mail<input id="emailEmpresa" type="mail" name="emailEmpresa" /></label>
                                <label>Senha<input id="senhaEmpresa" type="password" name="senhaEmpresa" /></label>
                                <label><button type="button" class="btn" onclick="validaFormEmpresa()">Próximo</button></label>
								<span>Ao se cadastrar, você automaticamente concorda<br>com os <a href="<?= base_url()?>texto/4/termo">Termos de Uso </a> da Go Talent.</span>
                            </form>
						</div>
                        <? } elseif ($passo == 2) { ?>
						<div class="passo2">
                        	<form class="form" method="post" action="<?=base_url()?>principal/planoEmpresa" name="formPlanoEmpresa" id="formPlanoEmpresa">
								<p id="erro"> <?= $this->session->flashdata('error') != "" ?  $this->session->flashdata('error') : "" ?> </p>
                                <label><input type="radio" name="plano" value="1" /><img src="<?=base_url()?>_imagens/planos-4.png" alt="" /><b>Plano Bronze</b>Anuncie 1 vaga por R$ 39,90</label>
                                <label><input type="radio" name="plano" value="2" /><img src="<?=base_url()?>_imagens/planos-5.png" alt="" /><b>Plano Prata</b>Anuncie 5 vagas por R$ 178,90</label>
                                <label><input type="radio" name="plano" value="3" /><img src="<?=base_url()?>_imagens/planos-6.png" alt="" /><b>Plano Ouro</b>Anuncie 10 vagas por R$ 318,90</label>
                                <button type="button" class="btn" onclick="validaFormPlanoEmpresa()">Efetuar Pagamento</button>
                            </form>
						</div>
                        <? } elseif ($passo == 4) { ?>
						<div class="passo4">
							<?=$msg?>
                                                    <script>ga('send', 'pageview', '/cadastroempresa');</script>
						</div>
                        <? }  ?>
					</form>
				</div>                 
			</div>
		</div>
	</section>