    <?php
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");
    ?>

<!--Banner-->
<section>
    <div id="banner">
        <div id="slider" class="flexslider loading">
            <ul class="slides">
                <li style="background-image:url(<? echo base_url() ?>_imagens/banner/3.jpg);background-position:right center">
                    <div class="container clearfix">
                        <div class="grid_10">
                            <h1 class="blue">NÃO PERCA TEMPO PARA ENCONTRAR SUA VAGA <b>SEJA UM PROFISSIONAL PREMIUM</b></h1>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>

<!--Cadastro-->
<section>
    <div id="cadastro" class="yellow">
        <div class="container clearfix">
            <div class="grid_5 prefix_2">
                <h2><b>CADATRE-SE GRÁTIS</b> E ENCONTRE SUA VAGA NO MERCADO DE TRABALHO DE TI</h2>
            </div>
            <div class="grid_3">
                <form>
                    <?
                        if (($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) && ($acessoAPP == true)){
                    ?>
                        <a href="<? echo base_url() ?>cadastro-profissional?acessoAPP=true" class="btn">CADASTRAR</a>
                    <?
                        }else{
                    ?>
                        <a href="<? echo base_url() ?>cadastro-profissional" class="btn">CADASTRAR</a>
                    <?
                        }
                    ?>
                </form>
            </div>
        </div>
    </div>
</section>

<!--Itens Premium-->
<section>
    <div id="itensPremium">
        <div class="container clearfix">
            <div class="grid_12">
                <h2>NA GO TALENT <b>PROFISSIONAL PREMIUM</b> <br> TEM MAIS VANTAGENS</h2>
            </div>
            <div class="grid_4">
                <img src="_imagens/itens/4.png" alt="Candidatura Imediata" />
                <h3>Candidatura imediata às vagas publicadas;</h3>					
            </div>
            <div class="grid_4">
                <img src="_imagens/itens/5.png" alt="Clube GO" />
                <h3>Membro do Clube GO; </h3>					
            </div>
            <div class="grid_4">
                <img src="_imagens/itens/6.png" alt="Mentoria" />
                <h3>Análise de currículo;</h3>
            </div>
            <div class="grid_4">
                <img src="_imagens/itens/7.png" alt="Empresas" />
                <h3>Empresas visualizam seu currículo em destaque no banco de currículos;</h3>
            </div>
            <div class="grid_8">
                <img src="_imagens/itens/8.png" alt="Chances" />
                <h3>Aumente as chances em até 80% na contratação; (O plano FREE precisa aguardar 48 horas para se candidatar a uma vaga publicada).</h3>
            </div>
        </div>
    </div>
</section>

<!--Seja Premium-->
<section>
    <div id="sejaPremium">
        <div class="container clearfix">
            <div class="grid_6">
                <h2>VOCÊ <b> SEMPRE NA FRENTE</b> DOS SEUS CONCORRENTES</h2>					
            </div>
            <div class="grid_3">
                <h3>Por apenas <b>15,90</b> mensais</h3>
            </div>
            <div class="grid_3">
                <? if ($this->session->userdata("tipo") == 'Profissional') {
                    if ($profissional[0]->tipoPlano == 'FREE') {
                        ?>	                                        
                        <!--<a target="_blank" href="<?= base_url() ?>planoController/planoPremium/2" class="btn">QUERO SER PREMIUM</a>-->
                        <a mp-mode="dftl" href="https://www.mercadopago.com/mlb/debits/new?preapproval_plan_id=1e7fc3bd9e35469897bb903a599db12a" name="MP-payButton" class="btn">QUERO SER PREMIUM</a>
                        <script type="text/javascript">
                        (function(){function $MPBR_load(){window.$MPBR_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = ("https:"==document.location.protocol?"https://www.mercadopago.com/org-img/jsapi/mptools/buttons/":"http://mp-tools.mlstatic.com/buttons/")+"render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPBR_loaded = true;})();}window.$MPBR_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPBR_load) : window.addEventListener('load', $MPBR_load, false)) : null;})();
                        </script>
                    <? } ?>
                <? } else { ?>

                    <?
                        if (($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) && ($acessoAPP == true)){
                    ?>
                        <a href="<?= base_url() ?>cadastro-profissional?acessoAPP=true" class="btn">QUERO SER PREMIUM</a> 
                    <?
                        }else{
                    ?>
                        <a href="<?= base_url() ?>cadastro-profissional" class="btn">QUERO SER PREMIUM</a> 
                    <?
                        }
                    ?>

                <? } ?>
            </div>
        </div>
    </div>
</section>

<!--Depoimentos
<section>
        <div id="depoimentos" class="yellow">
                <div class="container clearfix">
                        <div class="grid_2 prefix_2">
                                <div class="img">
                                        <img src="_imagens/site/icon16.png" alt="" />
                                </div>
                        </div>
                        <div class="grid_7">
                                <p>Ut malesuada dui nec arcu finibus, nec placerat mi dictum. Vivamus id convallis tellus. Duis faucibus justo a diam congue, id interdum purus finibus. Curabitur molestie vel urna eu aliquam. Nulla maximus commodo ante, eu luctus ex. Sed blandit eget felis id tempus. Etiam pretium mattis lacus, vitae vulputate urna dapibus ac. </p>
                                <b>Amanda da Silva</b>
                                <p>Superbiz</p>
                        </div>
                </div>
        </div>
</section>-->