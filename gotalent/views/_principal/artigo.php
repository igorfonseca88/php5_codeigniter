<!--Banner-->
<section>
    <div id="banner" class="blog">
        <div class="container clearfix">
            <div class="grid_12">
                <h1>BLOG</h1>
            </div>
        </div>
    </div>
</section>

<section>
    <div id="artigo">
        <div class="container clearfix">
            <div class="grid_12">
                
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Anúncio tipo banner -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-4017138694603338"
                     data-ad-slot="4043188129"
                     data-ad-format="auto"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                
                <h1><?= $post[0]->titulo ?></h1>
                <div class="share">
                    <a href="<? echo base_url() ?>blog">Blog</a> > <a href="<?= base_url() ?>blog/<?= url($categoria[0]->urlCat) ?>"><?= $categoria[0]->titulo ?></a> - Por GoTalent - <?= implode("/", array_reverse(explode("-", $post[0]->data))) ?>

                    <div class="rede face">
                        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.gotalent.com.br%2Fblog%2F<?= $categoria[0]->urlCat ?>/<?= $post[0]->urlPost ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; float:right;" allowTransparency="true"></iframe>
                    </div>

                    <div class="rede">
                        <a href="https://twitter.com/share" style="float:right;" class="twitter-share-button" data-url="<? echo base_url() ?>blog/<?= $categoria[0]->urlCat ?>/<?= $post[0]->urlPost ?>" data-related="PortalGotalent" data-via="PortalGotalent">Tweet</a>
                        <script>!function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                if (!d.getElementById(id)) {
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = p + '://platform.twitter.com/widgets.js';
                                    fjs.parentNode.insertBefore(js, fjs);
                                }
                            }(document, 'script', 'twitter-wjs');</script>
                    </div>

                </div>
            </div>
            <div class="grid_8">
                <div class="artigo">
                    <img src="<?= base_url() ?>upload/blog/<?= $post[0]->imgprincipal ?>"  alt="" />
                    <?= $post[0]->texto ?>
                </div>
                <? if (count($galeria) > 0) { ?>
                    <div class="galeria">
                        <? foreach ($galeria as $img) { ?>
                            <a href="<?= base_url() ?>upload/blog/<?= $img->imagem ?>" class="fancybox" rel="group" title=""><img src="<?= base_url() ?>upload/blog/<?= $img->imagem ?>" /></a>
                        <? } ?>
                    </div>
                <? } ?>
                <div class="comentarios">
                    <h4>Comentarios</h4>
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-comments" data-href="<?= base_url() ?>blog/<?= $categoria[0]->urlCat ?>/<?= $post[0]->urlPost ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                </div>
                <? if (count($relacionados) > 0) { ?>
                    <div class="relacionados">
                        <h4>Posts relacionados</h4>
                        <? foreach ($relacionados as $rel) { ?>
                            <a href="<?= base_url() ?>blog/<?= $rel->urlCat ?>/<? echo $rel->urlPost ?>">
                                <img src="<?= base_url() ?>upload/blog/<?= $rel->imgprincipal ?>" />
                                <?= $rel->titulo ?>
                            </a>
                        <? } ?>
                    </div>
                <? } ?>
            </div>
            <div class="grid_4">
                <div class="categorias">
                    <h4>Categorias</h4>
                    <? foreach ($categorias as $cat) { ?>
                        <p><a href="<?= base_url() ?>blog/<?= url(str_replace(" ", "-", $cat->urlCat)) ?>"><?= $cat->titulo ?></a></p>
                    <? } ?>
                </div>
                <h4>Publicidade</h4>
                <br>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>					
                <ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-4017138694603338" data-ad-slot="7149964123"></ins>
                <script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
            </div>
        </div>
    </div>
</section>
