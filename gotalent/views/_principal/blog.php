

<!--Banner-->
<section>
    <div id="banner" class="blog">
        <div class="container clearfix">
            <div class="grid_12">
                <h1>BLOG</h1>
            </div>
        </div>
    </div>
</section>

<!--Blog-->
<section>
    <div id="blog">
        <input type='hidden' id='current_page' />
        <input type='hidden' id='show_per_page' />
        <div class="container clearfix">
			<div class="grid_12">
                
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Anúncio tipo banner -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-4017138694603338"
                     data-ad-slot="4043188129"
                     data-ad-format="auto"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
			</div>
            <h2 class="grid_12">Posts <?= $categoria[0]->titulo != "" ? " sobre " . $categoria[0]->titulo : "" ?></h2>
			
            <div class="grid_8">
                <div id='content'>  
                    <? if ($posts) { ?>
                        <? foreach ($posts as $post) { ?>
                            <div class="blog">
                                <h3><a href="<?= base_url() ?>blog/<?= $post->urlCat ?>/<? echo $post->urlPost ?>"><?= $post->titulo ?></a></h3>
                                <div class="grid_2 alpha"> <a href="<?= base_url() ?>blog/<?= $post->urlCat ?>/<? echo $post->urlPost ?>"><img src="<?= base_url() ?>upload/blog/<?= $post->imgprincipal ?>"  alt="" /></a> </div>
                                <div class="grid_6 alpha omega"> 
                                    <p>
									<a href="<?= base_url() ?>blog/<?= $post->urlCat ?>/<? echo $post->urlPost ?>"><small><?= implode("/", array_reverse(explode("-", $post->data))) ?> - <?= $post->categoria ?> - Por GoTalent</small></a>
									</p>
                                    <p><a href="<?= base_url() ?>blog/<?= $post->urlCat ?>/<? echo $post->urlPost ?>"><?= $post->descricao ?></a></p>
                                    <p><a href="<?= base_url() ?>blog/<?= $post->urlCat ?>/<? echo $post->urlPost ?>">Leia mais...</a></p>
                                </div>
                            </div>
                        <? } ?>	

                        <div id='page_navigation'></div>
                    <? } else { ?>
                        <p>Nenhum post nesta categoria.</p>
                    <? } ?>
                </div>    
            </div>
            <div class="grid_4">
                <div class="categorias">
                    <h4>Categorias</h4>
                    <? foreach ($categorias as $cat) { ?>
                        <p><a href="<?= base_url() ?>blog/<?= url(str_replace(" ", "-", $cat->urlCat)) ?>"><?= $cat->titulo ?></a></p>
                    <? } ?>

                </div>
                <h4>Publicidade</h4>
                <br>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>					
                <ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-4017138694603338" data-ad-slot="7149964123"></ins>
                <script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
            </div>
        </div>
    </div>
</section>
